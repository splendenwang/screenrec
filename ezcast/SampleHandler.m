//
//  SampleHandler.m
//  ezcast
//
//  Created by Splenden on 2017/10/18.
//  Copyright © 2017年 Splenden. All rights reserved.
//


#import "SampleHandler.h"
//@import CoreVideo.CVImageBuffer;
//@import CoreMedia.CMSampleBuffer;
@import TransmitterKit;
@import WinnerWaveUtility;
@import CoreAudio;
#import "AMImageCompressor.h"
#import "AMh264ImageCompressor.h"
#import "AMJpegImageCompressor.h"
typedef NS_ENUM(NSInteger, AVSessionUtilityOutputFormat){
    AVSessionUtilityOutputFormatJpeg = 1,
    AVSessionUtilityOutputFormatH264 = 2
};
typedef NS_ENUM(NSInteger, AVSessionUtilityOutputQuality){
    AVSessionUtilityOutputQualityLow = 1,
    AVSessionUtilityOutputQualityMid = 2,
    AVSessionUtilityOutputQualityHigh = 3,
    AVSessionUtilityOutputQualityUltraHigh = 4
};
NSString * kAMAVSessionUtilityDomain = @"kAMAVSessionUtilityDomain";
NSString * AMCaptureOptionH264Bitrate = @"AMCaptureOptionH264Bitrate";
NSString * AMCaptureOptionJPEGQuality = @"AMCaptureOptionJPEGQuality";
NSString * AMCaptureOptionOutputFormat = @"AMCaptureOptionOutputType";
NSString * AMCaptureOptionOutputResolution = @"AMCaptureOptionOutputResolution";
NSString * AMCaptureOptionFramerate = @"AMCaptureOptionFramerate";
@interface UIScreen(extension)
-(UIInterfaceOrientation) interfaceOrientation;
@end
@interface SampleHandler ()
{
    AVSessionUtilityOutputFormat _format;
    AVSessionUtilityOutputQuality _quality;
    NSDictionary * _options;
    
    dispatch_queue_t _compressQueue;
    dispatch_queue_t _captureQueue;
    dispatch_queue_t _sendQueue;
    
    NSMutableData * pcmBufferData;
}
//if URL exists, it will write file to the URL.
@property (NS_NONATOMIC_IOSONLY, strong, nullable) NSURL * exportLPCMFilePathURL;
//if URL exists, it will write file to the URL.
@property (NS_NONATOMIC_IOSONLY, strong, nullable) NSURL * exportWavFilePathURL;
//second length export file will record, default is 5sec
@property (NS_NONATOMIC_IOSONLY, assign) NSInteger exportFileRecordSeconds;
@property (nonatomic, strong, nullable) AMMiracodeHelper * miracodeHelper;
@property (nonatomic, strong, nullable) AMImageCompressor * compressor;
@property CGSize dongleResolution;
@end
@implementation SampleHandler
#pragma mark - RPBroadcastSampleHandler
- (void)broadcastStartedWithSetupInfo:(NSDictionary<NSString *,NSObject *> *)setupInfo {
    // User has requested to start the broadcast. Setup info from the UI extension can be supplied but optional.
    
    NSPredicate* compoundPredicate =
    [NSCompoundPredicate orPredicateWithSubpredicates:@[ [AMGenericDeviceScanner ezcastPredicate],
                                                         [AMGenericDeviceScanner ezcastCarPredicate],
                                                         [AMGenericDeviceScanner ezcastLitePredicate],
                                                         [AMGenericDeviceScanner ezcastMusicPredicate],
                                                         [AMGenericDeviceScanner ezcastWirePredicate] ]];
    
    [[AMDeviceController sharedDeviceController] registerScanner:[AMGenericDeviceScanner class]
                                                scannerPredicate:compoundPredicate
                                                TransmitterClass:[AMGenericDeviceDataTransmitter class]
                                                  forDeviceClass:[AMGenericDevice class]];
    pcmBufferData = [NSMutableData new];
    __weak SampleHandler * weakSelf = self;
    self.miracodeHelper = [AMMiracodeHelper new];
    [self.miracodeHelper connectToAddress:@"192.168.203.1"];
    [self.miracodeHelper setConnectionBlock:^(AMPrototypeDevice *device) {
        NSLog(@"CONNECTED!");
        [[AMDeviceController sharedDeviceController] connect:device];
        [AMDeviceController sharedDeviceController].connectionCallback = ^(AMPrototypeDevice * _Nonnull device, NSError * _Nullable error) {
         
            [[AMDeviceController sharedDeviceController] startWifiDisplay];
            [AMDeviceController sharedDeviceController].wifiDisplayStartCallback =^(NSError * error){
                weakSelf.compressor = [weakSelf setupCompressor];
            };
        };
    }];
    [self.miracodeHelper setConnectionTimeoutBlock:^{
        NSLog(@"TIMEOUT!");
    }];
}

- (void)broadcastPaused {
    // User has requested to pause the broadcast. Samples will stop being delivered.
}

- (void)broadcastResumed {
    // User has requested to resume the broadcast. Samples delivery will resume.
}

- (void)broadcastFinished {
    // User has requested to finish the broadcast.
    [[AMDeviceController sharedDeviceController] stopWifiDisplay];
    if(self.compressor) {
        [self.compressor cleanup];
        self.compressor = nil;
    }
}

- (void)processSampleBuffer:(CMSampleBufferRef)sampleBuffer withType:(RPSampleBufferType)sampleBufferType {
    
    AMPrototypeDevice * device = [AMDeviceController sharedDeviceController].connectedDevice;
    if (device != nil) {
        switch (sampleBufferType) {
            case RPSampleBufferTypeVideo:{
                CGImagePropertyOrientation orientation = CMGetAttachment(sampleBuffer, (__bridge CFStringRef)RPVideoSampleOrientationKey, NULL);
                
                
                // Handle video sample buffer
                CVImageBufferRef imagebuffer = (CVImageBufferRef)CMSampleBufferGetImageBuffer(sampleBuffer);
                CMTime duration = CMSampleBufferGetDuration(sampleBuffer);
                CMTime pts = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
                if(imagebuffer==nil) { break; }
                __weak AMImageCompressor * weakCompressor = _compressor;
                CFRetain(imagebuffer);
                if(_compressor == nil){
                    NSLog(@"no compressor");
                }else{
                    dispatch_async(_compressQueue, ^{
                        @synchronized(self){    //thread safe issue on object _compressor
                            [weakCompressor compressRawImage:imagebuffer pts:pts duration:duration completion:^(NSData *compressedData, NSInteger width, NSInteger height) {
                                //                DLog(@"final data length:%zd, w:%zd, h:%zd", compressedData.length, width,height);
                                if([weakCompressor isKindOfClass:[AMh264ImageCompressor class]]){
                                    dispatch_async(_sendQueue, ^{
                                        [[self class] sendH264Data:compressedData width:width height:height];
                                    });
                                }
                                else if([weakCompressor isKindOfClass:[AMJpegImageCompressor class]]){
                                    dispatch_async(_sendQueue, ^{
                                        [[self class] sendJpegData:compressedData];
                                    });
                                }
                            }];
                            CFRelease(imagebuffer);
                        }
                    });
                }
                
                break;
            }
            case RPSampleBufferTypeAudioApp: {
                // Handle audio sample buffer for app audio
                AudioBufferList audioBufferList;
                CMBlockBufferRef blockBuffer;
                size_t bufferListSizeNeededOut;
                bufferListSizeNeededOut = 0;
                OSStatus err = noErr;


                CMSampleBufferGetAudioBufferListWithRetainedBlockBuffer(sampleBuffer, &bufferListSizeNeededOut, &audioBufferList, sizeof(AudioBufferList), NULL, NULL, kCMSampleBufferFlag_AudioBufferList_Assure16ByteAlignment, &blockBuffer);
                CMFormatDescriptionRef description = CMSampleBufferGetFormatDescription(sampleBuffer);
                //PCM Data, Mono
                if(err == noErr){
                    for( int y=0; y<audioBufferList.mNumberBuffers; y++ )
                    {
                        AudioBuffer audioBuffer = audioBufferList.mBuffers[y];
                        NSData * data = [NSData dataWithBytes:audioBuffer.mData length:audioBuffer.mDataByteSize];
                        
                        NSMutableData * stereoData = [NSMutableData new];
                        NSUInteger bytesPerFrame = 2;
                        for (NSUInteger startOffset = 0; startOffset < data.length; startOffset += bytesPerFrame) {
                            NSMutableData * emptyData = [NSMutableData data];
                            [emptyData increaseLengthBy:bytesPerFrame];
                            
                            //It is BigEndian, trans to little Endian, due to bytesPerFrame size is dynamic, cannot use Byte-Order Utilities
//                            [stereoData appendBytes:&data.bytes[startOffset] length:bytesPerFrame];
//                            [stereoData appendBytes:&emptyData.bytes[0] length:bytesPerFrame];
                            NSMutableData * littleEndianData = [NSMutableData new];
                            for (NSUInteger j = 1; j <= bytesPerFrame; j++) {
                                [littleEndianData appendBytes:&data.bytes[startOffset + bytesPerFrame - j] length:1];
                            }
                            [stereoData appendData:littleEndianData];
                            [stereoData appendData:littleEndianData];
                        }
                        
                        
//                        [pcmBufferData appendData:[NSData dataWithBytes:stereoBuf.mData length:stereoBuf.mDataByteSize]];
                        AMDataTransmitter<AMDataTransmitter_SocketStatus> * s = [AMDeviceController sharedDeviceController].transmitter.socketStatus;
                        if(s!=nil && s.readyToSendPCMData){ //support dynamic socket status, skip audio..
                            [[AMDeviceController sharedDeviceController].transmitter sendPCMData:stereoData];
                            pcmBufferData = [NSMutableData new];
                        }
                    }
                    
                    CFRelease(blockBuffer);
                } else {
                    NSLog(@"CMSampleBufferGetAudioBufferListWithRetainedBlockBuffer failed! (%ld)", (long)err);
                }

                break;
            }
            case RPSampleBufferTypeAudioMic:
                // Handle audio sample buffer for mic audio
                break;
                
            default:
                break;
        }
    }
}
#pragma mark - Compressor

- (AMImageCompressor *)setupCompressor {
    _quality = AVSessionUtilityOutputQualityMid;
    _format = AVSessionUtilityOutputFormatH264; //force h264
    
    NSInteger bitrate = 500000;
    CGFloat jpegQuality = 0.5;
    switch (_quality) {
        case AVSessionUtilityOutputQualityLow:
            bitrate = 500000;
            jpegQuality = 0.3;
            break;
        case AVSessionUtilityOutputQualityMid:
            bitrate = 1500000;
            jpegQuality = 0.5;
            break;
        case AVSessionUtilityOutputQualityHigh:
            bitrate = 3200000;
            jpegQuality = 0.7;
            break;
        case AVSessionUtilityOutputQualityUltraHigh:
            bitrate = 6000000;
            jpegQuality = 1.0;
        default:
            break;
    }
    
    
    AMDataTransmitter * dataTransmitter = [AMDeviceController sharedDeviceController].transmitter;
    AMPrototypeDevice * connectedDevice = dataTransmitter.connectedDevice;
    CGSize deviceResolution = CGSizeZero;
    if([connectedDevice conformsToProtocol:@protocol(AMPrototypeImageStreamingProtocol)]){
        deviceResolution = [(id<AMPrototypeImageStreamingProtocol>)connectedDevice getMaxResolution];
        NSInteger screenNumber = dataTransmitter.splitScreenController.screenNumber;
        if(screenNumber==2 || screenNumber==4){
            deviceResolution = CGSizeMake(800, 600);//the above line case crash to a guest under extension
        }
    }
    NSDictionary * options = @{AMCaptureOptionH264Bitrate: @(bitrate),
                               AMCaptureOptionFramerate : @(24),
                               AMCaptureOptionOutputFormat:@(_format),
                               AMCaptureOptionOutputResolution:[NSValue valueWithCGSize:deviceResolution],
                               };
    _options = options;
    
    if(!_compressQueue){
        _compressQueue = dispatch_queue_create("compressQueue", DISPATCH_QUEUE_SERIAL);
    }
    
    if(!_sendQueue){
        _sendQueue = dispatch_queue_create("sendQueue", DISPATCH_QUEUE_SERIAL);
    }
    
    return [[self class] setupImageCompressor:_format options:options];
}
+(AMImageCompressor*) setupImageCompressor:(AVSessionUtilityOutputFormat)format options:(NSDictionary*)options{
    AMImageCompressor * compressor = nil;
    CGSize deviceResolution = [((NSValue*) (options[AMCaptureOptionOutputResolution])) CGSizeValue];
    CGSize realResolution = [UIScreen mainScreen].bounds.size;
    realResolution = [[self class] restrictFromSize:[UIScreen mainScreen].bounds.size toSize:deviceResolution]; //resize(keep aspect ratio) aspect ratio)
    
    if(format == AVSessionUtilityOutputFormatJpeg){
        AMJpegImageCompressor * jpegCompressor = [[AMJpegImageCompressor alloc] init];
        if(CGSizeEqualToSize(realResolution, CGSizeZero)==NO){
            jpegCompressor.resolution = realResolution;
        }
        jpegCompressor.jpegQuality = ((NSNumber*)options[AMCaptureOptionJPEGQuality]).floatValue;
        [jpegCompressor setup];
        compressor = jpegCompressor;
    }
    else if (format == AVSessionUtilityOutputFormatH264){
        AMh264ImageCompressor * h264Compressor = [[AMh264ImageCompressor alloc] init];
        
        if(CGSizeEqualToSize(realResolution, CGSizeZero)==NO){
            h264Compressor.resolution = realResolution;
        }
        h264Compressor.bitrate = ((NSNumber*)[options objectForKey:AMCaptureOptionH264Bitrate]).integerValue;
        h264Compressor.keyframeInterval = 15;
        h264Compressor.fps = (uint32_t)((NSNumber*)[options objectForKey:AMCaptureOptionFramerate]).integerValue;
        
        //#if DEBUG
        //                h264Compressor.capSize = 2*1024*1024;  //2MB
        //                NSString * filePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Downloads/test.h264"];
        //                h264Compressor.filePath = filePath;
        //                h264Compressor.writeFileCompletionBlock = ^(NSString * filePath){
        //                    DLog(@"*********");
        //                };
        //#endif
        
        //setup compress completion block
        [h264Compressor setup];
        compressor = h264Compressor;
    }
    
    
    return compressor;
}

+(CGSize) restrictFromSize:(CGSize)originSize toSize:(CGSize)targetSize{
    if(CGSizeEqualToSize(originSize, CGSizeZero)){
        NSLog(@"originSize is CGSizeZero, ignoring, return targetSize");
        return targetSize;
    }
    
    CGFloat originW = (CGFloat) originSize.width;
    CGFloat originH = (CGFloat) originSize.height;
    CGFloat targetW = targetSize.width;
    CGFloat targetH = targetSize.height;
    if((originW>targetW || originH>targetH)){
        CGFloat returnW = 0;
        CGFloat returnH = 0;
        if(targetW < targetH){
            CGFloat ratio = targetW/originW;
            returnW = targetW;
            returnH = originH * ratio;
        }
        else{   //targetH < targetW
            CGFloat ratio = targetH/originH;
            returnH = targetH;
            returnW = originW * ratio;
        }
        NSLog(@"Resolution down scale from %1.fx%1.f to %1.fx%1.f due to ezcast fw resolution limit ", originW, originH, returnW, returnH);
        return  CGSizeMake(returnW, returnH);
    }
    return originSize;
}

#pragma mark -
+(void) sendH264Data: (NSData*) h264Data width:(NSInteger)width height:(NSInteger)height{
    if([AMDeviceController sharedDeviceController].transmitter.currentState == kTransmitterStateWIFIDisplayOff) return;
    [[AMDeviceController sharedDeviceController].transmitter sendH264Data:h264Data width:(uint32_t)width height:(uint32_t)height];
}
+(void) sendJpegData: (NSData*) jpegData{
    if([AMDeviceController sharedDeviceController].transmitter.currentState == kTransmitterStateWIFIDisplayOff) return;
    [[AMDeviceController sharedDeviceController].transmitter sendImageWithData:jpegData];
}
@end
#pragma mark - UIScreen
@implementation UIScreen(extension)
-(UIInterfaceOrientation) interfaceOrientation_WHICH_IS_NOT_WORKING {
    CGPoint point = [self.coordinateSpace convertPoint:CGPointZero toCoordinateSpace: self.fixedCoordinateSpace];
    if (CGPointEqualToPoint(point, CGPointZero)) {
        return UIInterfaceOrientationPortrait;
    } else if (point.x != 0.0 && point.y != 0.0) {
        return UIInterfaceOrientationPortraitUpsideDown;
    } else if (point.x == 0.0 && point.y != 0.0) {
        return UIInterfaceOrientationLandscapeLeft;
    } else if (point.x != 0.0 && point.y == 0.0) {
        return UIInterfaceOrientationLandscapeRight;
    }
    return UIInterfaceOrientationUnknown;
}
-(UIInterfaceOrientation) interfaceOrientation_WHICH_ALSO_NOT_WORKING {
    if (self.bounds.size.width > self.bounds.size.height) {
        return UIInterfaceOrientationLandscapeLeft;
    } else {
        return UIInterfaceOrientationPortrait;
    }
}
@end
