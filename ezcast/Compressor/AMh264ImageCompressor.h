//
//  AMh264ImageCompressor.h
//  EZWifiMac
//
//  Created by brianliu on 2014/12/1.
//  Copyright (c) 2014年 Actions-Micro. All rights reserved.
//

@import VideoToolbox;
#import "AMImageCompressor.h"
typedef void(^ FileCompleteCallback)(NSString * _Nullable path);  //for debugging
@interface AMh264Data : NSObject
@property (nonatomic, strong, nullable)    NSData * data;
@property (nonatomic, strong, nullable)    NSData * sps;
@property (nonatomic, strong, nullable)    NSData * pps;
@property (nonatomic, assign)              BOOL isKeyFrame;
@end

@interface AMh264ImageCompressor : AMImageCompressor
#pragma mark -




/**
 Default is 0.(No limit). Unit in bit.
 */
@property (nonatomic, assign) NSInteger bitrate;

@property (nonatomic, assign) NSInteger keyframeInterval;

/**
 Default 24. for VTSesseion encode frame duration usage.
 */
@property (nonatomic, assign) int32_t fps;

#pragma mark - Testing (write to disk)


/**
 After job is finished, the block will be called.
 */
@property (nonatomic, strong, nullable) FileCompleteCallback writeFileCompletionBlock;

/**
 A File path to write h.264 data into a raw h.264 stream file. If not provided, no file will be wriiten to disk.
 
 @note If nil, the job will be ignore.
 @note After job finished, this property will be cleared.
 */
@property (nonatomic, strong, nullable) NSString * filePath;


/**
 If filePath is provided, you also have to provide this value to let AMh264ImageCompressor object knowing how much size you would like to write to disk. Unit in byte.
 
 @note If 0, the job will be ignore.
 @note After job finished, this property will be cleared.
 */
@property NSInteger capSize;

-(BOOL) supportHardwareAcceleration;
/**
 Covenient transform method from samplebuffer to NSData
 */
+(AMh264Data * _Nullable)sampleBufferToData:(CMSampleBufferRef _Nonnull)sampleBuffer;
+(NSData * _Nonnull)createH264ContainerData:(NSData * _Nonnull) h264Data sps:(NSData* _Nonnull)spsData pps:(NSData* _Nonnull)ppsData isKeyFrame:(BOOL)isKeyFrame;

@end
