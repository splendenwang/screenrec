//
//  AMImageCompressor.h
//  EZWifiMac
//
//  Created by brianliu on 2014/12/1.
//  Copyright (c) 2014年 Actions-Micro. All rights reserved.
//

@import Foundation;
@import CoreMedia.CMTime;
@import CoreGraphics.CGGeometry;
@import CoreVideo.CVImageBuffer;
@import CoreImage;

/**
 Abstract class.
 Input: CVImageBufferRef
 output: NSData(compressed image data)
 */
@interface AMImageCompressor :NSObject
{
    void(^_completionBlock)(NSData*, NSInteger width, NSInteger height);
}
/**
 Targeting compressing size. Default value 1280x720.
 */
@property CGSize resolution;

-(void) compressRawImage:(CVImageBufferRef)imagebuffer completion:(void(^)(NSData * compressedData, NSInteger width, NSInteger height))completion;

-(void) compressRawImage:(CVImageBufferRef)imagebuffer pts:(CMTime)pts duration:(CMTime)duration completion:(void (^)(NSData *, NSInteger, NSInteger))completion;

-(BOOL) setup;

-(void) cleanup;
@end

