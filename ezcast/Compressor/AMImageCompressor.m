//
//  AMImageCompressor.m
//  EZWifiMac
//
//  Created by brianliu on 2014/12/1.
//  Copyright (c) 2014年 Actions-Micro. All rights reserved.
//

#import "AMImageCompressor.h"

@implementation AMImageCompressor

-(instancetype) init{
    self = [super init];
    if(self){
        self.resolution = CGSizeMake(1280.f, 720.f);
    }
    return self;
}


-(BOOL) setup{
    NSLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
    return NO;
}

-(void) compressRawImage:(CVImageBufferRef)imagebuffer completion:(void (^)(NSData *, NSInteger, NSInteger))completion{
    NSLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
}

-(void) compressRawImage:(CVImageBufferRef)imagebuffer pts:(id)pts duration:(id)duration completion:(void (^)(NSData *, NSInteger, NSInteger))completion {
    NSLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
}

-(void) cleanup{
    NSLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
}
@end
