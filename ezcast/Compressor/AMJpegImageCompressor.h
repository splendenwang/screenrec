//
//  AMJpegImageCompressor.h
//  EZWifiMac
//
//  Created by brianliu on 2014/12/3.
//  Copyright (c) 2014年 Actions-Micro. All rights reserved.
//

#import "AMImageCompressor.h"

@interface AMJpegImageCompressor : AMImageCompressor
@property CGFloat jpegQuality;  //default 0.5
@end
