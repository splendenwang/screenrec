//
//  AMJpegImageCompressor.m
//  EZWifiMac
//
//  Created by brianliu on 2014/12/3.
//  Copyright (c) 2014年 Actions-Micro. All rights reserved.
//

#import "AMJpegImageCompressor.h"
#import <QuartzCore/QuartzCore.h>
@import UIKit.UIImage;
@interface AMJpegImageCompressor ()
{

}
@end

@implementation AMJpegImageCompressor

-(instancetype) init{
    self = [super init];
    if(self){
        self.jpegQuality = 0.5;
    }
    return self;
}

-(BOOL) setup{
    return YES;
}
-(void) compressRawImage:(CVImageBufferRef)imagebuffer pts:(id)pts duration:(id)duration completion:(void (^)(NSData *, NSInteger, NSInteger))completion {
    [self compressRawImage:imagebuffer completion:completion];
}
-(void) compressRawImage:(CVImageBufferRef)imagebuffer completion:(void (^)(NSData *, NSInteger, NSInteger))completion{
    
    @autoreleasepool {
        CIImage *image=[[CIImage alloc] initWithCVImageBuffer:imagebuffer];
        
        
        CIFilter *scaleFilter = [CIFilter filterWithName:@"CILanczosScaleTransform"];
        int originalHeight=[image extent].size.height;
        int originalWidth=[image extent].size.width;
        
        float yScale=(float)self.resolution.height/ (float)originalHeight;
        float xScale=(float)self.resolution.width / (float)originalWidth;
        float scale=fminf(yScale, xScale);
        
        [scaleFilter setValue:[NSNumber numberWithFloat:scale]
                       forKey:@"inputScale"];
        [scaleFilter setValue:[NSNumber numberWithFloat:1.0]
                       forKey:@"inputAspectRatio"];
        [scaleFilter setValue: image
                       forKey:@"inputImage"];
        
        image = [scaleFilter valueForKey:@"outputImage"];
        
        NSData * jpegData = UIImageJPEGRepresentation([UIImage imageWithCIImage:image], _jpegQuality);
        if(completion){
            completion(jpegData, self.resolution.width,self.resolution.height);
        }
    }

}

-(void) cleanup{
    @synchronized(self)
    {
        _completionBlock = nil;
    }
}

-(void) dealloc{
    [self cleanup];
}
@end
