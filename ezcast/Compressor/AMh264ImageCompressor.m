//
//  AMh264ImageCompressor.m
//  EZWifiMac
//
//  Created by brianliu on 2014/12/1.
//  Copyright (c) 2014年 Actions-Micro. All rights reserved.
//

#import "AMh264ImageCompressor.h"
@import CoreMedia;
#import <mach/mach.h>
#import <mach/mach_time.h>
@import WinnerWaveUtility.NSData_toString;
@import UIKit.UIScreen;
@implementation AMh264Data
@end

@interface AMh264ImageCompressor ()
{
    
    VTCompressionSessionRef _compression_session;
    
    long long _frameCount;
    CFAbsoluteTime _firstFrameTime;
    CFAbsoluteTime _lastFrameTime;
    double _frame_time;
    mach_timebase_info_data_t _mach_timebase;
    
    NSMutableData * _dataToWrite;
}

@end

@implementation AMh264ImageCompressor

-(instancetype) init{
    self = [super init];
    if(self){
        self.fps = 24;
    }
    return self;
}

-(BOOL) setup{
    
    @synchronized(self){
        OSStatus status;
        
        mach_timebase_info(&_mach_timebase);
        
        _compression_session = NULL;
        
        status = VTCompressionSessionCreate(NULL, (int32_t)self.resolution.width, (int32_t)self.resolution.height, kCMVideoCodecType_H264, NULL, NULL, NULL, VideoCompressorReceiveFrame,  (__bridge void *)self, &_compression_session);
        
        if (status != noErr || !_compression_session)
        {
            NSLog(@"COMPRESSOR SETUP ERROR");
            return NO;
        }
        
        status = VTSessionSetProperty(_compression_session, kVTCompressionPropertyKey_AllowFrameReordering, kCFBooleanFalse);
        status = VTSessionSetProperty(_compression_session, (__bridge CFStringRef)@"RealTime", kCFBooleanTrue);
        
        if(self.keyframeInterval>0){
            NSInteger real_keyframe_interval = self.keyframeInterval;
            
            status = VTSessionSetProperty(_compression_session, kVTCompressionPropertyKey_MaxKeyFrameInterval, (__bridge CFTypeRef)@(real_keyframe_interval));
        }
        
        
        if(self.bitrate > 0){
            NSInteger real_bitrate = self.bitrate;
            status = VTSessionSetProperty(_compression_session, kVTCompressionPropertyKey_AverageBitRate, CFNumberCreate(NULL, kCFNumberIntType, &real_bitrate));
            
            NSArray *dataRateLimits = @[
                                        @(real_bitrate*10/8),
                                        @(10),
                                        ];
            
            OSStatus success = VTSessionSetProperty(_compression_session, kVTCompressionPropertyKey_DataRateLimits, (__bridge CFTypeRef)dataRateLimits);
            if (success != noErr)
            {
                NSLog(@"FAILED TO SET DATALIMITS");
            }
        }
        
        CFStringRef session_profile = nil;
        session_profile = kVTProfileLevel_H264_Main_AutoLevel;
        status = VTSessionSetProperty(_compression_session, kVTCompressionPropertyKey_ProfileLevel, session_profile);
        
        if(status == noErr) {
            VTCompressionSessionPrepareToEncodeFrames(_compression_session);
        }
    }

    
    return YES;
}

-(double)mach_time_seconds
{
    double retval;
    
    uint64_t mach_now = mach_absolute_time();
    retval = (double)((mach_now * _mach_timebase.numer / _mach_timebase.denom))/NSEC_PER_SEC;
    return retval;
}


-(void) compressRawImage:(CVImageBufferRef)imagebuffer pts:(CMTime)pts duration:(CMTime)duration completion:(void (^)(NSData *, NSInteger, NSInteger))completion {
    _completionBlock = completion;
    
    if(CMTimeCompare(pts, kCMTimeInvalid) == 0) { //duartion can be invalid, but pts cant
        _frame_time = [self mach_time_seconds];
        
        CMTime pts;
        CMTime duration;
        if (_firstFrameTime == 0)
        {
            _firstFrameTime = _frame_time;
        }
        
        CFAbsoluteTime ptsTime = _frame_time - _firstFrameTime;
        
        //    NSLog(@"PTS TIME IS %f", ptsTime);
        
        _frameCount++;
        _lastFrameTime = _frame_time;
        
        pts = CMTimeMake(ptsTime*1000000, 1000000);
        //    NSLog(@"PTS TIME IS %@", CMTimeCopyDescription(kCFAllocatorDefault, pts));
        
        duration = CMTimeMake(1000, self.fps * 1000);
    }
    
    OSStatus stat;
    stat = VTCompressionSessionEncodeFrame(_compression_session, imagebuffer, pts, duration, NULL, NULL, NULL);
    if(stat != noErr) {
        NSLog(@"COMPRESS FAILURE");
        VTCompressionSessionInvalidate(_compression_session);
        CFRelease(_compression_session);
        _compression_session = NULL;
        [self cleanup];
        return;
    }
}

-(void) compressRawImage:(CVImageBufferRef)imagebuffer completion:(void (^)(NSData *, NSInteger, NSInteger))completion{
    
    [self compressRawImage:imagebuffer pts:kCMTimeInvalid duration:kCMTimeInvalid completion:completion];
}

-(void) cleanup{
    @synchronized(self)
    {
        _completionBlock = nil;
    }
}
static char nal[4]= {0x00,0x00,0x00,0x01};
+(NSData *)createH264ContainerData:(NSData *) h264Data sps:(NSData*)spsData pps:(NSData*)ppsData isKeyFrame:(BOOL)isKeyFrame {
    NSMutableData * finalData= [NSMutableData new];
    
    //    NSLog(@"(%d)isKeyFrame:%d", count, isKeyFrame);
    if(isKeyFrame){
        //        NSLog(@"sps:%@, pps:%@", spsData, ppsData);
        if(spsData.length > 0){
            [finalData appendBytes:nal length:4];
            [finalData appendData:spsData];
        }
        if(ppsData.length > 0){
            [finalData appendBytes:nal length:4];
            [finalData appendData:ppsData];
        }
    }
    
    [finalData appendData:h264Data];
    return finalData;
}
-(void) gogo:(NSData *) h264Data sps:(NSData*)spsData pps:(NSData*)ppsData isKeyFrame:(BOOL)isKeyFrame{

//    NSLog(@"sps length:%zd, pps length:%zd", spsData.length, ppsData.length);
    NSMutableData * finalData= [NSMutableData new];
    
//    NSLog(@"(%d)isKeyFrame:%d", count, isKeyFrame);
    if(isKeyFrame){
//        NSLog(@"sps:%@, pps:%@", spsData, ppsData);
        if(spsData.length > 0){
            [finalData appendBytes:nal length:4];
            [finalData appendData:spsData];
        }
        if(ppsData.length > 0){
            [finalData appendBytes:nal length:4];
            [finalData appendData:ppsData];
        }
    }
    
    [finalData appendData:h264Data];
    
    if(_completionBlock){
        _completionBlock(finalData, self.resolution.width, self.resolution.height);
    }
    

    
    if(self.filePath== nil || self.capSize <= 0){
        return;
    }
    if(!_dataToWrite){
        _dataToWrite = [NSMutableData new];
    }
    
    if(isKeyFrame){
        if(spsData.length > 0){
            [_dataToWrite appendBytes:nal length:4];
            [_dataToWrite appendData:spsData];
        }
        if(ppsData.length > 0){
            [_dataToWrite appendBytes:nal length:4];
            [_dataToWrite appendData:ppsData];
        }
    }
    if(_dataToWrite.length < self.capSize){
        [_dataToWrite appendBytes:nal length:4];
        [_dataToWrite appendData:h264Data];
    }
    else{
        [_dataToWrite writeToFile:self.filePath atomically:YES];
        if(self.writeFileCompletionBlock){
            self.writeFileCompletionBlock(self.filePath);
            self.writeFileCompletionBlock = nil;
        }
        _dataToWrite = nil;
        self.filePath = nil;
        self.capSize = 0;
    }
}

+(AMh264Data *)sampleBufferToData:(CMSampleBufferRef)sampleBuffer {
    //SPS, PPS
    CMFormatDescriptionRef format = CMSampleBufferGetFormatDescription(sampleBuffer);
    size_t spsSize, ppsSize;
    size_t parmCount;
    const uint8_t* sps, *pps;
    int nalUnitLength = 0;
    
    CMVideoFormatDescriptionGetH264ParameterSetAtIndex(format, 0, &sps, &spsSize, &parmCount, &nalUnitLength );
    CMVideoFormatDescriptionGetH264ParameterSetAtIndex(format, 1, &pps, &ppsSize, &parmCount, &nalUnitLength );
    
    
    CFArrayRef attachments = CMSampleBufferGetSampleAttachmentsArray(sampleBuffer, false);
    
    bool isKeyframe = false;
    if(attachments != NULL) {
        CFDictionaryRef attachment;
        CFBooleanRef dependsOnOthers;
        attachment = (CFDictionaryRef)CFArrayGetValueAtIndex(attachments, 0);
        dependsOnOthers = (CFBooleanRef)CFDictionaryGetValue(attachment, kCMSampleAttachmentKey_DependsOnOthers);
        isKeyframe = (dependsOnOthers == kCFBooleanFalse);
    }
    
    CMBlockBufferRef my_buffer = CMSampleBufferGetDataBuffer(sampleBuffer);
    
    char *sampledata= NULL;
    size_t offset_length;
    size_t buffer_length;
    
    CMBlockBufferGetDataPointer(my_buffer, 0, &offset_length , &buffer_length, &sampledata);
    NSMutableData * data = [[NSMutableData alloc] initWithBytes:sampledata length:buffer_length];
    
    
    // Replace "header length" to "NALU 0x00000001" in each CMBlockBuffer(which actually contains mpeg4 data), to become a raw H.264 stream data
    // Inspiration by James Chen.
    // More details:
    // - WWDC 2014 session 513: Direct Access to Video Encoding and Decoding
    // - http://msdn.microsoft.com/zh-tw/library/windows/desktop/dd757808(v=vs.85).aspx
    // - http://stackoverflow.com/questions/18244513/strange-h-264-nal-headers
    // - http://asciiwwdc.com/2014/sessions/513
    NSInteger offset = 0;
    while (offset < data.length) {
        NSInteger payloadLength = [[data subdataWithRange:NSMakeRange(offset, 4)] NSIntegerValue];
        [data replaceBytesInRange:NSMakeRange(offset, 4) withBytes:nal length:4];
        offset += (4+payloadLength);
    }
    
    
    AMh264Data * resultData = [AMh264Data new];
    resultData.data = data;
    resultData.sps = [NSData dataWithBytes:sps length:spsSize];
    resultData.pps = [NSData dataWithBytes:pps length:ppsSize];
    resultData.isKeyFrame = isKeyframe;
    
    return resultData;
}
void VideoCompressorReceiveFrame(void *VTref, void *VTFrameRef, OSStatus status, VTEncodeInfoFlags infoFlags, CMSampleBufferRef sampleBuffer){
//    NSLog(@"status:%d, infoFlags:0x%x", status, infoFlags);

    
    AMh264ImageCompressor *selfobj = (__bridge AMh264ImageCompressor *)VTref;
    CFRetain(sampleBuffer);
    
    AMh264Data * h264Data = [AMh264ImageCompressor sampleBufferToData:sampleBuffer];
    [selfobj gogo:h264Data.data sps:h264Data.sps pps:h264Data.pps isKeyFrame:h264Data.isKeyFrame];
    
    CFRelease(sampleBuffer);

}

- (void)dealloc
{
    [self cleanup];
    @synchronized(self)
    {
        VTCompressionSessionInvalidate(_compression_session);
        CFRelease(_compression_session);
    }
}
@end
