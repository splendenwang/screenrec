//
//  AppDelegate.h
//  screenRec
//
//  Created by Splenden on 2017/10/18.
//  Copyright © 2017年 Splenden. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

