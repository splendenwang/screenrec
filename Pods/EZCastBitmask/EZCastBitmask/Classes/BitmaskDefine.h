//
//  BitmaskDefine.h
//  EZDisplay
//
//  Created by brianliu on 2017/9/21.
//  Copyright © 2017年 Actions-Micro Taipei. All rights reserved.
//

#ifndef BitmaskDefine_h
#define BitmaskDefine_h

typedef NSString * RemoteService NS_STRING_ENUM;
extern RemoteService const RemoteServiceABD;
extern RemoteService const kRemoteServiceEnable;
extern RemoteService const kRemoteServiceDisable;
extern RemoteService const kRemoteServicePhoto;
extern RemoteService const kRemoteServiceCamera;
extern RemoteService const kRemoteServiceMusic;
extern RemoteService const kRemoteServiceVideo;
extern RemoteService const kRemoteServiceDLNA;
extern RemoteService const kRemoteServiceEZMirror;
extern RemoteService const kRemoteServiceDocument;
extern RemoteService const kRemoteServiceWeb;
extern RemoteService const kRemoteServiceSetting;
extern RemoteService const kRemoteServiceEZAir;
extern RemoteService const kRemoteServiceCloudVideo;
extern RemoteService const kRemoteServiceMap;
extern RemoteService const kRemoteServiceCloudStorage;
extern RemoteService const kRemoteServiceTV;
extern RemoteService const kRemoteServiceSplitScreen;
extern RemoteService const kRemoteServiceEZCast;
extern RemoteService const kRemoteServiceComment;
extern RemoteService const kRemoteServiceUpdate;
extern RemoteService const kRemoteServiceNews;
extern RemoteService const kRemoteServiceMessages;
extern RemoteService const kRemoteServiceSocial;
extern RemoteService const kRemoteServiceSketch;
extern RemoteService const kRemoteServicePreference;
extern RemoteService const kRemoteServiceSerialControl;
extern RemoteService const kRemoteServiceAirDisk;
extern RemoteService const kRemoteServiceAirControl;
extern RemoteService const kRemoteServiceGame;
extern RemoteService const kRemoteServiceAirSetup;
extern RemoteService const kRemoteServiceLiveRadio;
extern RemoteService const kRemoteServiceAirView;
extern RemoteService const kRemoteServiceAdvanced;
extern RemoteService const kRemoteServiceHelp;
extern RemoteService const kRemoteServiceHotSpotConnection;
extern RemoteService const kRemoteServiceEZChannel;
extern RemoteService const kRemoteServiceMultiroom;
extern RemoteService const kRemoteServiceBookmark;
extern RemoteService const kRemoteServiceFirmwareOTA;
extern RemoteService const kRemoteServiceEZBoard;
extern RemoteService const kRemoteServiceEZNote;
extern RemoteService const kRemoteServiceEZKeep;
extern RemoteService const kRemoteServiceMirrorDisableAudio;
extern RemoteService const kRemoteServiceMirrorDefaultAudioOff;

#endif /* BitmaskDefine_h */
