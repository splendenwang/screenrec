//
//  BitmaskDefine.m
//  EZDisplay
//
//  Created by brianliu on 2017/9/21.
//  Copyright © 2017年 Actions-Micro Taipei. All rights reserved.
//



NSString * const kRemoteServicePhoto = @"photo";
NSString * const kRemoteServiceCamera = @"camera";
NSString * const kRemoteServiceMusic = @"music";
NSString * const kRemoteServiceVideo = @"video";
NSString * const kRemoteServiceDLNA = @"dlna";
NSString * const kRemoteServiceEZMirror = @"ezmirror";
NSString * const kRemoteServiceDocument = @"document";
NSString * const kRemoteServiceWeb = @"web";
NSString * const kRemoteServiceSetting = @"setting";
NSString * const kRemoteServiceEZAir = @"ezair";
NSString * const kRemoteServiceCloudVideo = @"cloud_video";
NSString * const kRemoteServiceMap = @"map";
NSString * const kRemoteServiceCloudStorage = @"cloud_storage";
NSString * const kRemoteServiceTV = @"tv";
NSString * const kRemoteServiceSplitScreen = @"split_screen";
NSString * const kRemoteServiceEZCast = @"ezcast";
NSString * const kRemoteServiceComment = @"comment";
NSString * const kRemoteServiceUpdate = @"update";
NSString * const kRemoteServiceNews = @"news";
NSString * const kRemoteServiceMessages = @"messages";
NSString * const kRemoteServiceSketch = @"sketch";
NSString * const kRemoteServiceSocial = @"social";
NSString * const kRemoteServicePreference = @"preference";
NSString * const kRemoteServiceSerialControl = @"serial_control";
NSString * const kRemoteServiceAirDisk = @"airdisk";
NSString * const kRemoteServiceAirControl = @"aircontrol";
NSString * const kRemoteServiceGame = @"game";
NSString * const kRemoteServiceAirSetup = @"airsetup";
NSString * const kRemoteServiceLiveRadio = @"radio";
NSString * const kRemoteServiceAirView = @"airview";
NSString * const kRemoteServiceAdvanced = @"advanced";
NSString * const kRemoteServiceHelp = @"help";
NSString * const kRemoteServiceHotSpotConnection = @"3g4g";
NSString * const kRemoteServiceEZChannel = @"ezchannel";
NSString * const kRemoteServiceMultiroom  = @"multiroom";
NSString * const kRemoteServiceBookmark = @"bookmark";
NSString * const kRemoteServiceFirmwareOTA = @"fw_ota";
NSString * const kRemoteServiceEZBoard = @"ezboard";
NSString * const kRemoteServiceEZNote = @"eznote";
NSString * const kRemoteServiceEZKeep = @"ezkeep";
NSString * const kRemoteServiceMirrorDisableAudio = @"mirror_disable_audio";
NSString * const kRemoteServiceMirrorDefaultAudioOff = @"mirror_default_audio_off";

