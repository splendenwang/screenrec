# EZCastBitmask

[![CI Status](http://img.shields.io/travis/raxcat/EZCastBitmask.svg?style=flat)](https://travis-ci.org/raxcat/EZCastBitmask)
[![Version](https://img.shields.io/cocoapods/v/EZCastBitmask.svg?style=flat)](http://cocoapods.org/pods/EZCastBitmask)
[![License](https://img.shields.io/cocoapods/l/EZCastBitmask.svg?style=flat)](http://cocoapods.org/pods/EZCastBitmask)
[![Platform](https://img.shields.io/cocoapods/p/EZCastBitmask.svg?style=flat)](http://cocoapods.org/pods/EZCastBitmask)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

EZCastBitmask is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'EZCastBitmask'
```

## Author

raxcat, brianliu@actions-micro.com

## License

EZCastBitmask is available under the MIT license. See the LICENSE file for more info.
