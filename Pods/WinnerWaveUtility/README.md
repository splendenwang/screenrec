# WinnerWaveUtility

[![CI Status](http://img.shields.io/travis/Brian Liu/WinnerWaveUtility.svg?style=flat)](https://travis-ci.org/Brian Liu/WinnerWaveUtility)
[![Version](https://img.shields.io/cocoapods/v/WinnerWaveUtility.svg?style=flat)](http://cocoapods.org/pods/WinnerWaveUtility)
[![License](https://img.shields.io/cocoapods/l/WinnerWaveUtility.svg?style=flat)](http://cocoapods.org/pods/WinnerWaveUtility)
[![Platform](https://img.shields.io/cocoapods/p/WinnerWaveUtility.svg?style=flat)](http://cocoapods.org/pods/WinnerWaveUtility)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Version

- 0.2.4
    - Merged in splendenwang/winnerwaveutility/fix/dongleInfo-cahche-policy (pull request #4)

- 0.2.3
    - Adopt `EZCastBitmask`

- 0.2.2
    - Add EZChannel media log API
    - info will upload to different host, path won’t change (T4604 cloud.iezvu.com 支援 alpha 站) . @splenden


- 0.2.1
    - Fix error API name
    - Fix warning

- 0.2.0
     - `bitcode` enable
     - xcode9 supported

- 0.1.5
-- Class AMColor is removed from WinnerWaveUtility
-- GoogleCast is update to 3.4.0
-- Improve xcode setting for macOS so that linking to GoogleCast warning is removed

- 0.1.4
-- Add endpoint parameter to AMEZChannelAPI class initializer

- 0.1.3
-- Set bitcode to NO (Brian)
-- NSHost-iOS-Alternative (Splenden)

- 0.1.2
-- Add Host name connection at `AMMiracodeHelper`
- 0.1.1


## Requirements

## Installation

WinnerWaveUtility is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "WinnerWaveUtility"
```

## Author

Brian Liu, brianliu@actions-micro.com

## License

WinnerWaveUtility is available under the MIT license. See the LICENSE file for more info.
