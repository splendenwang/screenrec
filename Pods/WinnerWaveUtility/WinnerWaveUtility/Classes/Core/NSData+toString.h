//
//  NSData+toString.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/9/18.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (toString)
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *hexString;

/**
 Convert a data object to NSInteger directly.
 
 @note Big endian.
 */
-(NSInteger) NSIntegerValue;
@end
