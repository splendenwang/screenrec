//
//  NSData+toString.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/9/18.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "NSData+toString.h"

@implementation NSData (toString)
-(NSString*) hexString{
    const unsigned char *dataBytes = [self bytes];
    NSMutableString *ret = [NSMutableString stringWithCapacity:[self length] * 2];
    for (int i=0; i<[self length]; ++i){
        [ret appendFormat:@"%02lX", (unsigned long)dataBytes[i]];
    }
    return ret;
}
-(NSInteger) NSIntegerValue{
    unsigned char bytes[4];
    [self getBytes:bytes length:4];
    NSInteger n = (int)bytes[0] << 24;
    n |= (int)bytes[1] << 16;
    n |= (int)bytes[2] << 8;
    n |= (int)bytes[3];
    return n;
}
@end
