//
//  NSString+NetworkUtility.m
//  AMCommon_iOS
//
//  Created by brianliu on 13/7/11.
//  Copyright (c) 2013年 ActionsMicro. All rights reserved.
//

#import "NSString+NetworkUtility.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/sockio.h>
#include <net/if.h>
#include <errno.h>
#include <net/if_dl.h>
@implementation NSString (NetworkUtility)
+(uint32_t) unsignedIntFromIPAddres:(NSString*) ipAddress{
    NSAssert(ipAddress!=nil, @"The parameter ipAddress should not be nil");
    struct sockaddr_in my_addr;
    int result = inet_aton([ipAddress cStringUsingEncoding:NSASCIIStringEncoding], &my_addr.sin_addr);
    if(result != 0){
        return ntohl(my_addr.sin_addr.s_addr);
    }
    return -1;
}
@end
