//
//  AMIPv4AddressNumberFormatter.h
//  AMCommon_Mac
//
//  Created by brianliu on 13/8/6.
//  Copyright (c) 2013年 ActionsMicro. All rights reserved.
//
//This class is written to valid a string in a NSTextField/UITextField
#import <Foundation/Foundation.h>

@interface AMIPv4AddressNumberFormatter : NSNumberFormatter

@end
