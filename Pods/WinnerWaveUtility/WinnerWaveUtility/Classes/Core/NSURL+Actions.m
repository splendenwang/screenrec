//
//  NSURL+Actions.m
//  EZRemoteControliOS
//
//  Created by brianliu on 12/7/27.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import "NSURL+Actions.h"
#import "NSString+AMStringUtility.h"
@implementation NSURL (Actions)
- (BOOL) isEqualToURL:(NSURL*)otherURL;
{
	return [[self absoluteURL] isEqual:[otherURL absoluteURL]] ||
	([self isFileURL] && [otherURL isFileURL] &&
	([[self path] isEqual:[otherURL path]]));
}

- (BOOL) isMovieURL{
    return [self.path isMovieFile];
}

- (BOOL) isAudioURL{
    return [self.path isAudioFile];
}
@end
