//
//  NSBundle+EZCastApplication.m
//  AMCommon_iOS
//
//  Created by brianliu on 3/24/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import "NSBundle+EZCastApplication.h"

@implementation NSBundle (EZCastApplication)
-(BOOL) isEZCastScreenEnable{
    return ((NSNumber*)[[[NSBundle mainBundle] infoDictionary] objectForKey:@"ezcast_screen_enable"]).boolValue;
}
-(BOOL) isEZCastSSOEnable{
    return ((NSNumber*)[[[NSBundle mainBundle] infoDictionary] objectForKey:@"ezcast_sso_enable"]).boolValue;
}
@end
