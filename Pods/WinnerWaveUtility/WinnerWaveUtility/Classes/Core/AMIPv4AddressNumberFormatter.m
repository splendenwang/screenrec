//
//  AMIPv4AddressNumberFormatter.m
//  AMCommon_Mac
//
//  Created by brianliu on 13/8/6.
//  Copyright (c) 2013年 ActionsMicro. All rights reserved.
//

#import "AMIPv4AddressNumberFormatter.h"

@implementation AMIPv4AddressNumberFormatter
-(BOOL) isPartialStringValid:(NSString *)partialString newEditingString:(NSString **)newString errorDescription:(NSString **)error{
    //    NSLog(@"partialString:%@, newString:%@", partialString, *newString);
    
    
    NSUInteger count = 0, length = [partialString length];
    NSRange range = NSMakeRange(0, length);
    while(range.location != NSNotFound)
    {
        range = [partialString rangeOfString: @"." options:0 range:range];
        if(range.location != NSNotFound)
        {
            range = NSMakeRange(range.location + range.length, length - (range.location + range.length));
            count++;
        }
    }
    //    NSLog(@"count of point '.': %lu", count);
    if(count>3){
        *newString = partialString;
        return NO;
    }
    
    BOOL returnValue = NO;
    NSCharacterSet * validSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
    partialString = [partialString stringByTrimmingCharactersInSet:[validSet invertedSet]];
    NSArray * array = [partialString componentsSeparatedByString:@"."];
    NSMutableArray * newArray = [[NSMutableArray alloc] initWithCapacity:1];
    
    for (NSString* component in array) {
        if([component integerValue] > 255)
        {
            [newArray addObject:@"255"];
        }
        else{
            [newArray addObject:component];
        }
    }
    static NSString * s_previousComponent = nil;
    NSMutableString * resultString = [[NSMutableString alloc] init];
    //    NSLog(@"newArray:%@", newArray);
    for (NSString * component in newArray) {
        //        NSLog(@"component:%@", component);
        
        if(resultString.length ==0){
            [resultString appendString:component];
        }
        else{
            if([s_previousComponent isEqualToString:@""])
                break;
            else
                [resultString appendFormat:@".%@", component];
        }
        
        s_previousComponent = component;
        //        NSLog(@"resultString:%@", resultString);
    }
    
    *newString = resultString;
    //    NSLog(@"%@ ", *newString);
    return returnValue;
    
}
- (BOOL)getObjectValue:(id *)obj forString:(NSString *)string errorDescription:(NSString  **)error{
    *obj = string;
    return YES;
}
- (NSString *)stringForObjectValue:(id)obj{
    return obj;
}
@end
