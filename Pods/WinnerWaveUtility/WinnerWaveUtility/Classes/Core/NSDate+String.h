//
//  NSDate+String.h
//  EZRemoteControliOS
//
//  Created by brianliu on 12/9/18.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (String)
@property (NS_NONATOMIC_IOSONLY, getter=getTimeString, readonly, copy) NSString *timeString;
+(NSString *) getLocalDateTimeString;
+(NSString *) iso8061StringFromDate:(NSDate*) date;
+(NSString*) stringTimeIntervalSinceNow:(NSTimeInterval)delay;
@end
