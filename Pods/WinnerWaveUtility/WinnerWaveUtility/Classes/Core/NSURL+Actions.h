//
//  NSURL+Actions.h
//  EZRemoteControliOS
//
//  Created by brianliu on 12/7/27.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (Actions)
- (BOOL) isEqualToURL:(NSURL*)otherURL;
@property (NS_NONATOMIC_IOSONLY, getter=isMovieURL, readonly) BOOL movieURL;
@property (NS_NONATOMIC_IOSONLY, getter=isAudioURL, readonly) BOOL audioURL;
@end
