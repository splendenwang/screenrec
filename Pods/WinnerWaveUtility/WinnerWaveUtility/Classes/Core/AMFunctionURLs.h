//
//  AMEZCastHelpURLGenerator.h
//  EZDisplay
//
//  Created by brianliu on 13/7/5.
//  Copyright (c) 2013年 Actions-Micro Taipei. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AMPrototypeDevice;

extern NSString * const kAMIsURLDebugMode;

extern NSString * const kAppHomeSite;

@interface AMFunctionURLs : NSObject
//Utility
/**
 Return iezvu site String:http://www.iezvu.com, if test mode,http://test.iezvu.com
 */
+(NSString *)siteString;
/**
 Return iezvu site URL:http://www.iezvu.com, if test mode,http://test.iezvu.com
 */
+(NSURL*) siteURL;

/**
 Return iezvu site String:https://channel.iezvu.com
 */
//+(NSString *)ezchannelString;
/**
 Return ezchannel site URL:https://channel.iezvu.com
 */
//+(NSURL *) ezchannelURL;
/**
 Set UrlDebugMode which affects site string
 */
+(void) setUrlDebugMode:(BOOL)aBool;
/**
 Return [[[NSUserDefaults standardUserDefaults] objectForKey:kAMIsURLDebugMode] boolValue]
 */
+(BOOL) isUrlDebugMode;
/**
 Return "support" directory path String in app sandbox, some legacy code for supportList
 */
+(NSString *) localSupportDirectoryPath;
/**
 Return [NSURL URLWithString:[string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]], to prevent some incorrect URL caused by escape
 */
+(NSURL*) URLNoEscapeWithUTF8String:(NSString*) string;
/**
 Return locale String by NSLocale kCFLocaleCountryCode
 */
+(NSString *)localeString;

//Function URLs
//Base
+(NSString *) helpURLString:(NSString *)function device:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSString *) infoURLString:(NSString *)function device:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSString *) jsonURLString:(NSString *)function device:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSString *) specificPage:(NSString *)pageName function:(NSString *)function device:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo extra:(NSDictionary *)extraQueries;
+(NSString *) webDefaultURLString:(NSString *)function device:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
//Help URL
+(NSURL *) ezcastHelpURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) dlnaHelpURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) ezairHelpURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
//Info URL
+(NSURL *) ezairSupportURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) dlnaSupportURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
//JSON URL
+(NSURL *) dlnaJSONSupportURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) ezairJSONSupportURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
//Default URL
+(NSURL *) webEntryURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) cloudVideoURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) newsURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) mapURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) tvURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) commentURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) updateURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) helpURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) moreAppsURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) shopURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) helpAndShopURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) ezboardURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo extra:(NSDictionary *)extra;
//specific URL
+(NSURL *) socialityURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
//JsHttpServer related URL
//+(NSURL *) ezchannelURL:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *)ezchannelURLForPushNotificationData:(NSDictionary*)data endpoint:(NSString*)endpoint;
+(NSURL *) radioURL:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
//Others URL
+(NSURL *) commentResponseURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) ezcastingAdvertisementURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) chromecastMoreSupportURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) demoModeSupportURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSURL *) wireNoticeLearnMoreURL;
+(NSURL *) firmwareOTAURL;
//Air series
+(NSURL *) airControlURLWithDevice:(AMPrototypeDevice *)device;
+(NSURL *) airDiskURLWithDevice:(AMPrototypeDevice *)device;
+(NSURL *) airSetupURLWithDevice:(AMPrototypeDevice *)device;
+(NSURL *) airViewURLWithDevice:(AMPrototypeDevice *)device;
+(NSURL *) advancedURLWithDevice:(AMPrototypeDevice *)device;
+(NSURL *) hotspotConnectionRequestURLWithDevice:(AMPrototypeDevice *)device ssid:(NSString *)ssid password:(NSString *)password;
//Schema push
+(NSString *) appInfoPathWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSString *) dongleInfoPathWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSString *) webVideoPathWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;
+(NSString *) webVideoReportPathWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo;

/**
 Return URL Divert JavaScript
 */
+(NSString *)urlDivertJSPath;
+(NSURL *)facebookShareVideoURLviaIEZVU:(NSURL *)url title:(NSString *)title;

/**
 2014/7/21 Brian add
 */
+(NSURL*) airControlURL:(NSString*) ip;
+(NSURL*) airDiskURL:(NSString*) ip;
+(NSURL*) airSetupURL:(NSString*) ip;

@end
