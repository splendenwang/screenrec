//
//  NSString+NetworkUtility.h
//  AMCommon_iOS
//
//  Created by brianliu on 13/7/11.
//  Copyright (c) 2013年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NetworkUtility)
//Deal with IPv4 address
+(uint32_t) unsignedIntFromIPAddres:(NSString*) ipAddress;
@end
