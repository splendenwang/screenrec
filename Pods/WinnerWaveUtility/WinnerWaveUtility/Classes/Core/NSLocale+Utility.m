//
//  NSLocale+Utility.m
//  AMCommon_iOS
//
//  Created by brianliu on 2/2/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import "NSLocale+Utility.h"

@implementation NSLocale (Utility)
+(BOOL) isChina{
    return [[[NSLocale currentLocale] objectForKey: NSLocaleCountryCode] isEqualToString:@"CN"];
}
@end
