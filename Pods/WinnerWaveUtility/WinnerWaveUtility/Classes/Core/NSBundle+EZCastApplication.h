//
//  NSBundle+EZCastApplication.h
//  AMCommon_iOS
//
//  Created by brianliu on 3/24/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (EZCastApplication)
-(BOOL)isEZCastScreenEnable;
-(BOOL)isEZCastSSOEnable;
@end
