//
//  AMEZCastHelpURLGenerator.m
//  EZDisplay
//
//  Created by brianliu on 13/7/5.
//  Copyright (c) 2013年 Actions-Micro Taipei. All rights reserved.
//

#import "AMFunctionURLs.h"
#import "AMSchemaPushController.h"
#import <TransmitterKit/AMPrototypeDevice+EZCastCheck.h>
@import TransmitterKit;
#import "NSString+AMStringUtility.h"
#import "AMEZChannelAPI.h"

NSString * const kAMIsURLDebugMode = @"kAMIsURLDebugMode";
NSString * const kAppHomeSite = @"kAppHomeSite";


@implementation AMFunctionURLs
#pragma mark - Utility
+(NSURL *) URLNoEscapeWithUTF8String:(NSString*) string{
    return [NSURL URLWithString:[string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
}
+(void) setUrlDebugMode:(BOOL)aBool{
    [[NSUserDefaults standardUserDefaults] setBool:aBool forKey:kAMIsURLDebugMode];
}
+(BOOL) isUrlDebugMode {
    return [[[NSUserDefaults standardUserDefaults] objectForKey:kAMIsURLDebugMode] boolValue];
}
+(NSString *)siteString{
    NSString * site = [[NSUserDefaults standardUserDefaults] objectForKey:kAppHomeSite];
    if(site.length > 0) {
        return site;
    }
    return @"https://www.iezvu.com";
}
+(NSURL *)siteURL{
    return [NSURL URLWithString:[self siteString]];
}
//+(NSURL *)ezchannelURL {
//    return [NSURL URLWithString:[AMEZChannelAPI ezchannelString]];
//}
#pragma mark - URL parameters
/**
 @return: app macaddress (attained via querying dongle)
 */
+(NSString *) appAddress:(NSDictionary *)appInfo{
    NSString * appAddr = appInfo[kAMAppInfoMacAddress];
    if(appAddr == nil){
        appAddr = @"UNKNOWN_IDEVICE_ADDRESSS";
    }
    NSAssert(appAddr != nil, @"App idnetity should not be nil");
    return appAddr;
}
/**
 @return: app bundle id (attained via querying dongle)
 */
+(NSString *) appIdentity:(NSDictionary *)appInfo{
    NSString * appID = appInfo[kAMAppInfoAppID];
    if(appID == nil){
        appID = @"UNKNOWN_BUNDLEID";
    }
    NSAssert(appID != nil, @"App idnetity should not be nil");
    return appID;
}
/**
 @return: app version
 */
+(NSString *) appVersion:(NSDictionary *)appInfo{
    NSString * appVersion = appInfo[kAMAppInfoAppVersion];
    if(appVersion == nil){
        appVersion = @"UNKNOWN_APPVERSION";
    }
    NSAssert(appVersion != nil, @"App version should not be nil");
    return appVersion;
}
/**
 @return: app Language
 */
+(NSString *) appLanguage:(NSDictionary *)appInfo{
    NSString * appLang = appInfo[kAMAppInfoLanguage];
    if(appLang == nil){
        appLang = @"UNKNOWN_LANGUAGE";
    }
    NSAssert(appLang != nil, @"App language should not be nil");
    return appLang;
}
/**
 @return: device os according TARGET_OS_IPHONE
 */
+(NSString *) deviceOS:(NSDictionary *)appInfo{
    NSString * deviceOS = appInfo[kAMAppInfoOsType];
    if(deviceOS == nil){
        deviceOS = @"UNKNOWN_IDEVICE_OS";
    }
    NSAssert(deviceOS != nil, @"device OS should not be nil");
    return deviceOS;
}
/**
 @return: connected device hostName
 */
+(NSString *) dongleHostName:(AMPrototypeDevice *)device{
    NSString * dongleHostName = device.hostName;
    if(dongleHostName == nil){
        dongleHostName = @"UNKNOWN_DONGLE";
    }
    NSAssert(dongleHostName != nil, @"Ooh, dongle display name is nil. How can you do that?");
    return dongleHostName;
}
/**
 @return: connectedDevice device ID
 */
+(NSString *) dongleAddress:(AMPrototypeDevice *)device{
    NSString * dongleAddress = [device objectForKey:kAMPrototypeDeviceID];
    if (dongleAddress == nil) {
        dongleAddress = @"UNKNOWN_DONGLE_ID";
    }
    NSAssert(dongleAddress != nil, @"Ooh, dongle deviceid is nil. This is noooooooooooo good!");
    return dongleAddress;
}
/**
 @return: dongle ip address
 */
+(NSString *) dongleIpAddress:(AMPrototypeDevice *)device{
    NSString * dongleIP = [device ipAddress];
    if(dongleIP == nil){
        dongleIP = @"UNKNOWN_DONGLE_IP";
    }
    NSAssert(dongleIP != nil, @"device ip should not be nil");
    return dongleIP;
}
/**
 @return: dongle ip address
 */
+(NSString *) dongleWebRootHost:(AMPrototypeDevice *)device{
    NSString * dongleWebRootHost;
    if([device isKindOfClass:[AMGenericDevice class]] == NO){
        DLog(@"%@ trying get webRootHost, something goes wrong",[device class]);
        return nil;
    }
    dongleWebRootHost = [(AMGenericDevice *)device remoteHost].webRootHost;
    NSAssert(dongleWebRootHost != nil, @"device ip should not be nil");
    return dongleWebRootHost;
}
/** >>> DEPRECATED_ATTRIBUTE <<<
 Dongle type return string ref url: http://www.actions-micro.com/tiki/tiki-index.php?page=CloudService
 Dongle type identify ref url: http://www.actions-micro.com/tiki/tiki-index.php?page=EZCast+Pro#Protocol_
 
 @return: dongle type
 >>> DEPRECATED_ATTRIBUTE <<< */
+(NSString *) dongleType:(AMPrototypeDevice *)device DEPRECATED_ATTRIBUTE{
    return device.analyticDongleType;
}
+(NSString *) localeString{
    // Get the current country and locale information of the user
    NSLocale *locale = [NSLocale currentLocale];
    NSString * locale3116_1_code = [locale objectForKey:(NSString *)kCFLocaleCountryCode];
    if(locale3116_1_code == nil){
        locale3116_1_code = @"UNKNOWN_LOCALE";
    }
    NSAssert(locale3116_1_code != nil, @"locale String should not be nil" );
    return locale3116_1_code;
}
/**
 Returns string identified by UI_USER_INTERFACE_IDIOM
 @return: tb/ph
 */
+(NSString *) phoneOrTablet{
#if TARGET_OS_IPHONE
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad?@"tb":@"ph";
#else
    return @"THIS_IS_OSX";
#endif
}

#pragma mark - URL parameters presets
+(NSString *) buildQueryPath:(NSDictionary *)paramDict{
    NSString * queryString = @"";
    NSMutableArray *parts = [NSMutableArray array];
    for (NSString * key in [paramDict allKeys]) {
        NSString *part = [NSString stringWithFormat: @"%@=%@", key,paramDict[key]];
        [parts addObject: part];
    }
    queryString = [parts componentsJoinedByString:@"&"];
    //    for (NSString * key in [paramDict allKeys]) {
    //        NSString *part = [NSString stringWithFormat: @"&%@=%@", key,[paramDict objectForKey:key]];
    //        queryString = [queryString stringByAppendingString:part];
    //    }
    return queryString;
}
+(NSString *) parametersBasic:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    NSDictionary * queryParameterDict = @{
                                          @"os" : [self deviceOS:appInfo],
                                          @"ac" : [self dongleHostName:device],
                                          @"da" : [self dongleAddress:device],
                                          @"type" : device.analyticDongleType,
                                          @"aa" : [self appAddress:appInfo],
                                          @"av" : [self appVersion:appInfo],
                                          @"app": [self appIdentity:appInfo]
                                          };
    return [self buildQueryPath:queryParameterDict];
}
+(NSString *) parametersBasicWithLocale:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    NSDictionary * queryParameterDict = @{
                                          @"al" : [self localeString],
                                          @"os" : [self deviceOS:appInfo],
                                          @"ac" : [self dongleHostName:device],
                                          @"da" : [self dongleAddress:device],
                                          @"type" : device.analyticDongleType,
                                          @"aa" : [self appAddress:appInfo],
                                          @"av" : [self appVersion:appInfo],
                                          @"app": [self appIdentity:appInfo]
                                          };
    return [self buildQueryPath:queryParameterDict];
}
+ (NSDictionary *)queriesFull:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo {
    return @{
#if TARGET_OS_IPHONE
             @"tt" : [self phoneOrTablet].URLEncodedString,
#endif
             @"al" : [self localeString].URLEncodedString,
             @"os" : [self deviceOS:appInfo].URLEncodedString,
             @"ac" : [self dongleHostName:device].URLEncodedString,
             @"da" : [self dongleAddress:device].URLEncodedString,
             @"type" : device.analyticDongleType.URLEncodedString,
             @"aa" : [self appAddress:appInfo].URLEncodedString,
             @"av" : [self appVersion:appInfo].URLEncodedString,
             @"app": [self appIdentity:appInfo].URLEncodedString
             };

}
+(NSString *) parametersFull:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [self buildQueryPath:[self queriesFull:device appinfo:appInfo]];
}
+ (NSString *)URLWithBaseURL:(NSString *)baseURL path:(NSString *)path queries:(NSDictionary *)queries {
    NSMutableString * url = [NSMutableString stringWithString:baseURL];
    
    if([path hasPrefix:@"/"]){  //path is "/setting" for example
        if([url hasSuffix:@"/"] == NO){ //url is "https://www.google.com"
            [url appendString:path];
        }
        else{   //url is "https://www.google.com/"
            [url appendString:[path substringWithRange:NSMakeRange(1, path.length-1)]];
        }
    }
    else{   //path is "setting" for example
        if([url hasSuffix:@"/"] == NO){ //url is "https://www.google.com"
            [url appendString:@"/"];
            [url appendString:path];
        }
        else{ //url is "https://www.google.com/"
            [url appendString:path];
        }
    }
    
    return [self URLWithBaseURL:url
               andAppendQueries:queries];
}
+ (NSString *)URLWithBaseURL:(NSString *)baseURL andAppendQueries:(NSDictionary *)queries {
    NSMutableString *url = [baseURL mutableCopy];
    if (queries.allKeys.count > 0) {
        if ([url rangeOfString:@"?"].location==NSNotFound) {
            [url appendString:@"?"];
        }
        for (id key in queries.allKeys) {
            if ([url hasSuffix:@"?"]) {
                [url appendFormat:@"%@=%@", key, queries[key]];
            } else {
                [url appendFormat:@"&%@=%@", key, queries[key]];
            }
        }
    }
    return url;
}
+(NSString *) specificPage:(NSString *)pageName function:(NSString *)function device:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo extra:(NSDictionary *)queries {
    if (queries.allKeys.count > 0) {
        queries = [queries mutableCopy];
        [(NSMutableDictionary *)queries setObject:function.URLEncodedString forKey:@"af"];
        return [self URLWithBaseURL:[self URLWithBaseURL:[self siteString] path:pageName queries:[self queriesFull:device appinfo:appInfo]]
                   andAppendQueries:queries];
    } else {
        return [self URLWithBaseURL:[self URLWithBaseURL:[self siteString] path:pageName queries:[self queriesFull:device appinfo:appInfo]]
                   andAppendQueries:@{@"af":function.URLEncodedString}];
    }
}
+(NSString *) helpURLString:(NSString *)function device:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [AMFunctionURLs specificPage:@"help.php" function:function device:device appinfo:appInfo extra:nil];
}
+(NSString *) infoURLString:(NSString *)function device:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [AMFunctionURLs specificPage:@"info.php" function:function device:device appinfo:appInfo extra:nil];
}
+(NSString *) jsonURLString:(NSString *)function device:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [AMFunctionURLs specificPage:@"test.php" function:function device:device appinfo:appInfo extra:nil];
}
+(NSString *) webDefaultURLString:(NSString *)function device:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo extra:(NSDictionary *)queries {
    return [AMFunctionURLs specificPage:@"webdefault.php" function:function device:device appinfo:appInfo extra:queries];
}
+(NSString *) webDefaultURLString:(NSString *)function device:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo {
    return [AMFunctionURLs webDefaultURLString:function device:device appinfo:appInfo extra:nil];
}
#pragma mark - Function URLs
#pragma mark - Help
+(NSURL *) ezcastHelpURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [NSURL URLWithString:[self helpURLString:@"ezcast" device:device appinfo:appInfo]];
}
+(NSURL *) dlnaHelpURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [NSURL URLWithString:[self helpURLString:@"dlna" device:device appinfo:appInfo]];
}
+(NSURL *) ezairHelpURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [NSURL URLWithString:[self helpURLString:@"ezair" device:device appinfo:appInfo]];
}
#pragma mark - Info
+(NSURL *) dlnaSupportURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [NSURL URLWithString:[self infoURLString:@"dlna" device:device appinfo:appInfo]];
}
+(NSURL *) ezairSupportURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [NSURL URLWithString:[self infoURLString:@"ezair" device:device appinfo:appInfo]];
}
#pragma mark - Json
+(NSURL *) dlnaJSONSupportURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [NSURL URLWithString:[self jsonURLString:@"dlna" device:device appinfo:appInfo]];
}
+(NSURL *) ezairJSONSupportURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [NSURL URLWithString:[self jsonURLString:@"ezair" device:device appinfo:appInfo]];
}
#pragma mark - Fucntion
+(NSURL *) webEntryURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [NSURL URLWithString:[self webDefaultURLString:@"web" device:device appinfo:appInfo]];
}
+(NSURL *) ezboardURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo extra:(NSDictionary *)extra{
    return [NSURL URLWithString:[self webDefaultURLString:@"ezboard"
                                                   device:device
                                                  appinfo:appInfo
                                                    extra:extra]];
}
+(NSURL *) cloudVideoURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [NSURL URLWithString:[self webDefaultURLString:@"cloudvideo"
                                                   device:device
                                                  appinfo:appInfo
                                                    extra:nil]];
}
+(NSURL *) newsURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [NSURL URLWithString:[self webDefaultURLString:@"news" device:device appinfo:appInfo]];
}
+(NSURL *) mapURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [NSURL URLWithString:[self webDefaultURLString:@"map" device:device appinfo:appInfo]];
}
+(NSURL *) tvURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [NSURL URLWithString:[self webDefaultURLString:@"tv" device:device appinfo:appInfo]];
}
+(NSURL *) commentURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [NSURL URLWithString:[self webDefaultURLString:@"comment" device:device appinfo:appInfo]];
}
+(NSURL *) updateURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [NSURL URLWithString:[self webDefaultURLString:@"update" device:device appinfo:appInfo]];
}
+(NSURL *) helpURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [NSURL URLWithString:[self webDefaultURLString:@"help" device:device appinfo:appInfo]];
}
+(NSURL *) moreAppsURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo {
    return [NSURL URLWithString:[self webDefaultURLString:@"apps" device:device appinfo:appInfo]];
}
+(NSURL *) shopURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo {
    return [NSURL URLWithString:[self webDefaultURLString:@"shopping" device:device appinfo:appInfo]];
}
+(NSURL *) helpAndShopURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    /*
     RE: 補圖
     Dear all,
     
     我先把第三個按鈕的轉址網址定下來.
     
     參數是 af=link
     
     例如: http://www.iezvu.com/webdefault.php?os=android&af=link&al=US&ac=EZCast-9B3EDF4B&da=ced29b3edf4b&aa=c8:9c:dc:a3:a4:f0&av=1.1.550&type=ezcast&app=com.benq.qcast&tt=tb
     
     文件: http://wiki.actions-micro.com/tiki/tiki-index.php?page=Web+URL
     */
    return [NSURL URLWithString:[self webDefaultURLString:@"link" device:device appinfo:appInfo extra:nil]];
}
+(NSURL *) socialityURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    //    麻烦把地址改一下,换成: http://www.iezvu.com/social_page.php?&os=XXX&af=social&al=国码&ac=EZCast-xxxxx&da=dongle mac addr&aa=apk mac address&av=apk_version
    return [NSURL URLWithString:[self specificPage:@"social_page.php" function:@"social" device:device appinfo:appInfo extra:nil]];
}
#pragma mark - JsHttpServer Related
+(NSURL *) radioURL:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [NSURL URLWithString:[self specificPage:@"liveradio.php"
                                          function:@"radio" device:device
                                           appinfo:appInfo
                                             extra:nil]];
}
//+(NSURL *) ezchannelURL:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
//#if DEBUG
////    return [[self class] ezchannelURL];
////    NSMutableDictionary * queries = [NSMutableDictionary dictionaryWithDictionary:@{
////                                                                                    @"checksum":[[[AMFunctionURLs dongleAddress:device] stringByAppendingString:@"\tbind"] md5Value].URLEncodedString,
////                                                                                    @"login_redirect_url":@"ezchannel://ezcast.app/login".URLEncodedString
////                                                                                    }];
////    [(NSMutableDictionary *)queries setObject:@"ezchannel" forKey:@"af"];
////    [(NSMutableDictionary *)queries setObject:langauge forKey:@"l"];
////    [queries addEntriesFromDictionary:[self queriesFull:device appinfo:appInfo]];
////    NSString * urlString = [self URLWithBaseURL:[AMEZChannelAPI ezchannelString] path:@"/" queries:queries];
////    return [NSURL URLWithString:urlString];
//#endif
//    NSString * langauge =  [[NSLocale preferredLanguages] objectAtIndex:0];
//    langauge = langauge != nil ? langauge : @"unknown";
//    return [NSURL URLWithString:[self webDefaultURLString:@"ezchannel"
//                                                   device:device
//                                                  appinfo:appInfo
//                                                    extra:nil]];
//
//}
+(NSURL *)ezchannelURLForPushNotificationData:(NSDictionary*)data endpoint:(NSString*)endpoint{
    NSString * fragment = [data objectForKey:@"fragment"];
    NSString * state = [data objectForKey:@"state"];
    NSString * path = [data objectForKey:@"path"];
    
    NSMutableDictionary * dict = [NSMutableDictionary new];
    
    if(state.URLEncodedString.length>0)
        [dict setObject:state.URLEncodedString forKey:@"state"];
    
    NSString * urlString = [self URLWithBaseURL:endpoint path:path queries:dict];
    if(fragment.length>1)
        urlString = [urlString stringByAppendingFormat:@"#%@", fragment];
    return [NSURL URLWithString:urlString];
}
#pragma mark - Others
+(NSURL *) airControlURLWithDevice:(AMPrototypeDevice *)device{
    return [self URLNoEscapeWithUTF8String:[NSString stringWithFormat:@"%@rs232_ctrl.html",[self dongleWebRootHost:device]]];
}
+(NSURL *) airDiskURLWithDevice:(AMPrototypeDevice *)device{
    return [self URLNoEscapeWithUTF8String:[NSString stringWithFormat:@"%@airdisk.html",[self dongleWebRootHost:device]]];
}
+(NSURL *) airSetupURLWithDevice:(AMPrototypeDevice *)device{
    return [self URLNoEscapeWithUTF8String:[NSString stringWithFormat:@"%@websetting.html",[self dongleWebRootHost:device]]];
}
+(NSURL *) airViewURLWithDevice:(AMPrototypeDevice *)device{
    return [self URLNoEscapeWithUTF8String:[NSString stringWithFormat:@"%@cbv.html",[self dongleWebRootHost:device]]];
}
+(NSURL *)advancedURLWithDevice:(AMProjectorDevice*)device{
    return [self URLNoEscapeWithUTF8String:[NSString stringWithFormat:@"%@main.html",[self dongleWebRootHost:device]]];
}
+(NSURL *) hotspotConnectionRequestURLWithDevice:(AMPrototypeDevice *)device ssid:(NSString *)ssid password:(NSString *)password{
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@cgi-bin/set_hotspot_ap.cgi?ssid=%@&psk=%@",
                                 [self dongleWebRootHost:device],
                                 ssid.URLEncodedString,
                                 password.URLEncodedString]];
}
+(NSURL *) commentResponseURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo {
    return [self URLNoEscapeWithUTF8String:[[self siteString] stringByAppendingFormat:@"/upgrade/ezcast/appupgrade/cmmt_notice.php?af=comment&%@",[self parametersBasic:device appinfo:appInfo]]];
}
+(NSURL *) ezcastingAdvertisementURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [self URLNoEscapeWithUTF8String:[[self siteString] stringByAppendingFormat:@"/adver.php?af=adver&%@",[self parametersFull:device appinfo:appInfo]]];
}
+(NSURL *) chromecastMoreSupportURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [self URLNoEscapeWithUTF8String:[[self siteString] stringByAppendingFormat:@"/msg/ezvschrome.php?af=vstable&%@",[self parametersFull:device appinfo:appInfo]]];
}
+(NSURL *) demoModeSupportURLWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [self URLNoEscapeWithUTF8String:[[self siteString] stringByAppendingFormat:@"/msg/ezmore.php?ac=%@&aa=%@&av=%@",[self dongleHostName:device],[self appAddress:appInfo],[self appVersion:appInfo]]];
}
+(NSURL *) wireNoticeLearnMoreURL {
    return [NSURL URLWithString:@"https://www.iezvu.com/ezcast_wire.php?l=en"];
}
+(NSURL *) firmwareOTAURL {
    return [NSURL URLWithString:@"https://www.iezvu.com/upgrade/ota_rx.php"];
}
#pragma mark - SchemaPush URLs
+(NSString *) appInfoPathWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    NSDictionary * queryParameterDict = @{@"type" : device.analyticDongleType,
                                          @"dongle" : [self dongleAddress:device]
                                          };
    return [[NSString stringWithFormat:@"/appinfo_rx.php?%@",[self buildQueryPath:queryParameterDict]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}
+(NSString *) dongleInfoPathWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    NSDictionary * queryParameterDict = @{@"type" : device.analyticDongleType,
                                          @"app": [self appAddress:appInfo]
                                          };
    return [[NSString stringWithFormat:@"/dongleinfo_rx.php?%@",[self buildQueryPath:queryParameterDict]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}
+(NSString *) webVideoPathWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    NSDictionary * queryParameterDict = @{@"type" : device.analyticDongleType
                                          };
    return [[NSString stringWithFormat:@"/webvideo_rx.php?%@",[self buildQueryPath:queryParameterDict]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}
+(NSString *) webVideoReportPathWithDevice:(AMPrototypeDevice *)device appinfo:(NSDictionary *)appInfo{
    return [[NSString stringWithFormat:@"/videoreport_rx.php"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

#pragma mark -
+(NSString *) urlDivertJSPath{
    return @"/view/updatejs.php";
}
+(NSURL *)facebookShareVideoURLviaIEZVU:(NSURL *)url title:(NSString *)title{
    NSURL *urlToShare = [NSURL URLWithString:[NSString stringWithFormat:@"%@/fb/video.php?next=%@&title=%@"
                                              ,[AMFunctionURLs siteString]
                                              ,[url absoluteString].URLEncodedString
                                              ,title.URLEncodedString]
                               relativeToURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/fb/video.php",[AMFunctionURLs siteString]]]
                         ];
    return urlToShare;
}
+(NSString *)localSupportDirectoryPath{
    NSString *appSupportDir = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) lastObject];
    if (![[NSFileManager defaultManager] fileExistsAtPath:appSupportDir]) {
        NSError *error = nil;
        //Create one
        [[NSFileManager defaultManager] createDirectoryAtPath:appSupportDir withIntermediateDirectories:YES attributes:nil error:&error];
        if(error)
            DLog(@"%@", error.localizedDescription);
    }
    return [NSString stringWithFormat:@"%@", appSupportDir];
}

/**
 2014/7/21 Brian add
 */
+(NSURL*) airControlURL:(NSString*) ip{
    NSAssert( ip!=nil, @"ip should not be nil while getting airControlURL");
    return [self URLNoEscapeWithUTF8String:[NSString stringWithFormat:@"http://%@/rs232_ctrl.html", ip]];
}
+(NSURL*) airDiskURL:(NSString*) ip{
    NSAssert( ip!=nil, @"ip should not be nil while getting airDiskURL");
    return [self URLNoEscapeWithUTF8String:[NSString stringWithFormat:@"http://%@/airdisk.html", ip]];
}
+(NSURL*) airSetupURL:(NSString*) ip{
    NSAssert( ip!=nil, @"ip should not be nil while getting airDiskURL");
    return [self URLNoEscapeWithUTF8String:[NSString stringWithFormat:@"http://%@/websetting.html", ip]];
}
@end
