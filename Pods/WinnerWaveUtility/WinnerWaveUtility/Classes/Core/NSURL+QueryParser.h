//
//  NSURL+QueryParser.h
//  AMCommon_iOS
//
//  Created by Splenden on 2014/12/11.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (QueryParser)
- (NSURL *) URLByAppendingQueryDictionary:(NSDictionary *) queryDic;
- (NSDictionary *)queryDictionary;
@property (readonly) NSDictionary * queryParameters; //brian, supports encoding
@end
@interface NSDictionary (URLQuery)
- (NSString*) urlQueryString;
@end