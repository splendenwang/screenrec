//
//  NSString+AMStringUtility.m
//  EZRemoteControliOS
//
//  Created by brian on 12/4/9.
//  Copyright (c) 2012年 Actions-Micro. All rights reserved.
//
#import "NSString+AMStringUtility.h"
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <net/ethernet.h>
#if TARGET_OS_IPHONE
#import <MobileCoreServices/MobileCoreServices.h>
#else
#import <CoreServices/CoreServices.h>
#endif
@class NSHost;
@implementation NSString (AMStringUtility)

- (BOOL)isValidIPAddress
{
    const char* utf8 = [self UTF8String];
    int success;
    
    struct in_addr dst;
    success = inet_pton(AF_INET, utf8, &dst);
    if (success != 1) {
        struct in6_addr dst6;
        success = inet_pton(AF_INET6, utf8, &dst6);
    }
    
    return (success == 1 ? TRUE : FALSE);
}

- (BOOL)isValidIPv4Address
{
    return [self isValidIPAddress];
}

- (NSString*)stringByRemovingEndingChar
{
    NSRange range = [self rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"\0"]];
    if (range.location != NSNotFound) {
        return [[self substringWithRange:NSMakeRange(0, self.length - 1)] copy];
    }
    return self;
}

- (unsigned int)hexIntValue
{
    
    NSScanner* scanner;
    unsigned int result;
    
    scanner = [NSScanner scannerWithString:self];
    
    [scanner scanHexInt:&result];
    
    return result;
}

- (BOOL)isExtPDFFile
{
    if ([self isPathExtension])
        return ([[self lowercaseString] isEqualToString:@"pdf"]);
    return ([[self.pathExtension lowercaseString] isEqualToString:@"pdf"]);
}
- (BOOL)isExtWordFile
{
    if ([self isPathExtension])
        return ([[self lowercaseString] isEqualToString:@"doc"] || [[self lowercaseString] isEqualToString:@"docx"]);
    return ([[self.pathExtension lowercaseString] isEqualToString:@"doc"] || [[self.pathExtension lowercaseString] isEqualToString:@"docx"]);
}
- (BOOL)isExtExcelFile
{
    if ([self isPathExtension])
        return ([[self lowercaseString] isEqualToString:@"xls"] || [[self lowercaseString] isEqualToString:@"xlsx"]);
    return ([[self.pathExtension lowercaseString] isEqualToString:@"xls"] || [[self.pathExtension lowercaseString] isEqualToString:@"xlsx"]);
}
- (BOOL)isExtPPTFile
{
    if ([self isPathExtension])
        return ([[self lowercaseString] isEqualToString:@"ppt"] || [[self lowercaseString] isEqualToString:@"pptx"]);
    return ([[self.pathExtension lowercaseString] isEqualToString:@"ppt"] || [[self.pathExtension lowercaseString] isEqualToString:@"pptx"]);
}
- (BOOL)isExtPagesFile
{
    if ([self isPathExtension])
        return ([[self lowercaseString] isEqualToString:@"pages"]);
    return ([[self.pathExtension lowercaseString] isEqualToString:@"pages"]);
}
- (BOOL)isExtNumbersFile
{
    if ([self isPathExtension])
        return ([[self lowercaseString] isEqualToString:@"numbers"]);
    return ([[self.pathExtension lowercaseString] isEqualToString:@"numbers"]);
}
- (BOOL)isExtKeynotesFile
{
    if ([self isPathExtension])
        return ([[self lowercaseString] isEqualToString:@"key"]);
    return ([[self.pathExtension lowercaseString] isEqualToString:@"key"]);
}
- (BOOL)isExtImageFile
{
    if ([self isPathExtension])
        return ([[self lowercaseString] isEqualToString:@"jpg"] || [[self lowercaseString] isEqualToString:@"jpeg"] || [[self lowercaseString] isEqualToString:@"png"]);
    return ([[self.pathExtension lowercaseString] isEqualToString:@"jpg"] || [[self.pathExtension lowercaseString] isEqualToString:@"jpeg"] || [[self.pathExtension lowercaseString] isEqualToString:@"png"]);
}

- (BOOL)isPathExtension
{
    return (self.pathExtension.length == 0 && [self.pathExtension isEqualToString:@""]) ? YES : NO;
}
- (BOOL)isMovieFile
{
    NSString* lowercase = [self isPathExtension] ? [self lowercaseString] : [self.pathExtension lowercaseString];
    for (NSString* string in [NSString allowedMovieFileTypes]) {
        if ([lowercase isEqualToString:string])
            return YES;
    }
    return NO;
}

//
//Mantis ID:0014082
//Reporter	鄭建章�@Jesse
//Assigned To	王聲蓁�@splendenwang
//Video:
//avi,divx,xvid,mp4,mov ,vob,dat,ts,m2ts,mts,mkv,rmvb,rm,mpg,mpeg,wmv,m4v,3gp
//
//Audio:
//ape,flac,ogg,mp3,wma,wav,rm,m4a,aac
//
// rm --> video by splendenwang
//
+ (NSArray*)allowedMovieFileTypes
{
    //    return @[@"mov", @"mkv", @"mp4", @"avi", @"divx", @"mpg", @"mpeg", @"", @"ts", @"xvid", @"rmvb", @"rm", @"wmv", @"m4v"];
    return @[ @"avi", @"divx", @"xvid", @"mp4", @"mov", @"vob", @"dat", @"ts", @"m2ts", @"mts", @"mkv", @"rmvb", @"rm", @"mpg", @"mpeg", @"wmv", @"m4v", @"3gp", @"flv", @"asf" ];
}

- (BOOL)isAudioFile
{
    NSString* lowercase = [self isPathExtension] ? [self lowercaseString] : [self.pathExtension lowercaseString];
    for (NSString* string in [NSString allowedAudioFileTypes]) {
        if ([lowercase isEqualToString:string]) {
            return YES;
        }
    }
    return NO;
}

+ (NSArray*)allowedAudioFileTypes
{
    return @[ @"ape", @"flac", @"ogg", @"mp3", @"wma", @"wav", @"m4a", @"aac", @"rm", @"dts", @"ac3", @"ra", @"aif", @"aiff", @"mka" ];
}

- (NSString*)thumbnailPath
{
    return [self thumbnailPathWithSuffix:@"thumbnail"];
}

- (NSString*)thumbnailPathWithSuffix:(NSString*)suffix
{
    return [[[self stringByDeletingPathExtension] stringByAppendingFormat:@"_%@", suffix] stringByAppendingPathExtension:[self pathExtension]];
}

- (BOOL)isValidWebURL
{
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:@"^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([\\da-z]{2,6}).*" options:NSRegularExpressionCaseInsensitive error:nil];
    
    NSTextCheckingResult* match = [regex firstMatchInString:self options:0 range:NSMakeRange(0, [self length])];
    return match != nil;
}

+(NSString *)mimeFromPathExtension:(NSString*) extension;
{
    //http://stackoverflow.com/questions/9801106/how-can-i-get-mime-type-in-ios-which-is-not-based-on-extension
    
    // Get the UTI from the file's extension:
    
    CFStringRef pathExtension = (__bridge_retained CFStringRef)extension;
    CFStringRef type = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension, NULL);
    CFRelease(pathExtension);
    
    // The UTI can be converted to a mime type:
    
    NSString *mimeType = (__bridge_transfer NSString *)UTTypeCopyPreferredTagWithClass(type, kUTTagClassMIMEType);
    
    //exctpions, stupid apple
    /**
     @https://developer.apple.com/library/mac/documentation/Miscellaneous/Reference/UTIRef/Articles/System-DeclaredUniformTypeIdentifiers.html
     public.audio, com.microsoft.advanced-​systems-format
     .wma, video/x-ms-wma
     Windows media audio.
     
     @see:https://support.microsoft.com/en-us/kb/288102
     .wma	audio/x-ms-wma
     */
    if([mimeType isEqualToString:@"video/x-ms-wma"]){
        mimeType = @"audio/x-ms-wma";
    }
    
    /**
     *  Apple 本身不提供UTI/MIME的檔案 i.e., mkv,ts
     */
    if (mimeType == nil) {
        mimeType = @"application/octet-stream";
    }
    if (type != NULL)
        CFRelease(type);
    return mimeType;
}

- (BOOL)isValidURLString
{
    NSURL* url = [NSURL URLWithString:self];
    if (url)
        return YES;
    return NO;
}

- (NSString*)correctedURLString
{
    if ([self isValidURLString]) {
        return self;
    }
    return [[self stringByRemovingPercentEncoding] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

static NSString* const kAFCharactersToBeEscapedInQueryString = @":/?&=;+!@#$',*";

static NSString* AFPercentEscapedQueryStringKeyFromStringWithEncoding(NSString* string, NSStringEncoding encoding)
{
    static NSString* const kAFCharactersToLeaveUnescapedInQueryStringPairKey = @"[].()";
    
    return (__bridge_transfer NSString*)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)string, (__bridge CFStringRef)kAFCharactersToLeaveUnescapedInQueryStringPairKey, (__bridge CFStringRef)kAFCharactersToBeEscapedInQueryString, CFStringConvertNSStringEncodingToEncoding(encoding));
}
- (NSString*)URLEncodedString
{
    return AFPercentEscapedQueryStringKeyFromStringWithEncoding(self, NSUTF8StringEncoding);
}
- (NSString*)stringEscapedForJavasacript
{
    // valid JSON object need to be an array or dictionary
    NSArray* arrayForEncoding = @[ self ];
    NSString* jsonString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:arrayForEncoding options:0 error:nil] encoding:NSUTF8StringEncoding];
    
    NSString* escapedString = [jsonString substringWithRange:NSMakeRange(2, jsonString.length - 4)];
    return escapedString;
}
NSString* const DURATION_UNKNOWN_VALUE = @"DURATION_UNKNOWN_VALUE";
NSString* const DURATION_ZERO_MIN = @"DURATION_ZERO_MIN";
NSString* const DURATION_LESS_THAN_3MINS = @"DURATION_LESS_THAN_3MINS";
NSString* const DURATION_3_10MINS = @"DURATION_3_10MINS";
NSString* const DURATION_10_30MINS = @"DURATION_10_30MINS";
NSString* const DURATION_30_60MINS = @"DURATION_30_60MINS";
NSString* const DURATION_1_2HR = @"DURATION_1_2HR";
NSString* const DURATION_MORE_THAN_2HR = @"DURATION_MORE_THAN_2HR";

+ (NSString*)levelStringFromDuration:(NSTimeInterval)duration
{
    if (duration == 0.0) {
        return DURATION_ZERO_MIN;
    }
    else if (duration < 180) {
        return DURATION_LESS_THAN_3MINS;
    }
    else if (duration < 600) {
        return DURATION_3_10MINS;
    }
    else if (duration < 1800) {
        return DURATION_10_30MINS;
    }
    else if (duration < 3600) {
        return DURATION_30_60MINS;
    }
    else if (duration <= 7200) {
        return DURATION_1_2HR;
    }
    else if (duration > 7200) {
        return DURATION_MORE_THAN_2HR;
    }
    return DURATION_UNKNOWN_VALUE;
}

+ (NSString*)documentFolder
{
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}

+ (NSString*)documentsPathForFileName:(NSString*)name
{
    
    NSString* docPath = [NSString documentFolder];
    NSString* appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
    docPath = [docPath stringByAppendingPathComponent:appName];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:docPath] == NO) {
        [[NSFileManager defaultManager] createDirectoryAtPath:docPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    return [docPath stringByAppendingPathComponent:name];
}
- (NSString*)urlStringByReplacingHost:(NSString*)newHost
{
    NSString* host = newHost;
    NSString* url = self;
    DLog(@"connectedHost: %@", url);
    DLog(@"url: %@", url);
    
    NSURLComponents* components = [[NSURLComponents alloc] initWithString:url];
    
    NSMutableString* newURL = [NSMutableString stringWithFormat:@"%@://%@",
                               components.scheme, host];
    if (components.port) {
        [newURL appendFormat:@":%zd", components.port.integerValue];
    }
    if (components.path) {
        [newURL appendString:components.path];
    }
    if (components.percentEncodedQuery) {
        [newURL appendFormat:@"?%@", components.percentEncodedQuery];
    }
    if (components.fragment.length > 0) {
        [newURL appendString:components.fragment];
    }
    return newURL;
}

- (NSString*)miracode
{
    /**
     *  Ascii code convert
     *  http://stackoverflow.com/questions/2832729/how-to-convert-ascii-value-to-a-character-in-objective-c
     */
    if ([self isValidIPv4Address] == NO)
        return nil;
    NSArray* ipComponentArray = [self componentsSeparatedByString:@"."];
    NSUInteger sum = 0;
    sum += (NSUInteger)[ipComponentArray[3] integerValue] * 256 * 256 * 256;
    sum += (NSUInteger)[ipComponentArray[2] integerValue] * 256 * 256;
    sum += (NSUInteger)[ipComponentArray[1] integerValue] * 256;
    sum += (NSUInteger)[ipComponentArray[0] integerValue];
    NSMutableString* result = [NSMutableString new];
    NSUInteger asciiCodeBase = 97; //lower-case a
    while (sum > 0) {
        NSString* asciiChar = [NSString stringWithFormat:@"%c", (char)(sum % 26 + asciiCodeBase)];
        [result appendString:asciiChar];
        sum = (NSUInteger)(sum / 26);
    }
    NSString* asciiChar = [NSString stringWithFormat:@"%c", (char)(sum % 26 + asciiCodeBase)];
    [result appendString:asciiChar];
    return result;
}
- (NSString*)ipFromMiraCode
{
    if (self.length != 7)
        return nil;
    NSUInteger asciiCodeBase = 97; //lower-case a
    NSUInteger sum = 0;
    for (int i = 0; i < (int)self.length; i++) {
        sum += (NSUInteger)(([self characterAtIndex:i] - asciiCodeBase) * powl(26, i));
    }
    
    NSMutableString* result = [NSMutableString new];
    
    [result appendFormat:@"%lu.%lu.%lu.%lu", sum % 256, sum / 256 % 256, sum / 256 / 256 % 256, sum / 256 / 256 / 256 % 256];
    
    return result;
}


+ (NSString *)stringWithUUID {

    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge NSString*)string;
}

+ (NSString *)stringWithUniquePath {
    NSString *unique = [NSString stringWithUUID];
    return [NSTemporaryDirectory() stringByAppendingPathComponent: unique];
}

@end

