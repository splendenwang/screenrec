//
//  NSLocale+Utility.h
//  AMCommon_iOS
//
//  Created by brianliu on 2/2/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSLocale (Utility)
+(BOOL) isChina;
@end
