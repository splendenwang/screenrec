//
//  NSDate+String.m
//  EZRemoteControliOS
//
//  Created by brianliu on 12/9/18.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import "NSDate+String.h"

@implementation NSDate (String)
-(NSString*)getTimeString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *timeStr = [dateFormatter stringFromDate:self];
#if ! __has_feature(objc_arc)
    [dateFormatter release];
#endif
    
    return timeStr;
}
+(NSString *) getLocalDateTimeString{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    return dateString;
}

+(NSString *) iso8061StringFromDate:(NSDate*) date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    
    return [dateFormatter stringFromDate:date];
}
+(NSString*) stringTimeIntervalSinceNow:(NSTimeInterval)delay{
    return [NSString stringWithFormat:@"%lld", (long long)([[NSDate date] timeIntervalSince1970]+ delay)];
}
@end
