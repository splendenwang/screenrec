//
//  NSString+AMStringUtility.h
//  EZRemoteControliOS
//
//  Created by brian on 12/4/9.
//  Copyright (c) 2012年 Actions-Micro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (AMStringUtility)

//works with ipv4 and ipv6
@property (NS_NONATOMIC_IOSONLY, getter=isValidIPAddress, readonly) BOOL validIPAddress;

//deprecated
@property (NS_NONATOMIC_IOSONLY, getter=isValidIPv4Address, readonly) BOOL validIPv4Address NS_DEPRECATED(10_9, 10.11, 8_0, 9_0);

@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString* stringByRemovingEndingChar;
//+(NSString *) broadcastIP:(NSString*) fromIPAddress;
@property (NS_NONATOMIC_IOSONLY, readonly) unsigned int hexIntValue;

//The following methods check on path extension
@property (NS_NONATOMIC_IOSONLY, getter=isExtPDFFile, readonly) BOOL extPDFFile;
@property (NS_NONATOMIC_IOSONLY, getter=isExtWordFile, readonly) BOOL extWordFile;
@property (NS_NONATOMIC_IOSONLY, getter=isExtExcelFile, readonly) BOOL extExcelFile;
@property (NS_NONATOMIC_IOSONLY, getter=isExtPPTFile, readonly) BOOL extPPTFile;
@property (NS_NONATOMIC_IOSONLY, getter=isExtPagesFile, readonly) BOOL extPagesFile;
@property (NS_NONATOMIC_IOSONLY, getter=isExtNumbersFile, readonly) BOOL extNumbersFile;
@property (NS_NONATOMIC_IOSONLY, getter=isExtKeynotesFile, readonly) BOOL extKeynotesFile;
@property (NS_NONATOMIC_IOSONLY, getter=isExtImageFile, readonly) BOOL extImageFile;

/**
 2014.3.10
 Deprecated. Using this class method is not recommended for now. User supportedVideoFormat on AMPrototypeDevice instead.
 */
//check on path extension if itself is a movie file
@property (NS_NONATOMIC_IOSONLY, getter=isMovieFile, readonly) BOOL movieFile;
+ (NSArray*)allowedMovieFileTypes;

/**
 2014.3.10
 Deprecated. Using this class method is not recommended for now. User supportedAudioFormat on AMPrototypeDevice instead.
 */
//Check on path extension to see whether it is an audio file
@property (NS_NONATOMIC_IOSONLY, getter=isAudioFile, readonly) BOOL audioFile;
+ (NSArray*)allowedAudioFileTypes;

// example: "/Photos/Nature/Sun.jpg" ----> "/Photos/Nature/Sun_thumbnail.jpg"
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString* thumbnailPath;

//example: "/Photo/Nature/Sun.jpg" with Suffix:@"Light" ----> "/Photos/Nature/SunLight.jpg"
//example: "/Photo/Nature/Sun.jpg" with Suffix:@"_Origin" ----> "/Photos/Nature/Sun_Origin.jpg"
- (NSString*)thumbnailPathWithSuffix:(NSString*)suffix;

/**
 use regular expression  "^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$" to validate url
 */
- (BOOL)isValidWebURL;

/**
 *  @see:http://stackoverflow.com/questions/9801106/how-can-i-get-mime-type-in-ios-which-is-not-based-on-extension
 *  About Apple UTI table
 *  @see:https://developer.apple.com/library/mac/documentation/Miscellaneous/Reference/UTIRef/Articles/System-DeclaredUniformTypeIdentifiers.html
 *  MS Mime Talbe
 *  @see:https://support.microsoft.com/en-us/kb/288102
 *  
 *  ogg, flac and some media files types are not supported. wma,asf will return video/...
 *  most mime type are from iOS system Uniform Type Identifiers, if cannot get it, it will return application/octet-stream as default
 *
 *  @return MIME type
 */
+ (NSString*)mimeFromPathExtension:(NSString*)extension;

/**
 Check if the a url string conforms to RFC 2396
 */
- (BOOL)isValidURLString;

/**
 We workaround for incorrect encoded url such as "http://192.168.203.1/airusb/sdcard/ezcast blank.mp3". Partially encoded url string(which is wrong).
 It should be "http://192.168.203.1/airusb/sdcard/ezcast%20blank.mp3" instead.
 */
- (NSString*)correctedURLString;
/**
 *  kAFCharactersToBeEscapedInQueryString = @":/?&=;+!@#$()',*";
 *  @return AFPercentEscapedQueryStringKeyFromStringWithEncoding string
 */
- (NSString*)URLEncodedString;
/**
 *  Using NSJSONSerialization to escape javascript string
 *  @return Escaped string for javascript
 */
- (NSString*)stringEscapedForJavasacript;
/**
 *  readable string for schema recording
 *  @param duration meida duration
 *
 *  @return Readable duration string
 */
+ (NSString*)levelStringFromDuration:(NSTimeInterval)duration;

+ (NSString*)documentFolder; //orignally designed for OSX
+ (NSString*)documentsPathForFileName:(NSString*)name; //orignally designed for OSX
/**
 *  replace host of a url string
 *
 *  @param newHost newHost (normally gets from connectedHost of server)
 *
 *  @return url string with newHost
 */
- (NSString*)urlStringByReplacingHost:(NSString*)newHost;
/**
 Miracode 基本上就是一組短碼能還原成 IP address,
 讓用戶透過 app 輸入 miracode來直接連到那支 dongle. (跟直接輸入 IP address 效果一樣,不過比輸入 ip address 輸入少一點字)
 
 我們參考的對象是用 6 碼英數字,也許是用 base 64 encode. 不過考慮到輸入不用切換大小寫或英數字會輸入更少,我們可以 encode 成 26 個小寫英文字母,會變成 7 碼.
 
 編碼方式:
 ip[3]*256*256*256+ip[2]*256*256+ip[1]*256+ip[0]   (考量同一網域的 dongle, ip 前幾碼可能相同,編出來能差異大一點)
 再除以 26得餘數,做 7 次得 7 個數字.
 
 a=0
 b=1
 …
 z=25
 
 例如 192.168.168.1 編成milxica
 例如 192.168.168.2 編成aszptda
 
 User 輸入 miracode,就反向還原成 IP address 再去連去它.
 
 *  @from Jesse's mail
 *
 * self must be an valid ipv4 addres(xxx.xxx.xxx.xxx) or it will return nil
 *  @return miracode converted from ipaddress
 */
- (NSString*)miracode;
/**
 *  Miracode 基本上就是一組短碼能還原成 IP address, 7碼
 *
 *  @return ipv4 address
 */
- (NSString*)ipFromMiraCode;


//https://gist.github.com/nschum/762038
+ (NSString *)stringWithUUID;
+ (NSString *)stringWithUniquePath; //generate unique path under /tmp

@end
