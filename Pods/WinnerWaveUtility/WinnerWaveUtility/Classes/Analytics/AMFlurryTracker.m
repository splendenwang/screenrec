//
//  AMAppUsageTracer.m
//  AMCommon_iOS
//
//  Created by brianliu on 5/7/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import "AMFlurryTracker.h"
#import "AMAnalytics.h"

#if TARGET_OS_IPHONE
#import "Flurry.h"
#endif

@interface AMFlurryTracker ()
@property (strong) id<AMUsage> usage;
@end

@implementation AMFlurryTracker
#if TARGET_OS_IPHONE
- (void)log:(id<AMUsage>)usage{
    DLog(@"%s, not implement yet", __PRETTY_FUNCTION__);
}

- (void)logEvent:(id<AMEvent>)event{
    
    NSAssert([event eventName]!=nil, @"eventName should not be nil");
    NSDictionary * parameters = nil;
    
    if([event respondsToSelector:@selector(parameters)])
        parameters = [event parameters];
    
    if(parameters){
        [Flurry logEvent:[event eventName] withParameters:parameters];
    }
    else{
        [Flurry logEvent:[event eventName]];
    }
}

-(void) usageDidBegin:(id<AMUsage>)usage{
    if(usage==nil){
        DLog(@"%s, empty usage obj", __PRETTY_FUNCTION__);
        return;
    }
    
    self.usage = usage;
    if([usage respondsToSelector:@selector(usageName)])
    {
        [Flurry logEvent:[usage usageName] timed:YES];
    }
}

-(void) usageDidCommit:(id<AMUsage>)usage{
    if(self.usage==nil){
        DLog(@"%s, self.usage is empty", __PRETTY_FUNCTION__);
        return;
    }
    NSDictionary * paratemters = nil;
    if([self.usage respondsToSelector:@selector(parameters)]){
        paratemters = [self.usage parameters];
    }
    [Flurry endTimedEvent:[self.usage usageName] withParameters:paratemters];
    self.usage = nil;
}
#else
- (void)log:(id<AMUsage>)usage
{
    
}
- (void)logEvent:(id<AMEvent>)event{
    
}
-(void) usageDidBegin:(id<AMUsage>)usage{
    
}
-(void) usageDidCommit:(id<AMUsage>)usage{
    
}
#endif

@end
