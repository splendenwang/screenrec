//
//  AMLogTracker.h
//  AMCommon_iOS
//
//  Created by James Chen on 2/6/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMAnalytics.h"

@interface AMLogTracker : NSObject<AMUsageTracker>

@end
