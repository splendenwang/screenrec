//
//  AMAppUsage.m
//  AMCommon_iOS
//
//  Created by brianliu on 5/7/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import "AMAppUsage.h"
@import TransmitterKit;
@interface AMAppUsage () {
    NSTimeInterval _duration;
    NSDate * _startDate;
}
@property (strong, NS_NONATOMIC_IOSONLY) id<AMUsageTracker> tracker;
@end
@implementation AMAppUsage
-(instancetype) initWithTracker:(id<AMUsageTracker>)tracker{
    self = [super init];
    if(self){
        self.tracker = tracker;
    }
    return self;
}

- (id)jsonRepresentation{
    //sb. please fill this out
    return nil;
}


-(void) begin{
    _startDate = [NSDate date];
    
    if([self.tracker respondsToSelector:@selector(usageDidBegin:)]){
        [self.tracker usageDidBegin:self];
    }
}

-(void) commit{
    NSAssert(_startDate!=nil, @"%s, startDate should not be nil, there maybe an error calling -(void)commit without -(void)begin.", __PRETTY_FUNCTION__);
    _duration = [[NSDate date] timeIntervalSinceDate:_startDate];
    _startDate = nil;
    
    if(self.autoAppendDeviceClassString){
        NSString * deviceClass= NSStringFromClass([AMDeviceController sharedDeviceController].connectedDevice.class);
        self.usageName = [self.usageName stringByAppendingFormat:@"(%@)", deviceClass];
    }
    
    if([self.tracker respondsToSelector:@selector(usageDidCommit:)]){
        [self.tracker usageDidCommit:self];
    }
    [self.tracker log:self];
}

-(NSTimeInterval) duration{
    return _duration;
}

@end
