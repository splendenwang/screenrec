//
//  AMScreenServerUsage.h
//  AMCommon_iOS
//
//  Created by James Chen on 2/6/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "AMAnalytics.h"

@interface AMScreenServerUsage : NSObject<AMUsage>
- (instancetype)initWithTracker:(id<AMUsageTracker>)tracker location:(CLLocationCoordinate2D)location;
- (void)begin;
- (void)commit;
- (id)jsonRepresentation;
- (NSString*) usageName;
@end
