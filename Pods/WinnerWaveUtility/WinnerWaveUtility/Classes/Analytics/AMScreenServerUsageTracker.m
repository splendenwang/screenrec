//
//  AMScreenServerUsageTracker.m
//  AMCommon_iOS
//
//  Created by James Chen on 2/6/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import "AMScreenServerUsageTracker.h"
@import TransmitterKit;
@import AFNetworking;

@implementation AMScreenServerUsageTracker
+ (id)produceEncryptedJson:(NSData *)jsonData {
    NSData *encryptedJson = [jsonData AESEncryptWithKey:@"28906462822699798631919982357480" settings:kAMAESECB256BitKeySettings];
    NSString *base64EncodedString = [encryptedJson base64EncodedStringWithOptions:0];
    
    return @{@"encrypted_data": base64EncodedString};
}
- (void)log:(id<AMUsage>)usage {
    // TODO
    // 1. [v] encrypt usage
    // 2. [?] save log for deferred upload
    // 3. [?] schedule a upload to http://cloud.iezvu.com/dongleinfo_rx.php?&type=ios
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[usage jsonRepresentation] options:NSJSONWritingPrettyPrinted error:&error];
    NSAssert(error == nil, @"error occurs");
    if (error != nil) {
        NSLog(@"AMLogTracker serialize usage:%@ failed. Error:%@", usage, error);
    } else {
        [[self class] uploadJson:[[self class] produceEncryptedJson:jsonData]];
    }
}

-(void) logEvent:(id<AMEvent>)event{
    NSLog(@"event:%@", event);
}

+ (void)uploadJson:(id)json {
    AFHTTPRequestOperationManager * manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableSet * set = [NSMutableSet setWithSet:manager.responseSerializer.acceptableContentTypes];
    [set addObjectsFromArray:@[@"text/plain",@"text/html"]];
    manager.responseSerializer.acceptableContentTypes = set;
    NSString * type = @"";
#if TARGET_OS_IPHONE
    type = @"ios";
#else
    type = @"mac";
#endif
    [manager POST:[NSString stringWithFormat:@"http://cloud.iezvu.com/dongleinfo_rx.php?&type=%@", type]
       parameters:json
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSString * message = @"DONE";
              if ([responseObject objectForKey:@"message"]) {
                  message = [responseObject objectForKey:@"message"];
              }
//              DLog(@"schema:%@ upload %@", operation.request.URL,message);
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//              DLog(@"schema:%@ upload %@", operation.request.URL, [error localizedDescription]);
          }];

}
@end
