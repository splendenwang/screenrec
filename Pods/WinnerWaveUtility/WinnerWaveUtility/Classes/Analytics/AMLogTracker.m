//
//  AMLogTracker.m
//  AMCommon_iOS
//
//  Created by James Chen on 2/6/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import "AMLogTracker.h"

@implementation AMLogTracker
- (void)log:(id<AMUsage>)usage {
    if([usage respondsToSelector:@selector(jsonRepresentation)] && [usage jsonRepresentation]!=nil){
        NSError *error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[usage jsonRepresentation] options:NSJSONWritingPrettyPrinted error:&error];
        if (error != nil) {
            NSLog(@"AMLogTracker serialize usage:%@ failed. Error:%@", usage, error);
        } else {
            NSLog(@"AMLogTracker log:%@", [NSString stringWithCString:jsonData.bytes encoding:NSUTF8StringEncoding]);
        }
    }
    if([usage respondsToSelector:@selector(usageName)] && [usage respondsToSelector:@selector(parameters)]){
        DLog(@"usageName:%@, parameters:%@", [usage usageName], [usage parameters]);
    }
}
- (void)logEvent:(id<AMEvent>)event{
    NSLog(@"event:%@, parameters:%@", [event eventName], [event parameters]);
}
@end
