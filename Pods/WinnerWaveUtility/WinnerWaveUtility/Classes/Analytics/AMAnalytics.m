//
//  AMAnalytics.m
//  AMCommon_iOS
//
//  Created by James Chen on 2/6/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMAnalytics.h"
#import <sys/sysctl.h>

@implementation AMAnalytics

+ (NSString *)deviceUUID {
#if TARGET_OS_IPHONE
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
#else
    return [[NSUUID UUID] UUIDString];
#endif
}
+ (NSString *)country {
    NSString * countryString = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode]?[[NSLocale currentLocale] objectForKey: NSLocaleCountryCode]:@"Unknown_country";
    return countryString;
}

#if TARGET_OS_IPHONE
+ (CGSize)screenSize {
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    //    That will give you the entire screen's resolution in points, so it would most typically be 320x480 for iPhones.
    //    Even though the iPhone4 has a much larger screen size iOS still gives back 320x480 instead of 640x960. This is mostly because of older applications breaking.
    CGFloat screenScale = [[UIScreen mainScreen] scale];
    //    This will give you the scale of the screen. For all iPhones and iPodTouches that do NOT have Retina Displays will return a 1.0f, while Retina Display devices will give a 2.0f.
    //        Now if you want to get the pixel width & height of the iOS device screen you just need to do one simple thing.
    CGSize screenSize = CGSizeMake(screenBounds.size.width * screenScale, screenBounds.size.height * screenScale);
    return screenSize;
}
+ (NSString *)systemVersion {
    NSString * systemVersionString = @"Unknown_version";
    UIDevice * device = [UIDevice currentDevice];
    systemVersionString = [device systemVersion];
    return systemVersionString;
}
+ (NSString *)readableDeviceModel {
    return [AMAnalytics readableMachineString];
}
#else
+ (CGSize)screenSize {
    NSScreen *mainScreen = [NSScreen mainScreen];
    CGSize screenSize = CGSizeMake(mainScreen.frame.size.width, mainScreen.frame.size.height);
    return screenSize;
}
+ (NSString *)systemVersion {
    NSString * systemVersionString = @"Unknown_version";
    NSDictionary * sv = [NSDictionary dictionaryWithContentsOfFile:@"/System/Library/CoreServices/SystemVersion.plist"];
    if([sv objectForKey:@"ProductVersion"])
        systemVersionString = [sv objectForKey:@"ProductVersion"];
    return systemVersionString;
}
+ (NSString *)readableDeviceModel {
    return [AMAnalytics readableModelString];
}
#endif
+ (NSString *)appVersion {
    NSString * appVersionString = @"Unknown_version";
    NSDictionary * infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString * mainVersion = [infoDictionary valueForKey:@"CFBundleShortVersionString"];
    NSString * bundleVersion = [infoDictionary valueForKey:@"CFBundleVersion"];
    if( mainVersion.length > 0 && bundleVersion.length >0 )
        appVersionString = [NSString stringWithFormat:@"%@(%@)",mainVersion, bundleVersion];
    return appVersionString;
}
+ (NSString *)language {
    NSString * languageString = [NSLocale preferredLanguages][0]?[NSLocale preferredLanguages][0]:@"Unknown_language";
    return languageString;
}
#pragma mark - AppInfo Pretty String retrun

+ (NSString *) machine {
    size_t size = 0;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    if (size) {
        char *machine = malloc(size);
        sysctlbyname("hw.machine", machine, &size, NULL, 0);
        NSString * returnVal = @(machine);
        free(machine);
        return returnVal;
    }
    return @"Unknown_machine";
}
+ (NSString *) model {
    size_t len = 0;
    sysctlbyname("hw.model", NULL, &len, NULL, 0);
    if (len) {
        char *model = malloc(len*sizeof(char));
        sysctlbyname("hw.model", model, &len, NULL, 0);
        NSString * returnVal = @(model);
        free(model);
        return returnVal;
    }
    return @"Unknown_model";
}

+ (NSString *) readableMachineString {
    NSString *machine = [AMAnalytics machine];
    //iphone
    if ([machine isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([machine isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([machine isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([machine isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([machine isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([machine isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([machine isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([machine isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([machine isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([machine isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([machine isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([machine isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([machine isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([machine isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    //ipod
    if ([machine isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([machine isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([machine isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([machine isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([machine isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    //ipad
    if ([machine isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([machine isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([machine isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([machine isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([machine isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    if ([machine isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([machine isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([machine isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([machine isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([machine isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([machine isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([machine isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([machine isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([machine isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([machine isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([machine isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([machine isEqualToString:@"iPad4,3"])      return @"iPad Air (Cellular)";
    if ([machine isEqualToString:@"iPad4,4"])      return @"iPad mini 2G (WiFi)";
    if ([machine isEqualToString:@"iPad4,5"])      return @"iPad mini 2G (Cellular)";
    if ([machine isEqualToString:@"iPad4,6"])      return @"iPad mini 2G (Cellular)";
    //simulator
    if ([machine isEqualToString:@"i386"])         return @"iOS Simulator";
    if ([machine isEqualToString:@"x86_64"])       return @"iOS Simulator";
    return machine;
}
/*
 @see imac http://support.apple.com/kb/HT1758
 @see macbookair http://support.apple.com/kb/HT3255
 @see macbookpro http://support.apple.com/kb/HT4132
 @see macbook http://support.apple.com/kb/ht1635
 */
+ (NSString *) readableModelString {
    NSString *model = [AMAnalytics model];
    //iMac
    if([model isEqualToString:@"iMac15,1"])        return @"iMac21.5 2014";
    if([model isEqualToString:@"iMac14,1"])        return @"iMac21.5 2013";
    if([model isEqualToString:@"iMac14,2"])        return @"iMac27 2013";
    if([model isEqualToString:@"iMac13,1"])        return @"iMac21.5 2012";
    if([model isEqualToString:@"iMac13,2"])        return @"iMac27 2012";
    if([model isEqualToString:@"iMac12,1"])        return @"iMac21.5 2011";
    if([model isEqualToString:@"iMac12,2"])        return @"iMac27 2011";
    if([model isEqualToString:@"iMac11,2"])        return @"iMac21.5 2010";
    if([model isEqualToString:@"iMac11,3"])        return @"iMac27 2010";
    if([model isEqualToString:@"iMac11,1"])        return @"iMac27 2009";
    if([model isEqualToString:@"iMac10,1"])        return @"iMac late 2009";
    if([model isEqualToString:@"iMac9,1"])         return @"iMac 2009";
    if([model isEqualToString:@"iMac8,1"])         return @"iMac 2008";
    if([model isEqualToString:@"iMac7,1"])         return @"iMac 2007";
    if([model isEqualToString:@"iMac5,1"])         return @"iMac 2006";
    if([model isEqualToString:@"iMac5,2"])         return @"iMac 2006CD";
    //MacBookPro13
    if([model isEqualToString:@"MacBookPro11,1"])  return @"MacbookProRetina13 late 2013/mid 2014";
    if([model isEqualToString:@"MacBookPro10,2"])  return @"MacbookProRetina13 late 2012/early 2013";
    if([model isEqualToString:@"MacBookPro9,2"])   return @"MacbookPro13 mid 2012";
    if([model isEqualToString:@"MacBookPro8,1"])   return @"MacbookPro13 2011";
    if([model isEqualToString:@"MacBookPro7,1"])   return @"MacbookPro13 2010";
    if([model isEqualToString:@"MacBookPro5,5"])   return @"MacbookPro13 2009";
    //MacBookPro15
    if([model isEqualToString:@"MacBookPro11,2"])  return @"MacbookProRetina15 late 2013/mid 2014";
    if([model isEqualToString:@"MacBookPro11,3"])  return @"MacbookProRetina15 late 2013/mid 2014";
    if([model isEqualToString:@"MacBookPro10,1"])  return @"MacbookPro15 2012/13";
    if([model isEqualToString:@"MacBookPro9,1"])   return @"MacbookPro15 2012";
    if([model isEqualToString:@"MacBookPro8,2"])   return @"MacbookPro15 2011";
    if([model isEqualToString:@"MacBookPro6,2"])   return @"MacbookPro15 2010";
    if([model isEqualToString:@"MacBookPro5,3"])   return @"MacbookPro15 2009";
    if([model isEqualToString:@"MacBookPro5,1"])   return @"MacbookPro15 2008";
    //MacBookPro17
    if([model isEqualToString:@"MacBookPro8,3"])   return @"MacbookPro17 2011";
    if([model isEqualToString:@"MacBookPro6,1"])   return @"MacbookPro15 2010";
    if([model isEqualToString:@"MacBookPro5,2"])   return @"MacbookPro17 2009";
    //MacBook
    if([model isEqualToString:@"MacBook7,1"])      return @"Macbook 2010";
    if([model isEqualToString:@"MacBook6,1"])      return @"Macbook mid 2009";
    if([model isEqualToString:@"MacBook5,2"])      return @"Macbook early 2009";
    if([model isEqualToString:@"MacBook5,1"])      return @"Macbook 2008";
    //MacBookAir
    if([model isEqualToString:@"MacBookAir6,2"])   return @"MacbookAir13 2013/early 2014";
    if([model isEqualToString:@"MacBookAir6,1"])   return @"MacbookAir11 2013/early 2014";
    if([model isEqualToString:@"MacBookAir5,2"])   return @"MacbookAir13 2012";
    if([model isEqualToString:@"MacBookAir5,1"])   return @"MacbookAir11 2012";
    if([model isEqualToString:@"MacBookAir4,2"])   return @"MacbookAir13 2011";
    if([model isEqualToString:@"MacBookAir4,1"])   return @"MacbookAir11 2011";
    if([model isEqualToString:@"MacBookAir3,2"])   return @"MacbookAir13 2010";
    if([model isEqualToString:@"MacBookAir3,1"])   return @"MacbookAir11 2010";
    if([model isEqualToString:@"MacBookAir2,1"])   return @"MacbookAir13 2008/09";
    if([model isEqualToString:@"MacBookAir1,1"])   return @"MacbookAir13 2008";
    return model;
}


@end
