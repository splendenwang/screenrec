//
//  AMAppPageView.m
//  AMCommon_iOS
//
//  Created by brianliu on 5/7/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import "AMAppEvent.h"
@import TransmitterKit;
@interface AMAppEvent () {
    
}
@property (strong, NS_NONATOMIC_IOSONLY) id<AMUsageTracker> tracker;
@end

@implementation AMAppEvent
-(instancetype) initWithTracker:(id<AMUsageTracker>)tracker{
    self = [super init];
    if(self){
        self.tracker = tracker;
        self.autoAppendDeviceClassString = YES;
    }
    return self;
}
-(void) commitEvent{
    if(self.autoAppendDeviceClassString){
        NSString * deviceClass= NSStringFromClass([AMDeviceController sharedDeviceController].connectedDevice.class);
        self.eventName = [self.eventName stringByAppendingFormat:@"(%@)", deviceClass];
    }
    [self.tracker logEvent:self];
}
@end
