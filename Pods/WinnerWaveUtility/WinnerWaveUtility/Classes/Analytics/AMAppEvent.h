//
//  AMAppPageView.h
//  AMCommon_iOS
//
//  Created by brianliu on 5/7/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMAnalytics.h"
@interface AMAppEvent : NSObject<AMEvent>
-(instancetype) initWithTracker:(id<AMUsageTracker>)tracker;
-(void) commitEvent;
@property (strong) NSString * eventName;
@property (strong) NSDictionary * parameters;
@property (strong) id context;
/**
 If it is YES, a device class string will be appended at the end of eventName property.
 
 Default is YES.
 */
@property (assign) BOOL autoAppendDeviceClassString;

@end
