//
//  AMCompoundTracker.m
//  AMCommon_iOS
//
//  Created by James Chen on 2/6/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import "AMCompoundTracker.h"
@interface AMCompoundTracker () {
    
}
@property (strong, NS_NONATOMIC_IOSONLY) NSArray *trackers;
@end
@implementation AMCompoundTracker
- (instancetype)initWithTrackers:(NSArray *)trackers {
    if (self = [super init]) {
        self.trackers = trackers;
    }
    return self;
}
- (void)log:(id<AMUsage>)usage {
    for (id<AMUsageTracker> tracker in _trackers) {
        [tracker log:usage];
    }
}
- (void) logEvent:(id<AMEvent>)event{
    for (id<AMUsageTracker> tracker in _trackers) {
        [tracker logEvent:event];
    }
}

-(void) usageDidBegin:(id<AMUsage>)usage{
    for (id<AMUsageTracker> tracker in _trackers) {
        if([tracker respondsToSelector:@selector(usageDidBegin:)])
        [tracker usageDidBegin:usage];
    }
}

-(void) usageDidCommit:(id<AMUsage>)usage{
    for (id<AMUsageTracker> tracker in _trackers) {
        if([tracker respondsToSelector:@selector(usageDidCommit:)])
            [tracker usageDidCommit:usage];
    }
}
@end
