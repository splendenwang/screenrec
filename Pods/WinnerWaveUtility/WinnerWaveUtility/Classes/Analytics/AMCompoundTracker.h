//
//  AMCompoundTracker.h
//  AMCommon_iOS
//
//  Created by James Chen on 2/6/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMAnalytics.h"

@interface AMCompoundTracker : NSObject<AMUsageTracker>
- (instancetype)initWithTrackers:(NSArray *)trackers;

- (void)log:(id<AMUsage>)usage;

- (void)logEvent:(id<AMEvent>)event;

-(void) usageDidBegin:(id<AMUsage>)usage;

-(void) usageDidCommit:(id<AMUsage>)usage;
@end
