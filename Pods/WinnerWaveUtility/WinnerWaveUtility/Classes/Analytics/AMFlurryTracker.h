//
//  AMAppUsageTracer.h
//  AMCommon_iOS
//
//  Created by brianliu on 5/7/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMAnalytics.h"

@interface AMFlurryTracker : NSObject<AMUsageTracker>
-(void)log:(id<AMUsage>)usage;
-(void)logEvent:(id<AMEvent>)event;
@end
