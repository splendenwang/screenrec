//
//  AMScreenServerUsage.m
//  AMCommon_iOS
//
//  Created by James Chen on 2/6/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import "AMScreenServerUsage.h"
@interface AMScreenServerUsage () {
    NSTimeInterval _duration;
    CLLocationCoordinate2D _location;
    BOOL _committed;
}
@property (strong, NS_NONATOMIC_IOSONLY) NSDate *startDate;
@property (strong, NS_NONATOMIC_IOSONLY) id<AMUsageTracker> tracker;
@end
@implementation AMScreenServerUsage
@synthesize duration = _duration;
- (instancetype)initWithTracker:(id<AMUsageTracker>)tracker location:(CLLocationCoordinate2D)location {
    NSAssert(tracker != nil, @"tracker should not be nil");
    if (self = [super init]) {
        self.tracker = tracker;
        _location = location;
    }
    return self;
}
- (void)begin {
    NSAssert(_startDate == nil, @"_startDate is not nil. probably call begin more than once.");
    if (_startDate == nil) {
        self.startDate = [NSDate date];
    }
    if([self.tracker respondsToSelector:@selector(usageDidBegin:)]){
        [self.tracker usageDidBegin:self];
    }
}
- (void)commit {
    NSAssert(_startDate != nil, @"_startDate should not be nil. -begin is not called befire -commit");
    NSAssert(!_committed, @"_committed shouldn't be true");
    if (_startDate && !_committed) {
        _duration = [[NSDate date] timeIntervalSinceDate:_startDate];
        [_tracker log:self];
    }
    if([self.tracker respondsToSelector:@selector(usageDidCommit:)]){
        [self.tracker usageDidCommit:self];
    }
}

-(NSString*) usageName{
    return @"EZCastScreen";
}

-(NSDictionary*)parameters{
    return [self jsonRepresentation];
}

- (id)jsonRepresentation {
    CGSize screenSize = [AMAnalytics screenSize];
    return @{
             @"schema_version" : @4,
             @"mac_address" : [AMAnalytics deviceUUID],
             @"resolution" : @{
                     @"width" : @(screenSize.width),
                     @"height" : @(screenSize.height)
                     },
             @"os_type" : @"ios",
             @"manufacturer" : @"Apple",
             @"model" : [AMAnalytics readableDeviceModel],
             @"app_version" : [AMAnalytics appVersion],
             @"country" : [AMAnalytics country],
             @"location" : @{
                     @"latitude" : @(_location.latitude),
                     @"longitude" : @(_location.longitude)
                     },
             @"language" : [AMAnalytics language],
             @"use_time" : @{
                     @"ezcast" : @(_duration)
                     },
             };
}
@end
