//
//  AMAppUsage.h
//  AMCommon_iOS
//
//  Created by brianliu on 5/7/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMAnalytics.h"

@interface AMAppUsage : NSObject<AMUsage>
-(instancetype) initWithTracker:(id<AMUsageTracker>)tracker;
-(void) begin;
-(void) commit;
@property (strong) NSString * usageName;
@property (strong) NSDictionary * parameters;

/**
 If it is YES, a device class string will be appended at the end of usageName property.
 
 Default is YES.
 */
@property (assign) BOOL autoAppendDeviceClassString;

@end
