//
//  AMAnalytics.h
//  AMCommon_iOS
//
//  Created by James Chen on 2/6/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#ifndef AMCommon_iOS_AMAnalytics_h
#define AMCommon_iOS_AMAnalytics_h

@protocol AMUsage <NSObject>
- (void)begin;
- (void)commit;
- (id)jsonRepresentation;
@property (nonatomic, readonly) NSTimeInterval duration;
@optional
- (NSString*) usageName;
- (NSDictionary*) parameters;
@end

@protocol AMUsageDelegate <NSObject>
@optional
-(void) usageDidBegin:(id<AMUsage>)usage;
-(void) usageDidCommit:(id<AMUsage>)usage;
@end

@protocol AMEvent <NSObject>
- (NSString*) eventName;
- (void) commitEvent;
@optional
- (NSDictionary*) parameters;
@end

@protocol AMUsageTracker <NSObject, AMUsageDelegate>
- (void)log:(id<AMUsage>)usage;
- (void)logEvent:(id<AMEvent>)event;
@end



@interface AMAnalytics : NSObject

+ (NSString *)deviceUUID;
+ (CGSize)screenSize;
+ (NSString *)readableDeviceModel;
+ (NSString *)appVersion;
+ (NSString *)country;
+ (NSString *)language;
@end
#endif
