//
//  AMEZChannelAPI.m
//  AMCommon_iOS
//
//  Created by James Chen on 3/18/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import "AMEZChannelAPI.h"
@import AFNetworking;
#import "AMAnalytics.h"

NSString *const EZChannelAPIErrorDomain = @"EZChannelAPIErrorDomain";

@interface AMEZChannelAPI () {
    
}
@property (strong, atomic) NSString *token;
@property (strong, atomic) NSString * endpoint;
@end

@implementation AMEZChannelAPI
- (instancetype)initWithAccessToken:(NSString *)token endpoint:(NSString*)endpoint{
    if (self = [super init]) {
        self.token = token;
        self.endpoint = endpoint;
    }
    return self;
}
+ (NSString *)contentSourceToString:(AMEZChannelContentSource)source {
    switch (source) {
        case AMEZChannelContentSourceWeb:
            return @"web";
            break;
        case AMEZChannelContentSourceAirDisk:
            return @"airdisk";
            break;
        case AMEZChannelContentSourceRadio:
            return @"radio";
            break;
        case AMEZChannelContentSourceApp:
            return @"app";
            break;
        case AMEZChannelContentSourceExtension:
            return @"extension";
            break;
        default:
            NSAssert(NO, @"Not handled enum value %zd", source);
            break;
    }
    return @"";
}
+ (AMEZChannelContentSource)contentSourceValueFromString:(NSString *)string {
    if ([@"web" isEqualToString:string]) {
        return AMEZChannelContentSourceWeb;
    } else if ([@"airdisk" isEqualToString:string]) {
        return AMEZChannelContentSourceAirDisk;
    } else if ([@"radio" isEqualToString:string]) {
        return AMEZChannelContentSourceRadio;
    } else if ([@"app" isEqualToString:string]) {
        return AMEZChannelContentSourceApp;
    } else if ([@"extension" isEqualToString:string]) {
        return AMEZChannelContentSourceExtension;
    }
    NSAssert(NO, @"Not handled contentSource string %@", string);
    return AMEZChannelContentSourceApp;
}

+ (NSString *)contentTypeToString:(AMEZChannelContentType)type {
    switch (type) {
        case AMEZChannelContentTypeHTML:
            return @"html";
            break;
        case AMEZChannelContentTypeStream:
            return @"stream";
            break;
        default:
            NSAssert(NO, @"Not handled enum value %zd", type);
            break;
    }
    return @"";
}
+ (AMEZChannelContentType)contentTypeValueFromString:(NSString *)string {
    if ([@"html" isEqualToString:string]) {
        return AMEZChannelContentTypeHTML;
    } else if ([@"stream" isEqualToString:string]) {
        return AMEZChannelContentTypeStream;
    }
    NSAssert(NO, @"Not handled contentType string %@", string);
    return AMEZChannelContentTypeHTML;
}
- (AFHTTPRequestOperation *)add:(NSString *)url
              toChannelWithName:(NSString *)name
                           from:(AMEZChannelContentSource)source
                           type:(AMEZChannelContentType)contentType
                          image:(NSString *)urlToThumbnail
                        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {
    if (_token == nil) {
        failure(nil, [NSError errorWithDomain:EZChannelAPIErrorDomain
                                         code:-1
                                     userInfo:nil]);
        return nil;
    }
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:self.endpoint]];
    NSMutableDictionary *parameters = [@{
                                        @"token": _token,
                                        @"url": url,
                                        @"name": name,
                                        @"source_from": [AMEZChannelAPI contentSourceToString:source],
                                        @"source_type": [AMEZChannelAPI contentTypeToString:contentType]
                                        } mutableCopy];
    if (urlToThumbnail) {
        [parameters setObject:urlToThumbnail forKey:@"image"];
    }
    return [manager GET:@"/ez/channel/movie/create" parameters:parameters
                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    if ([responseObject[@"status"] isEqual:@YES]) {
                        success(operation, responseObject);
                    } else {
                        failure(operation, [NSError errorWithDomain:EZChannelAPIErrorDomain
                                                               code:[responseObject[@"error"] integerValue]
                                                           userInfo:@{@"response" : responseObject}]);
                    }
                }
                failure:failure];
}

//-(NSURL*)baseURL{
//    NSString * baseURLString = [[[NSUserDefaults standardUserDefaults] objectForKey:@"kAMIsURLDebugMode"] boolValue]?
//    @"https://dev-channel.iezvu.com/": @"https://channel.iezvu.com/";
//    return [NSURL URLWithString:baseURLString];
//}

-(NSString*)osString{
#if TARGET_OS_IPHONE
    return @"ios";
#else
    return @"mac";
#endif
}

- (AFHTTPRequestOperation*)sendUserOnlineToServer:(NSString*)pushNotificationToken
                                          success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                          failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure{
    
    NSAssert(pushNotificationToken!=nil, @"What do you want? You gave me a nil token and sdk me to upload?");
    
    NSString * path = @"/ez/notification/app/online";
    
    if (_token == nil) {
        failure(nil, [NSError errorWithDomain:EZChannelAPIErrorDomain
                                         code:-1
                                     userInfo:nil]);
        return nil;
    }
    
    NSString * deviceIdentifier = nil;
#if TARGET_OS_IPHONE
    deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
#else
    deviceIdentifier = [NSHost currentHost].name.length>0 ? [NSHost currentHost].name : @"No any name set for this Mac device";
#endif
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:self.endpoint]];
    NSMutableDictionary *parameters = [@{
                                         @"token": _token,
                                         @"app_uuid":deviceIdentifier,
                                         @"app_os_type": [self osString],
                                         @"notification_app_id":[[NSBundle mainBundle] bundleIdentifier],
                                         @"notification_token":pushNotificationToken,
                                         @"app_locale":[AMAnalytics country]
                                         } mutableCopy];
    
    return [manager POST:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                            if ([responseObject[@"status"] isEqual:@YES]) {
                                if(success){
                                    success(operation, responseObject);
                                }
                            } else {
                                if(failure){
                                    failure(operation, [NSError errorWithDomain:EZChannelAPIErrorDomain
                                                                           code:[responseObject[@"error"] integerValue]
                                                                       userInfo:@{@"response" : responseObject}]);
                                }
                            }
    } failure:failure];
}

- (AFHTTPRequestOperation*)sendUserOfflineToServer:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure{
    NSString * path = @"/ez/notification/app/offline";
    
    if (_token == nil) {
        failure(nil, [NSError errorWithDomain:EZChannelAPIErrorDomain
                                         code:-1
                                     userInfo:nil]);
        return nil;
    }
    
    NSString * deviceIdentifier = nil;
#if TARGET_OS_IPHONE
    deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
#else
    deviceIdentifier = [NSHost currentHost].name.length>0 ? [NSHost currentHost].name : @"No any name set for this Mac device";
#endif
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:self.endpoint]];
    NSMutableDictionary *parameters = [@{
                                         @"token": _token,
                                         @"app_uuid":deviceIdentifier,
                                         @"app_os_type": [self osString],
                                         } mutableCopy];
    
    return [manager POST:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"status"] isEqual:@YES]) {
            if(success){
                success(operation, responseObject);
            }
        } else {
            if(failure){
                failure(operation, [NSError errorWithDomain:EZChannelAPIErrorDomain
                                                       code:[responseObject[@"error"] integerValue]
                                                   userInfo:@{@"response" : responseObject}]);
            }
        }
    } failure:failure];
}
#if TARGET_OS_IPHONE
- (AFHTTPRequestOperation*)logPlaybackHistory:(NSDictionary *)videoInfo
                                      success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                      failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {
    if (_token == nil) {
        failure(nil, [NSError errorWithDomain:EZChannelAPIErrorDomain
                                         code:-1
                                     userInfo:nil]);
        return nil;
    }
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:self.endpoint]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSString * countryString = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode]?[[NSLocale currentLocale] objectForKey: NSLocaleCountryCode]:@"Unknown_country";
    NSMutableDictionary *parameters = [@{
                                         @"token": _token,
                                         @"page": videoInfo[@"page"]?videoInfo[@"page"]:[NSNull null],
                                         @"src": videoInfo[@"src"]?videoInfo[@"src"]:[NSNull null],
                                         @"source_type": videoInfo[@"source_type"]?videoInfo[@"source_type"]:[NSNull null],
                                         @"title": videoInfo[@"title"]?videoInfo[@"title"]:[NSNull null],
                                         @"picture": videoInfo[@"image"]?videoInfo[@"image"]:[NSNull null],
                                         @"os": [self osString],
                                         @"osVersion": [[UIDevice currentDevice] systemVersion],
                                         @"region": countryString
                                         } mutableCopy];
    
    return [manager POST:@"/api/v1/histories" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(success){
            success(operation, responseObject);
        }
    } failure:failure];
}
#endif

@end
