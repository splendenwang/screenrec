//
//  AMEZChannelAPI.h
//  AMCommon_iOS
//
//  Created by James Chen on 3/18/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AFHTTPRequestOperation;

FOUNDATION_EXPORT NSString *const EZChannelAPIErrorDomain;

typedef NS_ENUM(NSInteger, AMEZChannelContentSource) {
    AMEZChannelContentSourceWeb,
    AMEZChannelContentSourceAirDisk,
    AMEZChannelContentSourceRadio,
    AMEZChannelContentSourceApp,
    AMEZChannelContentSourceExtension
};
typedef NS_ENUM(NSInteger, AMEZChannelContentType) {
    AMEZChannelContentTypeHTML,
    AMEZChannelContentTypeStream
};

@interface AMEZChannelAPI : NSObject

+ (AMEZChannelContentType)contentTypeValueFromString:(NSString *)string;
+ (AMEZChannelContentSource)contentSourceValueFromString:(NSString *)string;

- (instancetype)initWithAccessToken:(NSString *)token endpoint:(NSString*)endpoint;    //both parameters should not be nil

- (AFHTTPRequestOperation *)add:(NSString *)url
              toChannelWithName:(NSString *)name
                           from:(AMEZChannelContentSource)source
                           type:(AMEZChannelContentType)contentType
                          image:(NSString *)urlToThumbnail
                        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (AFHTTPRequestOperation*)sendUserOnlineToServer:(NSString*)pushNotificationToken
                                          success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                          failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (AFHTTPRequestOperation*)sendUserOfflineToServer:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                           failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

#if TARGET_OS_IPHONE
- (AFHTTPRequestOperation*)logPlaybackHistory:(NSDictionary *)videoInfo
                                      success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                      failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
#endif
@end
