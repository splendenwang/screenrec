//
//  AMEZChannelSharedData.m
//  AMCommon_iOS
//
//  Created by James Chen on 3/17/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import "AMEZChannelSharedData.h"
@interface AMEZChannelSharedData () {
    
}
@property (strong, nonatomic) NSUserDefaults *sharedUserDefaults;

@end

static NSString *EZCHANNEL_TOKEN_KEY = @"ezchannel.token";

@implementation AMEZChannelSharedData

+ (NSString *)appGroupName {
    return [@"group." stringByAppendingString:[NSBundle mainBundle].bundleIdentifier];
}
- (instancetype)initWithAppGroup:(NSString *)groupName {
    if (self = [super init]) {
        self.sharedUserDefaults = [[NSUserDefaults alloc] initWithSuiteName:groupName];
        
    }
    return self;
}
- (NSString *)accessToken {
    return [_sharedUserDefaults objectForKey:EZCHANNEL_TOKEN_KEY];
}
- (void)setAccessToken:(NSString *)accessToken {
    [_sharedUserDefaults setObject:accessToken forKey:EZCHANNEL_TOKEN_KEY];
    [_sharedUserDefaults synchronize];
}
@end
