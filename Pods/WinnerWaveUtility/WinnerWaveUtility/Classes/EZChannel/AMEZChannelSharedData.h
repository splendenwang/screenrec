//
//  AMEZChannelSharedData.h
//  AMCommon_iOS
//
//  Created by James Chen on 3/17/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AMEZChannelSharedData : NSObject

+ (NSString *)appGroupName;

@property (strong, atomic) NSString *accessToken;

- (instancetype)initWithAppGroup:(NSString *)groupName;
@end
