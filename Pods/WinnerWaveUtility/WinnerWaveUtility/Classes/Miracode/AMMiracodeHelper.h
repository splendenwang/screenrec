//
//  AMMiracodeHelper.h
//  
//
//  Created by Splenden on 2016/1/8.
//
//

#import <Foundation/Foundation.h>
@class AMPrototypeDevice;
@interface AMMiracodeHelper : NSObject
@property (nonatomic, strong) void (^connectionTimeoutBlock)(void);
@property (nonatomic, strong) void (^connectionBlock)(AMPrototypeDevice *device);
/**
 *  connect to specific address, timeout 12 secs
 *
 *  @param address it can be miracode or ipv4 address
 *
 *  @return return NO if it is invalid address
 */
- (BOOL)connectToAddress:(NSString *)address;
@end


@interface NSString(hostToIP)
- (NSString*)hostnameToIPAddress;
@end
