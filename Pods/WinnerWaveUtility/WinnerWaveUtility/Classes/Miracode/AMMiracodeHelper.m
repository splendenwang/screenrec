//
//  AMMiracodeHelper.m
//
//
//  Created by Splenden on 2016/1/8.
//
//
@import TransmitterKit;
#import "NSString+AMStringUtility.h"
#import "AMMiracodeHelper.h"
#include <netdb.h>
#include <arpa/inet.h>

@interface AMPrototypeDevice(miracode)
- (BOOL)isMiracodeDevice:(NSString *)ip;
@end
@interface AMMiracodeHelper() <AMDeviceControllerDelegate>
@property (strong, nonatomic, nullable) dispatch_source_t connectionTimeoutTimer;
@property (strong, nonatomic, nullable) NSString * ip;
@end
@implementation AMMiracodeHelper

- (BOOL)connectToAddress:(NSString *)address
{
    NSString* ip = [address copy];
    BOOL miracodeLimit = ip.length == 7;
    if ([ip isValidIPv4Address]) {
    }
    else if ([ip isValidIPv4Address] == NO && miracodeLimit && [[ip ipFromMiraCode] isValidIPv4Address]) {
        ip = [address ipFromMiraCode];
    } else if ([[ip hostnameToIPAddress] isValidIPv4Address]) {
        ip = [ip hostnameToIPAddress];
    } else {
        return NO;
    }
    self.ip = ip;
    __weak AMMiracodeHelper * weakSelf = self;
    AMDeviceController * deviceController = [AMDeviceController sharedDeviceController];
    for (AMPrototypeDevice * device in [deviceController discoveredDevices]) {
        if([device isMiracodeDevice:ip]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.connectionBlock(device);
            });
            return YES;
        }
    }
    
    deviceController.delegate = self;
    [deviceController stopScan];
    /**
     *  0016472: 直連小機，APP 通過輸入miracode 碼無法連接，提示timeout
     *  always trying connect default ip, someone's ask
     */
    [deviceController startScanAddresses:@[ip,@"192.168.168.1"]];
    double interval = 12.0f;
#if DEBUG
    interval = 12.0f;
#endif
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    if (timer) {
        dispatch_source_set_timer(timer, dispatch_time(DISPATCH_TIME_NOW, interval * NSEC_PER_SEC), interval * NSEC_PER_SEC, (1ull * NSEC_PER_SEC) / 10);
        dispatch_source_set_event_handler(timer, ^{
            AMPrototypeDevice * connectedDevice = [AMDeviceController sharedDeviceController].connectedDevice;
            BOOL connected = [connectedDevice isMiracodeDevice:weakSelf.ip];
            if( connected == NO && weakSelf.connectionTimeoutBlock) {
                weakSelf.connectionTimeoutBlock();
            }
            dispatch_source_cancel(timer);
        });
        dispatch_resume(timer);
    }
    
    if (self.connectionTimeoutTimer) {
        dispatch_source_cancel(self.connectionTimeoutTimer);
        self.connectionTimeoutTimer = nil;
    }
    self.connectionTimeoutTimer = timer;
    
    return YES;
}
- (void)dealloc {
    
}

#pragma mark - AMDeviceControllerDelegate
- (void)deviceController:(AMDeviceController*)controllder foundNewDevice:(AMPrototypeDevice*)device
{
    //dispatch after to make sure projector and remote been connected
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if([device isMiracodeDevice:self.ip]){
            if (self.connectionTimeoutTimer) {
                dispatch_source_cancel(self.connectionTimeoutTimer);
                self.connectionTimeoutTimer = nil;
            }
            
            if (self.connectionBlock) {
                self.connectionBlock(device);
                self.connectionBlock = nil;
            }
        }
    });
}
@end
@implementation AMPrototypeDevice (miracode)
- (BOOL)isMiracodeDevice:(NSString *)ip {
    if([self.ipAddress isEqualToString:ip] || [self.ipAddress isEqualToString:@"192.168.168.1"]) {
        return YES;
    }
    return NO;
}
@end
//hostName to IP, alternative way for iOS. NSHost is not available on iOS.
//@see http://blog.changyy.org/2014/06/ios-dns-lookuphost-name-to-ip-address.html
@implementation NSString (hostToIP)
- (NSString*)hostnameToIPAddress
{
    NSString * hostname = self;
    struct hostent *remoteHostEnt = gethostbyname([hostname UTF8String]);
    if(remoteHostEnt != NULL) {
        struct in_addr *remoteInAddr = (struct in_addr *) remoteHostEnt->h_addr_list[0];
        if(remoteInAddr != NULL) {
            return [NSString stringWithUTF8String:inet_ntoa(*remoteInAddr)];
        }
    }
    return nil;
}
@end
