//
//  AMSchemaVideoReportInfoController.h
//  AMCommon_iOS
//
//  Created by Splenden on 2014/8/13.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMSchemaBaseInfoController.h"

@interface AMSchemaWebVideoReportInfoController : AMSchemaBaseInfoController
-(void) setReportDictionary:(NSDictionary *) reportDictionary;
@end
