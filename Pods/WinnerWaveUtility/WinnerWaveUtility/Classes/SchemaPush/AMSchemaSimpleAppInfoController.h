//
//  AMSchemaSimpleAppInfoController.h
//  
//
//  Created by James Chen on 2/10/15.
//
//

#import "AMSchemaAppInfoController.h"

@interface AMSchemaSimpleAppInfoController : AMSchemaAppInfoController

- (instancetype)initWithDelegate:(id<AMEZCastSchemaBaseInfoDelegate>)delegate parameters:(NSDictionary *)parameterDictionary schemaID:(NSString *)schemaID;

@property (readonly, atomic) NSString *schemaID;

@end
