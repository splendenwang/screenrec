//
//  AMEZCastSchemaBaseInfoController.m
//  EZWifiMac
//
//  Created by Splenden on 2014/2/20.
//  Copyright (c) 2014年 Actions-Micro. All rights reserved.
//

#import "AMSchemaBaseInfoController.h"
@import TransmitterKit;

NSString * const kAMEZCastInfoControllerParameterHost = @"host";
NSString * const kAMEZCastInfoControllerParameterQueue = @"queue";

@implementation AMSchemaBaseInfoController
- (instancetype)init{
    NSAssert(NO, @"%s Must init with delegate, shouldnt call super init.",__PRETTY_FUNCTION__);
    self = [self initWithDelegate:nil parameters:nil];
    return nil;
}
- (instancetype)initWithDelegate:(id<AMEZCastSchemaBaseInfoDelegate>)delegate parameters:(NSDictionary *)parameterDictionary{
    self = [super init];
    if(self){
        NSAssert(delegate&&parameterDictionary,  @"init Parameters should not be nil");
        self.delegate = delegate;
        self.hostDeivce = parameterDictionary[kAMEZCastInfoControllerParameterHost];
        return self;
    }
    return nil;
}
- (NSDictionary *)infoDictionary{
    NSAssert(NO, @"This method should be overwrite");
    return nil;
}
- (NSDictionary *)encryptedDictionary{
    NSError *error = nil;
    NSString *unencryptedJsonString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[self infoDictionary] options:(NSJSONWritingOptions)0 error:&error] encoding:NSUTF8StringEncoding];
    NSDictionary *dict = @{@"encrypted_data": [unencryptedJsonString AESEncryptKey:@"28906462822699798631919982357480" settings:kAMAESECB256BitKeySettings]};
    
    if(!error){
        if([NSJSONSerialization isValidJSONObject:dict])
            return dict;
        else
            return nil;
    }
    return nil;
}
- (void)startSchemaInfoUpdate{
    NSAssert(NO, @"This method should be overwrite");
}
- (void)setSchemaInfoObject:(NSObject<NSCopying, NSMutableCopying, NSSecureCoding> *)value forKey:(NSString *)key{
    NSAssert(NO, @"This method should be overwrite");
}
- (void)increaseInfoUseCountForKey:(NSString *)key{
    NSMutableDictionary *useCountDictionary = [[self infoDictionary][kAMAppInfoUseCount] mutableCopy];
    NSString * count = @"0";
    if([useCountDictionary[key] isKindOfClass:[NSString class]]){
        count = [NSString stringWithFormat:@"%zd",[useCountDictionary[key] integerValue]+1];
        //some error handling, no idea use time should do this or not.
        if(count == nil){
            count = @"1";
        }
    }
    useCountDictionary[key] = count;
    [self setSchemaInfoObject:useCountDictionary forKey:kAMAppInfoUseCount];
}
- (void)increaseInfoUseTimeForKey:(NSString *)key interval:(double)timeInterval{
    NSMutableDictionary *useTimeDictionary = [[self infoDictionary][kAMAppInfoUseTime] mutableCopy];
    NSString *timeString = @"0.0";
    if([useTimeDictionary[key] isKindOfClass:[NSString class]]){
        timeString = [NSString stringWithFormat:@"%f",timeInterval + [useTimeDictionary[key] doubleValue]];
        DLog(@"[%@] use_time increased from %@ to %@",key, useTimeDictionary[key], timeString);
    }
    useTimeDictionary[key] = timeString;
    [self setSchemaInfoObject:useTimeDictionary forKey:kAMAppInfoUseTime];
}
@end
