//
//  AMEZCastSchemaDongleInfoController.m
//  EZWifiMac
//
//  Created by Splenden on 2014/2/20.
//  Copyright (c) 2014年 Actions-Micro. All rights reserved.
//

#import "AMSchemaDongleInfoController.h"
@import TransmitterKit.AMRemoteHost;
@interface AMSchemaDongleInfoController()
@property (nonatomic, strong) NSDictionary * dongleInfoDictionary;
@end

@implementation AMSchemaDongleInfoController
- (void)startSchemaInfoUpdate{
    NSAssert(self.hostDeivce, @"hostDevice should not be nil");
    [self updateDongleInfoFromDevice:self.hostDeivce];
}
- (NSDictionary *)infoDictionary{
    return self.dongleInfoDictionary;
}
- (void)updateDongleInfoFromDevice:(AMPrototypeDevice *)host{
    if([host isKindOfClass:[AMGenericDevice class]] == NO){
        DLog(@"%s %@ should not update dongleInfo",__PRETTY_FUNCTION__, [host class]);
        return;
    }
    AFHTTPRequestOperationManager * manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    NSMutableSet * set = [NSMutableSet setWithSet:manager.responseSerializer.acceptableContentTypes];
    [set addObject:@"text/html"];
    [set addObject:@"text/plain"];
    manager.responseSerializer.acceptableContentTypes = set;
    [manager GET:[NSString stringWithFormat:@"%@dongleInfo.json",[(AMGenericDevice *)host remoteHost].webRootHost]
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             self.dongleInfoDictionary = responseObject;
             [self.delegate didInfoControllerFinishUpdate:self Object:responseObject];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             DLog(@"DonlgeInfo went error:%@\n",error);
         }];
    
}
@end
