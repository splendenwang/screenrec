//
//  AMEZCastSchemaWebVideoInfoController.h
//  AMCommon_iOS
//
//  Created by Splenden on 2014/3/31.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMSchemaBaseInfoController.h"

@interface AMSchemaWebVideoInfoController : AMSchemaBaseInfoController
- (void)setStreamViewVideoInfoDictionary:(NSDictionary *)webVideoInfoDictionary;
@end
