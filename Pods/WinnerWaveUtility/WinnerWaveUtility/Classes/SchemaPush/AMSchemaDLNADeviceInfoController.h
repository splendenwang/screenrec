//
//  AMSchemaDLNADeviceInfoController.h
//  AMCommon_iOS
//
//  Created by James Chen on 2/10/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import "AMSchemaSimpleAppInfoController.h"

@interface AMSchemaDLNADeviceInfoController : AMSchemaSimpleAppInfoController

@end
