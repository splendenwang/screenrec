//
//  AMEZCastSchemaAppInfoController.m
//  EZWifiMac
//
//  In charge AppInfo update
//
//  Created by Splenden on 2014/2/19.
//  Copyright (c) 2014年 Actions-Micro. All rights reserved.
//

#import "AMSchemaAppInfoController.h"
@import TransmitterKit.AMRemoteHost;
@interface AMSchemaAppInfoController()
@end

@implementation AMSchemaAppInfoController
- (instancetype)initWithDelegate:(id<AMEZCastSchemaBaseInfoDelegate>)delegate parameters:(NSDictionary *)parameterDictionary{
    self = [super initWithDelegate:delegate parameters:parameterDictionary];
    if(self){
        [AMSchemaBaseInfo registerAppInfoSchema:kAMEZCastAppInfo];
        ///TODO: should create an active run loop thread
        /**
         * CLLocationManager
         * delegate object are called from the thread in which you started the corresponding location services.
         * That thread must itself have an active run loop, like the one found in your application's main thread.
         */
        //        [NSThread detachNewThreadSelector:@selector(startUpdateLocation) toTarget:self withObject:nil];
        //        self.locationThread = [[NSThread alloc] initWithTarget:self selector:@selector(startUpdateLocation) object:nil];
        return self;
    }
    return nil;
}
- (void)startSchemaInfoUpdate{
    [AMSchemaBaseInfo updateEZCastAppInfoSchema:kAMEZCastAppInfo];
    [self updateiDeviceMacFromDevice:self.hostDeivce completionBlock:^(NSError *error){
        [self updateiDeviceLocation];
        if(error){
            DLog(@"%s, error:%@", __PRETTY_FUNCTION__, error);
        }
    }];
}
- (NSString *)iDeviceUUID{
    NSString * uuidString = @"Unknown_device";
#if TARGET_OS_IPHONE
    uuidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
#else
    io_service_t platformExpert = IOServiceGetMatchingService(kIOMasterPortDefault,IOServiceMatching("IOPlatformExpertDevice"));
    if (platformExpert){
        CFTypeRef serialNumberAsCFString = IORegistryEntryCreateCFProperty(platformExpert,CFSTR(kIOPlatformUUIDKey),kCFAllocatorDefault, 0);
        if (serialNumberAsCFString){
            uuidString = (__bridge NSString *)(serialNumberAsCFString);
        }
        IOObjectRelease(platformExpert);
    }
#endif
    return uuidString;
}
#pragma mark - AppInfo setter/getter
- (NSDictionary *)infoDictionary{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDictionary *dict = [userDefault objectForKey:kAMEZCastAppInfo];
    if([NSJSONSerialization isValidJSONObject:[userDefault objectForKey:kAMEZCastAppInfo]])
        return dict;
    return nil;
}
- (void)setSchemaInfoObject:(NSObject<NSCopying, NSMutableCopying, NSSecureCoding> *)value forKey:(NSString *)key{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *appInfoDictionary = [[userDefault objectForKey:kAMEZCastAppInfo] mutableCopy];
    
    appInfoDictionary[key] = value;
    [userDefault setObject:appInfoDictionary forKey:kAMEZCastAppInfo];
    //FIXME: is synchronized needed?
    [userDefault synchronize];
}

#pragma mark - getiDevice Mac

- (BOOL)shouldObtainLocation {
    return ([CLLocationManager locationServicesEnabled] == YES) &&
    ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusRestricted && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied);
}
- (void)updateiDeviceLocation{
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self shouldObtainLocation]) {
            if (self.locationManager == nil) {
                self.locationManager = [[CLLocationManager alloc] init];
            }
#if TARGET_OS_IPHONE
            //iOS8
            if([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
                [self.locationManager requestWhenInUseAuthorization];
            }
#endif
            self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
            self.locationManager.distanceFilter = 500;
            self.locationManager.delegate = self;
            [self.locationManager startUpdatingLocation];
        } else {
#if TARGET_OS_IPHONE
            DLog(@"[Error] You currently have all location services for this device disabled. For better user experience, please turn on location services.");
//            static dispatch_once_t onceToken;
//            dispatch_once(&onceToken, ^{
//                
//                UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTable(@"Warning", @"AMCommon", @"Warning")
//                                                                                message:NSLocalizedStringFromTable(@"You currently have all location services for this device disabled. For better user experience, please turn on location services.", @"AMCommon", @"SchemaPush - location disabled message")
//                                                                               delegate:nil
//                                                                      cancelButtonTitle:@"OK"
//                                                                      otherButtonTitles:nil];
//                [servicesDisabledAlert show];
//                
//            });
#endif
            [self.delegate didInfoControllerFinishUpdate:self Object:[self encryptedDictionary]];
        }
    });
}
- (void)updateiDeviceMacFromDevice:(AMGenericDevice *)host completionBlock:(void (^)(NSError *))completionBlock{
//    DTrace();
    
    AFHTTPRequestOperationManager * manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSMutableSet * set = [NSMutableSet setWithSet:manager.responseSerializer.acceptableContentTypes];
    [set addObjectsFromArray:@[@"text/html", @"text/plain"]];
    manager.responseSerializer.acceptableContentTypes = set;
    AMRemoteHost * remote = host.remoteHost;
    [manager GET:[NSString stringWithFormat:@"%@cgi-bin/get_my_mac.cgi",remote.webRootHost]
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSString *macAddressString = [[NSString alloc] initWithBytes:[responseObject bytes] length:[responseObject length] encoding:NSASCIIStringEncoding];
             DLog(@"iDevice mac_address attained via Dongle: <%@>",macAddressString);
             NSError *error = nil;
             if(!macAddressString){
                 error = [NSError errorWithDomain:@"AFNetworkingErrorDomain" code:NSURLErrorZeroByteResource userInfo:@{NSLocalizedDescriptionKey:@"MacAddress should not be nil"}];
             } else {
                 [self setSchemaInfoObject:macAddressString forKey:kAMAppInfoMacAddress];
             }
             if(completionBlock)completionBlock(error);
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             DLog(@"Fetch iDevice mac_address via Dongle Fail\n Operation:%@\nError:%@\n",operation,error);
             if(completionBlock)completionBlock(error);
         }];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSString *errorString;
    [manager stopUpdatingLocation];
//    DTrace();
    DLog(@"Error: %@",[error localizedDescription]);
    switch([error code]) {
        case kCLErrorDenied:
            //Access denied by user
            errorString = @"Your location service for this application disabled. For better user experince, please turn on location services.";
            //Do something...
            break;
        case kCLErrorLocationUnknown:
            //Probably temporary...
            errorString = @"Location data unavailable";
            //Do something else...
            break;
        default:
            errorString = @"Location service has an unknown error occurred";
            break;
    }
    errorString = [[NSBundle mainBundle] localizedStringForKey:errorString value:@"" table:@"AMCommon"];
    
    [self.delegate didInfoControllerFinishUpdate:self Object:[self encryptedDictionary]];
}
- (void)locationDidUpdate:(CLLocation *)newLocation {
//    DTrace();
//    DLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        NSMutableDictionary *appInfoLocationDictionary = [NSMutableDictionary dictionary];
        appInfoLocationDictionary[kAMAppInfoLocationLongitude] = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        appInfoLocationDictionary[kAMAppInfoLocationLatitude] = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        
        [self setSchemaInfoObject:appInfoLocationDictionary forKey:kAMAppInfoLocation];
    }
    [self.delegate didInfoControllerFinishUpdate:self Object:[self encryptedDictionary]];

}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    [manager stopUpdatingLocation];
    [self locationDidUpdate:newLocation];
}
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    [manager stopUpdatingLocation];
    [self locationDidUpdate:[locations firstObject]];
}
@end
