//
//  AMEZCastSchemaChromeCastInfoController.m
//  EZWifiMac
//
//  Created by Splenden on 2014/3/21.
//  Copyright (c) 2014年 Actions-Micro. All rights reserved.
//

#import "AMSchemaChromeCastInfoController.h"

@interface AMSchemaChromeCastInfoController()

@end

@implementation AMSchemaChromeCastInfoController
- (instancetype)initWithDelegate:(id<AMEZCastSchemaBaseInfoDelegate>)delegate parameters:(NSDictionary *)parameterDictionary{
    self = [super initWithDelegate:delegate parameters:parameterDictionary schemaID:@"kAMEZCastChromecastInfo"];
    if(self){
    }
    return self;
}
@end
