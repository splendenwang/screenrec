//
//  AMSchemaOfflineInfoController.h
//  AMCommon_iOS
//
//  Created by Splenden on 2014/12/15.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMSchemaSimpleAppInfoController.h"

@interface AMSchemaOfflineInfoController : AMSchemaSimpleAppInfoController

@end
