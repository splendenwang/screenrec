//
//  AMSchemaVideoReportInfoController.m
//  AMCommon_iOS
//
//  Created by Splenden on 2014/8/13.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//
//  http://www.actions-micro.com/tiki/tiki-index.php?page=videoreport_rx.php&highlight=videoreport_rx.php
//
@import TransmitterKit.AMPrototypeDevice__internal;
#import "AMSchemaWebVideoReportInfoController.h"
#import "AMSchemaPushController.h"
@interface AMSchemaWebVideoReportInfoController()
@property (nonatomic, strong) NSDictionary * reportDictionary;
@end

@implementation AMSchemaWebVideoReportInfoController
-(void) startSchemaInfoUpdate{
    NSAssert(self.reportDictionary != nil, @"report dictionary shall not be nil");
    [self.delegate didInfoControllerFinishUpdate:self Object:[self encryptedDictionary]];
}
-(void) setReportDictionary:(NSDictionary *)reportDictionary{
    if(_reportDictionary != reportDictionary){
        _reportDictionary = reportDictionary;
    }
}
-(NSDictionary *) infoDictionary{
    NSDictionary * appInfoDictionary = [[AMSchemaPushController sharedInstance] appInfo];
    NSString *appInfoMacAddress = appInfoDictionary[kAMAppInfoMacAddress];
    NSString *appInfoVersion = appInfoDictionary[kAMAppInfoAppVersion];
    NSString *appInfoOS = appInfoDictionary[kAMAppInfoOsType];
    NSString *appInfoOSVersion = appInfoDictionary[kAMAppInfoOsVersion];
    NSString *appInfoiDeviceModel = appInfoDictionary[kAMAppInfoModel];
    NSString *appInfoManufacturer = appInfoDictionary[kAMAppInfoManufacturer];
    
    NSMutableDictionary * tmpDict = [NSMutableDictionary dictionaryWithDictionary:_reportDictionary];
    [tmpDict addEntriesFromDictionary:@{
                                       @"app_device_manufacturer": appInfoManufacturer,
                                       @"app_device_model":appInfoiDeviceModel,
                                       @"app_mac_address":appInfoMacAddress,
                                       @"app_version":appInfoVersion,
                                       @"app_os_type":appInfoOS,
                                       @"app_os_version":appInfoOSVersion,
                                       @"dongle_mac_address":[self.hostDeivce objectForKey:kAMPrototypeDeviceID],
                                       @"dongle_firmware_version":self.hostDeivce.srcvers
                                       }];
    return tmpDict;
}
@end
