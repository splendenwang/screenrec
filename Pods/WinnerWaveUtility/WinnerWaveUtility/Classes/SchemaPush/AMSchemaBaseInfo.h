//
//  AMEZCastShcemaAppInfo.h
//  EZWifiMac
//
//  Created by Splenden on 2014/2/19.
//  Copyright (c) 2014年 Actions-Micro. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kAMEZCastAppInfo;
extern NSString * const kAMAppInfoSchemaVersion;
extern NSString * const kAMAppInfoMacAddress;
extern NSString * const kAMAppInfoOsType;
extern NSString * const kAMAppInfoOsVersion;
extern NSString * const kAMAppInfoManufacturer;
extern NSString * const kAMAppInfoModel;
extern NSString * const kAMAppInfoAppVersion;
extern NSString * const kAMAppInfoResolution;
extern NSString * const kAMAppInfoResolutionWidth;
extern NSString * const kAMAppInfoResolutionHeight;
extern NSString * const kAMAppInfoLocation;
extern NSString * const kAMAppInfoLocationLatitude;
extern NSString * const kAMAppInfoLocationLongitude;
extern NSString * const kAMAppInfoLanguage;
extern NSString * const kAMAppInfoCountry;
extern NSString * const kAMAppInfoUseTime;
extern NSString * const kAMAppInfoUseCount;
extern NSString * const kAMAppPushNotificationToken;
extern NSString * const kAMAppInfoTimeZone;
extern NSString * const kAMAppInfoAppID;

@interface AMSchemaBaseInfo : NSObject
/*
 Register SchemaPush Appinfo dictionary NSUserDefault
 @see http://wiki.actions-micro.com/tiki/tiki-index.php?page=EZ+Cast#dongle_App_
 */
+ (void)registerAppInfoSchema:(NSString *)schemaInfoKey;
/*
 Set SchemaPush some Appinfo variables
 */
+ (void)updateEZCastAppInfoSchema:(NSString *)schemaInfoKey;
@end
