//
//  AMEZCastSchemaBaseInfoController.h
//  EZWifiMac
//
//  Created by Splenden on 2014/2/20.
//  Copyright (c) 2014年 Actions-Micro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@import AFNetworking;
@import TransmitterKit.AMPrototypeDevice;
@import TransmitterKit.AMGenericDevice;

#import "AMSchemaBaseInfo.h"

extern NSString * const kAMEZCastInfoControllerParameterHost;
extern NSString * const kAMEZCastInfoControllerParameterQueue;

@class AMSchemaBaseInfoController;
@protocol AMEZCastSchemaBaseInfoDelegate
@required
- (void)didInfoControllerFinishUpdate:(AMSchemaBaseInfoController *)infoCotnroller Object:(NSObject *)object;
@optional
@end

@interface AMSchemaBaseInfoController : NSObject
@property (nonatomic, assign) id<AMEZCastSchemaBaseInfoDelegate> delegate;
@property (nonatomic, assign) AMPrototypeDevice *                hostDeivce;

- (instancetype)initWithDelegate:(id<AMEZCastSchemaBaseInfoDelegate>)delegate parameters:(NSDictionary *)parameterDictionary ;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSDictionary *infoDictionary;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSDictionary *encryptedDictionary;
- (void)startSchemaInfoUpdate;
- (void)setSchemaInfoObject:(NSObject<NSCopying, NSMutableCopying, NSSecureCoding> *)value forKey:(NSString *)key;
- (void)increaseInfoUseCountForKey:(NSString *)key;
- (void)increaseInfoUseTimeForKey:(NSString *)key interval:(double)timeInterval;

@end
