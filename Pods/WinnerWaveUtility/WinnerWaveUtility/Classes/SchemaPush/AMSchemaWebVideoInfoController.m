//
//  AMEZCastSchemaWebVideoInfoController.m
//  AMCommon_iOS
//
//  Created by Splenden on 2014/3/31.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//
//  Schema format accroding following url
//  http://wiki.actions-micro.com/tiki/tiki-index.php?page=EZ+Cast&highlight=%E4%B8%8A%E5%82%B3%E8%A6%96%E9%A0%BB%E7%9A%84%E8%B3%87%E6%96%99
#import "AMSchemaWebVideoInfoController.h"
#import "AMSchemaPushController.h"
@import TransmitterKit.AMPrototypeDevice__internal;
#define WEBVIDEO_SCHEMA_VERSION @"3"
//@class AMEZCastSchemaPushController;

const NSString * kAMSchemaWebVideoDongleAddress = @"dongle_mac_address";
const NSString * kAMSchemaWebVideoDongleFirmware = @"firmware_version";
const NSString * kAMSchemaWebVideoAppAddress = @"app_mac_address";
const NSString * kAMSchemaWebVideoAppOS = @"app_os_type";
const NSString * kAMSchemaWebVideoAppVersion = @"app_version";
const NSString * kAMSchemaWebVideoContent = @"web_video";
const NSString * kAMSchemaWebVideoUserAgent = @"user_agent";


@interface AMSchemaWebVideoInfoController()
@property (nonatomic, strong) NSDictionary * streamViewVideoInfoDictionary;
@property (nonatomic, strong) NSMutableDictionary * streamViewInfoDictionary;
@end

@implementation AMSchemaWebVideoInfoController
- (id)initWithDelegate:(id<AMEZCastSchemaBaseInfoDelegate>)delegate parameters:(NSDictionary *)parameterDictionary{
    self = [super initWithDelegate:delegate parameters:parameterDictionary];
    if(self){
        self.streamViewVideoInfoDictionary = [NSMutableDictionary dictionary];
    }
    return self;
}
- (void)startSchemaInfoUpdate{
    NSAssert(self.streamViewVideoInfoDictionary, @"Must have streamViewDictionary");
//    DLog(@"%@",self.infoDictionary);
    [self.delegate didInfoControllerFinishUpdate:self Object:[self encryptedDictionary]];
}
- (void)setStreamViewVideoInfoDictionary:(NSDictionary *)webVideoInfoDictionary{
    if(_streamViewVideoInfoDictionary!=webVideoInfoDictionary)
        _streamViewVideoInfoDictionary = webVideoInfoDictionary;
}
- (void)setSchemaInfoObject:(NSObject<NSCopying,NSMutableCopying,NSSecureCoding> *)value forKey:(NSString *)key{
    [self.streamViewInfoDictionary setObject:value forKey:key];
}
- (NSMutableDictionary *)streamViewInfoDictionary{
    if(_streamViewInfoDictionary == nil){
        NSDictionary * appInfoDictionary = [[AMSchemaPushController sharedInstance] appInfo];
        NSString *appInfoMacAddress = [appInfoDictionary objectForKey:kAMAppInfoMacAddress];
        NSString *appInfoVersion = [appInfoDictionary objectForKey:kAMAppInfoAppVersion];
        NSString *appInfoOS = [appInfoDictionary objectForKey:kAMAppInfoOsType];
        _streamViewInfoDictionary = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                    kAMAppInfoSchemaVersion:WEBVIDEO_SCHEMA_VERSION,
                                                                                    kAMSchemaWebVideoDongleAddress:[self.hostDeivce objectForKey:kAMPrototypeDeviceID],
                                                                                    kAMSchemaWebVideoDongleFirmware:self.hostDeivce.srcvers,
                                                                                    kAMSchemaWebVideoAppAddress:appInfoMacAddress,
                                                                                    kAMSchemaWebVideoAppOS:appInfoOS,
                                                                                    kAMSchemaWebVideoAppVersion:appInfoVersion,
                                                                                    }];
    }
    return _streamViewInfoDictionary;
}
- (NSDictionary *)infoDictionary{
    NSMutableDictionary * infoDictionary = [NSMutableDictionary dictionaryWithDictionary:self.streamViewInfoDictionary];
    [infoDictionary addEntriesFromDictionary:@{
                                               kAMSchemaWebVideoContent:@[self.streamViewVideoInfoDictionary],
                                               }];
    return infoDictionary;
}
@end
