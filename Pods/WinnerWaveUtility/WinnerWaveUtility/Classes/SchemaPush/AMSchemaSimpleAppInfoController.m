//
//  AMSchemaSimpleAppInfoController.m
//  
//
//  Created by James Chen on 2/10/15.
//
//

#import "AMSchemaSimpleAppInfoController.h"
@import TransmitterKit.AMRemoteHost;
@interface AMSchemaSimpleAppInfoController () {
    
}
@property (strong, atomic) NSString *schemaID;

@end
@implementation AMSchemaSimpleAppInfoController
- (instancetype)initWithDelegate:(id<AMEZCastSchemaBaseInfoDelegate>)delegate parameters:(NSDictionary *)parameterDictionary schemaID:(NSString *)schemaID {
    self = [super initWithDelegate:delegate parameters:parameterDictionary ];
    if(self){
        self.schemaID = schemaID;
        [AMSchemaBaseInfo registerAppInfoSchema:_schemaID];
    }
    return self;
}
- (void)startSchemaInfoUpdate{
    NSAssert(self.hostDeivce, @"hostDevice should not be nil");
    [AMSchemaBaseInfo updateEZCastAppInfoSchema:_schemaID];
    [self updateiDeviceMacFromDevice:self.hostDeivce completionBlock:^(NSError *error){
        [self updateiDeviceLocation];
    }];
}
#pragma mark - AppInfo setter/getter
- (NSDictionary *)infoDictionary{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDictionary *dict = [userDefault objectForKey:_schemaID];
    if([NSJSONSerialization isValidJSONObject:[userDefault objectForKey:_schemaID]])
        return dict;
    return nil;
}
- (void)setSchemaInfoObject:(NSObject<NSCopying, NSMutableCopying, NSSecureCoding> *)value forKey:(NSString *)key{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *appInfoDictionary = [[userDefault objectForKey:_schemaID] mutableCopy];
    
    appInfoDictionary[key] = value;
    [userDefault setObject:appInfoDictionary forKey:_schemaID];
    //FIXME: is synchronized needed?
    [userDefault synchronize];
}
- (void)updateiDeviceMacFromDevice:(AMPrototypeDevice *)host completionBlock:(void (^)(NSError *))completionBlock{
    
    [self setSchemaInfoObject:[self iDeviceUUID] forKey:kAMAppInfoMacAddress];
    if(completionBlock)
        completionBlock(nil);
}

@end
