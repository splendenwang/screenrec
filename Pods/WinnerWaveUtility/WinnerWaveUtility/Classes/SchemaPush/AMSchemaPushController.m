//
//  AMEZCastSchemaPushController.m
//  EZWifiMac
//
//  Created by Splenden on 2014/2/19.
//  Copyright (c) 2014年 Actions-Micro. All rights reserved.
//

#import "AMSchemaPushController.h"
#import "AMSchemaChromeCastInfoController.h"
#import "AMSchemaScreenInfoController.h"
#import "AMSchemaDemoInfoController.h"
#import "AMSchemaOfflineInfoController.h"
#import "AMSchemaDLNADeviceInfoController.h"
@import TransmitterKit;
#import "AMFunctionURLs.h"
@import TransmitterKit.AMGenericDevice;
@import TransmitterKit.AMScreenDevice;
@import TransmitterKit.AMDemoDevice;

@interface AMSchemaPushController()

@property (nonatomic, strong) AMSchemaAppInfoController *appInfoController;
@property (nonatomic, strong) AMSchemaDongleInfoController *dongleInfoController;
@property (nonatomic, strong) AMSchemaWebVideoInfoController *webVideoInfoController;
@property (nonatomic, strong) AMSchemaWebVideoReportInfoController * webVideoReportController;

@property (nonatomic, strong) AMPrototypeDevice * deviceHost;

@end

@implementation AMSchemaPushController

- (AMSchemaWebVideoReportInfoController *) webVideoReportController{
    return _webVideoReportController;
}
- (AMSchemaWebVideoInfoController *)webVideoInfoController{
    return _webVideoInfoController;
}
- (AMSchemaAppInfoController *)appInfoController{
    return _appInfoController;
}
- (NSDictionary *)appInfo{
    return [_appInfoController infoDictionary];
}
- (AMSchemaDongleInfoController *)dongleInfoController{
    return _dongleInfoController;
}
- (NSDictionary *)dongleInfo{
    return [_dongleInfoController infoDictionary];
}

- (BOOL)deviceShouldUploadSchema:(AMPrototypeDevice *)device{
    return YES;
}
- (void)setDeviceHost:(AMPrototypeDevice *)device{
    if(_deviceHost != device && [self deviceShouldUploadSchema:device]){
        _deviceHost = device;
        self.appInfoController = nil;
        self.dongleInfoController = nil;
        self.webVideoInfoController = nil;
        NSDictionary * parameterDictionary = @{kAMEZCastInfoControllerParameterHost: device};
        
        if([device isKindOfClass:[AMScreenDevice class]])
        {
            self.appInfoController = [[AMSchemaScreenInfoController alloc] initWithDelegate:self parameters:parameterDictionary];
        }
        else if ([device isKindOfClass:[AMDemoDevice class]])
        {
            self.appInfoController = [[AMSchemaDemoInfoController alloc] initWithDelegate:self parameters:parameterDictionary];
        }
        else if ([device isKindOfClass:[AMGenericDevice class]])
        {
            self.appInfoController = [[AMSchemaAppInfoController alloc] initWithDelegate:self parameters:parameterDictionary];
        }
#if TARGET_OS_IPHONE
        else if([NSStringFromClass([device class]) isEqualToString:@"AMChromecastDevice"])
        {
            self.appInfoController = [[AMSchemaChromeCastInfoController alloc] initWithDelegate:self parameters:parameterDictionary];
        }
#endif
        else
        {
            self.appInfoController = [[AMSchemaAppInfoController alloc] initWithDelegate:self parameters:parameterDictionary];
        }
        
        self.webVideoInfoController = [[AMSchemaWebVideoInfoController alloc] initWithDelegate:self parameters:parameterDictionary];
        self.webVideoReportController = [[AMSchemaWebVideoReportInfoController alloc] initWithDelegate:self parameters:parameterDictionary];
        self.dongleInfoController = [[AMSchemaDongleInfoController alloc] initWithDelegate:self parameters:parameterDictionary];
    }
}
+ (AMSchemaPushController *) sharedInstance {
    static dispatch_once_t _singletonPredicate;
    static AMSchemaPushController *_singleton = nil;
    
    dispatch_once(&_singletonPredicate, ^{
        _singleton = [[self alloc] init];
        
    });
    
    return _singleton;
}
- (id) init {
    self = [super init];
    if (self) {
        _host = @"https://cloud.iezvu.com";
    }
    return self;
}
- (void)startUpdateInfos{
    /**
     There's delgate method didInfoControllerFinishUpdate for InfoControllers
     In this case, appInfoController finished update will trigger dongleInfo update
     */
//    DTrace();
    if([self deviceShouldUploadSchema:self.deviceHost]){
        [self.appInfoController startSchemaInfoUpdate];
    }
}
- (void)uploadInfo:(NSDictionary *)info host:(NSString *)host urlPath:(NSString *)urlPath{

//    DLog(@"%s Upload Schema To %@",__PRETTY_FUNCTION__,[[baseURL absoluteString] stringByAppendingString:urlPath]);
//    DLog(@"\nSchema Info:\n%@",[info description]);
//    DLog(@"Decrypted:\n%@",[[NSString alloc] initWithData:[[NSData base64DataFromString:[info objectForKey:@"encrypted_data"]] AESDecryptWithKey:@"28906462822699798631919982357480" settings:kAMAESECB256BitKeySettings] encoding:NSUTF8StringEncoding]);
    
    if(info){
        AFHTTPRequestOperationManager * manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        NSMutableSet * set = [NSMutableSet setWithSet:manager.responseSerializer.acceptableContentTypes];
        [set addObjectsFromArray:@[@"text/plain",@"text/html"]];
        manager.responseSerializer.acceptableContentTypes = set;
        [manager POST:[host stringByAppendingString:urlPath]
           parameters:info
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  NSString * message = @"DONE";
                  if([responseObject objectForKey:@"message"]){
                      message = [responseObject objectForKey:@"message"];
                  }
//                  DLog(@"schema:%@ upload %@", operation.request.URL,message);
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                  DLog(@"schema:%@ upload %@", operation.request.URL, [error localizedDescription]);
        }];
    }
}
#pragma mark - AMEZCastSchemaInfoDelegate
- (void)didInfoControllerFinishUpdate:(AMSchemaBaseInfoController *)infoCotnroller Object:(NSObject *)object{
//    DLog(@"NSStringFromClass([inf1oCotnroller class]):%@", NSStringFromClass([infoCotnroller class]));
    if([infoCotnroller isKindOfClass: [AMSchemaAppInfoController class]]){
        [self uploadInfo:(NSDictionary *)object
                 host:self.host
                 urlPath:[AMFunctionURLs appInfoPathWithDevice:self.deviceHost appinfo:[self appInfo]]];
        if([self.deviceHost isKindOfClass:[AMGenericDevice class]]){
            [self.dongleInfoController startSchemaInfoUpdate];
        }
    } else if([infoCotnroller isKindOfClass: [AMSchemaDongleInfoController class]]){
        [self uploadInfo:(NSDictionary *)object
                 host:self.host
                 urlPath:[AMFunctionURLs dongleInfoPathWithDevice:self.deviceHost appinfo:[self appInfo]]];
    } else if([infoCotnroller isKindOfClass: [AMSchemaWebVideoInfoController class]]){
        [self uploadInfo:(NSDictionary *)object
                 host:self.host
                 urlPath:[AMFunctionURLs webVideoPathWithDevice:self.deviceHost appinfo:[self appInfo]]];
//        [(AMEZCastSchemaWebVideoInfoController *)infoCotnroller setStreamViewVideoInfoDictionary:nil]; //clear videoInfoDictionary or not?
    } else if([infoCotnroller isKindOfClass: [AMSchemaWebVideoReportInfoController class]]){
        [self uploadInfo:(NSDictionary *)object
                 host:self.host
                 urlPath:[AMFunctionURLs webVideoReportPathWithDevice:self.deviceHost appinfo:[self appInfo]]];
    }
}
@end
