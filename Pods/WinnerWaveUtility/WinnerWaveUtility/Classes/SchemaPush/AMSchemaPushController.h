//
//  AMEZCastSchemaPushController.h
//  EZWifiMac
//
//  Created by Splenden on 2014/2/19.
//  Copyright (c) 2014年 Actions-Micro. All rights reserved.
//

#import "AMSchemaAppInfoController.h"
#import "AMSchemaDongleInfoController.h"
#import "AMSchemaWebVideoInfoController.h"
#import "AMSchemaWebVideoReportInfoController.h"

@interface AMSchemaPushController : NSObject<AMEZCastSchemaBaseInfoDelegate>

+ (AMSchemaPushController *) sharedInstance;
- (void)startUpdateInfos;
@property (nonatomic, readonly, strong) AMSchemaAppInfoController *appInfoController;
@property (nonatomic, readonly, copy) NSDictionary *appInfo;
@property (nonatomic, readonly, strong) AMSchemaDongleInfoController *dongleInfoController;
@property (nonatomic, readonly, copy) NSDictionary *dongleInfo;
@property (nonatomic, readonly, strong) AMSchemaWebVideoInfoController *webVideoInfoController;
@property (nonatomic, readonly) AMSchemaWebVideoReportInfoController *webVideoReportController;
- (void)setDeviceHost:(AMPrototypeDevice *)device;
@property (nonatomic, strong, nonnull) NSString *host;
@end
