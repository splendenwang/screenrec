//
//  AMEZCastSchemaAppInfoController.h
//  EZWifiMac
//
//  Created by Splenden on 2014/2/19.
//  Copyright (c) 2014年 Actions-Micro. All rights reserved.
//

#import "AMSchemaBaseInfoController.h"

@interface AMSchemaAppInfoController : AMSchemaBaseInfoController<CLLocationManagerDelegate>
@property (nonatomic, strong)CLLocationManager * locationManager;
- (void)updateiDeviceMacFromDevice:(AMPrototypeDevice *)host completionBlock:(void (^)(NSError *))completionBlock;
- (void)updateiDeviceLocation;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *iDeviceUUID;
@end
