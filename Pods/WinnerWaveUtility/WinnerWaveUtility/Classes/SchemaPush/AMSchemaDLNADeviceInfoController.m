//
//  AMSchemaDLNADeviceInfoController.m
//  AMCommon_iOS
//
//  Created by James Chen on 2/10/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import "AMSchemaDLNADeviceInfoController.h"

@implementation AMSchemaDLNADeviceInfoController
- (id)initWithDelegate:(id<AMEZCastSchemaBaseInfoDelegate>)delegate parameters:(NSDictionary *)parameterDictionary{
    self = [super initWithDelegate:delegate parameters:parameterDictionary schemaID:@"kAMDLNADeviceInfo"];
    if(self){
    }
    
    return self;
}
@end
