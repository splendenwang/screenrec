//
//  AMSchemaScreenInfoController.h
//  AMCommon_iOS
//
//  Created by Splenden on 2014/8/5.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMSchemaSimpleAppInfoController.h"

@interface AMSchemaScreenInfoController : AMSchemaSimpleAppInfoController

@end
