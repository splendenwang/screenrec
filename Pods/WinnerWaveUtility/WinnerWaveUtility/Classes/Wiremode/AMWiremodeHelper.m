//
//  AMWiremodeHelper.m
//  
//
//  Created by Splenden on 2016/1/12.
//
//

#import "AMWiremodeHelper.h"
@import TransmitterKit;
@interface AMWiremodeHelper() <AMDeviceControllerDelegate>
@property (strong) dispatch_source_t connectionTimeoutTimer;
@end
@implementation AMWiremodeHelper

- (BOOL)connectToWireDevice {
    AMDeviceController * deviceController = [AMDeviceController sharedDeviceController];
    __weak AMWiremodeHelper * weakSelf = self;
    for(AMPrototypeDevice *device in [deviceController discoveredDevices]) {
        if([device isEZCastWireDevice]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.connectionBlock(device, deviceController);
            });
            return YES;
        }
    }
    
    deviceController.delegate = self;
    [deviceController stopScan];
    [deviceController startScan];
    double interval = 2.0f;
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    if (timer) {
        dispatch_source_set_timer(timer, dispatch_time(DISPATCH_TIME_NOW, interval * NSEC_PER_SEC), interval * NSEC_PER_SEC, (1ull * NSEC_PER_SEC) / 10);
        dispatch_source_set_event_handler(timer, ^{
            if(weakSelf.connectionTimeoutBlock) {
                weakSelf.connectionTimeoutBlock();
            }
            dispatch_source_cancel(timer);
        });
        dispatch_resume(timer);
    }
    
    if (self.connectionTimeoutTimer) {
        dispatch_source_cancel(self.connectionTimeoutTimer);
        self.connectionTimeoutTimer = nil;
    }
    self.connectionTimeoutTimer = timer;
    
    return YES;
}

#pragma mark - AMDeviceControllerDelegate
- (void)deviceController:(AMDeviceController*)controllder foundNewDevice:(AMPrototypeDevice*)device
{
//    DLog(@"wa device-%@-%@",[(AMGenericDevice *)device projector],[(AMGenericDevice *)device remoteHost]);
    //dispatch after to make sure projector and remote been connected
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //            DLog(@"wb device-%@-%@",[(AMGenericDevice *)device projector],[(AMGenericDevice *)device remoteHost]);
        if([device isEZCastWireDevice]){
            if (self.connectionTimeoutTimer) {
                dispatch_source_cancel(self.connectionTimeoutTimer);
                self.connectionTimeoutTimer = nil;
            }
            if (self.connectionBlock) {
                self.connectionBlock(device, controllder);
                self.connectionBlock = nil;
            }
        }
    });
}
@end
