//
//  AMWiremodeHelper.h
//  
//
//  Created by Splenden on 2016/1/12.
//
//

#import <Foundation/Foundation.h>
@class AMPrototypeDevice;
@class AMDeviceController;
@interface AMWiremodeHelper : NSObject
@property (nonatomic, strong) void (^connectionTimeoutBlock)(void);
@property (nonatomic, strong) void (^connectionBlock)(AMPrototypeDevice *device, AMDeviceController * deviceController);
/**
 *  connect to type="wire" ezcast device, timeout 12 secs
 *
 *  @return return YES if it works
 */
- (BOOL)connectToWireDevice;
@end
