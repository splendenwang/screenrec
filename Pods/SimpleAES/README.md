# SimpleAES

[![CI Status](http://img.shields.io/travis/raxcat liu/SimpleAES.svg?style=flat)](https://travis-ci.org/raxcat liu/SimpleAES)
[![Version](https://img.shields.io/cocoapods/v/SimpleAES.svg?style=flat)](http://cocoapods.org/pods/SimpleAES)
[![License](https://img.shields.io/cocoapods/l/SimpleAES.svg?style=flat)](http://cocoapods.org/pods/SimpleAES)
[![Platform](https://img.shields.io/cocoapods/p/SimpleAES.svg?style=flat)](http://cocoapods.org/pods/SimpleAES)

# Dev Status #
- [x] Basic functions extracted from AMCommon
- [ ] Unit testing
- [ ] Demonstrate App with UI


# Version
- 0.1.4
     - Xcode9 compatibility check
     - Fix Xcode9 header doc compatibility


## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

###
The example in this pod uses a trick to treat cocoapod for local development pod path issue. ( https://github.com/CocoaPods/CocoaPods/issues/4335 )

```
## WORKAROUND for cocoapod local dev issue , COPY FRAMEWORKS to cheat coaoapods## 
## Brian Liu, brianliu@actions-micro.com ##
post_install do |installer|
  require 'fileutils'

  puts "current" + File.dirname(__FILE__)
  _origFrameworkFolder =  File.expand_path('../Frameworks', File.dirname(__FILE__))
  puts "original Frameworks path => " + _origFrameworkFolder 

  _destFrameworkFolder = File.expand_path('Pods/SimpleAES/Frameworks/', File.dirname(__FILE__))
  puts "dest Frameworks path => " +  _destFrameworkFolder

  FileUtils.copy_entry(_origFrameworkFolder, _destFrameworkFolder, :remove_destination => true)

end
```


## Requirements
- 0.1.2
    - Supports XCode8
    - iOS min deployment `iOS8.0`
	- macOS min deployment `macOS 10.9`

## Installation

SimpleAES is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "SimpleAES"
```

## Author

- Splenden Wang, splendenwang@actions-micro.com
- Brian Liu, brianliu@actions-micro.com

## License

Proprietary software license
