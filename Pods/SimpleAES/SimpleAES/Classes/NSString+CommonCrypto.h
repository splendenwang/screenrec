//
//  NSString+CommonCrypto.h
//  AMCommon_iOS
//
//  Created by James Chen on 12/21/12.
//  Copyright (c) 2012 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSData+AESEncrypt.h"
@interface NSString (CommonCrypto)

@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *md5Value;

/**
 *  NSString(self, string to encrypt) -> 1.dataUsingEncoding -> NSData(plain) -> 2.encrypt -> NSData(cipher) -> 3.base64EncodedString -> NSString(return value)
 *  Notice that 1.CCrypto default option(0x00) is CBC 2.ECB options will ignore iv 3.there'a problem that encryptedResult compare to webtool has initial few bytes garbled
 *
 *  @param key      decryption key, should be 16 or 32 bytes
 *  @param iv       initial vector, should be 16 or 8 bytes
 *  @param settings AMCommonCryptoSettings
 *
 *  @return encrypted string
 */
- (NSString *)AESEncryptKey:(NSString *)key iv:(NSString *)iv settings:(AMCommonCryptoSettings)settings;
- (NSString *)AESDecryptKey:(NSString *)key iv:(NSString *)iv settings:(AMCommonCryptoSettings)settings;


/**
 *  NSString(self, base64 encoded string to be decrypt) -> 3.base64DataFromString -> NSData(ciphertext) -> 2.decrypt -> NSData -> 1.stringWithData:encoding: -> NSString(return value)
 *  Notice that 1.CCrypto default option(0x00) is CBC 2.ECB options will ignore iv 3.there'a problem that decryptedResult compare to webtool has initial few bytes garbled
 *
 *  @param key      decryption key, should be 16 or 32 bytes
 *  @param settings AMCommonCryptoSettings
 *
 *  @return decrypted string
 */
- (NSString *)AESEncryptKey:(NSString *)key settings:(AMCommonCryptoSettings)settings;
- (NSString *)AESDecryptKey:(NSString *)key settings:(AMCommonCryptoSettings)settings;
@end
