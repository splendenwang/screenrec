//
//  NSString+CommonCrypto.m
//  AMCommon_iOS
//
//  Created by James Chen on 12/21/12.
//  Copyright (c) 2012 ActionsMicro. All rights reserved.
//

#import "NSString+CommonCrypto.h"
@import CommonCrypto;

@implementation NSString (CommonCrypto)
- (NSString *) md5Value
{
    const char *cStr = [self UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    return  output;    
}

- (NSString *)AESEncryptKey:(NSString *)key iv:(NSString *)iv settings:(AMCommonCryptoSettings)settings {
	NSData *plain = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSData *cipher = [plain AESEncryptWithKey:key iv:iv settings:settings];
    NSString * result = [cipher base64EncodedStringWithOptions:0];
    return result;
}
- (NSString *)AESEncryptKey:(NSString *)key settings:(AMCommonCryptoSettings)settings {
    return [self AESEncryptKey:key iv:nil settings:settings];
}

- (NSString *)AESDecryptKey:(NSString *)key iv:(NSString *)iv settings:(AMCommonCryptoSettings)settings {
    NSData *ciphertext = [NSData base64DataFromString:self];
    NSString * result = [[NSString alloc] initWithData:[ciphertext AESDecryptWithKey:key iv:iv settings:settings] encoding:NSUTF8StringEncoding];
    if(settings.options && 0x0001)//PKCS7Padd
        return result;
    else
        return [result stringByTrimmingCharactersInSet:[NSCharacterSet controlCharacterSet]];
}
- (NSString *)AESDecryptKey:(NSString *)key settings:(AMCommonCryptoSettings)settings {
    return [self AESDecryptKey:key iv:nil settings:settings];
}
@end
