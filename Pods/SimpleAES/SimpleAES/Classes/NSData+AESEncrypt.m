//
//  NSData+AES256Encrypt.m
//  AMCommon_iOS
//
//  Created by Splenden on 2014/3/28.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "NSData+AESEncrypt.h"
@import CommonCrypto;

const AMCommonCryptoSettings kAMAESECB128BitKeySettings = {
    .algorithm = kCCAlgorithmAES128,
    .blockSize = kCCBlockSizeAES128,
    .IVSize = kCCBlockSizeAES128,
    .options = kCCOptionPKCS7Padding|kCCOptionECBMode,
    .keySize = kCCKeySizeAES128
};
const AMCommonCryptoSettings kAMAESECB256BitKeySettings = {
    .algorithm = kCCAlgorithmAES128,
    .blockSize = kCCBlockSizeAES128,
    .IVSize = kCCBlockSizeAES128,
    .options = kCCOptionPKCS7Padding|kCCOptionECBMode,
    .keySize = kCCKeySizeAES256
};
const AMCommonCryptoSettings kAMAESCBC128BitKeySettings = {
    .algorithm = kCCAlgorithmAES128,
    .blockSize = kCCBlockSizeAES128,
    .IVSize = kCCBlockSizeAES128,
    .options = kCCOptionPKCS7Padding,
    .keySize = kCCKeySizeAES128
};
const AMCommonCryptoSettings kAMAESCBC256BitKeySettings = {
    .algorithm = kCCAlgorithmAES128,
    .blockSize = kCCBlockSizeAES128,
    .IVSize = kCCBlockSizeAES128,
    .options = kCCOptionPKCS7Padding,
    .keySize = kCCKeySizeAES256
};
@implementation NSData (AESEncrypt)
- (NSData *)AESEncryptWithKey:(NSString *)key iv:(NSString *)iv settings:(AMCommonCryptoSettings)settings {
    // 'key' should be 32 bytes for AES256, will be null-padded otherwise
    char keyPtr[settings.keySize+1]; // room for terminator (unused)
    bzero(keyPtr, sizeof(keyPtr)); // fill with zeroes (for padding)
    
    // fetch key data
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    NSMutableData * data = [self mutableCopy];
    
    //no-padding implement, fill with zeros to fit blockSize
    if((settings.options && kCCOptionPKCS7Padding) == NO) {
        unsigned long bytesToPadding = settings.blockSize - data.length % settings.blockSize;
        [data increaseLengthBy:bytesToPadding];
    }
    NSUInteger dataLength = [data length];
    
    //See the doc: For block ciphers, the output size will always be less than or
    //equal to the input size plus the size of one block.
    //That's why we need to add the size of one block here
    size_t bufferSize = dataLength + settings.blockSize;
    void *buffer = malloc(bufferSize);
    
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus;
    if (iv.length > 0) {
        char ivPtr[settings.IVSize + 1];
        [iv getCString:ivPtr maxLength:sizeof(ivPtr) encoding:NSUTF8StringEncoding];
        cryptStatus = CCCrypt(kCCEncrypt, settings.algorithm, settings.options,
                              keyPtr, settings.keySize,
                              ivPtr /* initialization vector (optional) */,
                              [data bytes], dataLength,                     /* input */
                              buffer, bufferSize,                     /* output */
                              &numBytesEncrypted);
    } else {
        cryptStatus = CCCrypt(kCCEncrypt, settings.algorithm, settings.options,
                              keyPtr, settings.keySize,
                              NULL /* initialization vector (optional) */,
                              [data bytes], dataLength,                         /* input */
                              buffer, bufferSize,                         /* output */
                              &numBytesEncrypted);
    }
    if (cryptStatus == kCCSuccess) {
        //the returned NSData takes ownership of the buffer and will free it on deallocation
        return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
    }
    
    free(buffer); //free the buffer;
    return nil;
    
}
- (NSData *)AESDecryptWithKey:(NSString *)key iv:(NSString *)iv settings:(AMCommonCryptoSettings)settings {
    
    // 'key' should be 32 bytes for AES256, will be null-padded otherwise
    char keyPtr[settings.keySize+1]; // room for terminator (unused)
    bzero(keyPtr, sizeof(keyPtr)); // fill with zeroes (for padding)
    
    // fetch key data
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    
    NSUInteger dataLength = [self length];
    
    //See the doc: For block ciphers, the output size will always be less than or
    //equal to the input size plus the size of one block.
    //That's why we need to add the size of one block here
    size_t bufferSize = dataLength + settings.blockSize;
    void *buffer = malloc(bufferSize);
    
    size_t numBytesDecrypted = 0;
    CCCryptorStatus cryptStatus;
    if (iv.length > 0) {
        char ivPtr[settings.IVSize + 1];
        [iv getCString:ivPtr maxLength:sizeof(ivPtr) encoding:NSUTF8StringEncoding];
        cryptStatus = CCCrypt(kCCDecrypt, settings.algorithm, settings.options,
                              keyPtr, settings.keySize,
                              ivPtr /* initialization vector (optional) */,
                              [self bytes], dataLength, /* input */
                              buffer, bufferSize, /* output */
                              &numBytesDecrypted);
    } else {
        /**
         *  if no iv, NULL
         *  BBAES:https://github.com/benoitsan/BBAES will extract IVsize bytes from head to be IV
         */
        //        NSData * newIV;
        //        NSData * newEncryptedData;
        //        if (!iv) {
        //            const NSUInteger ivLength = settings.IVSize;
        //            newIV = [self subdataWithRange:NSMakeRange(0,ivLength)];
        //            newEncryptedData = [self subdataWithRange:NSMakeRange(ivLength,[self length]-ivLength)];
        //        }
        cryptStatus = CCCrypt(kCCDecrypt, settings.algorithm, settings.options,
                              keyPtr, settings.keySize,
                              NULL /* initialization vector (optional) */,
                              [self bytes], dataLength, /* input */
                              buffer, bufferSize, /* output */
                              &numBytesDecrypted);
    }
    
    if (cryptStatus == kCCSuccess) {
        //the returned NSData takes ownership of the buffer and will free it on deallocation
        return [NSData dataWithBytesNoCopy:buffer length:numBytesDecrypted];
    }
    
    free(buffer); //free the buffer;
    return nil;
}
- (NSData *)AESEncryptWithKey:(NSString *)key settings:(AMCommonCryptoSettings)settings {
    return [self AESEncryptWithKey:key iv:nil settings:settings];
}
- (NSData *)AESDecryptWithKey:(NSString *)key settings:(AMCommonCryptoSettings)settings {
    return [self AESDecryptWithKey:key iv:nil settings:settings];
}

+ (NSData *)base64DataFromString: (NSString *)string{
    if([NSData respondsToSelector:@selector(initWithBase64EncodedString:options:)]) {
        return [[NSData alloc] initWithBase64EncodedString:string options:0];
    }
    
    unsigned long ixtext, lentext;
    unsigned char ch, inbuf[4], outbuf[3];
    short i, ixinbuf;
    Boolean flignore, flendtext = false;
    const unsigned char *tempcstring;
    NSMutableData *theData;
    
    if (string == nil)
    {
        return [NSData data];
    }
    
    ixtext = 0;
    
    tempcstring = (const unsigned char *)[string UTF8String];
    
    lentext = [string length];
    
    theData = [NSMutableData dataWithCapacity: lentext];
    
    ixinbuf = 0;
    
    while (true)
    {
        if (ixtext >= lentext)
        {
            break;
        }
        
        ch = tempcstring [ixtext++];
        
        flignore = false;
        
        if ((ch >= 'A') && (ch <= 'Z'))
        {
            ch = ch - 'A';
        }
        else if ((ch >= 'a') && (ch <= 'z'))
        {
            ch = ch - 'a' + 26;
        }
        else if ((ch >= '0') && (ch <= '9'))
        {
            ch = ch - '0' + 52;
        }
        else if (ch == '+')
        {
            ch = 62;
        }
        else if (ch == '=')
        {
            flendtext = true;
        }
        else if (ch == '/')
        {
            ch = 63;
        }
        else
        {
            flignore = true;
        }
        
        if (!flignore)
        {
            short ctcharsinbuf = 3;
            Boolean flbreak = false;
            
            if (flendtext)
            {
                if (ixinbuf == 0)
                {
                    break;
                }
                
                if ((ixinbuf == 1) || (ixinbuf == 2))
                {
                    ctcharsinbuf = 1;
                }
                else
                {
                    ctcharsinbuf = 2;
                }
                
                ixinbuf = 3;
                
                flbreak = true;
            }
            
            inbuf [ixinbuf++] = ch;
            
            if (ixinbuf == 4)
            {
                ixinbuf = 0;
                
                outbuf[0] = (inbuf[0] << 2) | ((inbuf[1] & 0x30) >> 4);
                outbuf[1] = ((inbuf[1] & 0x0F) << 4) | ((inbuf[2] & 0x3C) >> 2);
                outbuf[2] = ((inbuf[2] & 0x03) << 6) | (inbuf[3] & 0x3F);
                
                for (i = 0; i < ctcharsinbuf; i++)
                {
                    [theData appendBytes: &outbuf[i] length: 1];
                }
            }
            
            if (flbreak)
            {
                break;
            }
        }
    }
    
    return theData;
}

@end
