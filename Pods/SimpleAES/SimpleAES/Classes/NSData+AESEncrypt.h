//
//  NSData+AES256Encrypt.h
//  AMCommon_iOS
//
//  Created by Splenden on 2014/3/28.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface NSData (AESEncrypt)

typedef struct _AMCommonCryptoSettings
{
    /* CCAlgorithm */ uint32_t algorithm;
    /* kCCBlockSize */ size_t blockSize;
    /* kCCBlockSize */ size_t IVSize;
    /* kCCKeySize */ size_t keySize;
    /* CCOptions */ uint32_t options;
} AMCommonCryptoSettings;
extern const AMCommonCryptoSettings kAMAESECB128BitKeySettings;
extern const AMCommonCryptoSettings kAMAESECB256BitKeySettings;
extern const AMCommonCryptoSettings kAMAESCBC128BitKeySettings;
extern const AMCommonCryptoSettings kAMAESCBC256BitKeySettings;
- (NSData *)AESEncryptWithKey:(NSString *)key settings:(AMCommonCryptoSettings)settings;
- (NSData *)AESDecryptWithKey:(NSString *)key settings:(AMCommonCryptoSettings)settings;
- (NSData *)AESEncryptWithKey:(NSString *)key iv:(NSString *)iv settings:(AMCommonCryptoSettings)settings;
- (NSData *)AESDecryptWithKey:(NSString *)key iv:(NSString *)iv settings:(AMCommonCryptoSettings)settings;
+ (NSData *)base64DataFromString: (NSString *)string;

@end
