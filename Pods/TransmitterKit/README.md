# TransmitterKit

## Example
To run the example project, clone the repo, and run `pod install` from the Example directory first. 

## Versions

- 0.2.12
    - AMGenericDevice now creates a cache for max resolution

- 0.2.11
    - Remove EZCast bitmask definition from this pod
    - Disopen EZCastApplicationFeatures.h/m in this pod (They are now private)

- 0.2.9
    - Fixed OTA crashs
    - New OTA API signature

- 0.2.7
    - Fixed
        - Parameter `autoStartWifiDisplay`  of function `requestFullscreen:complete` works from now
    - New property `isFullscreen` available on DataTransmitter's split controller protocol

- 0.2.6
     - Deprecated APIs/Forbidden APIs
          - Private API `createNewTramsmitterForDevice:`  removed
          - `shouldDisplayModalDeviceController` is removed from AMDeviceControllerDelegate
          - `wifiDisplayConnectionCallback` is marked as deprecated
          - Some notifications defined in AMDeviceController are all removed
               - `kAMDeviceControllerDevicesChangedNotification`
               - `kAMDeviceControllerDevicesChangedDevice`
               - `kAMDeviceControllerDevicesChangedType`
               - `kAMDeviceControllerDevicesChangedTypeDisvoceried`
               - `kAMDeviceControllerDevicesChangedTypeDisappeared`
          - AMDataTransmitter_ConferenceControl old function `requestFullscreen` is marded as deprecated.
     - New APIs
          - AMDataTransmitter_ConferenceControl  function `requestFullscreen:completion`
     - Bugs fixed
          - Thread-Safe issue of `AMRemoteHost`


- 0.2.5
      - Fix thread-safe issue of `AMDPFImageSender`(wifi display class).

- 0.2.4
      - Fix: `169.254.255.255` causes iOS devices to fail on discovery.

- 0.2.3
      - Rename `ezmeeting` to `ezkeep`

- 0.2.2
     - Remove `customName` in AMGenericDevice description
     - Fix the long-time error of spelling  `discoverdDevice`

- 0.2.1
     - `NS_STRING_ENUM` is introduced in EZCastApplicationFeature.h for future swift compatibility
     - New bitmask service
          - `EZNote`
          - `EZMeeting`

- 0.2.0
     - `BitCode` is supported from this version
     - Fix warnings about: Designated Initializer, Unused vars
     - Xcode9 tested
     
---
     
- 0.1.12
     - Fix a crash issue if "OfflineModeCustomizationSetting.plist" does not exist in app bundle. (Do not throw exception anymore)
     - Adopt 'BytesMeter'

- 0.1.11
     - Remove -(UIView *)localPlayerView of AMDemoDataTransmitter 
     - Change offline behavior for AMDemoDevice. (Offline device now keeps previous connection's bitmask dictionary)

- 0.1.10
     - Class AMRemoteHost [isEqual:] does not compare 'parameters' dictionary anymore
     - AMDeviceController [connect:] will not only accept ony connection attempt from now
     - The property 'currentRole' now has a default vale 'NotAssigned'
         - Macro 'ROLE_TO_STRING' is also updated

- 0.1.9
     - Add bitmask: kRemoteServiceMirrorDefaultAudioOff = @"mirror_default_audio_off"

- 0.1.8
     - Improve class IPAdressProjectorScanner. Make it subnet-crossable.
     - Update EZCastSitesManager for Objective-C syntax which supports Swift3.
     - Update AudioDecoder/AuioPlayer for mp3(iTunes Generic File) file. [syzuchen]

- 0.1.7
     - Add readyToSendPCMData on AMDataTransmitter_SocketStatus 

- 0.1.6
     - Remove bitmask
         - kRemtoeServiceFrameRate(framerate)
         - kRemoteServiceAudioSwitch(audio_switch)
         - kRemoteServiceMoreApps(apps)
         - kRemoteServiceShop(shopping)
     - Add bitmask
         - kRemoteServiceMirrorDisableAudio(mirror_disable_audio)
     - Add new AMDataTransmitter_SocketStatus interface for transmitters. Call `readyToSendH264` for send status.

- 0.1.5 hotfix
     - Fix screen transmitter handshaking error

- 0.1.3
     - Enable playAudio and playVideo on pause state

## Requirements
* Supports XCode8
* iOS min deployment `iOS8.0`
* macOS min deployment `macOS 10.10`
#### Bitcode
* Before 0.2.0, `BitCode` is **not** supported now. Because of:
    * Flac.framework
    * Ogg.framework
    * Vorbis.framework
    * IDZAQAudioPlayer
* After 0.2.0(including), `BitCode` is supported.

## Installation
TransmitterKit is available through [WinnerWave CocoaPods](https://bitbucket.org/actions-micro/winnerwavepodspecs). To install
it, simply add the following line to your Podfile:

```ruby
source 'git@bitbucket.org:actions-micro/winnerwavepodspecs.git' #add this line at top of your Podfile
pod "TransmitterKit"
```

## Author (Alphabet order)
Brian Liu, brianliu@actions-micro.com
James Chen, jameschen@actions-micro.com
Splenden Wang, splendenwang@actions-mico.com
SzYu Cheng, szyuchen@actions-micro.com


## Future works
-[ ] Unit Testing of old codes
-[ ] Sample codes 

## License
TransmitterKit is available under the MIT license. See the LICENSE file for more info.
