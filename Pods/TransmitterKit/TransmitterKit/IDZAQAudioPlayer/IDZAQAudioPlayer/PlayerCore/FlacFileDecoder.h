//
//  FlacFileDecoder.h
//  IDZAQAudioPlayer
//
//  Created by brianliu on 2016/10/3.
//  Copyright © 2016 WinnerWave ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IDZAudioDecoder.h"
/**
 * @brief An Flac file decompressor conforming to IDZAudioDecoder.
 */
@interface FlacFileDecoder : NSObject <IDZAudioDecoder>
@end
