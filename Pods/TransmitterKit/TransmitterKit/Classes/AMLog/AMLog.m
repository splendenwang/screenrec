//
//  AMLog.m
//
//
//  Created by Splenden on 2016/1/21.
//
//

#import "AMLog.h"
@interface AMLog()
@end
@implementation AMLog
static NSMutableArray * loggers;
+ (void)initialize {
    NSLog(@"AMLog initializing");
    loggers = [NSMutableArray new];
}
+ (void)addLogger:(id<AMLoggerProtocol>)logger {
    NSLog(@"AMLog addlogger:%@",logger);
    if (logger == nil) return;
    [loggers addObject:logger];
}
+ (void)log:(NSString *)message {
    for (id<AMLoggerProtocol> logger in loggers) {
        [logger log:message];
    }
}
@end

@implementation AMLogConsoleLogger
- (void)log:(NSString *)message {
    NSLog(@"%@",message);
}
@end