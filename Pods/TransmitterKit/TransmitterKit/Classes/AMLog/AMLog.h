//
//  AMLog.h
//
//
//  Created by Splenden on 2016/1/21.
//
//

#import <Foundation/Foundation.h>
@protocol AMLoggerProtocol;
@interface AMLog : NSObject
+ (void)addLogger:(id<AMLoggerProtocol>)logger;
+ (void)log:(NSString *)message;
@end
@protocol AMLoggerProtocol <NSObject>
- (void)log:(NSString *)message;
@end
@interface AMLogConsoleLogger : NSObject <AMLoggerProtocol>
@end