//
//  AMMJpegHttpServer.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/14.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMMJpegHttpServer.h"
@import CocoaAsyncSocket;
@import WinnerWave_CocoaHTTPServer;
#import "HTTPMessage+LOG.h"

#define HTTP_WRITE_FIRST_RESPONSE   1000
#define HTTP_WRITE_MJPEG_DATA       1001
#define MAX_HEADER_LINE_LENGTH  8190
#define HTTP_REQUEST_HEADER                10

NSString * const kBoundary = @"ezcastmjpegstreamer";
const NSTimeInterval TIMEOUT = 10.0;

@interface AMMJpegHttpServer () <GCDAsyncSocketDelegate>
{
    GCDAsyncSocket * _socket;
    GCDAsyncSocket * _clientSocket;
    
    void(^_completionBlock)(NSError*);
    
    void(^_disconnectBlock)(NSError*);
    
    HTTPMessage * _request;
    
    NSData * _dataToSend;
}
@end


@implementation AMMJpegHttpServer
-(uint16_t) port{
    return [_socket localPort];
}

-(void) startOnPort:(uint16_t)port completionBlock:(void(^)(NSError * error))completionBlock disconnect:(void(^)(NSError * error))disconnectBlock{
    self.isReadyToSend = YES;
    _completionBlock = completionBlock;
    _disconnectBlock = disconnectBlock;
    
    if(_socket==nil){
        _socket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
        _socket.delegate = self;
    }
    NSError * error = nil;
    [_socket acceptOnPort:port error:&error];

    if (_completionBlock) {
        _completionBlock(error);
        _completionBlock = nil;
    }
}

-(void) stop{
    [_clientSocket disconnect];
    [_socket disconnect];
    _socket = nil;
}

-(void) sendJpgData:(NSData*)jpegdData{
    if(_clientSocket){
        NSMutableData * sendData = [NSMutableData new];
        [sendData appendData:[[NSString stringWithFormat:@"--%@\r\n", kBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [sendData appendData:[@"Content-type:image/jpeg\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [sendData appendData:[[NSString stringWithFormat:@"Content-Length:%zd\r\n\r\n", (NSUInteger)jpegdData.length] dataUsingEncoding:NSUTF8StringEncoding]];
        [sendData appendData:jpegdData];
        [sendData appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [sendData appendData:[[NSString stringWithFormat:@"--%@\r\n", kBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [sendData appendData:[@"Content-type:image/jpeg\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [sendData appendData:[[NSString stringWithFormat:@"Content-Length:%zd\r\n\r\n", (NSUInteger)jpegdData.length] dataUsingEncoding:NSUTF8StringEncoding]];
        [sendData appendData:jpegdData];
        [sendData appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        self.isReadyToSend = NO;
        [_clientSocket writeData:sendData withTimeout:TIMEOUT tag:HTTP_WRITE_MJPEG_DATA];
    }else{
        _dataToSend = jpegdData;
    }
}

#pragma mark - GCDAsyncSocketDelegate

- (void)socket:(GCDAsyncSocket *)sock didAcceptNewSocket:(GCDAsyncSocket *)newSocket{
    DTrace();
    
    //refuse to accept if we already got a connection
    if(_clientSocket){
        [_clientSocket disconnect];
        _clientSocket = nil;
    }
    
    
    _clientSocket = newSocket;
    
    NSMutableString * bufferString = [NSMutableString new];
    [bufferString appendFormat:@"HTTP/1.0 200 OK\r\n"];
    //    [bufferString appendFormat:@"Server: EZCastStreamer\r\n"];
    //    [bufferString appendFormat:@"Connection: close\r\n"];
    //    [bufferString appendFormat:@"Max-Age: 0\r\n"];
    //    [bufferString appendFormat:@"Expires: 0\r\n"];
    //    [bufferString appendFormat:@"Cache-Control: no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0\r\n"];
    //    [bufferString appendFormat:@"Pragma: no-cache\r\n"];
    [bufferString appendFormat:@"Content-Type:multipart/x-mixed-replace;boundary=--%@\r\n\r\n", kBoundary];
    
    [_clientSocket writeData:[bufferString dataUsingEncoding:NSUTF8StringEncoding] withTimeout:TIMEOUT tag:HTTP_REQUEST_HEADER];
    
    
    if(_dataToSend){
        [self sendJpgData:_dataToSend];
        _dataToSend = nil;
    }
    
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag{
    DLog(@"%s, tag:%ld", __PRETTY_FUNCTION__, tag);
    
    if(_request==nil){
        _request = [[HTTPMessage alloc] initEmptyRequest];
    }
    [_request appendData:data];
    if(_request.isHeaderComplete){
        NSString * request = [[NSString alloc] initWithData:_request.messageData encoding:NSUTF8StringEncoding];
        DLog(@"request:%@", request);
        _request = nil;
        
    }
    
    [_clientSocket readDataToData:[GCDAsyncSocket CRLFData]
                      withTimeout:TIMEOUT
                        maxLength:MAX_HEADER_LINE_LENGTH
                              tag:HTTP_REQUEST_HEADER];
}
//
//- (void)socket:(GCDAsyncSocket *)sock didReadPartialDataOfLength:(NSUInteger)partialLength tag:(long)tag{
//    DLog(@"%s, tag:%ld", __PRETTY_FUNCTION__, tag);
//
//}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag{
    //    DLog(@"%s, tag:%ld", __PRETTY_FUNCTION__, tag);
    if(tag == HTTP_WRITE_MJPEG_DATA){
        self.isReadyToSend = YES;
    }
}

//- (void)socket:(GCDAsyncSocket *)sock didWritePartialDataOfLength:(NSUInteger)partialLength tag:(long)tag{
////    DLog(@"%s, tag:%ld", __PRETTY_FUNCTION__, tag);
//            self.isReadyToSend = YES;
//}
//- (NSTimeInterval)socket:(GCDAsyncSocket *)sock shouldTimeoutReadWithTag:(long)tag
//                 elapsed:(NSTimeInterval)elapsed
//               bytesDone:(NSUInteger)length{
//    DLog(@"%s, tag:%ld", __PRETTY_FUNCTION__, tag);
//    return 0;
//}
//- (NSTimeInterval)socket:(GCDAsyncSocket *)sock shouldTimeoutWriteWithTag:(long)tag
//                 elapsed:(NSTimeInterval)elapsed
//               bytesDone:(NSUInteger)length{
//    DLog(@"%s, tag:%ld", __PRETTY_FUNCTION__, tag);
//    return 0;
//}
//- (void)socketDidCloseReadStream:(GCDAsyncSocket *)sock{
//    DTrace();
//}.
- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err{
    DLog(@"%s, %@", __PRETTY_FUNCTION__, [err localizedDescription]);
    self.isReadyToSend = YES;
    if(sock == _clientSocket){
        DLog(@"mjpg client disconnected");
        [_clientSocket disconnect];
        _clientSocket =nil;
        
        if(_disconnectBlock){
            _disconnectBlock(err);
            _disconnectBlock = nil;
        }
    }
}
-(void) dealloc{
    DTrace();
    [_clientSocket disconnect];
    [_socket disconnect];
}
@end
