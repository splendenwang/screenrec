//
//  AMMJpegHttpServer.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/14.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//
// Motion JPEG over single HTTP connection


#import <Foundation/Foundation.h>

@interface AMMJpegHttpServer : NSObject
@property (atomic) BOOL isReadyToSend;
-(void) startOnPort:(uint16_t)port completionBlock:(void(^)(NSError * error))completionBlock disconnect:(void(^)(NSError * error))disconnectBlock;
-(void) stop;
-(void) sendJpgData:(NSData*)jpegdData;
@property (NS_NONATOMIC_IOSONLY, readonly) uint16_t port;
@end
