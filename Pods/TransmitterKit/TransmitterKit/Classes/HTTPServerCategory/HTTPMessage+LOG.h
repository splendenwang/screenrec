//
//  HTTPMessage+LOG.h
//  AMCommon_iOS
//
//  Created by brianliu on 13/10/1.
//  Copyright (c) 2013年 ActionsMicro. All rights reserved.
//

@import WinnerWave_CocoaHTTPServer;

@interface HTTPMessage (LOG)
-(NSString*) messageString;
-(NSString*) bodyString;
-(NSString*) JSONString;
@end
