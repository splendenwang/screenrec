//
//  HTTPServer+StartCompletionBlock.m
//  AMCommon_iOS
//
//  Created by Splenden on 2014/7/30.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "HTTPServer+StartCompletionBlock.h"

@implementation HTTPServer (StartCompletionBlock)
-(void) startWithConnectionClass:(Class)customConnectionClass success:(void (^)(void))success failure:(void (^)(NSError *))failure{
    NSError *error = nil;
    if([self start:&error])
    {
        //        DLog(@"Started HTTP Server on port %hu", [self listeningPort]);
        
        // We're going to extend the base HTTPConnection class with our MyHTTPConnection class.
        // This allows us to do all kinds of customizations.
        if(customConnectionClass){
            [self setConnectionClass:customConnectionClass];
        }
        
        // Serve files from our embedded Web folder
        NSString *webPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"web"];
        //        DLog(@"Setting document root: %@", webPath);
        
        [self setDocumentRoot:webPath];
        
        if(success){
            success();
        }
    }
    else
    {
        DLog(@"Error starting HTTP Server: %@", error);
        if(failure){
            failure(error);
        }
    }
}
@end
