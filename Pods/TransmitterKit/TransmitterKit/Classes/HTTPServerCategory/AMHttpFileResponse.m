//
//  AMMediaHttpFileResponse.m
//  AMCommon_iOS
//
//  Created by brianliu on 2/5/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import "AMHttpFileResponse.h"
#if TARGET_OS_IPHONE
@import MobileCoreServices;
#else
@import CoreServices;
#endif
@implementation AMHttpFileResponse
//-(instancetype) initWithFilePath:(NSString *)filePath forConnection:(HTTPConnection *)connection{
//    self = [super initWithFilePath:filePath forConnection:connection];
//    if(self){
//        
//    }
//    return self;
//}

/**
 * If you want to add any extra HTTP headers to the response,
 * simply return them in a dictionary in this method.
 **/
- (NSDictionary *)httpHeaders{
    NSString * mimeType = [[self class] mimeFromPathExtension:self.filePath.pathExtension];
    return @{@"Content-Type":mimeType};
}
/**
 *  mimeFromPathExtension, there's a same method at AMStringUtility, but this is modified due to some unsolved issuse by dongle
 *
 *  i.e., ra,rm -> audio/rm, video/rm -> app/stream
 *
 */
+(NSString *)mimeFromPathExtension:(NSString*) extension;
{
    //http://stackoverflow.com/questions/9801106/how-can-i-get-mime-type-in-ios-which-is-not-based-on-extension
    
    // Get the UTI from the file's extension:
    
    CFStringRef pathExtension = (__bridge_retained CFStringRef)extension;
    CFStringRef type = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension, NULL);
    CFRelease(pathExtension);
    
    // The UTI can be converted to a mime type:
    
    NSString *mimeType = (__bridge_transfer NSString *)UTTypeCopyPreferredTagWithClass(type, kUTTagClassMIMEType);
    
    //exctpions, stupid apple
    /**
     @https://developer.apple.com/library/mac/documentation/Miscellaneous/Reference/UTIRef/Articles/System-DeclaredUniformTypeIdentifiers.html
     public.audio, com.microsoft.advanced-​systems-format
     .wma, video/x-ms-wma
     Windows media audio.
     
     @see:https://support.microsoft.com/en-us/kb/288102
     .wma	audio/x-ms-wma
     */
    if([mimeType isEqualToString:@"video/x-ms-wma"]){
        mimeType = @"audio/x-ms-wma";
    }
    
    /**
     *  為小機作的Workaround，雖然是對的Mimetype但是小機需要audio/video開頭的prefix，Application的prefix不被接受；例外是application/octet-stream
     */
    if([mimeType isEqualToString:@"application/vnd.rn-realmedia"]){
        mimeType = @"video/vnd.rn-realmedia"; //rm一律當做video 2015/1/21 Henry's request @ AMGenericDevice,supportedAudioFormat
//        mimeType = @"application/octet-stream";
    }
    if([mimeType isEqualToString:@"application/vnd.rn-realmedia-vbr"]){
        mimeType = @"video/vnd.rn-realmedia-vbr";
    }
    if([mimeType isEqualToString:@"video/x-ms-asf"]){
        mimeType = @"application/octet-stream";
    }

    /**
     *  Apple 本身不提供UTI/MIME的檔案 i.e., mkv,ts
     */
    if (mimeType == nil) {
        mimeType = @"application/octet-stream";
    }
    if (type != NULL)
        CFRelease(type);
    return mimeType;
}
@end
