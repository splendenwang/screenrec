//
//  AMMediaHttpFileResponse.h
//  AMCommon_iOS
//
//  Created by brianliu on 2/5/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

@import WinnerWave_CocoaHTTPServer;

/**
 This class add extra "Content-Type" to response header.
 **/
@interface AMHttpFileResponse : HTTPAsyncFileResponse

@end
