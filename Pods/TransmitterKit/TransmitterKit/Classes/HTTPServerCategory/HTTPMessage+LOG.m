//
//  HTTPMessage+LOG.m
//  AMCommon_iOS
//
//  Created by brianliu on 13/10/1.
//  Copyright (c) 2013年 ActionsMicro. All rights reserved.
//

#import "HTTPMessage+LOG.h"

@implementation HTTPMessage (LOG)
-(NSString*) messageString{
    NSString * string = [[NSString alloc] initWithData:self.messageData encoding:NSUTF8StringEncoding];
    return string;
}
-(NSString*) bodyString{
    NSString * string = [[NSString alloc] initWithData:self.body encoding:NSUTF8StringEncoding];
    return string;
}
-(NSString*) JSONString{
    return [self bodyString];
}
@end
