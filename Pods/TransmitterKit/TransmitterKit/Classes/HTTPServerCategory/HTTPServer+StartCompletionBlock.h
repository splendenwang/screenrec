//
//  HTTPServer+StartCompletionBlock.h
//  AMCommon_iOS
//
//  Created by Splenden on 2014/7/30.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

@import WinnerWave_CocoaHTTPServer;

@interface HTTPServer (StartCompletionBlock)
-(void) startWithConnectionClass:(Class)customConnectionClass
                         success:(void(^)(void))success
                         failure:(void(^)(NSError * error))failure;
@end
