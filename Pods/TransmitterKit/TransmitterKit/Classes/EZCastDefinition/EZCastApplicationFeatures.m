@import Foundation;

NSString * const kRemoteAllow = @"allow";
NSString * const kRemoteAllowFullscreen = @"fullscreen";
NSString * const kRemoteDeny = @"deny";
NSString * const kRemoteWaitForAnswer = @"wait";

NSString * const kRemoteHeartbeat = @"heartbeat";

//2016/8/3
//HID related events

/** 2016/8/2
 
 {
	"id":	12,
	"jsonrpc":	"2.0",
	"method":	"common.enable_control",
	"params":
	{
 "version":	1,
 "devlist":  		//modify
 {
 "lightpen“	:false,
 "keyboard“	:false,
 "mouse“	:true,
 "point“	:false	// Obsoleted at fw side
 }
	}
 }
 */
NSString * const kRemoteHIDEnableControl = @"common.enable_control";
NSString * const kRemoteHIDDisableControl = @"common.disable_control";
NSString * const kRemoteHIDDetect = @"common.send_hid_detect";
NSString * const kRemoteHIDSendMouse = @"common.send_mouse";
NSString * const kRemoteHIDSendControl = @"common.send_control";
NSString * const kRemoteQueryAppHTTPProxyInfo = @"common.QueryAppHTTPProxyInfo";


NSString * const kRemoteGetDeviceDescription = @"common.get_device_description";
NSString * const kRemoteSetDeviceDescription = @"common.set_device_description";
NSString * const kRemoteGetDeviceOTADescription = @"common.get_ota_config";
NSString * const kRemoteSetDeviceOTAReady = @"common.set_ota_ready";
NSString * const kRemoteAssignRole = @"common.assign_role";
NSString * const kRemoteDisconnect = @"common.disconnect";
NSString * const kRemoteJrpcParseError = @"error.parse_jrpc";
NSString * const kRemoteWaitRequestStream = @"common.waiting_request_stream";

NSString * const kRemoteAskRequestStreamFullscreen = @"common.ask_request_stream_fullscreen";
NSString * const kRemoteRequestStreamFullscreen = @"common.request_stream_fullscreen";
NSString * const kRemoteAnswerRequestStreamFullscreen = @"common.answer_request_stream_fullscreen";

NSString * const kRemoteAskRequestStream = @"common.ask_request_stream";
NSString * const kRemoteAnswerRequestStream = @"common.answer_request_stream";
NSString * const kRemoteCancelRequestStream = @"common.cancel_request_stream";
NSString * const kRemoteRequestStream = @"common.request_stream";

NSString * const kRemoteStopStream = @"common.stop_stream";
NSString * const kRemoteSetHostname = @"common.set_hostname";
NSString * const kRemoteHostname = @"hostname";
NSString * const kRemoteIPAddress = @"ip_address";
NSString * const kRemoteChangePosition = @"common.change_position";
NSString * const kRemoteSplitCount = @"split_count";
NSString * const kRemotePosition = @"position";
NSString * const kRemoteDisconnectAllWifiDisplay = @"common.disconnect_all";

NSString * const kRemoteRole = @"role";
NSString * const kRemoteRoleHost = @"host";
NSString * const kRemoteRoleGuest = @"guest";



NSString * const kRemoteGetService = @"common.get_service";

NSString * const kRemoteCountry = @"country";

NSString * const kRemoteClientOS = @"os";
NSString * const kRemoteClientiOS = @"ios";
NSString * const kRemoteClientMac = @"mac";

NSString * const kRemoteServiceEnable = @"enable";
NSString * const kRemoteServiceDisable = @"disable";

NSString * const kEZCast3rdAppLeaveNotification = @"kEZCast3rdAppLeaveNotification";

NSString * const kEZCastAppEZChannelPosition = @"EZCastApp.EZChannel.Position";
NSString * const kEZCastAppEZChannelPositionFirst = @"EZCastApp.EZChannel.Position.First";
NSString * const kEZCastAppEZChannelPositionInside = @"EZCastApp.EZChannel.Position.Inside";
