//
//  EZCastApplicationFeatures.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/3/26.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

/**
 This service definitions are used in REMOTE HOST BROADCAST BUT NOT WIFI DISPLAY BORADCAST, whcih means it is used in AMRemoteHost.
 SEPC. definition URL: https://office.iezvu.com/tiki/tiki-index.php?page=EZCast+功能開關&highlight=bitmask
*/

/**-------------------- IMPORTANT ----------------------
  THIS FILE IS NOW PRIVATE. IF YOU ARE LOOKING FOR BITMASK DEFINITION PLEASE TRY POD 'EZCastBitmask'.
 -------------------- IMPORTANT ----------------------*/

#ifndef AMCommon_iOS_EZCastApplicationFeatures_h
#define AMCommon_iOS_EZCastApplicationFeatures_h
typedef NS_OPTIONS(NSInteger, AMEZCastApplicationFeatures) {
    SERVICE_NONE =                  0,
    SERVICE_PHOTO =         (0x01<<0),
    SERVICE_CAMERA =        (0x01<<1),
    SERVICE_MUSIC =         (0x01<<2),
    SERVICE_VIDEO =         (0x01<<3),
    SERVICE_DLNA =          (0x01<<4),
    SERVICE_EZMIRROR =      (0x01<<5),
    SERVICE_DOCUMENT =      (0x01<<6),
    SERVICE_WEB =           (0x01<<7),
    SERVICE_SETTING =       (0x01<<8),
    SERVICE_EZAIR =         (0x01<<9),
    SERVICE_CLOUD_VIDEO =   (0x01<<10),
    SERVICE_MAP =           (0x01<<11),
    SERVICE_CLOUD_STORAGE = (0x01<<12),
    SERVICE_TV =            (0x01<<13),
    SERVICE_SPLIT_SCREEN =  (0x01<<14),
    SERVICE_EZCAST =        (0x01<<15),
    SERVICE_COMMENT =       (0x01<<16),//   2013/11/20 added
    SERVICE_UPDATE =        (0x01<<17),//   2013/11/20 added
    SERVICE_NEWS =          (0x01<<18),//   2013/11/20 added
    SERVICE_MESSAGES =      (0x01<<19),//   2013/11/20 added
    SERVICE_SOCIAL =        (0x01<<20),//   2014/02/21 added
    SERVICE_CLIENT_MODE =   (0x01<<21),//   2014/03/26 added
    SERVICE_PREFERENCE =    (0x01<<22),//   2014/03/26 added
    SERVICE_HOST_CONTROL =  (0x01<<23)
};

typedef NSString * RemoteCommand NS_STRING_ENUM;


/**
 *  New def 2014/5/5
 *  https://office.iezvu.com/tiki/tiki-index.php?page=EZCast+Pro&highlight=ezcast%20pro
 */

extern RemoteCommand const kRemoteAllow;
extern RemoteCommand const kRemoteAllowFullscreen;
extern RemoteCommand const kRemoteDeny;
extern RemoteCommand const kRemoteWaitForAnswer;
extern RemoteCommand const kRemoteHeartbeat;
/**
 An App should send this JRPC request if it need to receive HID event from a remote device.
 */
extern RemoteCommand const kRemoteHIDEnableControl;
extern RemoteCommand const kRemoteHIDDisableControl;
/**
 A remote device send JRPC method to app to tell app about new  HID device has been detected.
 By SPEC., app should send kRemoteHIDEnableControl again after receiving this event(kRemoteHIDDetect).
 */
extern RemoteCommand const kRemoteHIDDetect;
/**
 A remote device send JRPC method to app to tell app about mouse move evnet.
 */
extern RemoteCommand const kRemoteHIDSendMouse;
extern RemoteCommand const kRemoteHIDSendControl;

/**
  Quert from remote dongle to ask App level code providing port info of APP HTTP Proxy service.
 
 Dongle ===> App
 JSON-RPC command
 method: common.QueryAppHTTPPRoxyInfo
 Response: A dict
 {
    AppHTTPProxyPort: 50123
 }
 */
extern RemoteCommand const kRemoteQueryAppHTTPProxyInfo;
extern RemoteCommand const kRemoteGetDeviceDescription;
extern RemoteCommand const kRemoteSetDeviceDescription;
extern RemoteCommand const kRemoteAssignRole;

/**
 *  New def 2016/7/4
 *  for EZCast Wire OTA on Windows/OSX
 */
extern RemoteCommand const kRemoteGetDeviceOTADescription;
extern RemoteCommand const kRemoteSetDeviceOTAReady;
/**
 * The remote device tell the app to disconnect actively.
 */
extern RemoteCommand const kRemoteDisconnect;
extern RemoteCommand const kRemoteJrpcParseError;
extern RemoteCommand const kRemoteWaitRequestStream;
extern RemoteCommand const kRemoteAskRequestStream;
extern RemoteCommand const kRemoteCancelRequestStream;
extern RemoteCommand const kRemoteAnswerRequestStream;
extern RemoteCommand const kRemoteAnswerRequestStreamFullscreen;
extern RemoteCommand const kRemoteAskRequestStreamFullscreen;
extern RemoteCommand const kRemoteRequestStreamFullscreen;
extern RemoteCommand const kRemoteRequestStream;
extern RemoteCommand const kRemoteStopStream;
extern RemoteCommand const kRemoteChangePosition;
extern RemoteCommand const kRemoteDisconnectAllWifiDisplay;
extern RemoteCommand const kRemoteSetHostname;



typedef NSString * RemoteCommandTerm NS_STRING_ENUM;
extern RemoteCommandTerm const kRemoteRole;
extern RemoteCommandTerm const kRemoteRoleHost;
extern RemoteCommandTerm const kRemoteRoleGuest;
extern RemoteCommandTerm const kRemoteHostname;
extern RemoteCommandTerm const kRemoteIPAddress;
extern RemoteCommandTerm const kRemoteClientOS;
extern RemoteCommandTerm const kRemoteClientiOS;
extern RemoteCommandTerm const kRemoteClientMac;
extern RemoteCommandTerm const kRemoteCountry;
extern RemoteCommandTerm const kRemoteSplitCount;
extern RemoteCommandTerm const kRemotePosition;

/**
 common.get_service
 */
extern RemoteCommand const kRemoteGetService;


#define JSONRPC_ID_LIMIT 65535

typedef NSString * EZCastAppTerm NS_STRING_ENUM;

extern EZCastAppTerm const kEZCast3rdAppLeaveNotification __deprecated;

/**
 For app AB Testing
 Key: kEZCastAppEZChannelPosition
 */
extern EZCastAppTerm const kEZCastAppEZChannelPosition __deprecated;

/**
 For app AB Testing
 Value: kEZCastAppEZChannelPositionFirst
 */
extern EZCastAppTerm const kEZCastAppEZChannelPositionFirst __deprecated;

/**
 For app AB Testing
 Value: kEZCastAppEZChannelPositionInside
 */
extern EZCastAppTerm const kEZCastAppEZChannelPositionInside __deprecated;
#endif
