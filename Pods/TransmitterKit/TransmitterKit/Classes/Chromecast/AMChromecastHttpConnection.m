//
//  AMChromecastHttpConnection.m
//  EZDisplay
//
//  Created by brianliu on 2014/2/11.
//  Copyright (c) 2014年 Actions-Micro Taipei. All rights reserved.
//
#import <TargetConditionals.h>
#import "AMChromecastHttpConnection.h"
@import UIKit;

//static const int httpLogLevel = HTTP_LOG_LEVEL_WARN; // | HTTP_LOG_FLAG_TRACE;
@interface AMChromecastHttpConnection ()
@property (strong) UIImage * image;
@property (copy) NSURL * mediaLocation;
@end


@implementation AMChromecastHttpConnection
- (BOOL)supportsMethod:(NSString *)method atPath:(NSString *)path
{
	HTTPLogTrace();
	
	if ([method isEqualToString:@"GET"])
	{
//		if ([path isEqualToString:@"/post.html"])
		{
			// Let's be extra cautious, and make sure the upload isn't 5 gigs
			
			return requestContentLength < 50;
		}
	}
	
	return [super supportsMethod:method atPath:path];
}

//- (BOOL)expectsRequestBodyFromMethod:(NSString *)method atPath:(NSString *)path
//{
//	HTTPLogTrace();
//	
//	// Inform HTTP server that we expect a body to accompany a GET request
//	return NO;
//	
//	return [super expectsRequestBodyFromMethod:method atPath:path];
//}

- (NSObject<HTTPResponse> *)httpResponseForMethod:(NSString *)method URI:(NSString *)path
{
	HTTPLogTrace();
//    NSLog(@"%s, request.allHeaderFields:%@\nBodyString:%@\nmethod:%@, url:%@",__PRETTY_FUNCTION__, request.allHeaderFields, request.bodyString, request.method, request.url);
    if([method isEqualToString:@"GET"] && [path hasPrefix:@"/LocalVideo.html"]){
        HTTPLogVerbose(@"%@[%p]: postContentLength: %qu", THIS_FILE, self, requestContentLength);
        NSDictionary * dict = [self parseGetParams];
        NSURL * fileURL = [NSURL fileURLWithPath:dict[@"location"]];
		if(fileURL!=nil){
            self.mediaLocation = fileURL;
        }
        if(self.mediaLocation){
            NSObject <HTTPResponse> * response = [[HTTPFileResponse alloc]initWithFilePath:self.mediaLocation.path forConnection:self];
            return response;
        }
        else{
            DLog(@"ERROR!! Empty video.");
        }
    }
	
	return [super httpResponseForMethod:method URI:path];
}

- (void)prepareForBodyWithSize:(UInt64)contentLength
{
	HTTPLogTrace();
	
	// If we supported large uploads,
	// we might use this method to create/open files, allocate memory, etc.
}

- (void)processBodyData:(NSData *)postDataChunk
{
	HTTPLogTrace();
	
	// Remember: In order to support LARGE POST uploads, the data is read in chunks.
	// This prevents a 50 MB upload from being stored in RAM.
	// The size of the chunks are limited by the POST_CHUNKSIZE definition.
	// Therefore, this method may be called multiple times for the same POST request.
	
	BOOL result = [request appendData:postDataChunk];
	if (!result)
	{
		HTTPLogError(@"%@[%p]: %@ - Couldn't append bytes!", THIS_FILE, self, THIS_METHOD);
	}
}
-(void) dealloc{
    DLog(@"%s", __PRETTY_FUNCTION__);
}
@end

