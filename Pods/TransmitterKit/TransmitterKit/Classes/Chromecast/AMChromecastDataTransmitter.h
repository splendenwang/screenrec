//
//  AMChromecastDataTransmitter.h
//  EZDisplay
//
//  Created by brianliu on 2014/2/11.
//  Copyright (c) 2014年 Actions-Micro Taipei. All rights reserved.
//
#import "AMDataTransmitter.h"

extern NSString * const kAMChromecastDataTransmitterErrorDomain;


typedef NS_ENUM(NSInteger, kAMChromecastDataTransmitterErrorCode){
    kAMChromecastDataTransmitterErrorFailure = 1000,
    kAMChromecastDataTransmitterErrorTimeout = 1001,
    kAMChromecastDataTransmitterErrorNoWIFIIPAddress = 1001
};

@class Media;
@interface AMChromecastDataTransmitter : AMDataTransmitter 
{
    //Receiving message
    void(^_receivingMessageBlock)(NSString* message);
}
@end
