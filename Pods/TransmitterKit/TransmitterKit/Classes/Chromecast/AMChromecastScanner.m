//
//  AMChromecastScanner.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/18.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMChromecastScanner.h"
#import "AMChromecastDevice.h"
#import "AMChromecastDevice_private.h"
@import GoogleCast;

@interface AMChromecastScanner () <GCKDeviceScannerListener>
{
    GCKDeviceScanner * _scanner;
}
@end

@implementation AMChromecastScanner

-(instancetype) initWithScanDuration:(NSTimeInterval)timeInterval delegate:(id<AMPrototypeDeviceScannerDelegate>)delegate{
    self = [super initWithScanDuration:timeInterval delegate:delegate];
    if(self)
    {
        self.delegate = delegate;
//        _scanner = [[GCKDeviceScanner alloc] init];
    }
    return self;
}

-(void) startScan{
    @synchronized(self){
        if(self.isScanning==NO){
            _scanner = [[GCKDeviceScanner alloc] initWithFilterCriteria:[GCKFilterCriteria criteriaForAvailableApplicationWithID:@"D3D8AEDC"]];
            [_scanner addListener:self];
            [_scanner startScan];
            _isScanning = YES;
        }
    }
}

-(void) stopScan{
    //GCKDeviceScanner stopScan must be called in main thread.
    if(self.isScanning == YES){
        [_scanner stopScan];
        [_scanner removeListener:self];
        _scanner = nil;
        _isScanning = NO;
    }
}
#pragma mark - GCKDeviceScannerListener
- (void)deviceDidComeOnline:(GCKDevice *)device{
    AMChromecastDevice * wrapperDevice =  [[AMChromecastDevice alloc] initWithChromecastDevice:device];
    if([self.delegate respondsToSelector:@selector(scanner:discoveriedDevice:)]){
        [self.delegate scanner:self discoveriedDevice:wrapperDevice];
    }
}
- (void)deviceDidGoOffline:(GCKDevice *)device{
        AMChromecastDevice * wrapperDevice =  [[AMChromecastDevice alloc] initWithChromecastDevice:device];
    if([self.delegate respondsToSelector:@selector(scanner:disappearedDevice:)]){
        [self.delegate scanner:self disappearedDevice:wrapperDevice];
    }
}

@end

