//
//  AMChromecastDataTransmitter.m
//  EZDisplay
//
//  Created by brianliu on 2014/2/11.
//  Copyright (c) 2014年 Actions-Micro Taipei. All rights reserved.
//

#import "AMChromecastDataTransmitter.h"
#import "AMChromecastHttpConnection.h"
#import "ChromecastDeviceController.h"
#import "AMChromecastDevice.h"
#import "AMChromecastDevice_private.h"
@import GoogleCast;
#import "AMBufferController.h"
#import "AMMJpegHttpServer.h"
#import "AMiDeviceIpAddress.h"
#import "AMPrototypeDevice+_internal.h"
#import "AMJSONRPCObject.h"
#import "AMJSONRPCRequest.h"
#import "AMDataTransmitter_private.h"

NSString * const kAMChromecastDataTransmitterErrorDomain = @"kAMChromecastDataTransmitterErrorDomain";


@interface AMChromecastDataTransmitter () <AMBufferControllerDelegate, GCKDeviceManagerDelegate, GCKMediaControlChannelDelegate>
{
    HTTPServer *        _httpServer;
    AMMJpegHttpServer * _mjpegServer;
    float               _currentVolumeLevel;
    
    void (^_remoteSuccessBlock)(AMPrototypeDevice *);
    void (^_remoteFailureBlock)(NSError *, AMPrototypeDevice *);
    
    //the block that will be called after a chromecast application has been launched
    void(^_CCApplicationDidLaunchOperation)(void);
    
    void(^_CCApplicationDidDsconnectionOperation)(void);
}
@property (nonatomic, strong) GCKDeviceManager * deviceManager;
@property GCKMediaControlChannel * mediaControlChannel;
@property GCKApplicationMetadata * applicationMetadata;
@property GCKCastChannel *textChannel;
@property AMBufferController * bufferController;

@property (nonatomic, copy) NSString * officalReceiverAppID;
@property (nonatomic, copy) NSString * customizedReceiverAppID;
@property (nonatomic, assign) BOOL useOfficalReceiver;
@property (readonly) ChromecastDeviceController * chromecastDeviceController;
@property(strong, nonatomic) NSTimer* updateStreamTimer;
-(void) loadMedia:(Media *)media;
@end


@implementation AMChromecastDataTransmitter
@synthesize connectedDevice = _connectedDevice;

-(instancetype) init{
    self = [super init];
    if(self){
        _currentVolumeLevel = 1.0;
        self.useOfficalReceiver = NO;
        self.customizedReceiverAppID = @"E3A71BDC";
        self.officalReceiverAppID = @"D3D8AEDC";
        
        self.bufferController = [[AMBufferController alloc] init];
        self.bufferController.delegate = self;
        [self.bufferController startConsumerThreads];
        
        _httpServer = [[HTTPServer alloc] init];
        
        NSError *error = nil;
        if([_httpServer start:&error])
        {
            DLog(@"Started HTTP Server on port %hu", [_httpServer listeningPort]);
        }
        else
        {
            DLog(@"Error starting HTTP Server: %@", error);
        }
        // We're going to extend the base HTTPConnection class with our MyHTTPConnection class.
        // This allows us to do all kinds of customizations.
        [_httpServer setConnectionClass:[AMChromecastHttpConnection class]];
        
        // Serve files from our embedded Web folder
        NSString *webPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"web"];
        DLog(@"Setting document root: %@", webPath);
        
        [_httpServer setDocumentRoot:webPath];
        
        _chromecastDeviceController = [[ChromecastDeviceController alloc] init];
        
        // Scan for devices.
        [_chromecastDeviceController performScan:YES];
    }
    return self;
}
#pragma mark - AMDataTransmitterProtocol

-(void) launchCorrespondingChromecastApplication:(void(^)(void))operation{
    
    @synchronized(self){
        if(_CCApplicationDidDsconnectionOperation!=NULL || _CCApplicationDidLaunchOperation!=NULL)
        {return;}
        
        //In this situation, we have to chenage our application to the other type
        if(self.applicationMetadata == nil){
            DLog(@"assigning _CCApplicationDidLaunchOperation");
            _CCApplicationDidLaunchOperation = operation;
            [self.deviceManager launchApplication:self.useOfficalReceiver ? self.officalReceiverAppID:self.customizedReceiverAppID relaunchIfRunning:YES];
        }
        else if(self.applicationMetadata && [self.applicationMetadata.applicationID isEqualToString:self.useOfficalReceiver? self.officalReceiverAppID: self.customizedReceiverAppID] == NO){
            
            DLog(@"assigning _CCApplicationDidLaunchOperation (relaunch)");
            __weak AMChromecastDataTransmitter * weakSelf = self;
            _CCApplicationDidLaunchOperation = ^{
                [weakSelf startMJPEGServer];
                if(operation){
                    operation();
                }
            };
            
            _CCApplicationDidDsconnectionOperation = ^{
                DLog(@"relaunching chromecast application: useOfficalReceiver:%d", weakSelf.useOfficalReceiver);
                [weakSelf stopMJPEGServer];
                [weakSelf.deviceManager launchApplication:weakSelf.useOfficalReceiver ? weakSelf.officalReceiverAppID:weakSelf.customizedReceiverAppID relaunchIfRunning:YES];
            };
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.deviceManager leaveApplication];
            });

        }
        else{
            if(operation){
                operation();
            }
        }
    }
}

-(void) startMJPEGServer{
    _mjpegServer = [[AMMJpegHttpServer alloc] init];
    [_mjpegServer startOnPort:0 completionBlock:^(NSError * error){
        DLog(@"mjpeg server start on port:%d", [_mjpegServer port]);
    } disconnect:^(NSError *error) {
        DLog(@"mjpeg server fails to starts, error:%@", error.localizedDescription);
    }];
    DLog(@"Starting MJPEG HTTP Server---> http://%@:%d", [AMiDeviceIpAddress getWIFIIPAddress], [_mjpegServer port]);
    
    if([AMiDeviceIpAddress getWIFIIPAddress] == nil){
        NSError * error = [NSError errorWithDomain:kAMChromecastDataTransmitterErrorDomain code:kAMChromecastDataTransmitterErrorNoWIFIIPAddress userInfo:nil];
        if(_connectionFailureBlock){
            _connectionFailureBlock(error);
            _connectionFailureBlock = nil;
            _connectionSuccessBlock = nil;
            _afterDisconnectBlock = nil;
        }
    }
    else{
        AMJSONRPCObject * jsonrpc = [[AMJSONRPCRequest alloc] initWithRequest:@"display" parameters:@{@"url":[NSString stringWithFormat:@"http://%@:%d", [AMiDeviceIpAddress getWIFIIPAddress], [_mjpegServer port]]} aId:@0];
        DLog(@"Send [jsonrpc JSONRPCData]:%@", jsonrpc);
        [self.textChannel sendTextMessage:[[NSString alloc] initWithData:[jsonrpc JSONRPCData] encoding:NSUTF8StringEncoding]];
    }
}
-(void) stopMJPEGServer{
    [_mjpegServer stop];
    _mjpegServer = nil;
}

-(void)_startWifi:(void(^)(void))success
          failure:(void(^)(NSError * error))failure
       disconnect:(void(^)(AMPrototypeDevice * disconnectedDevice))disconnect{
    
    if(_connectedDevice==nil){
        if(failure){
            NSError * error = [NSError errorWithDomain:kAMChromecastDataTransmitterErrorDomain code:kAMChromecastDataTransmitterErrorFailure userInfo:nil];
            failure(error);
        }
        return;
    }
    
    if([_connectedDevice isKindOfClass:[AMChromecastDevice class]]==NO){
        NSAssert( NO, @"Error using this class");
    }
    
    //2014.5.15
    ///TODO:These codes may need to make it simpler sinece the flow has been changed.
    _connectionFailureBlock = failure;
    _afterDisconnectBlock = disconnect;
    
    if(_mjpegServer==nil){   //if there is already a mjpeg server, means already running. We tell the caller it's
        [self startMJPEGServer];
    }
    
    if(success){
        success();
    }
    [self.state onWIDIDisplayStart];
}

/**
 Disconnect from the connected device if there is any.
 */
-(void)_stopWifiDisplay{
    DTrace();
    [self stopMJPEGServer];
    if(self.useOfficalReceiver){
        [self.deviceManager stopApplication];
        self.useOfficalReceiver = NO;
        self.applicationMetadata = nil;
        [self launchCorrespondingChromecastApplication:nil];
    }
    else{
        AMJSONRPCObject * jsonrpc = [[AMJSONRPCRequest alloc] initWithRequest:@"stopDisplay" parameters:nil aId:@0];
        [self.textChannel sendTextMessage:[[NSString alloc] initWithData:[jsonrpc JSONRPCData] encoding:NSUTF8StringEncoding]];
        
    }
    [self.state onWIDIDisplayDisconnect];
    
}

-(void)_sendImageWithData:(NSData*) imageData{
    DTrace();
    [self.bufferController sendObject:imageData];
    
}

// Split Screen
-(void) setSplitControlWithScreenNumber:(uint16_t) screenNumber
                               position:(uint16_t)screenPosition
                           successBlock:(void(^)(NSUInteger splitNumber, NSUInteger splitPosition))successBlock
                                failure:(void(^)(NSError * error))failureBlock{
    DTrace();
}

-(void) registerSplitScreenPauseBlock:(void(^)(void))pauseBlock{
    DTrace();
}

-(void) registerSplitScreenResumeBlock:(void(^)(void))resumeBlock{
    DTrace();
}


// General text messaging
-(void) sendTextMessage:(NSString*) sendTextMessage{
    DTrace();
}

-(void) registerReceivingMessageBlock:(void(^)(NSString * message)) receivingMessageBlock{
    DTrace();
}

- (void) mediastreaming:(NSURL*)mediaURL isVideo:(BOOL)isVideo{
    _mediaURL = mediaURL;
    self.useOfficalReceiver = YES;
    [self launchCorrespondingChromecastApplication:^{
        NSString * mime = @"video/mp4";
        if([_mediaURL isFileURL]){
            
            if(isVideo){
                mime = @"video/mp4";
            }
            else{
                mime = @"audio/mpeg";
            }
            
            GCKMediaMetadata *metadata = [[GCKMediaMetadata alloc] init];
            GCKMediaInformation *mediaInformation =
            [[GCKMediaInformation alloc] initWithContentID:[[NSURL URLWithString:[[NSString stringWithFormat:@"http://%@:%d/LocalVideo.html?location=%@", [AMiDeviceIpAddress getWIFIIPAddress], [_httpServer listeningPort], _mediaURL.path] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] absoluteString]
                                                streamType:GCKMediaStreamTypeNone
                                               contentType:mime
                                                  metadata:metadata
                                            streamDuration:0
                                                customData:nil];
            NSInteger result = [self.mediaControlChannel loadMedia:mediaInformation autoplay:YES];
            DLog(@"result:%zd", result);
            if(result == kGCKInvalidRequestID){
                [self.state onMediaStreamingReject];
            }
            else{
                [self.state onMediaStreamingPlay];
            }
        }
        else if(self.deviceManager && self.deviceManager.isConnected){
            GCKMediaMetadata *metadata = [[GCKMediaMetadata alloc] init];
            GCKMediaInformation *mediaInformation =
            [[GCKMediaInformation alloc] initWithContentID:[_mediaURL absoluteString]
                                                streamType:GCKMediaStreamTypeNone
                                               contentType:mime
                                                  metadata:metadata
                                            streamDuration:0
                                                customData:nil];
            NSInteger result = [self.mediaControlChannel loadMedia:mediaInformation autoplay:YES playPosition:0];
            if(result == kGCKInvalidRequestID){
                [self.state onMediaStreamingReject];
            }
            else{
                [self.state onMediaStreamingPlay];
            }
        }
        else{
            DLog(@"no self.deviceManager or not connected");
            [self.state onMediaStreamingReject];
        }
    }];
}

- (void)_playVideoMedia:(NSURL *)mediaURL subtitle:(NSURL *)subURL userInfo:(id)userInfo{
    [self mediastreaming:mediaURL isVideo:YES];
}

- (void)_playAudioMeida:(NSURL *)mediaURL userInfo:(id)userInfo{
    [self mediastreaming:mediaURL isVideo:NO];
}

- (void)_pausePlayingMedia{
    if([self.mediaControlChannel mediaStatus].mediaSessionID != 0){
        [self.mediaControlChannel pause];
        if(self.mediaSteamingPause){
            self.mediaSteamingPause();
        }
    }
}

- (void)_resumePlayingMedia{
    if([self.mediaControlChannel mediaStatus].mediaSessionID != 0){
        [self.mediaControlChannel play];
        if(self.mediaSteamingResume){
            self.mediaSteamingResume();
        }
    }
}

- (void) volumeUp{
    if([self.mediaControlChannel mediaStatus].mediaSessionID != 0){
        _currentVolumeLevel += 0.1;
        if(_currentVolumeLevel>1.0)
        {
            _currentVolumeLevel = 1.0;
        }
        [self.mediaControlChannel setStreamVolume:_currentVolumeLevel];
        if(self.volumeChange){
            self.volumeChange(_currentVolumeLevel);
        }
    }
}

- (void)volumeDown{
    if([self.mediaControlChannel mediaStatus].mediaSessionID != 0){
        _currentVolumeLevel -= 0.1;
        if(_currentVolumeLevel<0.0)
        {
            _currentVolumeLevel = 0.0;
        }
        [self.mediaControlChannel setStreamVolume:_currentVolumeLevel];
        if(self.volumeChange){
            self.volumeChange(_currentVolumeLevel);
        }
    }
}

- (float)volume{
    return _currentVolumeLevel;
}

-(void)_stopPlayingMedia:(MediaStreamingEnd)endBlock{
    DTrace();
    NSURL *mediaURL = _mediaURL;
    if([self.mediaControlChannel mediaStatus].mediaSessionID != 0){
        [self.mediaControlChannel stop];
        [self.state onMediaStreamingStop];
        if(self.updateStreamTimer){
            [self.updateStreamTimer invalidate];
            self.updateStreamTimer = nil;
        }
    }
    sleep(2);
    if (endBlock) {
        NSError *error = nil;
        NSDictionary *userInfo = @{ NSLocalizedDescriptionKey: NSLocalizedStringFromTable(@"User canceled media streaming process", @"AMCommon", "DPFSender - User canceled media streaming process") };
        error = [NSError errorWithDomain:kAMMediaStreamingStandardErrorDomain code:NSUserCancelledError userInfo:userInfo];
        endBlock(mediaURL, error);
    }
}

-(void)_seekToTime:(NSTimeInterval)seekTime{
    if([self.mediaControlChannel mediaStatus].mediaSessionID != 0){
        [self.mediaControlChannel seekToTimeInterval:seekTime resumeState:GCKMediaControlChannelResumeStateUnchanged];
    }
}

-(BOOL) isWIFIDisplayOn{
    return _mjpegServer!=nil;
}

#pragma mark - AMBufferControllerDelegate
-(BOOL) readyToSendObj{
    DLog(@"%s, %@", __PRETTY_FUNCTION__, _mjpegServer.isReadyToSend?@"YES":@"NO");
    return _mjpegServer.isReadyToSend;
}
-(void) sendObject:(id) aObj{
    DTrace();
    self.useOfficalReceiver = NO;
    [self launchCorrespondingChromecastApplication:^{
        [_mjpegServer sendJpgData:aObj];
    }];
}

#pragma mark - AMRemoteDataTransmitterProtocol
-(void) connectToRemoteDevice:(AMPrototypeDevice *)device
                      success:(void(^)(AMPrototypeDevice * device))successBlock
                      failure:(void(^)(NSError * error, AMPrototypeDevice * device))failureBlock{
    
    if(self.connectedDevice){
        [self stopWifiDisplay];
        [self disconnectRemoteDevice];
    }

    if(device && [device isKindOfClass:[AMChromecastDevice class]]){
        _connectedDevice = device; //we have to avoid triggering KVO at this moment.
        _remoteSuccessBlock = successBlock;
        _remoteFailureBlock = failureBlock;
        
        GCKDevice * chromecastDevice = ((AMChromecastDevice*)device).chromecastDevice;
        NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
        NSString *appIdentifier = info[@"CFBundleIdentifier"];
        self.deviceManager = [[GCKDeviceManager alloc] initWithDevice:chromecastDevice clientPackageName:appIdentifier ignoreAppStateNotifications:YES];
        self.deviceManager.delegate = self;
        [self.deviceManager connect];
    }
    else if(device == nil){
        if(failureBlock){
            NSError * error = [NSError errorWithDomain:kAMChromecastDataTransmitterErrorDomain code:kAMChromecastDataTransmitterErrorFailure userInfo:nil];
            _remoteFailureBlock(error, device);
        }
    }
}

-(void) disconnectRemoteDevice{
    [self stopPlayingMedia:nil];
    [self stopWifiDisplay];
    if(self.deviceManager.isConnected){
        [self.deviceManager disconnect];
    }
}

#pragma mark - GCKDeviceManagerDelegate
- (void)deviceManagerDidConnect:(GCKDeviceManager *)deviceManager {
    DLog(@"%s, connected!!", __PRETTY_FUNCTION__);
    [self launchCorrespondingChromecastApplication:nil];
}

- (void)deviceManager:(GCKDeviceManager *)deviceManager didFailToConnectWithError:(GCKError *)error {
    DLog(@"%s, error:%@", __PRETTY_FUNCTION__, error );
    
    if(_remoteFailureBlock){
        NSError * anError = [NSError errorWithDomain:kAMChromecastDataTransmitterErrorDomain code:kAMChromecastDataTransmitterErrorFailure userInfo:nil];
        _remoteFailureBlock(anError, self.connectedDevice);
        _remoteFailureBlock = nil;
        _remoteSuccessBlock = nil;
        self.connectedDevice = nil;
    }
    
    [self deviceDisconnected];
    
}


- (void)deviceManager:(GCKDeviceManager *)deviceManager
didConnectToCastApplication:(GCKApplicationMetadata *)applicationMetadata
            sessionID:(NSString *)sessionID
  launchedApplication:(BOOL)launchedApplication {
    
    DLog(@"application has launched");
    if(self.mediaControlChannel)
    {
        [self.deviceManager removeChannel:self.mediaControlChannel];
    }
    self.mediaControlChannel = [[GCKMediaControlChannel alloc] init];
    self.mediaControlChannel.delegate = self;
    [self.deviceManager addChannel:self.mediaControlChannel];
    [self.mediaControlChannel requestStatus];
    
    if(self.textChannel){
        [self.deviceManager removeChannel:self.textChannel];
    }
    self.textChannel = [[GCKCastChannel alloc] initWithNamespace:@"urn:x-cast:com.actions-micro.ezcast"];
    [self.deviceManager addChannel:self.textChannel];
    
    self.applicationMetadata = applicationMetadata;
    
    if(_CCApplicationDidLaunchOperation){
        DLog(@"execute _CCApplicationDidLaunchOperation block...");
        _CCApplicationDidLaunchOperation();
        _CCApplicationDidLaunchOperation = nil;
    }
    
    
    if(_remoteSuccessBlock)
    {
        _remoteSuccessBlock(_connectedDevice);
        _remoteSuccessBlock = nil;
        _remoteFailureBlock = nil;
        [self willChangeValueForKey:@"connectedDevice"];
        [self didChangeValueForKey:@"connectedDevice"];
    }
    
    
}
- (void)deviceManager:(GCKDeviceManager *)deviceManager didFailToLaunchCastApplicationWithError:(NSError *)error {
    DLog(@"error:%@", error);
    
    if(error){
        if(_connectionFailureBlock){
            NSError * error = [NSError errorWithDomain:kAMChromecastDataTransmitterErrorDomain code:kAMChromecastDataTransmitterErrorFailure userInfo:nil];
            _connectionFailureBlock(error);
            _connectionFailureBlock = nil;
            _connectionSuccessBlock = nil;
            self.connectedDevice = nil;
        }
    }
    [self deviceDisconnected];
}

- (void)deviceManager:(GCKDeviceManager *)deviceManager didDisconnectFromApplicationWithError:(NSError *)error{
    DTrace();
    @synchronized(self){
        self.applicationMetadata = nil;
        if(_CCApplicationDidDsconnectionOperation){
            _CCApplicationDidDsconnectionOperation();
            _CCApplicationDidDsconnectionOperation = nil;
        }
    }
}

- (void)deviceManager:(GCKDeviceManager *)deviceManager didDisconnectWithError:(GCKError *)error {
    NSLog(@"Received notification that device disconnected");
    @synchronized(self){
        if (error != nil) {
            DLog(@"error:%@", error);
        }
        
        [self deviceDisconnected];
        //    [self updateCastIconButtonStates];
    }
}

- (void)deviceDisconnected {
    if(self.updateStreamTimer){
        [self.updateStreamTimer invalidate];
        self.updateStreamTimer = nil;
    }
    
    self.applicationMetadata = nil;
    self.mediaControlChannel = nil;
    self.deviceManager = nil;
    if(_afterDisconnectBlock){
        _afterDisconnectBlock(self.connectedDevice);
        _afterDisconnectBlock = nil;
    }
    _connectionSuccessBlock = nil;
    _connectionFailureBlock = nil;
    _splitScreenResumeBlock = nil;
    _splitScreenPauseBlock = nil;
    _receivingMessageBlock = nil;
    _mediaURL = nil;
    self.connectedDevice = nil;
    
    
    //    if ([self.delegate respondsToSelector:@selector(didDisconnect)]) {
    //        [self.delegate didDisconnect];
    //    }
}

- (void)deviceManager:(GCKDeviceManager *)deviceManager
didReceiveStatusForApplication:(GCKApplicationMetadata *)applicationMetadata {
    self.applicationMetadata = applicationMetadata;
}

- (void)deviceManager:(GCKDeviceManager *)deviceManager
volumeDidChangeToLevel:(float)volumeLevel
              isMuted:(BOOL)isMuted {
    NSLog(@"New volume level of %f reported!", volumeLevel);
    _currentVolumeLevel = volumeLevel;
    //    self.deviceVolume = volumeLevel;
    //    self.deviceMuted = isMuted;
}
#pragma mark - GCKMediaControlChannelDelegate
- (void)updateInterfaceFromCast:(NSTimer*)timer {
    if(self.mediaSteamingElapseTime){
        self.mediaSteamingElapseTime([self.mediaControlChannel approximateStreamPosition]);
    }
    DLog(@"streamDuration:%f, approximateStreamPosition:%f", self.mediaControlChannel.mediaStatus.mediaInformation.streamDuration, [self.mediaControlChannel approximateStreamPosition]);
    if((self.mediaControlChannel.mediaStatus.mediaInformation.streamDuration - [self.mediaControlChannel approximateStreamPosition]) <= 1.0){
        [self.updateStreamTimer invalidate];
        self.updateStreamTimer = nil;
        if(self.mediaSteamingEnd){
            self.mediaSteamingEnd(_mediaURL, nil);
            self.mediaSteamingEnd = nil;
        }
    }
}

- (void)mediaControlChannel:(GCKMediaControlChannel *)mediaControlChannel didCompleteLoadWithSessionID:(NSInteger)sessionID {
    DTrace();
    if(self.mediaSteamingTotalTime){
        self.mediaSteamingTotalTime(self.mediaControlChannel.mediaStatus.mediaInformation.streamDuration);
    }
    self.updateStreamTimer =
    [NSTimer scheduledTimerWithTimeInterval:1.0
                                     target:self
                                   selector:@selector(updateInterfaceFromCast:)
                                   userInfo:nil
                                    repeats:YES];
    if(self.mediaSteamingStart){
        self.mediaSteamingStart(_mediaURL);
    }
}
- (void)mediaControlChannel:(GCKMediaControlChannel *)mediaControlChannel didFailToLoadMediaWithError:(NSError *)error{
    DTrace();
    if(self.mediaSteamingEnd)
    {
        NSError * anError = [NSError errorWithDomain:kAMMediaStreamingStandardErrorDomain code:AV_RESULT_ERROR_STOP_FILE_FORMAT_UNSOPPORTED userInfo:@{NSLocalizedDescriptionKey:NSLocalizedStringFromTable(@"Chromecast failed to play this file.", @"AMCommon", @"Datatransmitter - Chromecast failed to play this file")}];
        self.mediaSteamingEnd(_mediaURL, anError);
        self.mediaSteamingEnd = nil;
    }
}

- (void)mediaControlChannelDidUpdateStatus:(GCKMediaControlChannel *)mediaControlChannel {
    //    [self updateStatsFromDevice];
    DTrace();
    //    if ([self.delegate respondsToSelector:@selector(didReceiveMediaStateChange)]) {
    //        [self.delegate didReceiveMediaStateChange];
    //    }
}

- (void)mediaControlChannelDidUpdateMetadata:(GCKMediaControlChannel *)mediaControlChannel {
    //    [self updateStatsFromDevice];
    DTrace();
    
    //    if ([self.delegate respondsToSelector:@selector(didReceiveMediaStateChange)]) {
    //        [self.delegate didReceiveMediaStateChange];
    //    }
}
- (void)mediaControlChannel:(GCKMediaControlChannel *)mediaControlChannel
       requestDidFailWithID:(NSInteger)requestID
                      error:(NSError *)error{
    DTrace();
}

#pragma mark -
- (void)logFromFunction:(const char *)function message:(NSString *)message {
    // Send SDK’s log messages directly to the console, as an example.
    DLog(@"%s  %@", function, message);
}
//should never be called
-(void) dealloc{
    _receivingMessageBlock = nil;
    [_httpServer stop];
    [_mjpegServer stop];
    [self.bufferController removeAllBuffer];
    [self.bufferController stopThreadGracefully];
    [_httpServer stop];
}

-(NSTimeInterval) streamDuration{
    return self.chromecastDeviceController.streamDuration;
}

-(NSTimeInterval) streamPosition{
    return self.chromecastDeviceController.streamPosition;
}

-(void) loadMedia:(Media *)media{
    
}

-(void) stopPlayingCurrentMedia{
    [self.chromecastDeviceController stopCastMedia];
}
-(void) pauseCurrentMedia{
    [self.chromecastDeviceController pauseCastMedia:YES];
}
-(void) resumeMedia{
    [self.chromecastDeviceController pauseCastMedia:NO];
}
//-(void) volumeUp{
//    [self.chromecastDeviceController changeVolumeIncrease:YES];
//}
//
//-(void) volumeDown{
//    [self.chromecastDeviceController changeVolumeIncrease:NO];
//}

@end
