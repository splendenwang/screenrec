//
//  AMChromecastScanner.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/18.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//
#import "AMPrototypeDeviceScanner.h"


@interface AMChromecastScanner : AMPrototypeDeviceScanner <AMPrototypeDeviceScannerProtocol>

@end

