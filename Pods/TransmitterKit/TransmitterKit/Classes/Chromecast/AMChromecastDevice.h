//
//  AMChromecastDevice.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/18.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//
#import "AMPrototypeDevice.h"

@interface AMChromecastDevice : AMPrototypeDevice
-(UInt32) servicePort;
-(NSString*) manufacturer;
-(NSString *) deviceID;
@end

