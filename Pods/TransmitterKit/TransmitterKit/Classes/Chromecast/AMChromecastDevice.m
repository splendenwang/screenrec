//
//  AMChromecastDevice.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/18.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//
#import "EZCastApplicationFeatures.h"
#import "AMPrototypeDevice+_internal.h"
#import "AMChromecastDevice.h"
@import GoogleCast;
@import EZCastBitmask;
#if TARGET_OS_IPHONE

@interface AMChromecastDevice ()
{
    GCKDevice * _device;
    NSMutableDictionary * _dict;
}

@end

@implementation AMChromecastDevice

-(GCKDevice *) chromecastDevice{
    return _device;
}

-(instancetype) initWithChromecastDevice:(GCKDevice *)device{
    self = [super init];
    if(self){
        _device = device;
        _dict = [NSMutableDictionary dictionary];
        _dict[@"ezcast.service.ios"] = @"0x13B4CF";
        self.bitmaskDictionary = @{
                                   kRemoteServicePhoto        : kRemoteServiceEnable,
                                   kRemoteServiceCamera       : kRemoteServiceEnable,
                                   kRemoteServiceMusic        : kRemoteServiceEnable,
                                   kRemoteServiceVideo        : kRemoteServiceEnable,
                                   kRemoteServiceDocument     : kRemoteServiceEnable,
                                   kRemoteServiceWeb          : kRemoteServiceEnable,
                                   kRemoteServiceCloudVideo   : kRemoteServiceEnable,
                                   kRemoteServiceCloudStorage : kRemoteServiceEnable,
                                   kRemoteServiceComment      : kRemoteServiceEnable,
                                   kRemoteServiceUpdate       : kRemoteServiceEnable,
//                                   kRemoteServiceMoreApps     :kRemoteServiceEnable,
//                                   kRemoteServiceShop         :kRemoteServiceEnable,
                                   //                                   hide Game by Jesse's ask
                                   //                                   kRemoteServiceGame         : kRemoteServiceEnable
                                   kRemoteServiceEZChannel      :kRemoteServiceEnable,  //henry's reuqets. 2015.6.15
                                   kRemoteServiceBookmark         : kRemoteServiceEnable,
                                   };
        self.hostName = @"Chromecast";
        _dict[kAMPrototypeDeviceID] = device.deviceID;
    }
    return  self;
}

-(NSComparisonResult) compare:(AMPrototypeDevice *)aDevice{
    if([_device.ipAddress isEqualToString:[aDevice ipAddress]]){
        return NSOrderedSame;
    }
    return NSOrderedAscending;
}

-(NSString*) ipAddress{
    return _device.ipAddress;
}

-(NSString*) displayName{
    return _device.friendlyName;
}

-(id) objectForKey:(NSString*) key{
    return _dict[key];
}

+(NSArray *) supportedAudioFormat{
    return @[@"mp3"];
}

+(NSArray *) supportedVideoFormat{
#if DEBUG
    return @[@"mp4", @"mov", @"m4v"];
#endif
    return @[@"mp4", @"mov"];
}

-(NSString*) srcvers{
    return _device.deviceVersion;
}
-(UInt32) servicePort{
    return _device.servicePort;
}
-(NSString*) manufacturer{
    return _device.manufacturer;
}
-(NSString *) deviceID{
    return _device.deviceID;
}

-(BOOL) isEqual:(id)object{
    if([object isKindOfClass:[self class]]){
        AMChromecastDevice * anotherDevice = (AMChromecastDevice *) object;
        return ([self.deviceID isEqualToString:anotherDevice.deviceID]);
    }
    return NO;
}
@end
#else
@implementation AMChromecastDevice
-(NSString*) manufacturer{
    return nil;
}
-(UInt32) servicePort{
    return 0;
}
-(NSString*)deviceID{
    return nil;
}
@end
#endif


#pragma mark AMChromecastDevice
@implementation AMChromecastDevice (Analytics)
-(NSString*)analyticDongleType{
    return @"chromecast";
}
@end
