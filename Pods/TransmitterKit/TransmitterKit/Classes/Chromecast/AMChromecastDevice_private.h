//
//  AMChromecastDevice_private.h
//  TransmitterKit
//
//  Created by brianliu on 2015/11/5.
//  Copyright (c) 2015年 winnerwave. All rights reserved.
//
//
//  CAUTION!!!
//  DO NOT EXPORT THIS HEADER FILE TO PUBLIC!!!
//  THAT WILL CAUSE INCLUDING/IMPORT HELL!!
//

#ifndef TransmitterKit_AMChromecastDevice_private_h
#define TransmitterKit_AMChromecastDevice_private_h


#import "AMChromecastDevice.h"
@import GoogleCast;

@interface AMChromecastDevice ()
-(instancetype) initWithChromecastDevice:(GCKDevice*)device NS_DESIGNATED_INITIALIZER;
@property (nonatomic, readonly) GCKDevice * chromecastDevice;
@end


#endif


//  CAUTION!!!
//  DO NOT EXPORT THIS HEADER FILE TO PUBLIC!!!
//  THAT WILL CAUSE INCLUDING/IMPORT HELL!!
//

//  CAUTION!!!
//  DO NOT EXPORT THIS HEADER FILE TO PUBLIC!!!
//  THAT WILL CAUSE INCLUDING/IMPORT HELL!!
//

//  CAUTION!!!
//  DO NOT EXPORT THIS HEADER FILE TO PUBLIC!!!
//  THAT WILL CAUSE INCLUDING/IMPORT HELL!!
//
