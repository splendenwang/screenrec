//
//  AMJSONRPCNotification.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/5/7.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMJSONRPCNotification.h"

@implementation AMJSONRPCNotification
-(instancetype) initWithNotification:(NSString*)notification params:(id) params{
    self = [super initWithRequest:notification parameters:params aId:nil];
    if(self){
        
    }
    return self;
}

-(instancetype) initWithRequest:(NSString *)request parameters:(id)parameters aId:(id)aId{
    self = [self initWithNotification:request params:parameters];
    if(self){
        
    }
    return self;
}


-(NSData*) JSONRPCData{
    NSData * data = nil;
    NSError * __autoreleasing localError = nil;
    NSDictionary *methodCall;
    if(self.parameters==nil){
        methodCall = @{ @"jsonrpc": @"2.0", @"method": self.request};
    }
    else{
        methodCall = @{ @"jsonrpc": @"2.0", @"method": self.request, @"params":self.parameters};
    }
    data = [NSJSONSerialization dataWithJSONObject:methodCall options:0 error:&localError];
    if(localError){
        DLog(@"Error converting %@, error:%@", NSStringFromClass(self.class), localError);
        data= nil;
    }
    return data;
}
@end
