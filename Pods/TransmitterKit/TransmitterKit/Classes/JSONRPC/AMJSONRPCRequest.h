//
//  AMJSONRPCRequest.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/5/7.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMJSONRPCObject.h"

@interface AMJSONRPCRequest : AMJSONRPCObject

@property (readonly, nonatomic) NSString * request;

@property (readonly, nonatomic) id parameters;

- (instancetype)init NS_UNAVAILABLE; 

/**
 A rpc call is represented by sending a Request object to a Server.
 */
-(instancetype)initWithRequest:(NSString*)request parameters:(id)parameters aId:(id) aId NS_DESIGNATED_INITIALIZER;
@end
