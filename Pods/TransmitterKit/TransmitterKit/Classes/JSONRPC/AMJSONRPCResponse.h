//
//  AMJSONRPCRespone.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/5/7.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMJSONRPCObject.h"

@interface AMJSONRPCResponse : AMJSONRPCObject
@property (readonly, nonatomic) id result;
@property (readonly, nonatomic) id error;

- (instancetype)init NS_UNAVAILABLE;

/**
 When a rpc call is made, the Server MUST reply with a Response, except for in the case of Notifications. The Response is expressed as a single JSON Object, with the following members:
 */
-(instancetype) initWithResult:(id)result error:(id)error aId:(id) aId NS_DESIGNATED_INITIALIZER;

/**
 *  0       Default return value without error;
 */
+(AMJSONRPCResponse *) defaultResponse:(id)aId;
/**
 *  -32700	Parse error	Invalid JSON was received by the server. An error occurred on the server while parsing the JSON text.
 */
+(AMJSONRPCResponse *) errorParseError:(id)aId;

/**
 *  -32600	Invalid Request	The JSON sent is not a valid Request object.
 */
+(AMJSONRPCResponse *) errorInvalidRequest:(id)aId;

/**
 *  -32601	Method not found	The method does not exist / is not available.
 */
+(AMJSONRPCResponse *) errorMethodNotFound:(id)aId;

/**
 *  -32602	Invalid params	Invalid method parameter(s).
 */
+(AMJSONRPCResponse *) errorInvalidParams:(id)aId;

/**
 *  -32603	Internal error	Internal JSON-RPC error.
 */
+(AMJSONRPCResponse *) errorInternalError:(id)aId;

@end
