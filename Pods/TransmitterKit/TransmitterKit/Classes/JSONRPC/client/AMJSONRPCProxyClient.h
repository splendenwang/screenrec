//
//  AMJSONRPCProxyClient.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/5/9.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMJSONRPCRequest.h"
#import "AMJSONRPCNotification.h"
#import "AMJSONRPCResponse.h"

@interface AMJSONRPCProxyClient : NSObject

@property (assign) NSInteger maxRandomNumber;

/**
 
 */
-(void) newRequest:(NSString*)request param:(id)params
         generated:(void(^)(AMJSONRPCRequest* newRequest))block
          response:(void(^)(AMJSONRPCResponse* response, AMJSONRPCRequest * origRequest))reponseBlock;

/**
    
 */
-(void) newRequest:(NSString*)request param:(id)params aID:(id)aID
         generated:(void(^)(AMJSONRPCRequest* newRequest))block
          response:(void(^)(AMJSONRPCResponse* response, AMJSONRPCRequest * origRequest))reponseBlock;

/**
 
 */
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSArray *pendingRequests;

/**
 
 */
-(void) receiveResponse:(AMJSONRPCResponse*)response;


@end
