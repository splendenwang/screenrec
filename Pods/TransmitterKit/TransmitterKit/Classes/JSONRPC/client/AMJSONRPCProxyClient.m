//
//  AMJSONRPCProxyClient.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/5/9.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMJSONRPCProxyClient.h"


@interface AMJSONRPCProxyClient ()
@property (strong) NSMutableDictionary * jrpcRequests;
@property (strong) NSMutableDictionary * jrpcResponseBlock;
@end

@implementation AMJSONRPCProxyClient

-(instancetype) init{
    self = [super init];
    if(self){
        self.jrpcRequests = [NSMutableDictionary new];
        self.jrpcResponseBlock = [NSMutableDictionary new];
        self.maxRandomNumber = 65535;
    }
    return self;
}

-(void) newRequest:(NSString*)request param:(id)params
         generated:(void(^)(AMJSONRPCRequest* newRequest))block
          response:(void (^)(AMJSONRPCResponse *, AMJSONRPCRequest *))reponseBlock{
    
    [self newRequest:request param:params aID:@([self randomInt]) generated:block response:reponseBlock];
}

-(void) newRequest:(NSString*)request param:(id)params aID:(id)aID
         generated:(void(^)(AMJSONRPCRequest* newRequest))block
          response:(void (^)(AMJSONRPCResponse *, AMJSONRPCRequest *))reponseBlock{
    
    AMJSONRPCRequest * aRequest = [[AMJSONRPCRequest alloc] initWithRequest:request parameters:params aId:aID];
    (self.jrpcRequests)[aRequest.aId] = aRequest;
    if(reponseBlock){
        (self.jrpcResponseBlock)[aRequest.aId] = reponseBlock;
    }
    if(block){
        block(aRequest);
    }
}

-(NSArray*) pendingRequests{
    return [self.jrpcRequests allValues];
}

-(void) receiveResponse:(AMJSONRPCResponse*)response{
    AMJSONRPCRequest * origRequest = (self.jrpcRequests)[response.aId];
    void(^responseBlock)(AMJSONRPCResponse *, AMJSONRPCRequest *) = (self.jrpcResponseBlock)[response.aId];
    if(response.aId){
        [self.jrpcRequests removeObjectForKey:response.aId];
    }
    if(responseBlock){
        responseBlock(response, origRequest);
    }
}

-(NSInteger) randomInt{
    int randomID = 0;
    do{
        randomID = arc4random()% self.maxRandomNumber;
    }
    while((self.jrpcRequests)[@(randomID)]!=nil);
    return randomID;
}

@end
