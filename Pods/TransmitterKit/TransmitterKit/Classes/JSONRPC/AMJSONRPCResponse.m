//
//  AMJSONRPCRespone.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/5/7.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMJSONRPCResponse.h"

@implementation AMJSONRPCResponse
-(instancetype) initWithResult:(id)result error:(id)error aId:(id)aId{
    self = [super init];
    if(self){
        _result = result;
        _error = error;
        self.aId= aId;
    }
    return self;
}

-(NSData*) JSONRPCData{
    NSData * data = nil;
    NSError * __autoreleasing localError = nil;
    NSDictionary *methodCall;
    if (self.error) {
        methodCall = @{ @"jsonrpc": @"2.0", @"error": self.error, @"id": self.aId };
    }
    else {
        methodCall = @{ @"jsonrpc": @"2.0", @"result": self.result, @"id": self.aId };
    }
    data = [NSJSONSerialization dataWithJSONObject:methodCall options:0 error:&localError];
    if(localError)
        data= nil;
    return data;
}
+(AMJSONRPCResponse *) defaultResponse:(id)aId{
    return [[AMJSONRPCResponse alloc] initWithResult:@0 error:nil aId:aId];
}
+(AMJSONRPCResponse *) errorParseError:(id)aId{
    return [[AMJSONRPCResponse alloc] initWithResult:nil error:@{@"code":@(-32700), @"message":@"Parse Error"} aId:aId];
}
+(AMJSONRPCResponse *) errorInvalidRequest:(id)aId{
    return [[AMJSONRPCResponse alloc] initWithResult:nil error:@{@"code":@(-32600), @"message":@"Invalid request"} aId:aId];
}

+(AMJSONRPCResponse *) errorMethodNotFound:(id)aId{
    return [[AMJSONRPCResponse alloc] initWithResult:nil error:@{@"code":@(-32601), @"message":@"Method not found"} aId:aId];
}

+(AMJSONRPCResponse *) errorInvalidParams:(id)aId{
    return [[AMJSONRPCResponse alloc] initWithResult:nil error:@{@"code":@(-32602), @"message":@"Invalid params"} aId:aId];
}
+(AMJSONRPCResponse *) errorInternalError:(id)aId{
    return [[AMJSONRPCResponse alloc] initWithResult:nil error:@{@"code":@(-32603), @"message":@"Internal error"} aId:aId];
}

@end
