//
//  AMJSONRPCObject.m
//  AMCommon2-iOS
//
//  Created by brianliu on 13/9/30.
//  Copyright (c) 2013年 ActionsMicro. All rights reserved.
//

#import "AMJSONRPCObject.h"
#import "AMJSONRPCRequest.h"
#import "AMJSONRPCResponse.h"
#import "AMJSONRPCNotification.h"

NSString * const AMJSONRPCObjectErrorDomain = @"AMJSONRPCObjectErrorDomain";
NSInteger const AMJSONRPCObjectBatchNotSupportError = 1;

@interface AMJSONRPCObject()
{

}
+(NSData*) jsonRPCDataWithJSONRPCObject:(NSDictionary*) jsonrpc error:(NSError * __autoreleasing *)error;

@end

@implementation AMJSONRPCObject

+(AMJSONRPCObject*) parse:(NSString*) jrpcString error:(NSError *__autoreleasing *)errorPtr{
    NSData * data = [jrpcString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary * incomingJSONDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:errorPtr];
    if(errorPtr && *errorPtr){
        DLog(@"error parsing JSON-RPC msg: %@", jrpcString);
        return nil;
    }

    AMJSONRPCObject* jrpcObject = nil;
    if([incomingJSONDict isKindOfClass:[NSArray class]]){
        if(errorPtr){
            *errorPtr = [NSError errorWithDomain:AMJSONRPCObjectErrorDomain code:AMJSONRPCObjectBatchNotSupportError
                                     userInfo:@{ NSLocalizedDescriptionKey: @"Batch calls not supported" }];
            return nil;
        }
    }else if([incomingJSONDict isKindOfClass:[NSDictionary class]]){
        NSString * jsonrpcMethod = incomingJSONDict[@"method"];
        if(jsonrpcMethod!=nil){
            id jsonrpcParameter = incomingJSONDict[@"params"];
            id aId = incomingJSONDict[@"id"];
            if(aId){    // request
                jrpcObject = [[AMJSONRPCRequest alloc] initWithRequest:jsonrpcMethod parameters:jsonrpcParameter aId:aId];
            }
            else{   //notification
                jrpcObject = [[AMJSONRPCNotification alloc] initWithNotification:jsonrpcMethod params:jsonrpcParameter];
            }
        }
        else{
            id result = incomingJSONDict[@"result"];
            id error = incomingJSONDict[@"error"];
            id aId = incomingJSONDict[@"id"];
            jrpcObject = [[AMJSONRPCResponse alloc] initWithResult:result error:error aId:aId];
        }
    }

    return jrpcObject;
}

-(NSData*) JSONRPCData{
    return nil;
}

-(NSString*) JSONRPCMessage{
    return [[NSString alloc] initWithData:[self JSONRPCData] encoding:NSUTF8StringEncoding];
}

+(NSData*) jsonRPCDataWithJSONRPCObject:(NSDictionary*) jsonrpc error:(NSError * __autoreleasing *)error{
    // Attempt to serialize the call payload to a JSON string
    NSData *postData = [NSJSONSerialization dataWithJSONObject:jsonrpc options:0 error:error];
    return postData;
}

-(NSString*) description{
    return [NSString stringWithFormat:@"<%@:%p> %@", NSStringFromClass(self.class), self, self.JSONRPCMessage];
}

@end
