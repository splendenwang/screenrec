//
//  AMJSONRPCRequest.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/5/7.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMJSONRPCRequest.h"

@implementation AMJSONRPCRequest
-(instancetype) initWithRequest:(NSString *)request parameters:(id)parameters aId:(id)aId{
    self = [super init];
    if(self){
        _request = [request copy];
        _parameters = parameters;
        self.aId = aId;
        
    }
    return self;
}
-(NSData*) JSONRPCData{
    NSData * data = nil;
    NSError * __autoreleasing localError = nil;
    NSDictionary *methodCall;
    {
        if (self.parameters) {
            methodCall = @{ @"jsonrpc": @"2.0", @"method": self.request, @"params": self.parameters, @"id": self.aId};
        }
        else {
            methodCall = @{ @"jsonrpc": @"2.0", @"method": self.request, @"id": self.aId};
        }
    }
    data = [NSJSONSerialization dataWithJSONObject:methodCall options:0 error:&localError];
    if(localError){
        DLog(@"Error converting %@, error:%@", NSStringFromClass(self.class), localError);
        data= nil;
    }
    
    return data;
}
@end
