//
//  AMDataResponse.m
//  AMCommon_iOS
//
//  Created by brianliu on 2/6/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import "AMJSONDataResponse.h"

@implementation AMJSONDataResponse
- (NSDictionary *)httpHeaders{
        return @{@"Content-Type":@"application/json"};
}
@end
