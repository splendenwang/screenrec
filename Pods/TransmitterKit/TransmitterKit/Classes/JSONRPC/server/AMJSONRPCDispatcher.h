//
//  AMJSONRPCDispatcher.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/5/9.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMJSONRPCResponse.h"
#import "AMJSONRPCRequest.h"
#import "AMJSONRPCNotification.h"
/**
 A JSON-RPC 2.0 server class.
 @see http://software.dzhuvinov.com/files/jsonrpc2server/javadoc/com/thetransactioncompany/jsonrpc2/server/Dispatcher.html
 */
@interface AMJSONRPCDispatcher : NSObject

/**
 Gets the names of the handled JSON-RPC 2.0 notification methods.
 @return The notification names that this dispatcher handles.
 */
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSArray *handledNotifications;

/**
 Gets the names of the handled JSON-RPC 2.0 request methods.
 @return The request names that this dispatcher handles.
 */
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSArray *handledRequests;

/**
 Process a request.
 @return A BOOL value indicates if this request has been processed.
 */
-(AMJSONRPCResponse *) processRequest:(AMJSONRPCRequest*) request context:(id)context;

/**
 Process a notification.
 */
-(BOOL) processNotification:(AMJSONRPCNotification *)notification context:(id)context;


#if NS_BLOCKS_AVAILABLE
/**
 Register a block that deal with a notification.
 @param block A block dealing with a specific notification.
 */
//-(void) registerBlock:block forNotification:(NSString *)notification NS_AVAILABLE(10_6, 4_0);
-(void) registerNotification:(NSString*)notificationName operation:(void(^)(AMJSONRPCNotification* notification, id context)) block NS_AVAILABLE(10_6, 4_0);
/**
 Clear a block which deal with a notification.
 @param notification Notification name.
 */
-(void) clearBlockForNotification:(NSString*) notification NS_AVAILABLE(10_6, 4_0);

/**
 Register a block that deal with a request.
 @param block A block dealing with a specific request.
 */
-(void) registerRequest:(NSString*)request operation:(AMJSONRPCResponse *(^)(AMJSONRPCRequest* request, id context))block NS_AVAILABLE(10_6, 4_0);
/**
 Clear a block which deal with a request.
 @param request request name.
 */
-(void) clearBlockForRequest:(NSString*) request NS_AVAILABLE(10_6, 4_0);

/**
 Clear all registerd blocks, including both notification blocks and request blocks.
 */
-(void) clearAllRegisteredBlock;
#endif
@end
