//
//  AMJSONRPCDispatcher.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/5/9.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMJSONRPCDispatcher.h"


@interface AMJSONRPCDispatcher ()
@property (strong) NSMutableDictionary * requestHanlderBlocks;
@property (strong) NSMutableDictionary * notificationHanlderBlocks;
@end

@implementation AMJSONRPCDispatcher
-(instancetype) init{
    self = [super init];
    if(self){
        self.requestHanlderBlocks = [NSMutableDictionary new];
        self.notificationHanlderBlocks = [NSMutableDictionary new];
    }
    return self;
}

-(NSArray*) handledNotifications{
    return self.notificationHanlderBlocks.allKeys;
}
-(NSArray*) handledRequests{
    return self.requestHanlderBlocks.allKeys;
}

-(void) registerNotification:(NSString *)notificationName operation:(void (^)(AMJSONRPCNotification *, id))block{
    @synchronized(self){
        if((self.notificationHanlderBlocks)[notificationName]!=nil){
            NSException * exception = [NSException exceptionWithName:NSInvalidArgumentException reason:[NSString stringWithFormat:@"Cannot register a duplicate JSON-RPC 2.0 handler for notification: %@", notificationName] userInfo:nil];
            [exception raise];
        }
        
        if(block){
            (self.notificationHanlderBlocks)[notificationName] = block;
        }
    }
}


- (void) registerRequest:(NSString *)request operation:(AMJSONRPCResponse *(^)(AMJSONRPCRequest *, id))block{
    @synchronized(self){
        if((self.requestHanlderBlocks)[request]!=nil){
            NSException * exception = [NSException exceptionWithName:NSInvalidArgumentException reason:[NSString stringWithFormat:@"Cannot register a duplicate JSON-RPC 2.0 handler for request: %@", request] userInfo:nil];
            [exception raise];
        }
        
        if(block){
            (self.requestHanlderBlocks)[request] = block;
        }
    }
}

-(void) clearBlockForNotification:(NSString*) notification NS_AVAILABLE(10_6, 4_0){
    @synchronized(self){
        if((self.notificationHanlderBlocks)[notification])
            [self.notificationHanlderBlocks removeObjectForKey:notification];
    }
}
-(void) clearBlockForRequest:(NSString*) request NS_AVAILABLE(10_6, 4_0){
    @synchronized(self){
        if((self.requestHanlderBlocks)[request])
            [self.requestHanlderBlocks removeObjectForKey:request];
    }
}


-(AMJSONRPCResponse *) processRequest:(AMJSONRPCRequest*)request context:(id)context{
    if((self.requestHanlderBlocks)[request.request]){
        AMJSONRPCResponse *(^block)(AMJSONRPCRequest *, id) =  (self.requestHanlderBlocks)[request.request];
        if(block){
            return block(request, context);
        }
    }
    return nil;
}
-(BOOL) processNotification:(AMJSONRPCNotification *)notification context:(id)context{
    if((self.notificationHanlderBlocks)[notification.request]){
        void(^block)(AMJSONRPCNotification *, id) =  (self.notificationHanlderBlocks)[notification.request];
        if(block){
            block(notification, context);
            return YES;
        }
    }
    return NO;
}

-(void) clearAllRegisteredBlock{
    @synchronized(self){
        [self.requestHanlderBlocks removeAllObjects];
        [self.notificationHanlderBlocks removeAllObjects];
    }
}

-(void) dealloc{
    [self clearAllRegisteredBlock];
}


@end
