//
//  AMJSONRPCNotification.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/5/7.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMJSONRPCObject.h"
#import "AMJSONRPCRequest.h"
@interface AMJSONRPCNotification : AMJSONRPCRequest

/*
 A Notification is a Request object without an "id" member.
 */
-(instancetype) initWithNotification:(NSString*)notification params:(id) params NS_DESIGNATED_INITIALIZER;

@end
