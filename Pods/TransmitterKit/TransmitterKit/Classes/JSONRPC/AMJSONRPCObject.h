//
//  AMJSONRPCObject.h
//  AMCommon2-iOS
//
//  Created by brianliu on 13/9/30.
//  Copyright (c) 2013年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const AMJSONRPCObjectErrorDomain;
extern NSInteger const AMJSONRPCObjectBatchNotSupportError;
const

//http://www.jsonrpc.org/specification#notification

@interface AMJSONRPCObject : NSObject

+(AMJSONRPCObject*) parse:(NSString*) jrpcString error:(NSError**)error;

/**
 *  JSON-RPC 2.0 id field. 
 *
 *  String, Number or NULL.
 */
@property (nonatomic, strong) id aId;

@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSData *JSONRPCData;

@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *JSONRPCMessage;

@end
