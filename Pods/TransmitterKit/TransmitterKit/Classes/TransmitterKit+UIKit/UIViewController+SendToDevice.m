//
//  UIViewController+SendToDevice.m
//  AMCommon_iOS
//
//  Created by brianliu on 13/2/25.
//  Copyright (c) 2013年 ActionsMicro. All rights reserved.
//

#import "UIViewController+SendToDevice.h"
#import "AMDeviceController.h"
#import "AMDeviceController+_internal.h"
#import "AMDataTransmitter.h"
#import "AMDemoDevice.h"
#import "AMDemoDataTransmitter.h"
#import "AMPrototypeDevice+EZCastCheck.h"



//@import TransmitterKit.AMDataTransmitter_private;

@implementation UIViewController (SendToDevice)

-(void) sendImage:(UIImage*)image{
    [self sendImage:image compress:0.7];
}
-(void) sendImage:(UIImage*)image compress:(float)factor{
    // 
    // 2014/9/9 updated
    // new protocol http://wiki.actions-micro.com/tiki/tiki-index.php?page=EZCast+Pro&highlight=family#Protocol_
    // there are family and type now
    // *在EZCast Music之下, Mirror Desktop的動作不要動作, 要關閉以節省頻寬(不只是URL Divert時, 而是anytime) (android/ios)*
    //
    // AM8252, which is limited edition from 8251, ezcast, model named "ezcast-lite";
    // we dont send image, it seems that 8252 only have DLNA/MIRACAST/AIRPLAY and update,comment function
    
    AMDataTransmitter * transmitter = [AMDeviceController sharedDeviceController].transmitter;
    
    if( transmitter.isMediaStreaming ) {
        [transmitter stopPlayingMedia:^(NSURL * mediaURL, NSError * error){
            [transmitter sendImageWithData:UIImageJPEGRepresentation(image,factor)];
        }];
    }
    else{
        [transmitter sendImageWithData:UIImageJPEGRepresentation(image,factor)];
    }
    
    
}

-(void) sendStreamingAudioData:(NSData*) pcmData{
    AMDataTransmitter * transmitter = [AMDeviceController sharedDeviceController].transmitter;
    [transmitter sendPCMData:pcmData];
}
static BOOL shouldRun = NO;
-(void) playAudioData:(NSData*) pcmData end:(void(^)(void))endBlock{
    if(pcmData==nil){
        @throw [NSException exceptionWithName:NSInvalidArgumentException reason:@"playAudio:end: should not receive a nil pcmData" userInfo:nil];
    }
    
    NSDictionary * dict = nil;
    if(endBlock){
        dict = @{@"pcmdata":pcmData, @"endBlock":endBlock };
    }
    else{
        dict = @{@"pcmdata":pcmData};
    }
    shouldRun = YES;
    [NSThread detachNewThreadSelector:@selector(playAudioTthread:) toTarget:self withObject:dict];
}

-(void) cancelPlayingAudo{
    //Letting the thead to end and exit normally.
    shouldRun = NO;
}

-(void) playAudioTthread:(id)userInfo{
    NSDictionary * dict = (NSDictionary*) userInfo;
    NSData * pcmData = dict[@"pcmdata"];
    void(^endBlock)(void) = dict[@"endBlock"];
    const NSInteger chunkLen = 5*1024;
    const NSInteger chunks = (pcmData.length / chunkLen)+ ((pcmData.length%chunkLen==0)?0:1);
    NSInteger dataLeft = pcmData.length;
    for (int i =0 ; i<chunks && shouldRun==YES; i++) {
        NSInteger transferSize = dataLeft>chunkLen?chunkLen:dataLeft;
        NSData * chunkData = [pcmData subdataWithRange:NSMakeRange(i*chunkLen, transferSize)];
        [self sendStreamingAudioData:chunkData];
        dataLeft -= transferSize;
        /* Black Magic! Don't touch*/
        usleep( ((float)(chunkLen*0.95)/176400.f)*1000000);
    }
    if(endBlock){
        endBlock();
    }
}

@end
