//
//  UIViewController+SendToDevice.h
//  AMCommon_iOS
//
//  Created by brianliu on 13/2/25.
//  Copyright (c) 2013年 ActionsMicro. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIViewController (SendToDevice)

/**
 Send Data to current connected device. This function transfer UIImage object to JPEG data representation. the default compress factor of this message is 0.7.
 @param image An UIImage object to send.
 */
-(void) sendImage:(UIImage*)image;

/**
 Send Data to current connected device. This function transfer UIImage object to JPEG data representation.
 @param image An UIImage object to send.
 @param factor compress factor sent to UIImageJPEGRepresentation.
 */
-(void) sendImage:(UIImage*)image compress:(float)factor;


/**
 Send live PCM data to current connected device.
 
 @param pcmData Liner PCM data. Format: Stereo, 16bit, 44100Hz.
 */
//-(void) sendStreamingAudioData:(NSData*) pcmData;

/**
 Play audio PCM data.
 
 @param pcmData Liner PCM data. Format: Stereo, 16bit, 44100Hz.
 @param completion Completion block called after playing this audio data.
 @note If the you have to leave your view controller(or controller) and the endBlock has not been called yet, make sure you call cancelPlayingAudio:. before dealloc.
 */
//-(void) playAudioData:(NSData*) pcmData end:(void(^)(void))endBlock;

/**
 Cancel the current playing operation.
 
 @see -(void) playAudioData:(NSData*) pcmData end:(void(^)(void))endBlock
 */
//-(void) cancelPlayingAudo;
@end
