//
//  AMDataTransmitter_ConferenceControl.h
//  AMCommon_iOS
//
//  Created by brianliu on 2015/8/13.
//  Copyright (c) 2015年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AMDataTransmitter;

typedef NS_ENUM(NSInteger, HostControlSplitType){
    HostControlSplitTypeShareScreen = 1,
    HostControlSplitTypeFullScreen  = 2,
};

typedef NS_ENUM(NSInteger, TransmitterRole) {
    TransmitterRoleHost,
    TransmitterRoleGuest,
    TransmitterRoleNotDefined
};


@protocol AMDataTransmitterHostControlDelegate <NSObject>
@optional
/**
 Notifies the delegate about role change event.
 
 @note General delegate method.
 */
-(void) transmitterHostControlRoleDidChange:(AMDataTransmitter*)transmitter;

/**
 The host ask you to wait for the answer.
 
 @note Guest delegate method. (Only guest will receive this delegate method)
 */
-(void) transmitterHostControlWaitForResponse:(AMDataTransmitter*)transmitter;

/**
 The host accepts your request.
 
 @note Guest delegate method. (Only guest will receive this delegate method)
 */
-(void) transmitterHostControlHostDidAcceptRequest:(AMDataTransmitter*)transmitter;

/**
 The host denies your your request.
 
 @notes Guest delegate method. (Only guest will receive this delegate method)
 */
-(void) transmitterHostControlDenyFromHost:(AMDataTransmitter*)transmitter ;

/**
 @param guestIP The IPAddress of a requesting client.
 @param guestName The name of a requesting client.
 @param completionBlock A block the finally tells the caller if it should accept a reuqest stream.
 @return A BOOL value indicates if a transmitter should wait for user's decision. (And this transmitter will send a wait command to remote device if this return value is YES)
 @notes Host delegate method. (Only host will receive this delegate method)
 */
-(BOOL) transmitterHostControlDidReceiveAskRequestStream:(AMDataTransmitter*)transmitter
                                               fromGuest:(NSString*) guestIP
                                                    name:(NSString*) guestName
                                              completion:(void(^)(BOOL isAllow, HostControlSplitType type))completionBlock;

/**
 @param guestIP The ip address of a requesting client who canceled a request.
 
 @notes Host delegate method. (Only host will receive this delegate method)
 */
-(void)transmitterHostControlDidReceiveCancelRequestStream:(AMDataTransmitter*)transmitter
                                                   fromGuest:(NSString*) guestIP;

/**
 Notify the delegate that it should disconnect right away.
 
 @note General delegate method.
 */
-(void)transmitterHostControlDidAskedToDisconnect:(AMDataTransmitter*)transmitter;

/**
 Host delegate method. A host will get this call if there is guest requesting full screen request(specially, media streaming).
 
 Host Control protocol v2.
 
 @param guestIP IP address of a guest.
 @param guestName Display name of a guest.
 @param completionBlock A Completion block to notifiy conference controller to 'allow share'/'allow fullscreen'/'reject'.
 */
-(void)hostControlDidReceiveAskFullscreen:(AMDataTransmitter*)transmitter
                                               fromGuest:(NSString*) guestIP
                                                    name:(NSString*) guestName
                                              completion:(void(^)(BOOL isAllow, HostControlSplitType type))completionBlock;

/**
 Return jpeg image data to send to remote device.
 */
-(NSData*)hostControlDidAskedToAutoWifiDisplay:(AMDataTransmitter*)transmitter;

-(void)hostControlAutoWifiDisplayDidDisconnected:(AMDataTransmitter*)transmitter;

/**
 *  Notify the delegate that it parse message failed
 *
 * @param transmitter transmitter
 * @param error error
 */
-(void)transmitterHostControlDidParseJRPCMessageFailed:(AMDataTransmitter *)transmitter error:(NSError *)error;

/**
 *  Notify the delegate that projector notify disconnect, one scene is invoked by conference control
 *
 *  @param transmitter transmitter
 */
-(void)transmitterProjectorDidNotifyDisconnect:(AMDataTransmitter *)transmitter;
@end

@protocol AMDataTransmitter_ConferenceControl <NSObject>

@property (nonatomic, readonly) NSNumber * version;

/**
 Set the delegate of host control behaviors.
 
 @note This delegate needs to be set before calling the following code \"connectToRemoteDevice:success:failure:userInfo:\"
 */
//
-(void)setHostControlDelegate:(id<AMDataTransmitterHostControlDelegate>)hostControlDelegate;

-(id<AMDataTransmitterHostControlDelegate>)hostControlDelegate;


/**
 Deprecated. Use requestFullscreen:complete: with autoStartWifidisplay = NO instead.
 The original code executes as
 ```
 [transmitter requestFullscreen:NO completion:block];
 ```
 */
-(void)requestFullscreen:(void(^)(BOOL isAllow))complete __attribute__((deprecated("requestFullscreen: is deprecated, use requestFullscreen:complete: with autoStartWifidisplay = NO instead.")));

/**
  @b host @bb, @b guest @bb -> host. Requesting full screen.
 @param autoStartWifiDisplay Auto Start wifidisplay service if host allows.
 @param block Completion block
 */
-(void)requestFullscreen:(BOOL)autoStartWifiDisplay complete:(void(^)(BOOL allowed))block;

-(void)setHostName:(NSString*)name;

#define STRING_TO_ROLE(x) ([x isEqualToString:@"host"]?TransmitterRoleHost:TransmitterRoleGuest)
#define ROLE_TO_STRING(x) ((x == TransmitterRoleNotDefined) ? @"NO ROLE" :  ((x == TransmitterRoleHost)?@"host" : @"guest"))



@property (readonly) TransmitterRole currentRole;
@property (readonly) BOOL isHost;
@property (readonly) BOOL isGuest;

@end

