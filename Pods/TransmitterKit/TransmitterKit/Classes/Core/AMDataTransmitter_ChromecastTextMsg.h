//
//  AMDataTransmitter_Chromecast.h
//  AMCommon_iOS
//
//  Created by brianliu on 2015/8/13.
//  Copyright (c) 2015年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AMDataTransmitter_ChromecastTextMsg <NSObject>
-(void) sendTextMessage:(NSString*) sendTextMessage;

-(void) registerReceivingMessageBlock:(void(^)(NSString * message)) receivingMessageBlock;
@end
