//
//  AMPrototypeDevice+EZCastCheck.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/6/26.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//
#import "AMPrototypeDevice+EZCastCheck.h"
#import "AMGenericDevice.h"
#import "AMGenericDeviceScanner.h"
#import "AMRemoteHost.h"
#import "AMDemoDevice.h"
#import "AMScreenDevice.h"
#import "AMDemoDevice.h"
#import "AMPrototypeDevice+_internal.h"
#import "EZcastApplicationFeatures.h"
@import SimpleAES;

@implementation AMPrototypeDevice (EZCastCheck)
#pragma mark - AMProtypeDevice_TypeCheck
-(BOOL) isEZCastDevice{
    if([self isKindOfClass:[AMGenericDevice class]]){
        AMRemoteHost * remote = ((AMGenericDevice*) self).remoteHost;;
        return [[AMGenericDeviceScanner ezcastPredicate:nil] evaluateWithObject:remote];
    }
    return NO;
}
-(BOOL) isEZCastProDevice{
    if([self isKindOfClass:[AMGenericDevice class]]){
        AMRemoteHost * remote = ((AMGenericDevice*) self).remoteHost;
        return [[AMGenericDeviceScanner ezcastProPredicate:nil] evaluateWithObject:remote];
    }
    return NO;
}
-(BOOL) isEZCastCarDevice{
    if([self isKindOfClass:[AMGenericDevice class]]){
        AMRemoteHost * remote = ((AMGenericDevice*) self).remoteHost;;
        return [[AMGenericDeviceScanner ezcastCarPredicate:nil] evaluateWithObject:remote];
    }
    return NO;
}
-(BOOL) isEZCastLiteDevice{
    if([self isKindOfClass:[AMGenericDevice class]]){
        AMRemoteHost * remote = ((AMGenericDevice*) self).remoteHost;;
        return [[AMGenericDeviceScanner ezcastLitePredicate] evaluateWithObject:remote];
    }
    return NO;
}
-(BOOL) isEZCastMusicDevice{
    if([self isKindOfClass:[AMGenericDevice class]]){
        AMRemoteHost * remote = ((AMGenericDevice*) self).remoteHost;;
        return [[AMGenericDeviceScanner ezcastMusicPredicate] evaluateWithObject:remote];
    }
    return NO;
}
-(BOOL)isEZCastWireDevice {
    if([self isKindOfClass:[AMGenericDevice class]]){
        AMRemoteHost * remote = ((AMGenericDevice*) self).remoteHost;;
        return [[AMGenericDeviceScanner ezcastWirePredicate] evaluateWithObject:remote];
    }
    return NO;
}
-(BOOL) isDemoDevice{
    if([self isKindOfClass:[AMDemoDevice class]])
        return YES;
    return NO;
}
- (BOOL)validateProperty:(NSString*)propertyKey withRegularExpression:(NSString*)re{
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:re options:NSRegularExpressionCaseInsensitive error:nil];
    
    NSString * propertyValue = [self valueForKey:propertyKey];
    
    if(!propertyValue){
        return YES; //old protocol return nil hostName
    }
    
    NSTextCheckingResult *match = [regex firstMatchInString:propertyValue options:0 range:NSMakeRange(0, propertyValue.length)];
    return match != nil;
}
- (BOOL)isOfficialEZCastDevice{
    return [self validateProperty:@"hostName" withRegularExpression:@"(.+)[-_#][0-9A-Fa-f]{8}$"];
}
@end


#pragma mark - Analytics
/**
 Read-Only properties for analytics. 2016/1 Brian
 */
#pragma mark AMPrototypeDevice
@implementation AMPrototypeDevice (Analytics)
-(NSString*)analyticDongleType{
    return @"ezcast";//default string. spec requested
}
-(NSString*)analyticHostname{
    return @"UNKNOWN_DONGLE";
}
-(NSString*)analyticChecksum{
    return nil;
}
-(NSString*)analyticDongleAddress{
    return nil;
}
-(NSDictionary*)analyicDictionary{
    NSMutableDictionary * dict = [NSMutableDictionary new];
    dict[@"type"] = self.analyticDongleType;
    dict[@"name"] = self.analyticHostname;
    if (self.analyticChecksum != nil) {
        dict[@"checksum"] = self.analyticChecksum;
    }
    if (self.analyticDongleAddress != nil) {
        dict[@"id"] = self.analyticDongleAddress;
    }
    return dict;
}
@end
#pragma mark AMGenericDevice
@implementation AMGenericDevice (Analytics)
-(NSString*)analyticDongleType{
    //accroding http://www.actions-micro.com/tiki/tiki-index.php?page=EZCast+Pro#Protocol_
    /**
     1. Remote control discovery 的部份 (新增兩個 key)
     *vendor
     ezcast 及 ezcast pro 公版的值都是 『ezcast'
     客戶案就看是哪個客戶, 例如 『benq', 『acer'
     *model
     ezcast 及 ezcast pro 公版的值都是 『ezcast' (8252 的值是 『ezcast-lite')
     客戶案就看是哪個型號,例如 『MX661』, 『GP30』 等等 (目前沒用到)
     *family
     新增的值,用來分辨是 ezcast 或 ezcastpro (及衍生出的客戶案)
     值有 『ezcast' 及 『ezcastpro'.
     如果沒這個 key,就表示可能是舊的 ezcast
     
     *type
     family==ezcast情況下，有以下可能：
     'music'         'car'         'lite'
     family==ezcastpro情況下，有以下可能：
     'tv'         'box'         'dongle'         'projector'
     
     用來分辨 ezcast pro不同的種類. 目前用來在app device list 顯示不同的 icon.
     目前定義的值有 『tv', 『box', 『dongle', 『projector', 'music', 'car', 'lite'
     App 判斷的 rule
     
     EZCast pro 公版 app : check family=ezcastpro
     EZCast pro 客戶案 app: check family=ezcastpro 且 vendor=benq or acer 之類的
     EZCast app: check 「family=ezcast且小機 ip address 是 192.168.203.1」 或是 「沒有 family 這個 key (舊的 ezcast dongle), 但小機 ip address 是 192.168.203.1」
     */
    AMRemoteHost * remote = [(AMGenericDevice *)self remoteHost];
    NSString * model = remote.deviceModel;
    //        NSString * family = remote.family;
    NSString * type = remote.type;
    
    if([model isEqualToString:@"ezcast-lite"]){
        return @"ezcastlite";
    } else {
        if([remote.family isEqualToString:@"ezcastpro"])
        {
            if([type isEqualToString:kRemoteTypeBox]){
                return @"ezcastpro";
            }
            else if([type isEqualToString:kRemoteTypeProjector]){
                return @"ezcastpro";
            }
            else if([type isEqualToString:kRemoteTypeTV]){
                return @"ezcastpro";
            }
            else if([type isEqualToString:kRemoteTypeDongle]){
                return @"ezcastpro";
            }
            else{   //no any matching, fall through to default : ezcastpro
                return @"ezcastpro";
            }
            
        }
        else if([remote.family isEqualToString:@"ezcast"]){
            if([type isEqualToString:kRemoteTypeCar]){
                return @"ezcastcar";
            }
            else if([type isEqualToString:kRemoteTypeMusic]){
                return @"ezcastmusic";
            }
            else if([type isEqualToString:kRemoteTypeLite]){
                return @"ezcastlite";
            }
            else if([type isEqualToString:kRemoteTypeWire]){
                return @"ezcastwire";
            }
            else{   //no any matching, fall through to default : ezcast
                return @"ezcast";
            }
        }
    }
    return [super analyticDongleType];
}
-(NSString*)analyticHostname{
    return self.hostName;
}
-(NSString*)analyticChecksum{
    return [[self analyticDongleAddress] stringByAppendingString:@"\tbind"].md5Value;
}
-(NSString*)analyticDongleAddress{
    return [self objectForKey:kAMPrototypeDeviceID];
}
@end

//#pragma mark AMChromecastDevice
//
//
//  Inplementation of AMChromecastDevice has been move to app project level.
//  SRC is AMCommon level but build level is on App target
//  Brian Liu, 2016/9/1
//
//

#pragma mark AMScreenDevice
@implementation AMScreenDevice (Analytics)
-(NSString*)analyticDongleType{
    return [self deviceOS];
}
@end
#pragma mark AMDemoDevice
@implementation AMDemoDevice (Analytics)
-(NSString*)analyticDongleType{
    return @"demo";
}
@end



