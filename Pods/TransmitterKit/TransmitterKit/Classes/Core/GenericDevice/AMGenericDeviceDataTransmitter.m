//
//  AMGenericDeviceDataTransmitter.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/19.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//


#import "AMGenericDeviceDataTransmitter.h"
#import "AMGenericDevice.h"
#import "AMPicoRequest.h"
#import "AMProjectorDevice.h"
#import "AMRemoteHost.h"
#import "EZCastApplicationFeatures.h"
#import "AMJSONRPCDispatcher.h"
#import "AMJSONRPCProxyClient.h"
#import "AMPrototypeDevice+_internal.h"
#import "AMDataTransmitter_private.h"
#import "AMProjectorScanner.h"
#import "AMDPFImageSender.h"
#import "AMPrototypeDevice+EZCastCheck.h"
@import WinnerWave_CocoaHTTPServer;
#import "AMDemoDevice.h"
@import AFNetworking;
@import EZCastBitmask;
#define JSONRPC_OK_RESPONSE ([[AMJSONRPCResponse alloc] initWithResult:@0 error:nil aId:request.aId])

NSString * const kHostControl = @"host_control";
NSString * const kVersion =     @"version";
NSString * const kCapability =  @"capability";
NSString * const kEncryptionKey = @"key";
NSString * const kResult =      @"result";


NSString * const kAMGenericDeviceDataTransmitterErrorDomain = @"kAMGenericDeviceDataTransmitterErrorDomain";
@interface AMGenericDeviceDataTransmitter () <AMDPFImageSenderApplicationDelegate,AMDPFImageSenderMediaStreamingDelegate,AMRemoteHostMessageListener, NSURLSessionDownloadDelegate>
{
    AMDPFImageSender * _sender;
    
    NSOperationQueue * _operationQueue;
    
    void (^_remoteConnectSuccessBlock)(AMPrototypeDevice * device);
    
    dispatch_source_t _heartbeatTimer;
    
    NSDictionary * _capability;
    NSString * _encryptionKey;
    
    void(^_mediaStremaingRequestFullscreenComplete)(BOOL isAllow);
    
    short _desireScreenNumber;
    short _desirePosition;
    
    CGSize s_sentScreenSize;
    BOOL _isLeftMouseDown;
    BOOL _isTouchDown;
    
    void (^_otaFirmwareDownloadProgressBlock)(NSInteger totoalBytes, NSInteger totalBytesWritten);
    void (^_otaFirmwareDownloadCompletionBlock)(AMJSONRPCResponse * response);
    
}
@property (strong, nonatomic) NSMutableSet * listeners;
@property (strong, nonatomic) NSMutableArray * HIDEventListeners;
@property (strong, nonatomic) AMJSONRPCDispatcher * jrpcDispatcher; //server
@property (strong, nonatomic) AMJSONRPCProxyClient * jrpcProxyClient; //proxy client
@property (strong, nonatomic) HTTPServer * otaFileServer;
@property (strong, nonatomic) NSURL * otaFirmwareURL;
@property (nonatomic, weak) id<AMDataTransmitterHostControlDelegate> hostControlDelegate;
@property (assign) TransmitterRole currentRole;
@end

@implementation AMGenericDeviceDataTransmitter
@synthesize connectedDevice = _connectedDevice;
@synthesize enableHIDControl = _enableHIDControl;
//@synthesize screenNumber = _screenNumber;
//@synthesize screenPosition = _screenPosition;

-(instancetype) init{
    self = [super init];
    if(self){
        self.listeners = [NSMutableSet new];
        self.HIDEventListeners = [NSMutableArray new];
        self.jrpcDispatcher = [[AMJSONRPCDispatcher alloc] init];
        self.jrpcProxyClient = [[AMJSONRPCProxyClient alloc] init];
        _enableHIDControl = NO;
        s_sentScreenSize = CGSizeZero;
        _isLeftMouseDown = NO;
        _isTouchDown = NO;
        _currentRole = TransmitterRoleNotDefined;
    }
    return self;
}

-(void) dealloc{
    [self stopWifiDisplay];
    [self disconnectRemoteDevice];
}

-(void) setHostControlDelegate:(id<AMDataTransmitterHostControlDelegate>)hostControlDelegate{
    if(_hostControlDelegate!=hostControlDelegate){
        _hostControlDelegate = hostControlDelegate;
        if([self.hostControlDelegate respondsToSelector:@selector(transmitterHostControlRoleDidChange:)]){
            [self.hostControlDelegate transmitterHostControlRoleDidChange:self];
        }
    }
}

-(void) _startWifi:(void (^)(void))success
          failure:(void (^)(NSError * error))failure
       disconnect:(void (^)(AMPrototypeDevice * disconnectedDevice))disconnect{
    if([_sender connectedProjector]){    //already connected, will call success block directly.
        if(success){
            success();
        }
    }
    else{
        _connectionSuccessBlock = success;
        _connectionFailureBlock = failure;
        _afterDisconnectBlock = disconnect;
        
        //if current role is guest
        if(self.isGuest){

            [self.jrpcProxyClient newRequest:kRemoteRequestStream param:nil generated:^(AMJSONRPCRequest *newRequest) {
                [self sendJSONRPCOBject:newRequest];
            } response:^(AMJSONRPCResponse *response, AMJSONRPCRequest * origRequest) {
                
//                DLog(@"Got response for %@", kRemoteRequestStream);
                NSString * result = (response.result)[kResult];
                if([result isEqualToString:kRemoteAllow]){
                    [self adaptToDPFSender];
                }
                else if([result isEqualToString:kRemoteDeny]){
                    
                    if([self.hostControlDelegate respondsToSelector:@selector(transmitterHostControlDenyFromHost:)]){
                        [self.hostControlDelegate transmitterHostControlDenyFromHost:self];
                    }
                    
                    _connectionFailureBlock = nil;
                    _connectionSuccessBlock = nil;
                    _afterDisconnectBlock = nil;
                }
                else if([result isEqualToString:kRemoteWaitForAnswer]){
                    if([self.hostControlDelegate respondsToSelector:@selector(transmitterHostControlWaitForResponse:)]){
                        [self.hostControlDelegate transmitterHostControlWaitForResponse:self];
                    }
                }
            }];
        }
        else{
            [self adaptToDPFSender];
            
//            if(self.isHIDControlEnable){
//                //2016/8/2 Send "common.enable_control"
//                [self sendHIDEnableToRemote];
//            }
        }
    }
    
}

-(void) cancelRequest{
    _connectionSuccessBlock = nil;
    _connectionFailureBlock = nil;
    _afterDisconnectBlock = nil;
    [self.jrpcProxyClient newRequest:kRemoteCancelRequestStream param:nil generated:^(AMJSONRPCRequest *newRequest) {
        [self sendJSONRPCOBject:newRequest];
    } response:^(AMJSONRPCResponse *response, AMJSONRPCRequest *origRequest) {
        DLog(@"kRemoteCancelRequestStream response");
    }];
}

- (void)adaptToDPFSender {
    if ( _sender == nil) {
        _sender = [[AMDPFImageSender alloc] init];
        _sender.applicationDelegate = self;
        _sender.mediaStreamingDelegate = self;
        
        CGSize deviceRes = [(id<AMPrototypeImageStreamingProtocol>)self.connectedDevice getMaxResolution];
        NSLog(@"projector maxResolution, width = %f, height = %f", deviceRes.width, deviceRes.height);
        
        IPAddressProjectorScanner * s = [[IPAddressProjectorScanner alloc] init];
        AMProjectorDevice * projector = [s seekProjector:self.connectedDevice.ipAddress withTimeout:5];
        if(projector==nil){
            DLog(@"Unable to connect to projector service[self.connectedDevice.ipAddress]");
            _sender = nil;
            return;
        }
        [_sender connectToProjector:projector];
    }
}


-(void) _stopWifiDisplay{
    _sender.applicationDelegate = nil;
    _sender.mediaStreamingDelegate = nil;
    [_sender stopMediaStreaming];
    [_sender disconnect];
    s_sentScreenSize = CGSizeZero;
    _sender = nil;
    // due to stopWifi want to be sync, it clear delegate, we have to call projectorDidDisconnect by ourselves
    [self projectorDidDisconnect:nil error:nil];
}

-(void) _sendImageWithData:(NSData *)imageData{
    [_sender sendImageWithData:imageData];
}

-(BOOL)readyToSendH264{
    if (_sender == nil ){
        return NO;
    }
    return _sender.readyToSendImage;
}
-(BOOL)readyToSendPCMData{
    if (_sender == nil ){
        return NO;
    }
    return _sender.readyToSendAudio;
}

-(void) _sendH264Data:(NSData*)data width:(uint32_t)width height:(uint32_t)height{
    s_sentScreenSize = CGSizeMake(width, height);
    [_sender sendH264Data:data width:width height:height];
}

-(void) _sendPCMData:(NSData*) pcmData{
    [_sender sendPCMData:pcmData];
}

- (void)mediaStreaming:(NSURL*)subtitleURL isVideo:(BOOL)isVideo
{
    if (isVideo) {
        //if subtitle exists, upload it first
        
        if (subtitleURL) {
            AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
            manager.responseSerializer = [AFHTTPResponseSerializer serializer];
            NSMutableSet* set = [NSMutableSet setWithSet:manager.responseSerializer.acceptableContentTypes];
            [set addObjectsFromArray:@[ @"text/html", @"text/plain" ]];
            manager.responseSerializer.acceptableContentTypes = set;
            [manager POST:[NSString stringWithFormat:@"%@cgi-bin/upload.cgi", [(AMGenericDevice*)self.connectedDevice remoteHost].webRootHost]
               parameters:nil
constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    [formData appendPartWithFileData:[NSData dataWithContentsOfURL:subtitleURL] name:@"fileName" fileName:[@"ezsubtitle" stringByAppendingPathExtension:[subtitleURL pathExtension]] mimeType:@"text/plain"];
}
                  success:^(AFHTTPRequestOperation* operation, id responseObject) {
                      DLog(@"SUCCESS:%@",[operation description]);
                      [_sender requestForHttpMediaStreaming];
                  }
                  failure:^(AFHTTPRequestOperation* operation, NSError* error) {
                      DLog(@"FAIL:%@",[operation description]);
                      [_sender requestForHttpMediaStreaming];
                  }];
        }
        else {
            [_sender requestForHttpMediaStreaming];
        }
    }
    else {
        [_sender requestForHttpMediaStreaming];
    }
}

#pragma mark -
-(void) _stopPlayingMedia:(MediaStreamingEnd)endBlock{
    [_sender stopMediaStreaming];
    usleep(500000); //cause there is no mechanism letting App knows about the stop process has completed. I have to keep this sleep.
    [self.state onMediaStreamingStop];
    if (endBlock) {
        NSError *error = nil;
        NSDictionary *userInfo = @{ NSLocalizedDescriptionKey: NSLocalizedStringFromTable(@"User canceled media streaming process", @"AMCommon", "DPFSender - User canceled media streaming process") };
        error = [NSError errorWithDomain:kAMMediaStreamingStandardErrorDomain code:NSUserCancelledError userInfo:userInfo];
        endBlock(_mediaURL, error);
    }
}

-(void) _seekToTime:(NSTimeInterval)seekTime{
    [_sender seekToTime:seekTime];
}

-(void) _playVideoMedia:(NSURL *)mediaURL subtitle:(NSURL *)subURL userInfo:(id)userInfo{
    [self play:mediaURL subtitle:subURL isVideo:YES];
}

-(void) _playAudioMeida:(NSURL *)mediaURL userInfo:(id)userInfo{
    [self play:mediaURL subtitle:nil isVideo:NO];
}

-(void) play:(NSURL*)url subtitle:(NSURL *)subURL isVideo:(BOOL)isVideo{
    __weak AMGenericDeviceDataTransmitter * weakself = self;
    //host control v2, and it is not fullscreen, issue a reuqest for full screen
    if(self.conferenceController.version.integerValue == 2 && _screenNumber>1){
        
        [weakself requestFullscreen:NO complete:^(BOOL isAllow) {
            if(isAllow){
                [weakself startWifi:^{
                    _mediaURL = url;
                    [weakself mediaStreaming:isVideo?subURL:nil isVideo:isVideo];
                } failure:^(NSError *error) {
                    NSLog(@"Fail to start media streaming(wifi display service is unable to launch)");
                } disconnect:nil];
            }
            else{
                [weakself stopPlayingMedia:nil];
            }
        }];
    }
    else{   // host control v1, no need to issue a fullscreen request
        [self startWifi:^{
            _mediaURL = url;
            [weakself mediaStreaming:isVideo?subURL:nil isVideo:isVideo];
        } failure:^(NSError *error) {
            NSLog(@"Fail to start media streaming(wifi display service is unable to launch)");
        } disconnect:nil];
    }
}


-(void) _pausePlayingMedia{
    [_sender pauseMediaStreaming];
    if(self.mediaSteamingPause){
        self.mediaSteamingPause();
    }
}

-(void) _resumePlayingMedia{
    [_sender resumeMediaStreaming];
    if(self.mediaSteamingResume){
        self.mediaSteamingResume();
    }
}
-(void) volumeUp{
     [_sender volumnUp];
    if(self.volumeChange)
    {
        self.volumeChange(-1.0);
    }
}
-(void) volumeDown{
    [_sender volumnDown];
    if(self.volumeChange)
    {
        self.volumeChange(-1.0);
    }
}
-(float) volume{
    return -1;
}
#pragma mark -

-(BOOL)isFullscreen{
    if(self.screenNumber == 1 && self.screenPosition ==1){
        return YES;
    }
    return NO;
}

-(void) setSplitControlWithScreenNumber:(uint16_t)screenNumber
                               position:(uint16_t)screenPosition{
    [_sender requestForNewSplit:screenNumber newPosition:screenPosition];
}

-(void) registerSplitScreenPauseBlock:(void(^)(void))pauseBlock{
    _splitScreenPauseBlock = pauseBlock;
}

-(void) registerSplitScreenResumeBlock:(void(^)(void))resumeBlock{
    _splitScreenResumeBlock = resumeBlock;
}

-(void) disconnectAllWifiDisplay{
   [self.jrpcProxyClient newRequest:kRemoteDisconnectAllWifiDisplay param:nil generated:^(AMJSONRPCRequest *newRequest) {
       [self sendJSONRPCOBject:newRequest];
   } response:^(AMJSONRPCResponse *response, AMJSONRPCRequest *origRequest) {
       
   }];
}

-(void) registerBlockToJRPCDispatcher{
    
    __weak AMGenericDeviceDataTransmitter * weakSelf = self;
    AMGenericDevice * connected = (AMGenericDevice*)self.connectedDevice;
    //Register kRemoteAssignRole
    [self.jrpcDispatcher registerRequest:kRemoteAssignRole operation:^AMJSONRPCResponse *(AMJSONRPCRequest *request, id context) {
        NSDictionary * params = request.parameters;
        AMJSONRPCResponse * responseJRPC = [[AMJSONRPCResponse alloc] initWithResult:@0 error:nil aId:request.aId];
        [self sendJSONRPCOBject:responseJRPC];
        
        self.currentRole = STRING_TO_ROLE(params[kRemoteRole]);
        
        NSDictionary * param = @{kRemoteRole: params[kRemoteRole], kRemoteClientOS: connected.OSStringForRetrivingBitmaskDictionary, kRemoteCountry: @"old_compability", @"language": [[NSLocale preferredLanguages] firstObject] };
        
        [self.jrpcProxyClient newRequest:kRemoteGetService param:param generated:^(AMJSONRPCRequest *newRequest) {
            [self sendJSONRPCOBject:newRequest];
        } response:^(AMJSONRPCResponse *response, AMJSONRPCRequest *origRequest) {
            
            
            if([response.result isKindOfClass:[NSDictionary class]]){
                
                //inject bookmark for some reason. 2015.9.23. brian
                /*
                 
                 補充一點, 為了向前相容, “bookmark” 如果沒出現的話, UI 上還是要出現. bookmark 是 enable 就顯示, bookmark 是 disable 就不顯示.
                 
                 http://wiki.actions-micro.com/tiki/tiki-index.php?page=%E5%8A%9F%E8%83%BD%E9%96%8B%E9%97%9C
                 
                 
                 From: Jesse Cheng [mailto:jccheng@actions-micro.com]
                 Sent: Monday, September 21, 2015 12:18 PM
                 To: 尤靖富
                 Cc: Brian Liu; chaochyilai@actions-micro.com; 陳家豪; 史訓綱; 陳思裕; '李英瑞'; darrelllao@actions-micro.com
                 Subject: bookmark icon on/off
                 
                 Dear Richard,
                 
                 目前 web/cloud video 的bookmark icon希望能夠從小機來控制 app 的 bookmark icon 要不要顯示出來.
                 
                 預計是加一個 “bookmark” 的功能開關.
                 
                 Acer 可以再出一版 firmware 加上這個東西嗎?
                 
                 */
                if(response.result[kRemoteServiceBookmark]==nil){
                    NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)response.result];
                    dict[kRemoteServiceBookmark] = kRemoteServiceEnable;
                    connected.bitmaskDictionary = dict;
                }
                else{
                    connected.bitmaskDictionary = (NSDictionary*)response.result;
                }
                DLog(@"%@: result:%@", kRemoteGetService, response.result);
                
                if([connected isEZCastProDevice]){  //ezcast pro 2.0 spec
                    [AMDemoDevice saveOfflineBitmask:response.result];
                }
            }
            else{
                NSException * ec = [NSException exceptionWithName:@"AMGenericDeviceDataTransmitterException" reason:[NSString stringWithFormat:@"Receiving error service dictionary from remote device(%@)", self.connectedDevice.displayName] userInfo:response.result];
                [ec raise];
            }
            
            DLog(@"transmitter[%@] role:%@", self,ROLE_TO_STRING(self.currentRole));
            if([self.hostControlDelegate respondsToSelector:@selector(transmitterHostControlRoleDidChange:)]){
                [self.hostControlDelegate transmitterHostControlRoleDidChange:self];
            }
            
            if(_remoteConnectSuccessBlock){
                _remoteConnectSuccessBlock(self.connectedDevice);
                _remoteConnectSuccessBlock = nil;
            }
            [self willChangeValueForKey:@"connectedDevice"];
            [self didChangeValueForKey:@"connectedDevice"];
        }];
        return JSONRPC_OK_RESPONSE;
    }];
    
    // kRemoteAskRequestStream
    [self.jrpcDispatcher registerRequest:kRemoteAskRequestStream operation:^AMJSONRPCResponse *(AMJSONRPCRequest *request, id context) {
        NSDictionary * params = request.parameters;
        
        NSString * clientIP = params[kRemoteIPAddress];
        NSString * clientName = params[kRemoteHostname];
        void(^completionBlock)(BOOL, HostControlSplitType) = ^(BOOL isAllow, HostControlSplitType type){
            NSDictionary * resultDict = @{
                                          kResult: isAllow ? (type==HostControlSplitTypeShareScreen)?kRemoteAllow:kRemoteAllowFullscreen: kRemoteDeny,
                                          kRemoteIPAddress : (request.parameters)[kRemoteIPAddress]
                                          };
            [self.jrpcProxyClient newRequest:kRemoteAnswerRequestStream param:resultDict generated:^(AMJSONRPCRequest *newRequest) {
                [self sendJSONRPCOBject:newRequest];
            } response:^(AMJSONRPCResponse *response, AMJSONRPCRequest * origRequest) {
                //do nothing with {"jsonrpc": "2.0", "result": 0, "id": 1}
            }];
        };
        
        BOOL wait = NO;
        
        if([self.hostControlDelegate respondsToSelector:@selector(transmitterHostControlDidReceiveAskRequestStream:fromGuest:name:completion:)]){
            wait = [self.hostControlDelegate transmitterHostControlDidReceiveAskRequestStream:self fromGuest:clientIP name:clientName completion:completionBlock];
        }
        
        if(wait){   //according to spec. send wait back
            NSDictionary * resultDict = @{ kResult: kRemoteWaitForAnswer,
                                           kRemoteIPAddress : params[kRemoteIPAddress]
                                           };
            AMJSONRPCResponse * response = [[AMJSONRPCResponse alloc] initWithResult:resultDict error:nil aId:request.aId];
            [self sendJSONRPCOBject:response];
        }
        
        return JSONRPC_OK_RESPONSE;
    }];
    
    // kRemoteAskRequestStreamFullscreen, host control v2
    [self.jrpcDispatcher registerRequest:kRemoteAskRequestStreamFullscreen operation:^AMJSONRPCResponse *(AMJSONRPCRequest *request, id context) {
        NSDictionary * params = request.parameters;
        {
            NSString * clientIP = params[kRemoteIPAddress];
            NSString * clientName = params[kRemoteHostname];
            void(^completionBlock)(BOOL, HostControlSplitType) = ^(BOOL isAllow, HostControlSplitType type){
                //In this case, we transform any allow(fullscreen/share) to just 'allow', because there is no difference.
                NSDictionary * resultDict = @{
                                              kResult: isAllow ? kRemoteAllow: kRemoteDeny,
                                              kRemoteIPAddress : (request.parameters)[kRemoteIPAddress]
                                              };
                [self.jrpcProxyClient newRequest:kRemoteAnswerRequestStreamFullscreen param:resultDict generated:^(AMJSONRPCRequest *newRequest) {
                    [self sendJSONRPCOBject:newRequest];
                } response:^(AMJSONRPCResponse *response, AMJSONRPCRequest * origRequest) {

                }];
            };
            
            if([self.hostControlDelegate respondsToSelector:@selector(hostControlDidReceiveAskFullscreen:fromGuest:name:completion:)]){
                [self.hostControlDelegate hostControlDidReceiveAskFullscreen:self fromGuest:clientIP name:clientName completion:completionBlock];
            }
        }
        return JSONRPC_OK_RESPONSE;
    }];

    
    //kremoteCancelRequestStream
    [self.jrpcDispatcher registerRequest:kRemoteCancelRequestStream operation:^AMJSONRPCResponse *(AMJSONRPCRequest *request, id context) {
        NSDictionary * params = request.parameters;
        
        if([self.hostControlDelegate respondsToSelector:@selector(transmitterHostControlDidReceiveCancelRequestStream:fromGuest:)]){
            [self.hostControlDelegate transmitterHostControlDidReceiveCancelRequestStream:self fromGuest:params[kRemoteIPAddress]];
        }
        
        AMJSONRPCObject * responseJRPC = [[AMJSONRPCResponse alloc] initWithResult:@0 error:nil aId:request.aId];
        [self sendJSONRPCOBject:responseJRPC];
        return JSONRPC_OK_RESPONSE;
    }];
    
    
    //kRemoteAnswerRequestStream
    [self.jrpcDispatcher registerRequest:kRemoteAnswerRequestStream operation:^AMJSONRPCResponse *(AMJSONRPCRequest *request, id context) {
        NSDictionary * params = request.parameters;
        BOOL isAllow = [params[kResult] isEqualToString:kRemoteAllow];
        if(isAllow){
            if([self.hostControlDelegate respondsToSelector:@selector(transmitterHostControlHostDidAcceptRequest:)]){
                [self.hostControlDelegate transmitterHostControlHostDidAcceptRequest:self];
            }
            
            //Host allow me(guest) to project
            [self adaptToDPFSender];
            AMJSONRPCObject * responseJRPC = [[AMJSONRPCResponse alloc] initWithResult:@0 error:nil aId:request.aId];
            [self sendJSONRPCOBject:responseJRPC];
        }
        else{
            if([self.hostControlDelegate respondsToSelector:@selector(transmitterHostControlDenyFromHost:)]){
                [self.hostControlDelegate transmitterHostControlDenyFromHost:self];
            }
            
            _connectionFailureBlock = nil;
            _connectionSuccessBlock = nil;
            _afterDisconnectBlock = nil;
        }
        return JSONRPC_OK_RESPONSE;
    }];
    
    //kRemoteAnswerRequestStreamFullscreen, host control v2, we got a answer fullscreen request from host.
    [self.jrpcDispatcher registerRequest:kRemoteAnswerRequestStreamFullscreen operation:^AMJSONRPCResponse *(AMJSONRPCRequest *request, id context) {
        BOOL isAllow = [request.parameters[kResult] isEqualToString:kRemoteAllow];
        if(_mediaStremaingRequestFullscreenComplete)
        {
            _mediaStremaingRequestFullscreenComplete(isAllow);
            _mediaStremaingRequestFullscreenComplete = nil;
        }
        
        if(!isAllow){
            if([self.hostControlDelegate respondsToSelector:@selector(transmitterHostControlDenyFromHost:)]){
                [self.hostControlDelegate transmitterHostControlDenyFromHost:self];
            }
        }
        else{
            if([self.hostControlDelegate respondsToSelector:@selector(transmitterHostControlHostDidAcceptRequest:)]){
                [self.hostControlDelegate transmitterHostControlHostDidAcceptRequest:self];
            }
        }
        return JSONRPC_OK_RESPONSE;
    }];
    
    
    [self.jrpcDispatcher registerNotification:kRemoteDisconnect operation:^(AMJSONRPCNotification *notification, id context) {
        if([self.hostControlDelegate respondsToSelector:@selector(transmitterHostControlDidAskedToDisconnect:)]){
            [self.hostControlDelegate transmitterHostControlDidAskedToDisconnect:self];
        }
    }];
    
    [self.jrpcDispatcher registerNotification:kRemoteJrpcParseError operation:^(AMJSONRPCNotification *notification, id context) {
        if([self.hostControlDelegate respondsToSelector:@selector(transmitterHostControlDidParseJRPCMessageFailed:error:)]){
            NSError * error = nil;
            NSDictionary * errorDic = notification.parameters;
            if(errorDic) {
                error = [NSError errorWithDomain:errorDic[@"domain"] code:(NSInteger)errorDic[@"code"] userInfo:@{NSLocalizedDescriptionKey:errorDic[@"message"]}];
            }
            [self.hostControlDelegate transmitterHostControlDidParseJRPCMessageFailed:self error:error];
        }
    }];
    
    
    [self.jrpcDispatcher registerRequest:kRemoteGetDeviceDescription operation:^AMJSONRPCResponse *(AMJSONRPCRequest *request, id context) {
        NSDictionary * result = @{kCapability:@{kHostControl:@{kVersion:@1}}, kRemoteHostname:[self deviceNameString]!=nil?[self deviceNameString]:@"Device"};
        AMJSONRPCResponse * response = [[AMJSONRPCResponse alloc] initWithResult:result error:nil aId:request.aId];
        [self sendJSONRPCOBject:response];
        return JSONRPC_OK_RESPONSE;
    }];
    
    //v2 spec. After receiving a common.waiting_request_stream command, start wifi display connection and send a start image.
    [self.jrpcDispatcher registerRequest:kRemoteWaitRequestStream operation:^AMJSONRPCResponse *(AMJSONRPCRequest *request, id context) {
        _desireScreenNumber = (short) ((NSNumber*)request.parameters[@"splitnum"]).integerValue;
        _desirePosition = (short) ((NSNumber*)request.parameters[@"position"]).integerValue;
        
        if([self.hostControlDelegate respondsToSelector:@selector(hostControlDidAskedToAutoWifiDisplay:)]){
            NSData *  imagedata = [self.hostControlDelegate hostControlDidAskedToAutoWifiDisplay:self];
            
            _connectionSuccessBlock = ^{
                [weakSelf sendImageWithData:imagedata];
            };
            
            _afterDisconnectBlock = ^(AMPrototypeDevice * device){
                if([weakSelf.hostControlDelegate respondsToSelector:@selector(hostControlDidAskedToAutoWifiDisplay:)]){
                    [weakSelf.hostControlDelegate hostControlAutoWifiDisplayDidDisconnected:weakSelf];
                }
            };
            
            [self adaptToDPFSender];
        }
        return JSONRPC_OK_RESPONSE;
    }];
    
    [self.jrpcDispatcher registerRequest:kRemoteHIDDetect operation:^AMJSONRPCResponse *(AMJSONRPCRequest *request, id context) {
//        DLog(@"%@", request);
        NSString * mouseEvent = (NSString *) request.parameters[@"mouse"];
        if([mouseEvent isEqualToString:@"plugin"]){
            for (NSValue * value in self.HIDEventListeners) {
                id<GenericDeviceHIDEventListener> listener = (id<GenericDeviceHIDEventListener>) [value nonretainedObjectValue];
                if([listener respondsToSelector:@selector(mousePluggedIn)]){
                    [listener mousePluggedIn];
                }
            }
        }else if([mouseEvent isEqualToString:@"plugout"]){
            _isLeftMouseDown = NO;
            _isTouchDown = NO;
            for (NSValue * value in self.HIDEventListeners) {
                id<GenericDeviceHIDEventListener> listener = (id<GenericDeviceHIDEventListener>) [value nonretainedObjectValue];
                if([listener respondsToSelector:@selector(mousePluggedOut)]){
                    [listener mousePluggedOut];
                }
            }
        }
        [self sendHIDEnableToRemote];
        return JSONRPC_OK_RESPONSE;
    }];

    
    //register to receive dongle mouse event
    [self.jrpcDispatcher registerRequest:kRemoteHIDSendMouse operation:^AMJSONRPCResponse *(AMJSONRPCRequest *request, id context) {
        DLog(@"received \"%@\" event", kRemoteHIDSendMouse);
        [weakSelf processHIDEvent:request];
        AMJSONRPCObject * responseJRPC = [[AMJSONRPCResponse alloc] initWithResult:@0 error:nil aId:request.aId];
        [self sendJSONRPCOBject:responseJRPC];
        return JSONRPC_OK_RESPONSE;
    }];
    
    //register to receive dongle touch event
    [self.jrpcDispatcher registerRequest:kRemoteHIDSendControl operation:^AMJSONRPCResponse *(AMJSONRPCRequest *request, id context) {
        DLog(@"received \"%@\" event", kRemoteHIDSendControl);
        [weakSelf processHIDEvent:request];
        AMJSONRPCObject * responseJRPC = [[AMJSONRPCResponse alloc] initWithResult:@0 error:nil aId:request.aId];
        [self sendJSONRPCOBject:responseJRPC];
        return JSONRPC_OK_RESPONSE;
    }];
    
    [self.jrpcDispatcher registerRequest:kRemoteQueryAppHTTPProxyInfo operation:^AMJSONRPCResponse *(AMJSONRPCRequest *request, id context) {
        DLog(@"[HTTP Proxy] received \"%@\" event", kRemoteQueryAppHTTPProxyInfo);
        
        if( weakSelf.HTTPProxyRequestActivationBlock){
            dispatch_queue_t queue = (weakSelf.HTTPProxyRequestCallbackQueue != nil) ? weakSelf.HTTPProxyRequestCallbackQueue : dispatch_get_main_queue();
            dispatch_async(queue, ^{
                uint16_t port = weakSelf.HTTPProxyRequestActivationBlock(YES);
                DLog(@"[HTTP Proxy] returning port : %zd", port);
                AMJSONRPCObject * responseJRPC = [[AMJSONRPCResponse alloc] initWithResult:@{@"AppHTTPProxyPort":@(port)} error:nil aId:request.aId];
                [weakSelf sendJSONRPCOBject:responseJRPC];
            });
        }
        else{
            AMJSONRPCObject * responseJRPC = [AMJSONRPCResponse errorMethodNotFound:request.aId];
            [weakSelf sendJSONRPCOBject:responseJRPC];
        }

        return JSONRPC_OK_RESPONSE;
    }];

}



-(void) setupHeartbeat{
    if(!_heartbeatTimer){
        dispatch_time_t now = dispatch_walltime(DISPATCH_TIME_NOW, 0);
        _heartbeatTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
        dispatch_source_set_timer(_heartbeatTimer, now, NSEC_PER_SEC*1ull, 0ull);
        __weak AMGenericDeviceDataTransmitter * weakSelf = self;
        dispatch_source_set_event_handler(_heartbeatTimer, ^{
            AMJSONRPCNotification * noti = [[AMJSONRPCNotification alloc] initWithNotification:kRemoteHeartbeat params:nil];
            [weakSelf sendJSONRPCOBject:noti];
        });
        dispatch_resume(_heartbeatTimer);
    }
}

-(BOOL) isWIFIDisplayOn{
    return [_sender connectedProjector]!=nil;
}


-(void) uploadLanguageToRemote:(AMPrototypeDevice *)host{
    if([host isKindOfClass:[AMGenericDevice class]] == NO){
        return;
    }
    
    //get ISO 8601 date string
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    NSDate *now = [NSDate date];
    NSString *iso8601String = [dateFormatter stringFromDate:now];
    
    NSDictionary * infoDict = @{ @"schema_version":@"1", @"language": [[NSLocale preferredLanguages] firstObject], @"time": iso8601String};
    
    AFHTTPRequestOperationManager * manager = [AFHTTPRequestOperationManager manager];
    AFHTTPResponseSerializer * serializer = [[AFHTTPResponseSerializer alloc] init];
    serializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"text/html", nil];
    manager.responseSerializer = serializer;
    [manager POST:[NSString stringWithFormat:@"%@cgi-bin/upload.cgi",[(AMGenericDevice *)host remoteHost].webRootHost]
       parameters:nil
constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:infoDict options:kNilOptions error:nil];
    [formData appendPartWithFileData:jsonData name:@"fileName" fileName:@"appInfo.json" mimeType:@"application/json"];
} success:^(AFHTTPRequestOperation *operation, id responseObject) {
    DLog(@"%s upload DONE",__PRETTY_FUNCTION__);
} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    DLog(@"%s upload FAIL",__PRETTY_FUNCTION__);
}];
}

#pragma mark - AMRemoteDataTransmitterProtocol

-(void) connectToRemoteDevice:(AMPrototypeDevice *)device
                      success:(void (^)(AMPrototypeDevice *))successBlock
                      failure:(void (^)(NSError *, AMPrototypeDevice *))failureBlock{
    
    NSAssert(device != nil , @"Do not pass a nil device to me...");
    
    if(self.connectedDevice){
        [self stopPlayingMedia:nil];
        [self stopWifiDisplay];
        [self disconnectRemoteDevice];
    }
    _connectedDevice = device;    //we have to avoid triggering KVO at this moment.
    __weak AMGenericDeviceDataTransmitter * weakself =  self;
    [((AMGenericDevice*)self.connectedDevice).remoteHost establishTCPConnection:^(BOOL connected, NSError *error) {
        if(connected == NO){
            failureBlock(error, device); //the passed in remote deive is nil.
            _connectedDevice = nil;
            return;
        }
        _desireScreenNumber = 0;
        _desirePosition = 0;
        [weakself registerBlockToJRPCDispatcher];
        [((AMGenericDevice*)weakself.connectedDevice).remoteHost addMessageListener:weakself];
        [weakself uploadLanguageToRemote:device];
        //New Protocol, need to send set_device_description first.
        if(([weakself.connectedDevice serviceBitMask] & SERVICE_HOST_CONTROL)!=0){
//            DLog(@"[HOSTCONTROLABLE DEVICE] ----> HOST_CONTROL_ON");
            [weakself setupHeartbeat];  //send remote control channel heartbeat only when the remote device support host control.
            _remoteConnectSuccessBlock = successBlock;
            [weakself.jrpcProxyClient newRequest:kRemoteSetDeviceDescription
                                           param:@{kCapability:@{kHostControl: @{kVersion:@2}},//app host control version 2
                                                   kRemoteHostname: ([weakself deviceNameString]!=nil)?[weakself deviceNameString]:@"Device"}
                                       generated:^(AMJSONRPCRequest *newRequest) {
                                           [weakself sendJSONRPCOBject:newRequest];
                                       }
                                        response:^(AMJSONRPCResponse *response, AMJSONRPCRequest * origRequest) {
                                            if([response.result isKindOfClass:[NSDictionary class]]){
                                                DLog(@"Remote Device HostControl version:2");
                                                _capability = response.result[kCapability];
                                                id key = response.result[kEncryptionKey];
                                                if([key isKindOfClass:[NSString class]]){
                                                    [((AMGenericDevice*)weakself.connectedDevice).remoteHost setJrpcEncryptionKey:key];
                                                }
                                            }
                                            else if([((NSNumber*)response.result) isEqualToNumber:@(0)]){
                                                DLog(@"Remote Device HostControl version:1");
                                                DLog(@"The remote device accept %@, we are going to wait for a role assignment command", kRemoteSetDeviceDescription);
                                            }
                                        }];
        }
        /**  OLD protocol, direct go through */
        else{
            DLog(@"[LEGACY DEVICE] ----> HOST_CONTROL_OFF");
            if(successBlock){
                successBlock(weakself.connectedDevice);
                [weakself willChangeValueForKey:@"connectedDevice"];
                [weakself didChangeValueForKey:@"connectedDevice"];
            }
        }
    }];
}


-(void) disconnectRemoteDevice{
    [self stopPlayingMedia:nil];
    [self stopWifiDisplay];
    if(_heartbeatTimer){
        dispatch_source_cancel(_heartbeatTimer);
        _heartbeatTimer = nil;
    }
    AMGenericDevice * device = (AMGenericDevice*)self.connectedDevice;
    [device.remoteHost close];
    self.connectedDevice = nil;
    [self.jrpcDispatcher clearAllRegisteredBlock];
}

- (BOOL)sendKey:(AMRemoteHostStandardKeyCode)keyCode{
    if(self.isHost){
        [((AMGenericDevice*)self.connectedDevice).remoteHost sendKey:keyCode];
        return YES;
    }
    return NO;
}
- (void)sendVendorKey:(NSInteger)keyCode{
    [((AMGenericDevice*)self.connectedDevice).remoteHost sendVendorKey:keyCode];
}

-(void) sendJSONRPCOBject:(AMJSONRPCObject *)jrpcObject{
    [self sendJSONRPCMessage:jrpcObject.JSONRPCMessage];
}

-(void) sendJSONRPCMessage:(NSString *)jrpcMsg{
    [((AMGenericDevice*)self.connectedDevice).remoteHost sendJSONRPCMessage:jrpcMsg encrypted:YES];
}

- (void)addMessageListener:(id<AMRemoteHostMessageListener>)listener{
    [self.listeners addObject:[NSValue valueWithNonretainedObject:listener]];
    
}
- (void)removeMessageListener:(id<AMRemoteHostMessageListener>)listener{
    [self.listeners removeObject:[NSValue valueWithNonretainedObject:listener]];
}

//Receive message from remost host, we should pass to our own listeners
- (void)didReceiveMessage:(NSString *)message fromRemoteHost:(AMRemoteHost *)remoteHost{
    [self dispatchMessage:message success:nil failure:nil];
    
    //after notifiy all listeners about the disconnection, disconnect wifi and remote connection.
    if([message isEqualToString:kAMRemoteHostMessageTCPDisconnected]){
        [self disconnectRemoteDevice];
    }
}

- (void)didReceiveJsonrpcMessage:(NSString *)jrpcMsg jsonrpcObject:(AMJSONRPCObject*)jrpcObject fromRemoteHost:(AMRemoteHost *)remoteHost{
    if(((AMGenericDevice*)self.connectedDevice).remoteHost){
        //The incoming JSONRPC is a request.
        if([jrpcObject isKindOfClass:[AMJSONRPCRequest class]]){
            if([jrpcObject isKindOfClass:[AMJSONRPCNotification class]]){  //notification is kind of special request
                [self.jrpcDispatcher processNotification:(AMJSONRPCNotification *)jrpcObject context:nil];
            }else{
                if([self.jrpcDispatcher processRequest:(AMJSONRPCRequest *)jrpcObject context:nil] == nil){
                    AMJSONRPCObject * responseJRPC = [AMJSONRPCResponse errorMethodNotFound:jrpcObject.aId];
                    [self sendJSONRPCOBject:responseJRPC];
                }
            }
        }
        else if([jrpcObject isKindOfClass:[AMJSONRPCResponse class]]){  //The incoming JSONRPC is a result(response). Find out original JSONRPC request
            AMJSONRPCResponse * jrpcResponse = (AMJSONRPCResponse *) jrpcObject;
            [self.jrpcProxyClient receiveResponse:jrpcResponse];
        }
    }
}

- (void)dispatchMessage:(NSString *)message success:(void (^)(void))success failure:(void (^)(NSError *))failure{
    if(((AMGenericDevice*)self.connectedDevice).remoteHost){
        for (NSValue *listener in [_listeners allObjects]) {
            [[listener nonretainedObjectValue] didReceiveMessage:message fromRemoteHost:((AMGenericDevice*)self.connectedDevice).remoteHost];
        }
        if(success!=nil){
            success();
        }
    }
    else if(failure!=nil){
        failure(nil);
    }
    
}

#pragma mark - AMDataTransmitter_OTA
/**
 *  Send kRemoteGetDeviceOTADescription JRPC Msg
 Request
 {
	"jsonrpc":	"2.0",
	"method":	" common.get_ota_config "
	"id":	1
 }
 
 Response
 {
	"jsonrpc":	"2.0",
	"result":	{
 "content":	{
 "version":      1,
 "vendor":       "ezwire",
 "mac_address":  "0C090A87",
 "softap_ssid":  "EZWire-0C090A87",
 "firmware_version":     "15478000"
 }
	},
	"id":	6
 }
 
 *  @param completion A Completion block whitch brings back json response
 */
-(void) getDeviceOTADescription:(void (^)(AMJSONRPCResponse *))completion {
    [self.jrpcProxyClient newRequest:kRemoteGetDeviceOTADescription
                               param:nil
                           generated:^(AMJSONRPCRequest* newRequest) {
                               [self sendJSONRPCOBject:newRequest];
                           }
                            response:^(AMJSONRPCResponse* response, AMJSONRPCRequest* origRequest) {
                                completion(response);
                            }];
}
/**
 *  get OTA info from device > send OTA info to Server > get another OTA info > compare it, if device is update to date returns nil ,else returns server version string
 */
-(id) getOTAUpdateVersion:(NSURL *)otaSiteURL {
    return [self getOTAUpdateVersion:otaSiteURL completion:nil];
}
-(id) getOTAUpdateVersion:(NSURL *)otaSiteURL completion:(void(^)(BOOL enforce, NSString * version, NSURL * firmwareURL, NSError * error))completion {
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    __block id result = nil;
    __block NSString * otaConfURLString = nil;
    __block NSString * otaFirmwareURLString = nil;
    __block NSNumber * enforce = [NSNumber numberWithBool:NO];
    
    NSString * errorDomain = @"kOTAUpdateErrorDomain";
    [self getDeviceOTADescription:^(AMJSONRPCResponse* response) {
        //        DLog(@"response from device:%@", response);
        NSURL* url = otaSiteURL;
        NSURLSession* session = [NSURLSession sharedSession];
        NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
        /**
         {
         "vendor":"ezwire",
         "firmware_version":"15489000",
         "softap_ssid":"EZWire-0C090A87",
         "mac_address":"0C090A87",
         "version":1
         }
         */
        NSDictionary * deviceOTAJson = response.result[@"content"];
        
        if(deviceOTAJson == nil) {
            result = [NSError errorWithDomain:errorDomain code:1 userInfo:@{NSLocalizedDescriptionKey:@"Cannot get OTA content"}];
            dispatch_semaphore_signal(semaphore);
        } else if ([NSJSONSerialization isValidJSONObject:deviceOTAJson] == NO) {
            result = [NSError errorWithDomain:errorDomain code:2 userInfo:@{NSLocalizedDescriptionKey:@"Invalid JSON"}];
            dispatch_semaphore_signal(semaphore);
        } else {
            NSError * postJsonErrr = nil;
            NSData* data = [NSJSONSerialization dataWithJSONObject:deviceOTAJson options:0 error:&postJsonErrr];
            if (postJsonErrr) {
                result = [NSError errorWithDomain:errorDomain code:3 userInfo:@{NSLocalizedDescriptionKey:postJsonErrr.localizedDescription}];
                dispatch_semaphore_signal(semaphore);
            } else {
                [request setHTTPMethod:@"POST"];
                [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
                
                NSURLSessionUploadTask* task = [session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData* _Nullable data, NSURLResponse* _Nullable response, NSError* _Nullable error) {
                    //            DLog(@"response from php:%@/n%@/n%@",data,response,error);
                    if(data){
                        /**
                         {
                         "ota_conf_file" = "http://www.iezvu.com/upgrade/ezwire/ezwire_test.conf";
                         "ota_enforce" = 0;
                         "ota_fw_file" = "http://www.iezvu.com/upgrade/ezwire/ezwire_test.gz";
                         }
                         */
                        
                        NSError * error = nil;
                        NSDictionary * responseJson = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                        if(error) {
                            result = error;
                        } else {
                            DLog(@"%@",responseJson);
                            otaConfURLString = responseJson[@"ota_conf_file"];
                            otaFirmwareURLString = responseJson[@"ota_fw_file"];
                            if (responseJson[@"ota_enforce"]) {
                                enforce = responseJson[@"ota_enforce"];
                            }
                            if(otaConfURLString.length > 0 && otaFirmwareURLString.length > 0){
                                NSURL * otaConfURL = [NSURL URLWithString:otaConfURLString];
                                
                                NSString * otaConfString = [NSString stringWithContentsOfURL:otaConfURL encoding:NSUTF8StringEncoding error:&error];
                                otaConfString = [otaConfString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                                NSDictionary * otaConfDic = [otaConfString parseDeviceConfig];
                                //                        DLog(@"response from ota url:%@",otaConfURLString);
                                
                                if([otaConfDic[@"FIRMWARE"] isGreaterThanVersion:deviceOTAJson[@"firmware_version"]]) {
                                    self.otaFirmwareURL = [NSURL URLWithString:otaFirmwareURLString];
                                    result = otaConfDic[@"FIRMWARE"];
                                    
                                } else {
                                    result = nil;
                                }
                            } else {
                                result = [NSError errorWithDomain:errorDomain code:2 userInfo:@{NSLocalizedDescriptionKey:@"Cannot get OTA content"}];
                            }
                        }
                        dispatch_semaphore_signal(semaphore);
                    }
                }];
                [task resume];
            }
        }
    }];
    
    dispatch_time_t timeoutTime = dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC);
    if (dispatch_semaphore_wait(semaphore, timeoutTime)) {
        result = [NSError errorWithDomain:errorDomain code:0 userInfo:@{NSLocalizedDescriptionKey:@"Timeout"}]; //timeout
    }
    
    if(completion){
        if ([result isKindOfClass:[NSError class]]) {
            completion(NO, nil, nil, result);
        } else if ([result isKindOfClass:[NSString class]]) {
            completion(enforce.boolValue, result, [NSURL URLWithString:otaFirmwareURLString], nil);
        }
    }
    return result;
}



/**
 *  Send kRemoteSetDeviceOTAReady JRPC Msg /w param: ota_fw_file:$http_server_file_path
 
 Request
 {
	"jsonrpc":	"2.0",
	"method":	" common.set_ota_ready ",
	"params":	{
 "ota_fw_file":	"http://192.xxx.xxx.xxx/ezwire_test.gz"
	},
	"id":	1
 }
 
 Response
 Useless, undefined
 
 *  @param completion A Completion block whitch brings back json response
 */
-(void) setDeviceOTAReady:(void(^)(AMJSONRPCResponse * response))completion progress:(void(^)(NSInteger totalBytes, NSInteger totalBytesWritten))progress {
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSURL * supportDirURL = [fileManager URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask][0];
    if(self.otaFileServer==nil){
        self.otaFileServer = [HTTPServer new];
        [self.otaFileServer setConnectionClass:[HTTPConnection class]];
        [self.otaFileServer setDocumentRoot:supportDirURL.path];
        [self.otaFileServer start:nil];
    }
    _otaFirmwareDownloadCompletionBlock = completion;
    _otaFirmwareDownloadProgressBlock = progress;
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    NSURLSessionDownloadTask * task = [session downloadTaskWithURL:self.otaFirmwareURL];
    [task resume];
}
-(void) setDeviceOTAReady:(void (^)(AMJSONRPCResponse *))completion {
    [self setDeviceOTAReady:completion progress:nil];
}

#pragma mark - NSURLSessionDownloadDelegate
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSURL * supportDirURL = [fileManager URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask][0];
    NSString * destinationPath = [supportDirURL.path stringByAppendingPathComponent:@"ezcast_firmware.gz"];
    NSError * error = nil;
    if( [fileManager isReadableFileAtPath:destinationPath]) {
        [fileManager removeItemAtPath:destinationPath error:&error];
        DLog(@"OTA remove support dir old one:%@",error);
    }
    if ([fileManager isReadableFileAtPath:location.path]) {
        [fileManager copyItemAtURL:location toURL:[NSURL fileURLWithPath:destinationPath] error:&error];
    }
    DLog(@"OTA move temp to support dir:%@",error);
    
    NSString * otaFirmwarePath = [NSString stringWithFormat:@"http://%@:%d/%@",
                                  [(AMGenericDevice *)self.connectedDevice remoteHost].localHost,self.otaFileServer.listeningPort,destinationPath.lastPathComponent];
    DTrace();
    DLog(@"OTA FilePath:%@",destinationPath);
    DLog(@"OTA Firmware:%@",otaFirmwarePath);
    [self.jrpcProxyClient newRequest:kRemoteSetDeviceOTAReady
                               param:@{@"ota_fw_file":otaFirmwarePath}
                           generated:^(AMJSONRPCRequest *newRequest) {
                               [self sendJSONRPCOBject:newRequest];
                           } response:^(AMJSONRPCResponse *response, AMJSONRPCRequest *origRequest) {
                               _otaFirmwareDownloadCompletionBlock(response);
                           }];
}
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    dispatch_async(dispatch_get_main_queue(), ^{
        if(_otaFirmwareDownloadProgressBlock) {
            _otaFirmwareDownloadProgressBlock(totalBytesExpectedToWrite, totalBytesWritten);
        }
    });
}
#pragma mark - AMDPFImageSenderApplicationDelegate
-(void) projectConnectStatus:(BOOL)connected object:(AMDPFImageSender*) dpfSender{
    if(connected){
        if([dpfSender.connectedProjector.protocolVersion isEqualToString:@"1"]==NO){ //if protocl is >= 2, need to make a request
//            DLog(@"protocol = %@, make a auto split request", dpfSender.connectedProjector.protocolVersion);
            [dpfSender requestForNewSplit:_desireScreenNumber newPosition:_desirePosition];
            _desireScreenNumber = 0;
            _desirePosition = 0;
        }
    }
    else{
        DLog(@"device not connected...");
    }
}

-(void) projectorRequest:(AMPicoRequest*)request status:(BOOL) accecpted fromObject:(AMDPFImageSender*) dpfSender{
    if(accecpted){
        
        [self willChangeValueForKey:@"screenNumber"];
        _screenNumber = request.splitNumber;
        [self didChangeValueForKey:@"screenNumber"];
        
        [self willChangeValueForKey:@"screenPosition"];
        _screenPosition = request.splitPosition;
        [self didChangeValueForKey:@"screenPosition"];
        
//        DLog(@"%s new screenNumber:%zd, new screen position:%zd",__PRETTY_FUNCTION__, self.screenNumber, self.screenPosition );
        
        //Initail state
        [self.state onWIDIDisplayStart];
        
        if(_connectionSuccessBlock){
            _connectionSuccessBlock();
            _connectionSuccessBlock = nil;
        }
    }
    else{
        if(_connectionFailureBlock){
            NSError * error = [NSError errorWithDomain:kAMGenericDeviceDataTransmitterErrorDomain
                                                  code:kAMGenericDeviceDataTransmitterConnectionFailure
                                              userInfo:@{NSLocalizedDescriptionKey: @"Projection request failed"}];
            _connectionFailureBlock(error);
            [self _stopWifiDisplay];    //stop socket directly. Semantically, it is not WIFIDisplayOn, so we disconnect it directly.
        }
    }
}

-(void) projectorDidNotifyToStop:(AMDPFImageSender*)dpfSender{
    if(_splitScreenPauseBlock){
        _splitScreenResumeBlock();
    }
}
-(void) projectorDidNotifyToStart:(AMDPFImageSender *)dpfSender{
    if(_splitScreenResumeBlock)
        _splitScreenResumeBlock();
}
-(void) projectorDidNotifyToDisconnect:(AMDPFImageSender *) dpfSender{
    [self stopWifiDisplay];
    if([self.hostControlDelegate respondsToSelector:@selector(transmitterProjectorDidNotifyDisconnect:)]){
        [self.hostControlDelegate transmitterProjectorDidNotifyDisconnect:self];
    }
}

-(void) projectorDidDisconnect:(AMDPFImageSender*)dpfSender error:(NSError*) error{
    
    /**
     *  When WIFI display service disconnected, we don't have to clear the property "connectedDevice". "connectedDevice" mainly means "remote host" in current architecture.
     */
    
    _connectionSuccessBlock = nil;
    _connectionFailureBlock = nil;
    _splitScreenResumeBlock = nil;
    _splitScreenPauseBlock = nil;
    _mediaURL = nil;
    
    self.mediaSteamingStart = nil;
    self.mediaSteamingEnd = nil;
    self.mediaSteamingTotalTime = nil;
    self.mediaSteamingElapseTime = nil;
    self.mediaSteamingPause = nil;
    self.mediaSteamingResume = nil;
    self.volumeChange = nil;
    
    _sender = nil;
    
    [self.state onWIDIDisplayDisconnect];
    
    if(_afterDisconnectBlock){
        DLog(@"%s, error:%@", __PRETTY_FUNCTION__, error.localizedDescription);
        _afterDisconnectBlock(self.connectedDevice);
        _afterDisconnectBlock = nil;
    }
}

#pragma mark - AMDPFImageSenderMediaStreamingDelegate
-(NSString *) mediaLocation{
    NSAssert(_mediaURL!=nil, @"%s, _mediaURL should not be nil", __PRETTY_FUNCTION__);
    if([_mediaURL isFileURL]){
        return [_mediaURL path];
    }
    return [_mediaURL absoluteString];
}

-(void) projectorDidAcceptMediaStreaming{
    [self.state onMediaStreamingPlay];
    if(self.mediaSteamingStart){
        self.mediaSteamingStart(_mediaURL);
        self.mediaSteamingStart = nil;
    }
}

/**
 Case: AV_RESULT_ERROR_START_OCCUPIED_OTHER_USER. The projector is in a split-screen state.
 */
-(void) projectorDidRejectMediaStreaming:(NSError *)error{
    [self.state onMediaStreamingReject];
    if(self.mediaSteamingEnd){
        self.mediaSteamingEnd(_mediaURL, error);
        self.mediaSteamingEnd = nil;
    }
}

/*
 Keys in the userInfo of error(If there is any):
 kAMMediaStreamingErrorDomain: media streaming error domain
 kAMMediaStreamingSourceURL: the original media url that is pass to AMDPFImageSender for media streaming
 */
-(void) projectorDidEndMediaStreamingWithError:(NSError *)error{
    [self.state onMediaStreamingStop];
    
    if(self.mediaSteamingEnd){
        self.mediaSteamingEnd(_mediaURL, error);
        self.mediaSteamingEnd = nil;
    }
    self.mediaSteamingTotalTime = nil;
    self.mediaSteamingElapseTime = nil;
    
}

-(void) mediaStreamingTotalTime:(uint32_t)totalTime{
    if(self.mediaSteamingTotalTime)
    {
        self.mediaSteamingTotalTime(totalTime);
    }
}

-(void) mediaStreamingElapseTime:(uint32_t)current{
    if(self.mediaSteamingElapseTime)
    {
        self.mediaSteamingElapseTime(current);
    }
}

#pragma mark - Convinence
- (NSString*) osString{
#if TARGET_OS_IPHONE
    return kRemoteClientiOS;
#else
    return kRemoteClientMac;
#endif
}

- (NSString*) deviceNameString{
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
    NSString * storedHostName = [userDefault objectForKey:kEZCastPreferenceHostName];
    if (storedHostName) {
        return storedHostName;
    }
#if TARGET_OS_IPHONE
    return [UIDevice currentDevice].name;
#else
    return  [[NSHost currentHost] localizedName];
#endif
}

#pragma mark - Host Control
- (NSNumber*)version{
    if(_capability){
        return (_capability[kHostControl])[kVersion];
    }
    return @(1);    //fallback to version 1
}

- (void) requestFullscreen:(void(^)(BOOL isAllow))complete{
    _mediaStremaingRequestFullscreenComplete = complete;
    [self.jrpcProxyClient newRequest:kRemoteRequestStreamFullscreen param:nil generated:^(AMJSONRPCRequest *newRequest) {
        [self sendJSONRPCOBject:newRequest];
    } response:^(AMJSONRPCResponse * response, AMJSONRPCRequest * request){
        NSString * result = (response.result)[kResult];
        if([result isEqualToString:kRemoteWaitForAnswer]){
            if([self.hostControlDelegate respondsToSelector:@selector(transmitterHostControlWaitForResponse:)]){
                [self.hostControlDelegate transmitterHostControlWaitForResponse:self];
            }
        }
    }];
}

-(void)requestFullscreen:(BOOL)autoStartWifiDisplay complete:(void(^)(BOOL allowed))block{
    __weak AMGenericDeviceDataTransmitter *  weakself = self;
    _mediaStremaingRequestFullscreenComplete = ^(BOOL allowed) {
        if(allowed &&autoStartWifiDisplay){
            [weakself adaptToDPFSender];
        }
        if(block) { block(allowed); }
    };
    [self.jrpcProxyClient newRequest:kRemoteRequestStreamFullscreen param:nil generated:^(AMJSONRPCRequest *newRequest) {
        [self sendJSONRPCOBject:newRequest];
    } response:^(AMJSONRPCResponse * response, AMJSONRPCRequest * request){
        NSString * result = (response.result)[kResult];
        if([result isEqualToString:kRemoteWaitForAnswer]==NO){ return; }    //kind of guard in swift
        if([self.hostControlDelegate respondsToSelector:@selector(transmitterHostControlWaitForResponse:)] == NO){ return; } ////kind of guard in swift
        [self.hostControlDelegate transmitterHostControlWaitForResponse:self];
    }];
}

-(void)setHostName:(NSString*)name{
    NSString * nameToSet = name!=nil?name:@"iPhone Device";
    DLog(@"setting new hostname :%@", name);
    [self.jrpcProxyClient newRequest:kRemoteSetDeviceDescription
                               param:@{kCapability:@{kHostControl: @{kVersion:@2}},//app host control version 2
                                       kRemoteHostname: nameToSet}
                           generated:^(AMJSONRPCRequest *newRequest) {
                               [self sendJSONRPCOBject:newRequest];
                           }
                            response:^(AMJSONRPCResponse *response, AMJSONRPCRequest * origRequest) {
                                if([response.result isKindOfClass:[NSDictionary class]]){
                                    DLog(@"Remote Device HostControl version:2");
//                                    _capability = response.result[kCapability];
                                }
                                else if([((NSNumber*)response.result) isEqualToNumber:@(0)]){
//                                    DLog(@"Remote Device HostControl version:1");
//                                    DLog(@"The remote device accept %@, we are going to wait for a role assignment command", kRemoteSetDeviceDescription);
                                }
                            }];
}


#pragma mark - HID

-(void)setEnableHIDControl:(BOOL)enableHIDControl{
    _enableHIDControl = enableHIDControl;
    if(_enableHIDControl){
        [self sendHIDEnableToRemote];
    }
    else{
        [self sendHIDDisableToRemote];
    }
}

-(void)sendHIDEnableToRemote{
    DLog(@"sendHIDEnableToRemote");
    NSDictionary * param = @{@"version":@1,
                             @"devlist":@{
                                     @"touch":@YES,
                                     @"keyboard":@NO,
                                     @"mouse":@YES,
                                     @"point":@NO}
                             };
    __weak AMGenericDeviceDataTransmitter * weakSelf = self;
    [self.jrpcProxyClient newRequest:kRemoteHIDEnableControl param:param generated:^(AMJSONRPCRequest *newRequest) {
        [weakSelf sendJSONRPCOBject:newRequest];
    } response:^(AMJSONRPCResponse *response, AMJSONRPCRequest *origRequest) {
        DLog(@"sendHIDEnable -> response:%@", response);
    }];
}
-(void)sendHIDDisableToRemote{
    DLog(@"sendHIDDisableToRemote");
    NSDictionary * param = @{@"version":@1,
                             @"devlist":@{
                                     @"touch":@YES,
                                     @"keyboard":@NO,
                                     @"mouse":@YES,
                                     @"point":@NO}
                             };
    __weak AMGenericDeviceDataTransmitter * weakSelf = self;
    [self.jrpcProxyClient newRequest:kRemoteHIDDisableControl param:param generated:^(AMJSONRPCRequest *newRequest) {
        [weakSelf sendJSONRPCOBject:newRequest];
    } response:^(AMJSONRPCResponse *response, AMJSONRPCRequest *origRequest) {
        DLog(@"sendHIDDisable -> response:%@", response);
    }];
}

-(void)processHIDEvent:(AMJSONRPCRequest *)HIDEvent{
//    DLog(@"\nRaw HID Event: %@", HIDEvent);   //be aware of this line of log, it pulls whole performance DOWN.
    NSString * hidType =    (NSString *)HIDEvent.parameters[@"type"];
    
    if([hidType isEqualToString:@"mouse"]){
        [self processHIDEventMouse:HIDEvent];
    }else if([hidType isEqualToString:@"touch"]){
        [self processHIDEventTouch:HIDEvent];
    }
}

//handle only the first touch point. (spec)
//There will be totally 10 points, but we handle only the very first point. (spec)
-(void)processHIDEventTouch:(AMJSONRPCRequest*)HIDEvent{
    
    NSString * hidType =    (NSString *)HIDEvent.parameters[@"type"];
    
    if ([hidType isEqualToString:@"touch"] == NO) {
        return;
    }
    
    NSArray * points = (NSArray *)HIDEvent.parameters[@"points"];

    if(points.count == 0){ return; }
    
    NSDictionary * firstPoint = points.firstObject;
    
    
    NSInteger absX = ((NSNumber*)firstPoint[@"x"]).integerValue;
    NSInteger absY = ((NSNumber*)firstPoint[@"y"]).integerValue;
    NSString * status =     (NSString *)firstPoint[@"status"];
    
    if([status isEqualToString:@"down"]){
        
        
        if(_isTouchDown){   //this time should be a drag
            for (NSValue * value in self.HIDEventListeners) {
                id<GenericDeviceHIDEventListener> listener = (id<GenericDeviceHIDEventListener>) [value nonretainedObjectValue];
                if([listener respondsToSelector:@selector(touchEventConvertedToSingleMouseEvent_Dragged:screenBase:)]){
                    [listener touchEventConvertedToSingleMouseEvent_Dragged:CGPointMake(absX, absY) screenBase:s_sentScreenSize];
                }
            }
        }
        else{   //first down
            for (NSValue * value in self.HIDEventListeners) {
                id<GenericDeviceHIDEventListener> listener = (id<GenericDeviceHIDEventListener>) [value nonretainedObjectValue];
                if([listener respondsToSelector:@selector(touchEventConvertedToSingleMouseEvent_Down:screenBase:)]){
                    [listener touchEventConvertedToSingleMouseEvent_Down:CGPointMake(absX, absY) screenBase:s_sentScreenSize];
                }
            }
        }
        _isTouchDown = [status isEqualToString:@"down"];

    }else{
        if(_isTouchDown)
        {
            for (NSValue * value in self.HIDEventListeners) {
                id<GenericDeviceHIDEventListener> listener = (id<GenericDeviceHIDEventListener>) [value nonretainedObjectValue];
                if([listener respondsToSelector:@selector(touchEventConvertedToSingleMouseEvent_Up:screenBase:)]){
                    [listener touchEventConvertedToSingleMouseEvent_Up:CGPointMake(absX, absY) screenBase:s_sentScreenSize];
                }
            }
        _isTouchDown = NO;
        }
    }
}

-(void)processHIDEventMouse:(AMJSONRPCRequest*)HIDEvent{
    
    NSString * hidType =    (NSString *)HIDEvent.parameters[@"type"];
    
    if ([hidType isEqualToString:@"mouse"] == NO) {
        return;
    }
    
    NSString * element =    (NSString *)HIDEvent.parameters[@"element"];
    NSInteger dX =          ((NSNumber*)HIDEvent.parameters[@"dx"]).integerValue;
    NSInteger dY =          ((NSNumber*)HIDEvent.parameters[@"dy"]).integerValue;
    NSString * status =     (NSString *)HIDEvent.parameters[@"status"];
    
    if([element isEqualToString:@"none"]){  //mouse move
        
        if(_isLeftMouseDown){
            for (NSValue * value in self.HIDEventListeners) {
                id<GenericDeviceHIDEventListener> listener = (id<GenericDeviceHIDEventListener>) [value nonretainedObjectValue];
                if([listener respondsToSelector:@selector(leftMouseDragged:)]){
                    [listener leftMouseDragged:CGPointMake(dX, dY)];
                }
            }
        }
        else
        {
            for (NSValue * value in self.HIDEventListeners) {
                id<GenericDeviceHIDEventListener> listener = (id<GenericDeviceHIDEventListener>) [value nonretainedObjectValue];
                if([listener respondsToSelector:@selector(mouseMoveDetected:)]){
                    [listener mouseMoveDetected:CGPointMake(dX, dY)];
                }
            }
        }
        
        
    }else if ([element isEqualToString:@"left"]){
        if([status isEqualToString:@"down"]){
            
            for (NSValue * value in self.HIDEventListeners) {
                id<GenericDeviceHIDEventListener> listener = (id<GenericDeviceHIDEventListener>) [value nonretainedObjectValue];
                if([listener respondsToSelector:@selector(LeftMouseDownDetected)]){
                    [listener LeftMouseDownDetected];
                }
            }
            _isLeftMouseDown = YES;
        }else if([status isEqualToString:@"up"]){
            
            for (NSValue * value in self.HIDEventListeners) {
                id<GenericDeviceHIDEventListener> listener = (id<GenericDeviceHIDEventListener>) [value nonretainedObjectValue];
                if([listener respondsToSelector:@selector(LeftMouseUpDetected)]){
                    [listener LeftMouseUpDetected];
                }
            }
            _isLeftMouseDown = NO;
        }
    }else if([element isEqualToString:@"right"]){
        if([status isEqualToString:@"down"]){
            for (NSValue * value in self.HIDEventListeners) {
                id<GenericDeviceHIDEventListener> listener = (id<GenericDeviceHIDEventListener>) [value nonretainedObjectValue];
                if([listener respondsToSelector:@selector(RightMouseDownDetected)]){
                    [listener RightMouseDownDetected];
                }
            }
        }else if([status isEqualToString:@"up"]){
            for (NSValue * value in self.HIDEventListeners) {
                id<GenericDeviceHIDEventListener> listener = (id<GenericDeviceHIDEventListener>) [value nonretainedObjectValue];
                if([listener respondsToSelector:@selector(RightMouseUpDetected)]){
                    [listener RightMouseUpDetected];
                }
            }
        }
    }else if([element isEqualToString:@"middle"]){  //wheel down
        if([status isEqualToString:@"down"]){
            for (NSValue * value in self.HIDEventListeners) {
                id<GenericDeviceHIDEventListener> listener = (id<GenericDeviceHIDEventListener>) [value nonretainedObjectValue];
                if([listener respondsToSelector:@selector(wheelDownDetected)]){
                    [listener wheelDownDetected];
                }
            }
        }else if([status isEqualToString:@"up"]){
            for (NSValue * value in self.HIDEventListeners) {
                id<GenericDeviceHIDEventListener> listener = (id<GenericDeviceHIDEventListener>) [value nonretainedObjectValue];
                if([listener respondsToSelector:@selector(wheelUpDetected)]){
                    [listener wheelUpDetected];
                }
            }
        }
    }
    else if([element isEqualToString:@"wheel"]){
        if([status isEqualToString:@"down"]){
            for (NSValue * value in self.HIDEventListeners) {
                id<GenericDeviceHIDEventListener> listener = (id<GenericDeviceHIDEventListener>) [value nonretainedObjectValue];
                if([listener respondsToSelector:@selector(wheelScrollDetected:)]){
                    [listener wheelScrollDetected:CGPointMake(0, 1)];
                }
            }
        }else if([status isEqualToString:@"up"]){
            for (NSValue * value in self.HIDEventListeners) {
                id<GenericDeviceHIDEventListener> listener = (id<GenericDeviceHIDEventListener>) [value nonretainedObjectValue];
                if([listener respondsToSelector:@selector(wheelScrollDetected:)]){
                    [listener wheelScrollDetected:CGPointMake(0, -1)];
                }
            }
        }
    }
}

-(void)addHIDEventListener:(id<GenericDeviceHIDEventListener>)listener{
    if ([self.HIDEventListeners containsObject:[NSValue valueWithNonretainedObject:listener]]){
        return;
    }
    [self.HIDEventListeners addObject:[NSValue valueWithNonretainedObject:listener]];
    
}
-(void)removeHIDEventListener:(id<GenericDeviceHIDEventListener>)listener{
    [self.HIDEventListeners removeObject:[NSValue valueWithNonretainedObject:listener]];
}

-(BOOL)isHost{
    if(self.currentRole == TransmitterRoleNotDefined){
        return NO;
    }
    return self.currentRole == TransmitterRoleHost;
}

-(BOOL)isGuest{
    if(self.currentRole == TransmitterRoleNotDefined){
        return NO;
    }
    return self.currentRole == TransmitterRoleGuest;
}

@end

#pragma mark - NSString(AMDeviceVersionCompare)
@implementation NSString(AMDeviceVersionCompare)
- (NSComparisonResult)compareToVersion:(NSString *)version {
    NSInteger origin = [self normalizedDeviceVersion].integerValue;
    NSInteger target = [version normalizedDeviceVersion].integerValue;
    if(target == origin)
        return NSOrderedSame;
    else if (target > origin)
        return NSOrderedDescending;
    return NSOrderedAscending;
}
- (BOOL)isGreaterThanVersion:(NSString *)version {
    if ([self compareToVersion:version] == NSOrderedAscending) {
        return YES;
    }
    return NO;
}
- (NSString *)normalizedDeviceVersion {
    NSRange nonDecimalDigitRange = [self rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet] options:0];
    if(nonDecimalDigitRange.location == NSNotFound) {
    //        15478000
        NSString * targetString = self;
        while (targetString.length < @"15478000".length) {
            targetString = [targetString stringByAppendingString:@"0"];
        }
        return targetString;
    }
    return self;
}
- (NSDictionary *)parseDeviceConfig {
    NSArray * linesOfOTAConf = [self componentsSeparatedByString:@"\n"];
    NSMutableDictionary * otaConfDic = [NSMutableDictionary new];
    for (NSString * line in linesOfOTAConf) {
        NSArray * wordsArray = [line componentsSeparatedByString:@"="]; // AAA = BBB or empty line
        if(wordsArray.count == 2) {
            NSString * value = [wordsArray[1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSString * key = [wordsArray[0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            [otaConfDic setObject:value forKey:key];
        }
    }
    return otaConfDic;
}
@end //end of @implementation NSString(AMDeviceVersionCompare)
