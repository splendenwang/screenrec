//
//  AMGenericDeviceScanner.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/19.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMPrototypeDeviceScanner.h"

@interface AMGenericDeviceScanner : AMPrototypeDeviceScanner <AMPrototypeDeviceScannerProtocol, AMPrototypeDeviceScannerDelegate>

/**
 EZCast Device with features: 
 
 - vendor == "ezcast".
 - family == "ezcast" || family == nil
 - ip == "192.168.203.1" || isClientMode == YES
 - type!="car" && type!="lite" && type!="music"
 
 */
+(NSPredicate *) ezcastPredicate;

/**
 EZCast Device with features:
 
 - family == "ezcastpro"
 - ip == "192.168.168.1" || isClientMode == YES
 
 */
+(NSPredicate *) ezcastProPredicate;


/**
 EZCast Device with features:
 
 - vendor == "ezcast".
 - family == "ezcast"
 - ip == "192.168.203.1" || isClientMode == YES
 - type!="car" && type!="lite" && type!="music"
 - vendor = $(vendor)
 
 @note If vendor is nil, a predicate with no vendor restriction will be return.
 */
+(NSPredicate *) ezcastPredicate:(NSString*)vendor;

/**
 EZCast Device with features:
 
 - family == "ezcastpro"
 - ip == "192.168.168.1" || isClientMode == YES
 - vendor = $(vendor)
 
 @note If vendor is nil, a predicate with no vendor restriction will be return.
 */
+(NSPredicate *) ezcastProPredicate:(NSString*)vendor;

/**
 EZCast Device with features:
 
 - vendor == "ezcast".
 - family == "ezcast"
 - ip == "192.168.203.1" || isClientMode == YES
 - type=="car"
 
 */
+(NSPredicate*) ezcastCarPredicate;
/**
 EZCast Device with features:
 
 - vendor == $(vendor)
 - family == "ezcast"
 - ip == "192.168.203.1" || isClientMode == YES
 - type=="car"
 
  @note If vendor is nil, a predicate with no vendor restriction will be return.
 */
+(NSPredicate*) ezcastCarPredicate:(NSString *)vendor;

/**
 EZCast Device with features:
 
 - vendor == "ezcast".
 - family == "ezcast"
 - ip == "192.168.203.1" || isClientMode == YES
 - type=="music"
 
 */
+(NSPredicate*) ezcastMusicPredicate;
/**
 EZCast Device with features:
 
 - vendor == $(vendor)
 - family == "ezcast"
 - ip == "192.168.203.1" || isClientMode == YES
 - type=="music"
 
  @note If vendor is nil, a predicate with no vendor restriction will be return.
 */
+(NSPredicate*) ezcastMusicPredicate:(NSString *)vendor;

/**
 EZCast Device with features:
 
 - vendor == "ezcast".
 - family == "ezcast"
 - ip == "192.168.203.1" || isClientMode == YES
 - type=="lite"
 
 */
+(NSPredicate*) ezcastLitePredicate;
/**
 EZCast Device with features:
 
 - vendor == $(vendor)
 - family == "ezcast"
 - ip == "192.168.203.1" || isClientMode == YES
 - type=="music"
 
  @note If vendor is nil, a predicate with no vendor restriction will be return.
 */
+(NSPredicate*) ezcastLitePredicate:(NSString *)vendor;

/**
 EZCast Device with features:
 
 - vendor == $(vendor)
 - family == "ezcast"
 - ip == "192.168.203.1" || isClientMode == YES
 - type=="wire"
 
 @note If vendor is nil, a predicate with no vendor restriction will be return.
 */
+(NSPredicate*) ezcastWirePredicate;
/**
 EZCast Device with features:
 
 - vendor == $(vendor)
 - family == "ezcast"
 - ip == "192.168.203.1" || isClientMode == YES
 - type=="wire"
 
 @note If vendor is nil, a predicate with no vendor restriction will be return.
 */
+(NSPredicate*) ezcastWirePredicate:(NSString *)vendor;
@end
