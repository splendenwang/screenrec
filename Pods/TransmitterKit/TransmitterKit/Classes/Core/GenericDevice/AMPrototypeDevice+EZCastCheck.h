//
//  AMPrototypeDevice+EZCastCheck.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/6/26.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//
#import "AMPrototypeDevice.h"

@interface AMPrototypeDevice (EZCastCheck)
- (BOOL)isEZCastDevice;
- (BOOL)isEZCastProDevice;
- (BOOL)isEZCastCarDevice;
- (BOOL)isEZCastLiteDevice;
- (BOOL)isEZCastMusicDevice;
- (BOOL)isEZCastWireDevice;
- (BOOL)isDemoDevice;
- (BOOL)validateProperty:(NSString* __nonnull)propertyKey withRegularExpression:(NSString* __nullable)re;
- (BOOL)isOfficialEZCastDevice; //Check if it match official rule;
@end


@interface AMPrototypeDevice (Analytics)

/// Dongle type return string ref url: http://www.actions-micro.com/tiki/tiki-index.php?page=CloudService
/// Dongle type identify ref url: http://www.actions-micro.com/tiki/tiki-index.php?page=EZCast+Pro#Protocol_
/// - return: dongle type
/// key 'type'
@property (readonly, nonnull, copy) NSString * analyticDongleType;
///key 'name'
@property (readonly, nonnull, copy) NSString * analyticHostname;
///key 'checksum'
@property (readonly, nullable, copy) NSString * analyticChecksum;
///key 'id'
@property (readonly, nullable, copy) NSString * analyticDongleAddress;

///
///var deviceInfo = {
///    "type" : "ezcast", // 例：ezcast, chromecast, demo 等等原本網址列會透過type這query string帶入的值。
///    "name" : "EZCast-xxxxxxxx", // 例：EZCast-xxxxxxxx 原本網址列會透過ac這query string帶入的值。
///    "checksum" : "e0516272d7axxxx2da35d2ea270e5f41", // 只有EZCast家族的設備才有這欄位。例：e0516272d7axxxx2da35d2ea270e5f41原本網址列會透過checksum這query string帶入的值。
///    "id" : "EA4E060265A2" // 例：EA4E060265A2 原本網址列會透過da這query string帶入的值。
///};
///window.ezchannelService.onConnectToDevice(deviceInfo);
@property (readonly, nonnull) NSDictionary * analyicDictionary;
@end