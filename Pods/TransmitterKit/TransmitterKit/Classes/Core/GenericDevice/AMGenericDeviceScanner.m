//
//  AMGenericDeviceScanner.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/19.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMGenericDeviceScanner.h"
#import "AMRemoteHostScanner.h"
#import "AMProjectorScanner.h"
#import "AMRemoteHost.h"
#import "AMGenericDevice.h"
#import "AMGenericDeviceScanner_Debug.h"

@implementation AMGenericDeviceScanner
{
    AMRemoteHostScanner *    _remoteScanner;
    NSMutableDictionary *   _genericDevices;
}

-(instancetype) initWithScanDuration:(NSTimeInterval)timeInterval delegate:(id<AMPrototypeDeviceScannerDelegate>)delegate{
    self = [super initWithScanDuration:timeInterval delegate:delegate];
    if(self){
        _duration = timeInterval;
        self.delegate = delegate;
        _genericDevices = [NSMutableDictionary new];
    }
    return self;
}
-(void) startScanAddresses:(NSArray *)addresses{
    @synchronized(self){
        if(self.isScanning==NO){
            if(!_remoteScanner){
                _remoteScanner = [AMRemoteHostScanner defaultScanner];
                _remoteScanner.delegate = self;
            }
            [_remoteScanner startScanAddresses:addresses];
            _isScanning = YES;
        }
        
    }
}
-(void) startScan{
    @synchronized(self){
        if(self.isScanning==NO){
            if(!_remoteScanner){
                _remoteScanner = [AMRemoteHostScanner defaultScanner];
                _remoteScanner.delegate = self;
            }
            [_remoteScanner startScan];
            _isScanning = YES;
        }
        
    }
}

-(void) stopScan{
    @synchronized(self){
        if(self.isScanning==YES){
            [_remoteScanner stopScan];
            _isScanning = NO;
            [_genericDevices removeAllObjects];
        }
    }
    
}

-(void) scanner:(id<AMPrototypeDeviceScannerProtocol>)scanner disappearedDevice:(AMPrototypeDevice *)removedDevice{
    
    @synchronized(_genericDevices){ 
        AMGenericDevice * device = _genericDevices[[removedDevice ipAddress]];
        if(device==nil)
            return;
        if([removedDevice isKindOfClass:[AMRemoteHost class]]){
            [_genericDevices removeObjectForKey:removedDevice.ipAddress];
            if([self.delegate respondsToSelector:@selector(scanner:disappearedDevice:)]){
                [self.delegate scanner:self disappearedDevice:device];
            }
        }
    }
}

-(void) scanner:(id<AMPrototypeDeviceScannerProtocol>)scanner discoveriedDevice:(AMPrototypeDevice *)newDevice{
    @synchronized(_genericDevices){
        AMGenericDevice * device = _genericDevices[[newDevice ipAddress]];
        if(device==nil){
            device = [[AMGenericDevice alloc] init];
        }
        
        if([newDevice isKindOfClass:[AMRemoteHost class]]){
            device.remoteHost = (AMRemoteHost*) newDevice;
//            DLog(@"device ip:%@, remoteHost capability added", device.ipAddress);
        }
        
        
        BOOL result = NO;
        NSPredicate * targetingPredicate = nil;
        if([newDevice isKindOfClass:[AMRemoteHost class]] && self.predicate ){
//            for (NSPredicate * predicate in self.predicates)
                result = [self.predicate evaluateWithObject:newDevice];
                if(result==NO){
//                    DLog(@"\nScanner predicate:%@ evaluates failed on AMRemoteDevice:%@", predicate, newDevice);
                }
                else{
                    targetingPredicate= self.predicate;
                }
        }
        
        if( ([newDevice isKindOfClass:[AMRemoteHost class]] && targetingPredicate && result) || (self.predicate==nil) ){

            _genericDevices[[newDevice ipAddress]] = device;
            if( [self.delegate respondsToSelector:@selector(scanner:discoveriedDevice:)]){
                [self.delegate scanner:self discoveriedDevice:device];
            }
        }
    }
}


-(void) scanner:(id<AMPrototypeDeviceScannerProtocol>)scanner failToScan:(NSError *)error{
    if([self.delegate respondsToSelector:@selector(scanner:failToScan:)]){
        [self.delegate scanner:self failToScan:error];
    }
}

+(NSPredicate *) ezcastPredicate{
    return [AMGenericDeviceScanner ezcastPredicate:@"ezcast"];
}

+(NSPredicate *) ezcastPredicate:(NSString*)vendor{
    
    NSPredicate * predicateFamily = [NSPredicate predicateWithFormat:@"family LIKE[c] 'ezcast' || family == nil"];
    NSPredicate * predicateIP = [NSPredicate predicateWithFormat:@"ipAddress LIKE[c] '192.168.203.1' || ipAddress LIKE[cd] '192.168.111.1' || isClientModeEnable == YES"];
    NSPredicate * predicateType = [NSPredicate predicateWithFormat:@"(type!='car' && type!='lite' && type!='music' && type!='wire')"];
    
    return [AMGenericDeviceScanner compondPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:@[predicateFamily, predicateIP, predicateType]] withVendor:vendor];
}

+(NSPredicate *) ezcastProPredicate{
    return [AMGenericDeviceScanner ezcastProPredicate:nil];
}

/**
 Additionaly, restrict vendor.
 */
+(NSPredicate *) ezcastProPredicate:(NSString*)vendor{
    NSPredicate * familyPredicate =  [NSPredicate predicateWithFormat:@"family LIKE[c] 'ezcastpro'"];
    NSPredicate * predicateIP = [NSPredicate predicateWithFormat:@"ipAddress LIKE[c] '192.168.168.1' || isClientModeEnable == YES"];
    NSPredicate * ezcastProPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[familyPredicate, predicateIP]];

    return [AMGenericDeviceScanner compondPredicate:ezcastProPredicate withVendor:vendor];
}

/**
 @param vendor The vendor string to identify remote device
 @return A NSPredicate ojbect
 @see http://www.actions-micro.com/tiki/tiki-index.php?page=EZCast+Pro#Protocol_
 */
+(NSPredicate *) vendorPredicate:(NSString *)vendor{
    NSAssert(vendor!=nil && vendor.length > 0, @"vendor should not be nil");
    return [NSPredicate predicateWithFormat:@"vendor LIKE[c] %@",vendor];
}

/**
 Fetching AMModelString from info.plist
 @see http://www.actions-micro.com/tiki/tiki-index.php?page=EZCast+Pro#Protocol_
 */
//+(NSPredicate *) modelPredicate{
//    NSString * model = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AMModelString"];
//    NSAssert(model != nil, @"AMModelString should not be nil");
//    //stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] to prevent string with white-spaces
//    if(model == nil || [model isEqualToString:@""] || [model stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] <= 0) {
//        return [NSPredicate predicateWithValue:YES];
//    }
//    return [NSPredicate predicateWithFormat:@"model LIKE[c] %@ OR model ==nil", model];
//}

+(NSPredicate *) ezcastDebugPredicate{
    NSPredicate * predicateVendor = [NSPredicate predicateWithFormat:@"vendor LIKE[c] 'ezcast'"];
    
    NSPredicate * predicateFamily = [NSPredicate predicateWithFormat:@"family LIKE[c] 'ezcast' || family == nil"];
    
    NSPredicate * finalPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicateVendor, predicateFamily]];
    
    return finalPredicate;
}

+(NSPredicate*) ezcastCarPredicate{
    return [AMGenericDeviceScanner ezcastCarPredicate:@"ezcast"];
}
+(NSPredicate*) ezcastMusicPredicate{
    return [AMGenericDeviceScanner ezcastMusicPredicate:@"ezcast"];
}
+(NSPredicate*) ezcastLitePredicate{
    return [AMGenericDeviceScanner ezcastLitePredicate:@"ezcast"];
}
+(NSPredicate*) ezcastWirePredicate{
    return [AMGenericDeviceScanner ezcastWirePredicate:@"ezcast"];
}

+(NSPredicate*) ezcastCarPredicate:(NSString *)vendor{
    NSPredicate * predicateFamily = [NSPredicate predicateWithFormat:@"family LIKE[c] 'ezcast' || family == nil"];
    NSPredicate * predicateIP = [NSPredicate predicateWithFormat:@"ipAddress LIKE[c] '192.168.203.1' || ipAddress LIKE[cd] '192.168.111.1' || isClientModeEnable == YES"];
    NSPredicate * predicateType = [NSPredicate predicateWithFormat:@"type LIKE[c] 'car'"];
    
    return [AMGenericDeviceScanner compondPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:@[predicateFamily, predicateIP, predicateType]] withVendor:vendor];
}
+(NSPredicate*) ezcastMusicPredicate:(NSString *)vendor{
    NSPredicate * predicateFamily = [NSPredicate predicateWithFormat:@"family LIKE[c] 'ezcast' || family == nil"];
    NSPredicate * predicateIP = [NSPredicate predicateWithFormat:@"ipAddress LIKE[c] '192.168.203.1' || ipAddress LIKE[cd] '192.168.111.1' || isClientModeEnable == YES"];
    NSPredicate * predicateType = [NSPredicate predicateWithFormat:@"type LIKE[c] 'music'"];
    
    return [AMGenericDeviceScanner compondPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:@[predicateFamily, predicateIP, predicateType]] withVendor:vendor];
}
+(NSPredicate*) ezcastLitePredicate:(NSString *)vendor{
    NSPredicate * predicateFamily = [NSPredicate predicateWithFormat:@"family LIKE[c] 'ezcast' || family == nil"];
    NSPredicate * predicateIP = [NSPredicate predicateWithFormat:@"ipAddress LIKE[c] '192.168.203.1' || ipAddress LIKE[cd] '192.168.111.1' || isClientModeEnable == YES"];
    NSPredicate * predicateType = [NSPredicate predicateWithFormat:@"type LIKE[c] 'lite'"];
    
    return [AMGenericDeviceScanner compondPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:@[predicateFamily, predicateIP, predicateType]] withVendor:vendor];
}
+(NSPredicate*) ezcastWirePredicate:(NSString *)vendor{
    NSPredicate * predicateFamily = [NSPredicate predicateWithFormat:@"family LIKE[c] 'ezcast' || family == nil"];
    NSPredicate * predicateIP = [NSPredicate predicateWithFormat:@"ipAddress LIKE[c] '192.168.203.1' || ipAddress LIKE[cd] '192.168.111.1' || isClientModeEnable == YES"];
    NSPredicate * predicateType = [NSPredicate predicateWithFormat:@"type LIKE[c] 'wire'"];
    
    return [AMGenericDeviceScanner compondPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:@[predicateFamily, predicateIP, predicateType]] withVendor:vendor];
}

#pragma mark - 串燒阿
+(NSPredicate*) compondPredicate:(NSPredicate*)pre withVendor:(NSString*)vendor{
    if(vendor){
        NSPredicate * vendorPredicate = [AMGenericDeviceScanner vendorPredicate:vendor];
        return [NSCompoundPredicate andPredicateWithSubpredicates:@[vendorPredicate,pre]];
    }
    return pre;
}
@end
