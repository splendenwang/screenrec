//
//  AMGenericDevice.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/19.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMPrototypeDevice.h"
NS_ASSUME_NONNULL_BEGIN
extern NSString * const kEZCastPreferencePreferredDevice;
extern NSString * const kEZCastPreferencePreferredDeviceName;
extern NSString * const kEZCastPreferenceAllowRequest;
extern NSString * const kEZCastPreferenceDoNotDisturb;
extern NSString * const kEZCastPreferencePreferredDeviceSerialzationData;
extern NSString * const kAMPreviousConnectedDeviceIPAddress;
extern NSString * const kEZCastPreferenceHostName;
NS_ASSUME_NONNULL_END
@class AMProjectorDevice, AMRemoteHost;
@interface AMGenericDevice : AMPrototypeDevice <NSCoding, AMPrototypeImageStreamingProtocol>

@property (strong, nonatomic) AMRemoteHost * _Nullable remoteHost;

@property (readonly) NSString * _Null_unspecified customName;

/**
 Fast creation of a ezcastpro device from IP and hostname(SSID). This is useful when your need to connect to it DIRECTLY.
 
 @parameter connectionTimeout timeout value for connecting to a device.
 */
+(AMGenericDevice * _Nonnull)ezcastproDeviceFromIP:(NSString * _Nonnull)ip hostname:(NSString * _Nonnull)hostname connectionTimeout:(NSTimeInterval)timeout;


/**
 Retrive dongle info through http from a device.
 
 @param completeBlock An EZCast-compatible device.
 
 */
-(void) getDongleInfoFromDevice:(void(^ _Nonnull)(id _Null_unspecified json, NSError * _Nullable error))completeBlock;



/**
 Return Max resolution of this device.
 @discussion This getter trigger an async call to aquire max resolution for callers. Semaphore is applied to make this function a sync call.  
 @note Synchronous call
 @return CGSize
 */
@property (nonatomic, getter=getMaxResolution) CGSize maxResolution;

/*
 Return a BOOL value indicates if this device supports H.264 video streaming decoding
@discussion This getter trigger an async call to aquire H.264 capability. Semaphore is applied to make this function a sync call.
 @note Synchronous call
 @return BOOL value
 */
@property (readonly, nonatomic) BOOL isEZEncodeProEnable;


/**
 Default value "ios"/"mac", depending which platform you are running. If you need to get bitmask dictionary for other platforms, assign your own OS string prior to connection process.
 */
@property (strong, nonatomic, nonnull) NSString * OSStringForRetrivingBitmaskDictionary;
@end


@interface AMGenericDevice()
-(nonnull NSDictionary*)convertedLegacyBitmask;
@end


typedef NSString * RemoteDeviceType NS_STRING_ENUM;
extern _Nonnull RemoteDeviceType const kRemoteTypeBox;
extern _Nonnull RemoteDeviceType const kRemoteTypeProjector;
extern _Nonnull RemoteDeviceType const kRemoteTypeTV;
extern _Nonnull RemoteDeviceType const kRemoteTypeDongle;
extern _Nonnull RemoteDeviceType const kRemoteTypeCar;
extern _Nonnull RemoteDeviceType const kRemoteTypeMusic;
extern _Nonnull RemoteDeviceType const kRemoteTypeLite;
extern _Nonnull RemoteDeviceType const kRemoteTypeWire;
