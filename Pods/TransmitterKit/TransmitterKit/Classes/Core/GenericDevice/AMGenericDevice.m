//
//  AMGenericDevice.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/19.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//
#import "AMPrototypeDevice+_internal.h"
#import "AMGenericDevice.h"
#import "AMProjectorScanner.h"
#import "AMRemoteHost.h"
#import "EZCastApplicationFeatures.h"
@import AFNetworking;
@import EZCastBitmask;
NSString * const kEZCastPreferencePreferredDevice = @"kEZCastPreferencePreferredDevice";
NSString * const kEZCastPreferencePreferredDeviceName = @"kEZCastPreferencePreferredDeviceName";
NSString * const kEZCastPreferenceAllowRequest = @"kEZCastPreferenceAllowRequest";
NSString * const kEZCastPreferenceDoNotDisturb = @"kEZCastPreferenceDoNotDisturb";
NSString * const kEZCastPreferencePreferredDeviceSerialzationData = @"kEZCastPreferencePreferredDeviceSerialzationData";
NSString * const kEZCastPreferenceHostName = @"kEZCastPreferenceHostName";
NSString * const kAMPreviousConnectedDeviceIPAddress = @"kAMPreviousConnectedDeviceIPAddress";

@interface AMGenericDevice()
@property (assign) CGSize resolutionCache;
@end


@implementation AMGenericDevice

-(instancetype) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _remoteHost = [aDecoder decodeObjectForKey:@"remoteHost"];
        _resolutionCache = CGSizeZero;
    }
    return self;
}

-(void) encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.remoteHost forKey:@"remoteHost"];
}


-(void) setRemoteHost:(AMRemoteHost *)remoteHost{
    _remoteHost = remoteHost;
    if(_remoteHost)
        self.ipAddress = _remoteHost.ipAddress;
}

-(NSString*) displayName{
    if(self.remoteHost)
        return self.remoteHost.displayName;
    return @"UNKNOWN_EZCAST_DEVICE";
}


-(BOOL) isEqual:(id)object{
    if([object isKindOfClass:[self class]]){
        AMGenericDevice * anotherDevice = (AMGenericDevice *) object;
        return [anotherDevice.remoteHost isEqual:self.remoteHost];
    }
    return NO;
}


-(NSComparisonResult) compare:(AMGenericDevice*) aDevice{
    NSComparisonResult result = NSOrderedSame;
    if([aDevice.ipAddress isEqualToString:self.ipAddress]==NO){
        result = NSOrderedAscending;
    }
    if([aDevice.hostName isEqualToString:self.hostName]==NO){
        result = NSOrderedAscending;
    }
    if([aDevice class] != self.class){
        result = NSOrderedAscending;
    }
    
    return result;
}

-(NSString*) deviceModel{
    return self.remoteHost.deviceModel;
}

-(NSString*) vendor{
    return self.remoteHost.vendor;
}

-(NSString*) hostName{
    return self.remoteHost.hostName;
}

-(NSString*) srcvers{
    return self.remoteHost.srcvers;
}

-(NSDictionary*) parameters{
    return self.remoteHost.parameters;
}



-(id) objectForKey:(NSString *)key{
    if([key isEqualToString:@"displayName"]){
        return self.displayName;
    }
    if([key isEqualToString:@"ipAddress" ]){
        return self.remoteHost.ipAddress;
    }
    return [self.remoteHost objectForKey:key];
}

-(NSString *)localeString{
    // Get the current country and locale information of the user
    NSLocale *locale = [NSLocale currentLocale];
    NSString * locale3116_1_code = [locale objectForKey:(NSString*)kCFLocaleCountryCode];
    return locale3116_1_code;
}


-(unsigned) serviceBitMask{
    NSString *servicesString = nil;
    
    NSString * platform = @"ezcast.service.ios"; //prevent nil string
#if !TARGET_OS_IPHONE
    platform = @"ezcast.service.mac";
#else
    platform = @"ezcast.service.ios";
#endif
    
    unsigned serviceMask = 0;
    if([self objectForKey:[NSString stringWithFormat:@"%@.%@", platform, [self localeString]]]) {
        servicesString = [self objectForKey:[NSString stringWithFormat:@"%@.%@", platform, [self localeString]]];
    } else if ([self objectForKey:platform]){
        servicesString = [self objectForKey:platform];
    } else {
        servicesString = [NSString stringWithFormat:@"%x",0x000003df];
    }
    if (servicesString.length>0) {
        NSScanner *scanner = [NSScanner scannerWithString:servicesString];
        [scanner scanHexInt:&serviceMask];
    }
    return serviceMask;
}

+(NSArray *) supportedAudioFormat{
    //remove "rm" by henry's request.
    return @[@"ape",@"flac",@"ogg",@"mp3",@"wma",@"wav",@"m4a",@"aac",@"dts",@"ac3",@"ra",@"aif",@"aiff",@"mka"];
}

+(NSArray *) supportedVideoFormat{
    return @[@"avi",@"divx",@"xvid",@"mp4",@"mov",@"vob",@"dat",@"ts",@"m2ts",@"mts",@"mkv",@"rmvb",@"rm",@"mpg",@"mpeg",@"wmv",@"m4v",@"3gp",@"flv", @"asf"];
}

+(NSArray *) supportedSubtitleFormat{
    return @[@"srt", @"ssa", @"smi", @"cdg"];
}

-(void) getDongleInfoFromDevice:(void(^)(id json, NSError * error))completeBlock{
    AFHTTPRequestOperationManager * manager = [AFHTTPRequestOperationManager manager];
    NSMutableSet * set = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
    [set addObject:@"text/html"];
    [set addObject:@"text/plain"];
    manager.responseSerializer.acceptableContentTypes = set;
    [manager GET:[NSString stringWithFormat:@"%@dongleInfo.json",self.remoteHost.webRootHost]
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             if(completeBlock){
                 completeBlock(responseObject, nil);
             }
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//             DLog(@"DonlgeInfo went error:%@\n",error);
             if(completeBlock){
                 completeBlock(nil, error);
             }
         }];
}

- (void)getAppID:(void (^)(NSString * appID ,NSError * error))completionBlock{
    DTrace();
    
    AFHTTPRequestOperationManager * manager = [AFHTTPRequestOperationManager manager];
    AFHTTPResponseSerializer * serializer = [[AFHTTPResponseSerializer alloc] init];
    serializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/plain", nil];
    manager.responseSerializer = serializer;
    
    [manager GET:[NSString stringWithFormat:@"%@cgi-bin/get_my_mac.cgi",self.remoteHost.webRootHost]
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSString *macAddressString = [[NSString alloc] initWithBytes:[responseObject bytes] length:[responseObject length] encoding:NSASCIIStringEncoding];
//             DLog(@"iOS device mac_address via Dongle: <%@>",macAddressString);
             if(completionBlock)completionBlock(macAddressString, nil);
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//             DLog(@"Fetch iDevice mac_address via Dongle Fail\n Operation:%@\nError:%@\n",operation,error);
             if(completionBlock)completionBlock(nil, error);
         }];
}

-(NSString *) customName{
    return self.remoteHost.customName;
}

-(CGSize) getMaxResolution{
    if( CGSizeEqualToSize(self.resolutionCache, CGSizeZero) == NO){
        return self.resolutionCache;
    }
    IPAddressProjectorScanner * s = [IPAddressProjectorScanner new];
    AMProjectorDevice * device = [s seekProjector:self.ipAddress withTimeout:5];
    self.resolutionCache = device.getMaxResolution;
    return self.resolutionCache;
}

-(NSString*)description{
    return [NSString stringWithFormat:@"\n\t--- AMGenericDevice] --- \n\t[hostname   ]:%@\n\t[displayname]:%@\n\t[vendor]:%@\n\t[ip address]:%@\n", self.hostName, self.displayName, self.vendor, self.ipAddress];
}

+(AMGenericDevice * _Nonnull)ezcastproDeviceFromIP:(NSString * _Nonnull)ip hostname:(NSString * _Nonnull)hostname connectionTimeout:(NSTimeInterval)timeout{
    NSAssert(hostname!=nil, @"parameter [ip] should not be nil");
    NSAssert(hostname!=nil, @"parameter [hostname] should not be nil");
    
    AMRemoteHost * remote = [[AMRemoteHost alloc] init];
    remote.ipAddress = ip;
    remote.hostName = hostname;
    remote.deviceModel = @"ezcast";
    remote.vendor = @"ezcast";
    remote.timeout = timeout;
    NSMutableDictionary * para = [NSMutableDictionary new];
    para[@"name"] = hostname;
    para[@"family"] = @"ezcastpro";
    para[@"ezcast.service.ios"] = @"0xFFFFFF";
    para[@"ezcast.service.mac"] = @"0xFFFFFF";
    para[kAMPrototypeDeviceID] = @"VIRTUAL-DONGLE";
    
    remote.parameters = [NSDictionary dictionaryWithDictionary:para];
    
    AMProjectorDevice * projector = [[AMProjectorDevice alloc] init];
    projector.protocolVersion = @"2";
    projector.userName = @"root";
    projector.hostName = hostname;
    projector.ipAddress = ip;
    
    AMGenericDevice * device = [[AMGenericDevice alloc] init];
    device.ipAddress = ip;
    device.remoteHost = remote;
    
    return device;
    
    
}

-(BOOL)isEZEncodeProEnable{
    IPAddressProjectorScanner * s = [IPAddressProjectorScanner new];
    AMProjectorDevice * device = [s seekProjector:self.ipAddress withTimeout:5];
    return device.isEZEncodeProEnable;
}

-(NSString*)OSStringForRetrivingBitmaskDictionary{
    if (_OSStringForRetrivingBitmaskDictionary == nil) {
#if TARGET_OS_IPHONE
        return kRemoteClientiOS;
#else
        return kRemoteClientMac;
#endif
    }
    return _OSStringForRetrivingBitmaskDictionary;
}


#pragma mark -
-(nonnull NSDictionary*)convertedLegacyBitmask{
    unsigned serviceMask = [self serviceBitMask];
    NSMutableDictionary * dict = [NSMutableDictionary new];
    if((serviceMask & SERVICE_PHOTO)!=0){
        [dict setObject:kRemoteServiceEnable forKey:kRemoteServicePhoto];
    }
    if ((serviceMask & SERVICE_CAMERA)!=0){
        [dict setObject:kRemoteServiceEnable forKey:kRemoteServiceCamera];
    }
    if((serviceMask & SERVICE_MUSIC)!=0){
        [dict setObject:kRemoteServiceEnable forKey:kRemoteServiceMusic];
    }
    if((serviceMask & SERVICE_VIDEO)!=0){
        [dict setObject:kRemoteServiceEnable forKey:kRemoteServiceVideo];
    }
    if((serviceMask & SERVICE_DLNA)!=0){
        [dict setObject:kRemoteServiceEnable forKey:kRemoteServiceDLNA];
    }
    if((serviceMask & SERVICE_EZAIR)!=0){
        [dict setObject:kRemoteServiceEnable forKey:kRemoteServiceEZAir];
    }
    if((serviceMask & SERVICE_SETTING)!=0){
        [dict setObject:kRemoteServiceEnable forKey:kRemoteServiceSetting];
    }
    if((serviceMask & SERVICE_EZMIRROR)!=0){
        [dict setObject:kRemoteServiceEnable forKey:kRemoteServiceEZMirror];
    }
    if((serviceMask & SERVICE_DOCUMENT)!=0){
        [dict setObject:kRemoteServiceEnable forKey:kRemoteServiceDocument];
    }
    if((serviceMask & SERVICE_WEB)!=0){
        [dict setObject:kRemoteServiceEnable forKey:kRemoteServiceWeb];
    }
    if((serviceMask & SERVICE_CLOUD_STORAGE)!=0){
        [dict setObject:kRemoteServiceEnable forKey:kRemoteServiceCloudStorage];
    }
    if((serviceMask & SERVICE_TV)!=0){
        [dict setObject:kRemoteServiceEnable forKey:kRemoteServiceTV];
    }
    if((serviceMask & SERVICE_CLOUD_VIDEO)!=0){
        [dict setObject:kRemoteServiceEnable forKey:kRemoteServiceCloudVideo];
    }
    if((serviceMask & SERVICE_MAP)!=0){
        [dict setObject:kRemoteServiceEnable forKey:kRemoteServiceMap];
    }
    if((serviceMask & SERVICE_SPLIT_SCREEN)!=0){
        [dict setObject:kRemoteServiceEnable forKey:kRemoteServiceSplitScreen];
    }
    if((serviceMask & SERVICE_COMMENT)!=0){
        [dict setObject:kRemoteServiceEnable forKey:kRemoteServiceComment];
    }
    if((serviceMask &SERVICE_SOCIAL)!=0){
        [dict setObject:kRemoteServiceEnable forKey:kRemoteServiceSocial];
    }
    if((serviceMask &SERVICE_PREFERENCE)!=0){
        [dict setObject:kRemoteServiceEnable forKey:kRemoteServicePreference];
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}
@end

NSString * const kRemoteTypeBox = @"box";
NSString * const kRemoteTypeProjector = @"projector";
NSString * const kRemoteTypeTV = @"tv";
NSString * const kRemoteTypeDongle = @"dongle";
NSString * const kRemoteTypeCar = @"car";
NSString * const kRemoteTypeMusic = @"music";
NSString * const kRemoteTypeLite = @"lite";
NSString * const kRemoteTypeWire = @"wire";

