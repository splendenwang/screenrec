//
//  AMGenericDeviceDataTransmitter.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/19.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMDataTransmitter.h"


extern NSString * _Nonnull const kAMGenericDeviceDataTransmitterErrorDomain;

typedef NS_ENUM(NSInteger, kAMGenericDeviceDataTransmitterErrorCode){
    kAMGenericDeviceDataTransmitterConnectionFailure = 1000,
    kAMGenericDeviceDataTransmitterConnectionTimeout = 1001,
//    kAMGenericDeviceDataTransmitterHostDeny= 1002,
};

@interface AMGenericDeviceDataTransmitter : AMDataTransmitter
<
AMDataTransmitter_SendKey,
AMDataTransmitter_SplitScreen,
AMDataTransmitter_ConferenceControl,
AMDataTransmitter_OTA,
AMGenericDevice_HIDEventController,
AMDataTransmitter_HTTPProxy,
AMDataTransmitter_SocketStatus
>

@property (nonatomic, assign) NSUInteger screenNumber;
@property (nonatomic, assign) NSUInteger screenPosition;

// <AMDataTransmitter_HTTPProxy>
@property (strong, nullable) uint16_t(^HTTPProxyRequestActivationBlock)(BOOL activate);
@property (strong, nullable) dispatch_queue_t HTTPProxyRequestCallbackQueue;

@property (nonatomic, readonly) BOOL readyToSendH264;

@end

@interface NSString(AMDeviceVersionCompare)
- (NSComparisonResult)compareToVersion:(NSString * _Nonnull)version;
- (BOOL)isGreaterThanVersion:(NSString * _Nonnull)version;
- (NSString * _Nullable)normalizedDeviceVersion;
- (NSDictionary * _Nullable)parseDeviceConfig;
@end



/* EZCast command . Defined by Jesse */
typedef NS_ENUM(NSInteger, EZCastCustomRemoteKey){
    EZCAST_SWITCH_TO = 100,         // switch to EZ Cast
    EZCAST_ON = 101,                // turn on EZ Cast
    EZCAST_OFF = 102,               // turn off EZ Cast
    DLNA_SWITCH_TO = 103,           // switch to DLNA
    DLNA_ON = 104,                  // turn on DLNA
    DLNA_OFF = 105,                 // turn off DLNA
    MIRACAST_SWITCH_TO = 106,       // switch to Miracast
    MIRACAST_ON = 107,              // turn on Miracast
    MIRACAST_OFF = 108,             // turn off Miracast
    EZAIR_SWITCH_TO = 109,          // switch to EZ Air
    EZAIR_ON = 110,                 // turn on EZ Air
    EZAIR_OFF = 111,                // turn off EZ Air
    QUERY_STATUS = 112,             // query on/off status of ez cast / dlna / miracast / ez air
    MAIN_SWITCH_TO = 113,           // restore to the main page when no any app is using the device.
    SETTING_SWITCH_TO = 114,        // switch to settings page
};





