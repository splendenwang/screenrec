//
//  AMDataTransmitter_SplitScreen.h
//  AMCommon_iOS
//
//  Created by brianliu on 2015/8/13.
//  Copyright (c) 2015年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AMDataTransmitter_SplitScreen <NSObject>

/**
 KVO-compliant
 */
@property (nonatomic, readonly) NSUInteger screenNumber;

/**
 KVO-compliant
 */
@property (nonatomic, readonly) NSUInteger screenPosition;

/**
 Returns if the current projection is in full screen. YES if(screen == 1 && position == 1) , otherwise returns NO.
 */
@property (nonatomic, readonly) BOOL isFullscreen;

-(void) setSplitControlWithScreenNumber:(uint16_t) screenNumber
                               position:(uint16_t)screenPosition;

-(void) registerSplitScreenPauseBlock:(void(^)(void))pauseBlock;

-(void) registerSplitScreenResumeBlock:(void(^)(void))resumeBlock;

//Ask wifi display to disconnect all wifi display connection.
-(void) disconnectAllWifiDisplay;
@end
