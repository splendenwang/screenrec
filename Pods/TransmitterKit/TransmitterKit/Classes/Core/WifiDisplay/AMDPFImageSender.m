

//
//  AMDPFImageSender.m
//  EZRemoteControliOS
//
//  Created by brianliu on 12/7/11.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import "AMDPFImageSender.h"
#import "AMPicoImage.h"
#import "AMPicoRequest.h"
#import "AMPicoNotification.h"
#import "AMSocketDefine.h"
#import "AMPicoSetDataCommand.h"
#import "AMPicoMediaStreamingCommand.h"
#import "AMPicoMediaStreamingDataBlock.h"
#import "AMPicoQueryInfoCommand.h"
#import "AMPicoHeartbeat.h"
#import "AMPicoH264Data.h"
#import "Pico.h"
#import "AMPicoAudio.h"
#import "AMMediaStreamingFileDataSource.h"
#import "AMMediaStreamingHTTPDataSource.h"
#import "AMBufferController.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/time.h>
#import "AMProjectorDevice.h"
#import "AMMediaStreamingStandardErrorDefinition.h"
#import "AMBufferController.h"
@import CocoaAsyncSocket;
@import WinnerWave_CocoaHTTPServer;
#import "HTTPServer+StartCompletionBlock.h"
#import "AMScreenMediaHttpConnection.h"
#import "AMiDeviceIpAddress.h"
@import BytesMeter;

/*Stop debug loggin in this file*/
#ifdef DEBUG
#undef DLog
#define DLog(...)
#undef DTrace
#define DTrace();
#endif
/********************************/

#if TARGET_OS_IPHONE
@import UIKit;
#else
@import Cocoa;
#endif

#define DATA_HAS_DATA               1
#define DATA_NO_DATA                2

#define AUDIO_BUFFER_SIZE 2

#define TCP_TIMEOUT     (-1)
static BOOL stopThread = YES;

static AMDPFImageSender * _currentSessionSender = nil;
NSString * const kAMMediaStreamingSourceURL = @"kAMMediaStreamingSourceURL";

@interface AMDPFImageSender () <AMBufferControllerDelegate, GCDAsyncSocketDelegate>
{
    AMProjectorDevice * mainProjector;
    dispatch_queue_t  aQueue;
    dispatch_queue_t  delegateQueue;
    dispatch_source_t heartbeatTimer;
    
//    CFAbsoluteTime startTime;
//    CFAbsoluteTime endTime;
//    NSInteger measureSize;
    
    /* We will create a semaphore of 1, make sure there is only one chunk of data is being sent under media streaming. Because reading source is much faster than sending socket under media streaming, we will need to use dispatch_semaphore to restrict the order of reading file and sending packets.
     
     When dispatch_semaphore_wait(socketSemaphore) is used, it means that on the moment it is need to wait for a socket to be ready.
     
     When dispatch_semaphore_signal(socketSemaphore) is used, it means that the socket is ready to send packet.
     
     */
    dispatch_semaphore_t socketSemaphore;
    
    dispatch_source_t sendImageEventSource;
    
    dispatch_source_t sendPCMEventSource;
}
@property (atomic, strong)    GCDAsyncSocket *  tcpSocket;

@property (atomic, assign)    uint32_t          sequence;

@property (atomic, strong)    AMMediaStreamingFileDataSource *    fileDataSource;

@property (atomic, strong)    AMMediaStreamingHTTPDataSource *    httpDataSource;

@property (nonatomic, assign)    BOOL readyToSendImage;
@property (nonatomic, assign) BOOL readyToSendAudio;

@property (atomic, strong)    NSMutableArray * audioBufferPool;

@property (atomic, strong)    NSConditionLock * audioConditionLock;

@property (strong) AMDPFSenderWebURLBlock retrieveWebURLBlock;

@property AMBufferController * bufferController;

@property (atomic, assign) BOOL isConsumerWorking;

@property (NS_NONATOMIC_IOSONLY, strong) HTTPServer * mediaHttpServer;

@property (NS_NONATOMIC_IOSONLY, strong) BytesMeter * bytesMeter;
@property (atomic, strong) NSMutableArray<NSNumber*> * sendBytes;
@end

@implementation AMDPFImageSender

-(instancetype) init{
    self = [super init];
    if(self){
        
        aQueue = dispatch_queue_create("com.actions.dpf.sender", NULL);
        delegateQueue = dispatch_queue_create("com.actions.dpf.dQueue", NULL);
        
        self.tcpSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:delegateQueue socketQueue:nil];
        self.tcpSocket.delegate = self;
        socketSemaphore = dispatch_semaphore_create(1); //the socketSemaphore is create with value 1.
        
        self.audioBufferPool = [[NSMutableArray alloc] initWithCapacity:AUDIO_BUFFER_SIZE];
        self.audioConditionLock = [[NSConditionLock alloc] initWithCondition:DATA_NO_DATA];
        
        self.bufferController = [AMBufferController new];
        self.bufferController.delegate = self;
        self.isConsumerWorking = NO;
        
        _mediaHttpServer =
        _mediaHttpServer = [[HTTPServer alloc] init];
        [_mediaHttpServer startWithConnectionClass:[AMScreenMediaHttpConnection class]
                                           success:^{
                                           }
                                           failure:^(NSError* error) {
                                               DLog(@"Error starting HTTP Server: %@", error);
                                           }];
        _bytesMeter = [[BytesMeter alloc] init];
        [_bytesMeter addObserver:self forKeyPath:@"speed" options:NSKeyValueObservingOptionNew context:NULL];
        
    }
    return self;
}

-(void)prepareSockets{
    if(self.tcpSocket == nil){
        self.tcpSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:delegateQueue socketQueue:nil];
        self.tcpSocket.delegate = self;
    }
}

-(void) connectToProjector:(AMProjectorDevice*) projector{
    DTrace();
    [self prepareSockets];
    mainProjector = projector;
    [self.tcpSocket connectToHost:projector.ipAddress onPort:PORT_EZWIFIDISPLAY withTimeout:5 error:nil];
}
-(void) disconnect{
    [self.tcpSocket disconnect];
}

-(void) dealloc{
    @synchronized(self){
        DLog(@"%s:start", __PRETTY_FUNCTION__);
        stopThread = YES;
        self.bufferController.stopThread = YES;
        [self stopMediaStreaming];
        [self releaseHeartbeatDispatchTimer];
        [self.tcpSocket disconnect];
        self.tcpSocket.delegate = nil;
        self.mediaStreamingDelegate = nil;
        self.applicationDelegate = nil;
        
        [AMDPFImageSender endCurrentSession];
        
        [self.mediaHttpServer stop];
    
         //make sure the this semaphore is greater or equal than the value it is created.
        DLog(@"%s:stop", __PRETTY_FUNCTION__);
        
        @try{
            [self.bytesMeter removeObserver:self forKeyPath:@"speed"];
        }@catch(id anException){
            NSLog(@"Unable to remove observer on BytesMeter, keyPath: 'speed'");
        }
    }
    
}

-(void) newRequestLayout:(AMPicoRequest *)request{
    [self sendPicoCommand:request];
}
-(AMProjectorDevice *) connectedProjector{
    return mainProjector;
}

-(void) retrieveWebURL:(AMDPFSenderWebURLBlock)block{
    AMPicoQueryInfoCommand * queryCommand = [[AMPicoQueryInfoCommand alloc] init];
    
    [self sendPicoCommand:queryCommand];
    
    self.retrieveWebURLBlock = block;
    
}

-(BOOL) readyToSendObj{
    return self.readyToSendImage;
}


-(void) sendObject:(id)aObj{
    self.readyToSendImage = NO;
    if([aObj isKindOfClass:[AMPicoCommand class]]){
        [self sendPicoCommand:aObj];
    }
    else{
        DLog(@"");
    }
}

#pragma mark - Send Queue
//-(void) sendPicoCommandEX:(NSData*)picoCommand{
//    AMPicoImage * picoImage = [[AMPicoImage alloc] initWithCompressedData:picoCommand];
//    [self sendPicoCommand:picoImage];
//}
-(void) sendPicoCommand:(AMPicoCommand*)picoCommand{
    NSData * commandData = [picoCommand data];
    TCP_HEADER header;
    header.sequence = self.sequence;
    header.size = (uint32_t) commandData.length;
    NSData * headerData = [NSData dataWithBytes:&header length:sizeof(TCP_HEADER)];
//    DLog(@"AsyncCommand->(sequence:%u)(command.tag:%d)", weakSelf.sequence, picoCommand.tag);

    long tag = 0;
    if([picoCommand isKindOfClass:[AMPicoImage class]]){
        tag = TCP_TAG_WRITE_PICOIMAGE;
    }
    else if([picoCommand isKindOfClass:[AMPicoMediaStreamingDataBlock class]]){
        tag = TCP_TAG_WRITE_STREAM;
    }
    else if([picoCommand isKindOfClass:[AMPicoHeartbeat class]]){
        tag = TCP_TAG_WRITE_HEARTBEAT;
    }
    else if([picoCommand isKindOfClass:[AMPicoAudio class]]){
        
        tag = TCP_TAG_WRITE_AUDIO;
    }
    else if([picoCommand isKindOfClass:[AMPicoH264Data class]]){
        tag = TCP_TAG_WRITE_H264;
    }
    else{
        tag = TCP_TAG_WRITE_OTHER_COMMAND;
    }
    
    @synchronized (_tcpSocket){
//        if(tag == TCP_TAG_WRITE_H264){
//            startTime = CFAbsoluteTimeGetCurrent();
//            measureSize = commandData.length;
//            endTime = startTime;
//        }
        [_tcpSocket writeData:headerData withTimeout:self.timeoutInterval>0?self.timeoutInterval:TCP_TIMEOUT tag:TCP_TAG_WRITE_HEADER];
        [self.sendBytes addObject:@(headerData.length)];
        [_tcpSocket writeData:commandData withTimeout:self.timeoutInterval>0?self.timeoutInterval:TCP_TIMEOUT tag:tag];
        [self.sendBytes addObject:@(commandData.length)];
        _sequence++;
    }
}

// 2012.10.8
// Producer of buffer queue
// The send image operation use a Producer-Consumer pattern.
// Sync is made by using condition lock

// 2013.5.23 review, the implementation need to be re-implement to DoubleBuffer-like scheme.
-(void) sendImageWithData:(NSData *) imageData{
    AMPicoImage * picoImage = [[AMPicoImage alloc] initWithCompressedData:imageData];
    [self.bufferController sendObject:picoImage];
}

#if TARGET_OS_IPHONE
-(void) sendImage:(UIImage*)image{
    [self sendImageWithData:UIImageJPEGRepresentation(image, 0.7)];
}
#else
-(void) sendImage:(NSImage*)image{
    NSArray *representations; NSData * bitmapData;
    representations = [image representations];
    NSNumber * rate = @0.5;
    NSDictionary * properties = [NSDictionary dictionaryWithObjectsAndKeys:rate, NSImageCompressionFactor,nil];
    bitmapData = [NSBitmapImageRep representationOfImageRepsInArray:representations usingType:NSJPEGFileType properties:properties];
//    DLog(@"length:%ldKB", bitmapData.length/1024);
    
    NSUInteger imageSizeLimit = 0;
//    if([self.applicationDelegate respondsToSelector:@selector(imageSizeLimit)])
//        imageSizeLimit = [self.applicationDelegate imageSizeLimit];
    
    if(imageSizeLimit == NSUIntegerMax){    //no limit we
        rate = @0.7;
        properties = [NSDictionary dictionaryWithObjectsAndKeys:rate, NSImageCompressionFactor,nil];
        bitmapData = [NSBitmapImageRep representationOfImageRepsInArray:representations usingType:NSJPEGFileType properties:properties];
    }
    else if(imageSizeLimit!=0 && ((NSUInteger)bitmapData.length/1024) > imageSizeLimit){
        float relationFactor = imageSizeLimit/(bitmapData.length/1024);
        rate = @(relationFactor*0.5);
        properties = [NSDictionary dictionaryWithObjectsAndKeys:rate, NSImageCompressionFactor,nil];
        bitmapData = [NSBitmapImageRep representationOfImageRepsInArray:representations usingType:NSJPEGFileType properties:properties];
//        DLog(@"resize to %ldKB", bitmapData.length/1024);
    }

    [self sendImageWithData:bitmapData];
}
#endif

-(void) sendH264Data:(NSData*)data width:(uint32_t)width height:(uint32_t)height{
    AMPicoH264Data * h264DataCommand = [[AMPicoH264Data alloc] initWithH264Data:data width:width height:height];
    [self sendObject:h264DataCommand];
}


-(void) sendPCMData:(NSData*) pcmData{
    NSAssert(pcmData!=nil, @"%s, para pcm nil", __PRETTY_FUNCTION__);
    @autoreleasepool {
        AMPicoAudio * pcmDataCommand = [[AMPicoAudio alloc] initWithPCMData:pcmData];
        self.readyToSendAudio = NO;
        [self sendPicoCommand:pcmDataCommand];
    }
}

-(void) installHeartbeatDispatchTimer{
    DTrace();
    dispatch_time_t now = dispatch_walltime(DISPATCH_TIME_NOW, 0);
    heartbeatTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)); //not the key
    dispatch_source_set_timer(heartbeatTimer, now, NSEC_PER_SEC*2ull, 0ull);
    __weak AMDPFImageSender * weakSelf = self;
    dispatch_source_set_event_handler(heartbeatTimer, ^{
        AMPicoHeartbeat * heart = [[AMPicoHeartbeat alloc] init];
        [weakSelf sendPicoCommand:heart];
    });
    dispatch_resume(heartbeatTimer);
}

-(void) releaseHeartbeatDispatchTimer{
    if(heartbeatTimer){
        DTrace();
        heartbeatTimer = nil;
    }
}

-(void) audioConsumerThread{
    [NSThread setThreadPriority:1.0];
    while(stopThread==NO){
        [self.audioConditionLock lockWhenCondition:DATA_HAS_DATA];
        {
            @autoreleasepool {
                if(self.audioBufferPool.count>0){
                    AMPicoAudio * pcmDataCommand = (self.audioBufferPool)[0];
                    [self sendPicoCommand:pcmDataCommand];
                    [self.audioBufferPool removeObject:pcmDataCommand];
                }
            }
        }
        [self.audioConditionLock unlockWithCondition:(self.audioBufferPool.count>0)? DATA_HAS_DATA : DATA_NO_DATA];
    }
    DLog(@"%s Audio consumer thread stopped",__PRETTY_FUNCTION__);
}

// Consumer of buffer queue
-(void) startConsumerThreads{
    DTrace();
    @synchronized(self){
        if(self.isConsumerWorking == NO){
            stopThread = NO;
            self.bufferController.stopThread = NO;
            //    [NSThread detachNewThreadSelector:@selector(imageConsumerThread) toTarget:self withObject:nil];
            [self.bufferController startConsumerThreads];
            [NSThread detachNewThreadSelector:@selector(audioConsumerThread) toTarget:self withObject:nil];
            self.isConsumerWorking = YES;
        }
        else{
            DLog(@"alreadly working");
        }
    }
    

}

#pragma mark - Processing data from sockets
-(void) processAMPicoRequest:(AMPicoRequest *) request{
    DLog(@"%s, %@", __PRETTY_FUNCTION__, request);
    __weak AMDPFImageSender * weakSelf = self;
    

    
    if(request.isSplitRequestAccept)    //request accept
    {
        
        if(self.bufferController.cacheObj){
            [self sendPicoCommand:self.bufferController.cacheObj];
        }
        
        mainProjector.split = request.splitNumber;
        mainProjector.position = request.splitPosition;
        if([self.applicationDelegate respondsToSelector:@selector(projectorRequest:status:fromObject:)])
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.applicationDelegate projectorRequest:request status:YES fromObject:weakSelf];
            });
        //protocol V2 start send after request is permitted
        [self startConsumerThreads];
    }
    else{
        if([self.applicationDelegate respondsToSelector:@selector(projectorRequest:status:fromObject:)])
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.applicationDelegate projectorRequest:request status:NO fromObject:weakSelf];
            });
    }
}

-(void) processAMPicoNotification:(AMPicoNotification *) picoNotification{
    DLog(@"%s: %@", __PRETTY_FUNCTION__, picoNotification);
    switch (picoNotification.notificationType) {
        case NOTIFICATION_STOP_STREAM:
            
            stopThread = YES;
            self.bufferController.stopThread = YES;
            self.isConsumerWorking = NO;
            [self.bufferController removeAllBuffer];
            [self.audioBufferPool removeAllObjects];
            
            if([self.applicationDelegate respondsToSelector:@selector(projectorDidNotifyToStop:)]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.applicationDelegate projectorDidNotifyToStop:self];
                });
            }
            DLog(@"End of NOTIFICATION_STOP_STREAM");
            break;
        case NOTIFICATION_PLEASE_TRY: {
            // if we receive a notification(try), we need to check if there is any chache image, if yes, request to project
            if(self.bufferController.cacheObj){
                [self sendPicoCommand:self.bufferController.cacheObj];
            }
            AMPicoRequest * request = [[AMPicoRequest alloc] init];
            request.splitNumber = picoNotification.data1;
            request.splitPosition = picoNotification.data2;
            [self newRequestLayout:request];
            
            if([self.applicationDelegate respondsToSelector:@selector(projectorDidNotifyToStart:)]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.applicationDelegate projectorDidNotifyToStart:self];
                });
            }
            break;
        }
        case NOTIFICATION_DISCONNECT:
            
            [self stopMediaStreaming];
            
            if([self.applicationDelegate respondsToSelector:@selector(projectorDidNotifyToDisconnect:)]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.applicationDelegate projectorDidNotifyToDisconnect:self];
                });
            }
            break;
        case NOTIFICATION_CHANGE_STREAM:
        {
            //resend a cache image data to projector
            if(self.bufferController.cacheObj){
                [self sendPicoCommand:self.bufferController.cacheObj];
            }
            AMPicoRequest * request = [[AMPicoRequest alloc] init];
            request.splitNumber = picoNotification.data1;
            request.splitPosition = picoNotification.data2;
            [self newRequestLayout:request];
            
            break;
        }
        default:
            break;
    }
}

-(void) startReadFileWithPosition:(uint64_t) position{
    DTrace();
    DLog(@"read command offset:%lld", position);
    [self sendPicoCommand:[AMPicoMediaStreamingCommand readMediaStreamingCommand:position]];
    [_fileDataSource startReadFileWithPosition:position];

}

- (void)stopReadingFileAndWait {
    DTrace();
    [_fileDataSource stopReadingFile];
    dispatch_semaphore_signal(socketSemaphore); //letting the last read operation complete it's data sending.
    [_fileDataSource waitUntilStopReading]; //Wait until the previous read batch operation has completed and finish stopping. 
}
- (void)projectorDidAcceptMediaStreaming {
    if ([self.mediaStreamingDelegate respondsToSelector:@selector(projectorDidAcceptMediaStreaming)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.mediaStreamingDelegate projectorDidAcceptMediaStreaming];
        });
    }
}

- (void)projectorRejectedMediaStreaming {
    if([self.mediaStreamingDelegate respondsToSelector:@selector(projectorDidRejectMediaStreaming:)]){
        dispatch_async(dispatch_get_main_queue(), ^{
            NSError * error = [NSError errorWithDomain:kAMMediaStreamingStandardErrorDomain code:AV_RESULT_ERROR_START_OCCUPIED_OTHER_USER
                                              userInfo:@{NSLocalizedDescriptionKey:@"Media streaming is not permitted right now. The projector is probably in a split-screen state."}];
            [self.mediaStreamingDelegate projectorDidRejectMediaStreaming:error];
        });
    }
}

-(void) processAMPicoMediaStreamingCommand:(AMPicoMediaStreamingCommand *) streamCommand{
   
    DLog(@"%s, %@", __PRETTY_FUNCTION__,streamCommand);

    
    switch (streamCommand.fileOperation) {
        case AV_FILE_START:
        case AV_FILE_START_AUDIO:
            if(streamCommand.requestResult == AV_RESULT_OK){
                DLog(@"**** GO STREAMING!!!");
                [self projectorDidAcceptMediaStreaming];
            }
            else{
                DLog(@"**** STREAMING FAIL!!!!!");
                [self projectorRejectedMediaStreaming];
            }
            DLog(@"*** -> AV_FILE_START:%zd", streamCommand.requestResult);
            break;
            
        case AV_FILE_STOP:
            if (_fileDataSource) {
                [_fileDataSource stopMediaStreaming];
                DLog(@"receive STOP, send it back!");
                [self sendPicoCommand:[AMPicoMediaStreamingCommand stopMediaStreamingCommand]];
                if([self.mediaStreamingDelegate respondsToSelector:@selector(projectorDidEndMediaStreamingWithError:)]){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSError * error = nil;
                        if(streamCommand.requestResult == AV_RESULT_OK){
                            error = nil;
                        }
                        else if(streamCommand.requestResult == AV_RESULT_ERROR_STOP_ABORTED){
                            NSString * localizedDescription = [NSString stringWithFormat:@"(%@)%@", _fileDataSource.filePath.lastPathComponent, NSLocalizedStringFromTable(@"Streaming is aborted by device", @"AMCommon", @"DPFSender - Streaming is aborted by device")];
                            NSDictionary * userInfo = @{NSLocalizedDescriptionKey:localizedDescription, kAMMediaStreamingSourceURL:_fileDataSource.filePath };
                            error = [NSError errorWithDomain:kAMMediaStreamingStandardErrorDomain code:AV_RESULT_ERROR_STOP_ABORTED userInfo:userInfo];
                        }
                        else if(streamCommand.requestResult == AV_RESULT_ERROR_STOP_FILE_FORMAT_UNSOPPORTED){
                            NSString * localizedDescription = [NSString stringWithFormat:@"(%@)%@", _fileDataSource.filePath.lastPathComponent, NSLocalizedStringFromTable(@"Unsupported or corrupted file", @"AMCommon", @"DPFSender - Unsupported or corrupted file")];
                            NSDictionary * userInfo = @{NSLocalizedDescriptionKey:localizedDescription, kAMMediaStreamingSourceURL:_fileDataSource.filePath};
                            error = [NSError errorWithDomain:kAMMediaStreamingStandardErrorDomain code:AV_RESULT_ERROR_STOP_FILE_FORMAT_UNSOPPORTED userInfo:userInfo];
                        }
                        [self.mediaStreamingDelegate projectorDidEndMediaStreamingWithError:error];
                    });
                }
            }
            break;
        case AV_FILE_GET_LENGTH:
        {
            DLog(@"(1)file length:%llu", [_fileDataSource getFileLength]);
            [self sendPicoCommand:[AMPicoMediaStreamingCommand getLengthMediaStreamingCommand:[_fileDataSource getFileLength]]];
            DLog(@"(2)file length:%llu", [_fileDataSource getFileLength]);
            
        }
            break;
        case AV_FILE_GET_SEEKABLE:

            [self sendPicoCommand:[AMPicoMediaStreamingCommand seekableMediaStreamingCommand]];
            break;
        case AV_FILE_READ:
        {
            uint64_t filePos = streamCommand.additionalData;
            DLog(@"read file offset:%llu",filePos);
            [self stopReadingFileAndWait];
            [self startReadFileWithPosition:filePos];
        }
            break;
        case AV_FILE_PAUSE:
            [self stopReadingFileAndWait];
            
            break;
        case AV_FILE_EOF:
            
            break;
        case AV_PLAYER_GET_LENGTH:{
            uint64_t time = streamCommand.additionalData;
//            DLog(@"total time:%lld", time);
            if([self.mediaStreamingDelegate respondsToSelector:@selector(mediaStreamingTotalTime:)]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    //Spec: Project wants 32bit long data!
                    [self.mediaStreamingDelegate mediaStreamingTotalTime:(uint32_t)time];
                });
            }
            break;
        }
        case AV_PLAYER_GET_TIME:
        {
            uint64_t time = streamCommand.additionalData;
//            DLog(@"current time:%lld", time);
            if([self.mediaStreamingDelegate respondsToSelector:@selector(mediaStreamingElapseTime:)]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    //Spec: Project wants 32bit long data!
                    [self.mediaStreamingDelegate mediaStreamingElapseTime:(uint32_t)time];
                });
            }
            break;
        }
        case AV_HTTP_START: {
            if(streamCommand.requestResult == AV_RESULT_OK){
                DLog(@"(http)**** GO STREAMING!!!");
                [self projectorDidAcceptMediaStreaming];
            }
            else{
                DLog(@"(http)**** STREAMING FAIL!!!!!");
                [self projectorRejectedMediaStreaming];
            }
            DLog(@"(http)*** -> AV_HTTP_START:%zd", (NSInteger)streamCommand.requestResult);
            break;
        }
        case AV_HTTP_STOP: {
            DLog(@"(http)receive STOP, send it back!");
            AMPicoMediaStreamingCommand * stopCommand = [AMPicoMediaStreamingCommand stopHTTPMeidaStreamingCommand] ;
            [self sendPicoCommand: stopCommand];
            if([self.mediaStreamingDelegate respondsToSelector:@selector(projectorDidEndMediaStreamingWithError:)]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSError * error = nil;
                    if(streamCommand.requestResult == AV_RESULT_OK){
                        error = nil;
                    }
                    else if(streamCommand.requestResult == AV_RESULT_ERROR_STOP_ABORTED){
                        NSDictionary * userInfo = @{NSLocalizedDescriptionKey:NSLocalizedStringFromTable(@"Streaming is aborted by device", @"AMCommon", @"DPFSender - Streaming is aborted by device"), kAMMediaStreamingSourceURL: _httpDataSource.url};
                        error = [NSError errorWithDomain:kAMMediaStreamingStandardErrorDomain code:AV_RESULT_ERROR_STOP_ABORTED userInfo:userInfo];
                    }
                    else if(streamCommand.requestResult == AV_RESULT_ERROR_STOP_FILE_FORMAT_UNSOPPORTED){
                        NSDictionary * userInfo = @{NSLocalizedDescriptionKey:NSLocalizedStringFromTable(@"Unsupported or corrupted file", @"AMCommon", @"DPFSender - Unsupported or corrupted file"), kAMMediaStreamingSourceURL: _httpDataSource.url};
                        error = [NSError errorWithDomain:kAMMediaStreamingStandardErrorDomain code:AV_RESULT_ERROR_STOP_FILE_FORMAT_UNSOPPORTED userInfo:userInfo];
                    }
                    [self.mediaStreamingDelegate projectorDidEndMediaStreamingWithError:error];
                });
            }

            break;
        }
        case AV_HTTP_GET_URL: {
            NSAssert(_httpDataSource != nil, @"_httpDataSource should not be nil");
            if (_httpDataSource != nil) {
                [self sendPicoCommand:[AMPicoMediaStreamingCommand getURLResponseWithURL:_httpDataSource.url]];
            }
            break;
        }
        case AV_HTTP_GET_USERAGENT: {
            NSAssert(_httpDataSource != nil, @"_httpDataSource should not be nil");
            if (_httpDataSource != nil) {
                [self sendPicoCommand:[AMPicoMediaStreamingCommand getUserAgentResponseWithUserAgent:_httpDataSource.userAgent]];
            }
            break;
        }
        default:
            break;
    }

}

-(void) processAMQueryInfoCommand:(AMPicoQueryInfoCommand*) queryInfo rawData:(NSData*)rawData{
    
    @autoreleasepool {
        DLog(@"%s: webURL: %@", __PRETTY_FUNCTION__, queryInfo.webURL);
        self.retrieveWebURLBlock(queryInfo.webURL);
        self.retrieveWebURLBlock = nil;
    }
}


-(void) processAMPicoCommand:(AMPicoCommand *) command rawData:(NSData*)rawData{
    DLog(@"%s, %@", __PRETTY_FUNCTION__, command);
    @autoreleasepool {
        @synchronized(self){
            if(command.tag==PICO_REQUEST){
                AMPicoRequest * result = [[AMPicoRequest alloc] initWithData:rawData];
                [self processAMPicoRequest:result];
            }
            else if(command.tag==PICO_NOTIFICATION){
                AMPicoNotification * picoNotification = [[AMPicoNotification alloc] initWithData:rawData];
                [self processAMPicoNotification:picoNotification];
            }
            else if(command.tag==PICO_SET_DATA){
                //
            }
            else if(command.tag==PICO_AV_STREAM_CMD){
                AMPicoMediaStreamingCommand * streamCommand = [[AMPicoMediaStreamingCommand alloc] initWithData:rawData];
                [self processAMPicoMediaStreamingCommand:streamCommand];
            }
            else if (command.tag ==PICO_QUERY_INFO){
                AMPicoQueryInfoCommand * queryInfo = [[AMPicoQueryInfoCommand alloc] initWithData:rawData];
                [self processAMQueryInfoCommand:queryInfo rawData:rawData];
            }
            else{
                DLog(@"Unhandled AMPicoCommand:%@", command);
            }
        }
    }
}
-(void) requestForAutoScreenAssign{
    [self requestForNewSplit:0 newPosition:0];
}

-(void) requestForNewSplit:(NSInteger)newSplitNumber newPosition:(NSInteger) newPosition{
    AMPicoRequest * request = [[AMPicoRequest alloc] init];
    request.splitNumber = newSplitNumber;
    request.splitPosition = newPosition;
    DLog(@"%s :new request to projector:%@", __PRETTY_FUNCTION__, request);
    [self sendPicoCommand:request];
}

#pragma mark - GCDAsyncSocketDelegate

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port{
    DTrace();

//    // This is the way to disable nagle's algorithm
//    [self.tcpSocket performBlock:^{
//        int fd = [self.tcpSocket socketFD];
//        int on = 1;
//        if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (char*)&on, sizeof(on)) == -1) {
//            /* handle error */
//            DLog(@"error when setting TCP_NODELAY");
//        }
//    }];
    
    [self.bytesMeter reset];
    @synchronized (_tcpSocket){
        self.sendBytes = [NSMutableArray new];
    }
    
    self.readyToSendImage = YES;
    self.readyToSendAudio = YES;
    
    if([mainProjector.protocolVersion isEqualToString:@"1"]==NO){    //protocol v2
        [self.tcpSocket readDataToLength:sizeof(TCP_HEADER) withTimeout:TCP_TIMEOUT tag:TCP_TAG_READ_HEADER];
        
        AMPicoSetDataCommand * setDataCommand = [[AMPicoSetDataCommand alloc] init];
#if TARGET_OS_IPHONE
        setDataCommand.hostname = [UIDevice currentDevice].name;
#else
        setDataCommand.hostname = [NSHost currentHost].localizedName;
#endif
        [self sendPicoCommand:setDataCommand];

    }
    else{   //protcol v1
        [self startConsumerThreads];
    }
    
    if([self.applicationDelegate respondsToSelector:@selector(projectConnectStatus:object:)]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.applicationDelegate projectConnectStatus:YES object:self];
        });
    }
    
    [self installHeartbeatDispatchTimer];

}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag{
    @synchronized (_tcpSocket){
        NSNumber * b = self.sendBytes.firstObject;
        if(b){
            [self.sendBytes removeObject:b];
            [self.bytesMeter addBytesCount:b.unsignedIntegerValue];
        }
    }

    dispatch_async(aQueue, ^{
        if (tag==TCP_TAG_WRITE_HEARTBEAT){
        }
        else if(tag == TCP_TAG_WRITE_HEADER ){
        }
        else if(tag==TCP_TAG_WRITE_PICOIMAGE || tag == TCP_TAG_WRITE_H264){
//            endTime = CFAbsoluteTimeGetCurrent();
//            CFAbsoluteTime elapsed = endTime - startTime;
//            if((float)measureSize/1024.0>100.0){
//            NSLog(@"h264 data size:%.2fKB (%.2fms)", (float)measureSize/1024.0, elapsed*1000);
//            }
            self.readyToSendImage = YES;
        }
        else if(tag==TCP_TAG_WRITE_STREAM){
            dispatch_async(aQueue, ^{
                dispatch_semaphore_signal(socketSemaphore);
            });
        }
        else if(tag == TCP_TAG_WRITE_OTHER_COMMAND){
        }
        else if(tag == TCP_TAG_WRITE_AUDIO){
            self.readyToSendAudio = YES;
        }
        else{
        }
    });
}


- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err{
    DLog(@"%s:%@", __FUNCTION__, err);

    @synchronized(self){
        [self stopMediaStreaming];
        [self releaseHeartbeatDispatchTimer];
        
        /**
         * When the socket disconnected, we should let the consumer safely exit it's thread.
         * 1. Set the stopThread flag to stop the thread loop.
         * 2. Unlock the conditional lock to exit the lock if it is locked.
         **/
        stopThread = YES;
        self.bufferController.stopThread = YES;
        [self.bufferController removeAllBuffer];
        [self.bufferController stopThreadGracefully];
        self.isConsumerWorking = NO;
        [self.audioBufferPool removeAllObjects];
        [self.audioConditionLock unlockWithCondition:DATA_HAS_DATA];
        
        /* After disconnection, we can't gurantee that this semaphore is signaled in socket:didWriteData:.(eg: disconnection just right after a send operation) So we need to signal it here.
         */
        dispatch_semaphore_signal(socketSemaphore); 
        
        self.tcpSocket.delegate = nil;
        self.tcpSocket = nil;
        
        mainProjector = nil;
        
        [self.bufferController removeAllBuffer];
        [self.audioBufferPool removeAllObjects];
        
        if([err.domain isEqualToString: NSPOSIXErrorDomain] && err.code == 61){ //connection refused
            if([self.applicationDelegate respondsToSelector:@selector(projectConnectStatus:object:)]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.applicationDelegate projectConnectStatus:NO object:self];
                });
            }
        }
        if([self.applicationDelegate respondsToSelector:@selector(projectorDidDisconnect:error:)]){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.applicationDelegate projectorDidDisconnect:self error:err];
            });
        }
        @try{
            [self.bytesMeter removeObserver:self forKeyPath:@"speed"];
        }@catch(id anException){
            NSLog(@"Unable to remove observer on BytesMeter, keyPath: 'speed'");
        }
    }


}


- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag{
    dispatch_async(aQueue, ^{
        if(tag==TCP_TAG_READ_HEADER){
            NSData *headerData = data;
            TCP_HEADER header;
            if(headerData.length == sizeof(TCP_HEADER)){
                memset(&header, 0, sizeof(TCP_HEADER));
                [headerData getBytes:&header length:sizeof(TCP_HEADER)];
                DLog(@"Received TCP header, sequence:%d, size:%d", header.sequence, header.size);
                [self.tcpSocket readDataToLength:header.size withTimeout:(-1) tag:TCP_TAG_READ_PAYLOAD];
            }
            else{
                DLog(@"Read TCP header error");
            }
        }
        else if(tag==TCP_TAG_READ_PAYLOAD){
            DLog(@"Received TCP payload, length:%lu", (unsigned long)data.length);
            AMPicoCommand * command = [[AMPicoCommand alloc] initWithData:data];
            [self processAMPicoCommand:command rawData:data];
            [self.tcpSocket readDataToLength:sizeof(TCP_HEADER) withTimeout:(-1) tag:TCP_TAG_READ_HEADER];
        }
        else{
            DLog(@"There might somthing error when receving PAYLOAD packet");
        }
    
    });

}

- (void)socket:(GCDAsyncSocket *)sock didReadPartialDataOfLength:(NSUInteger)partialLength tag:(long)tag{
    DTrace();
}
- (NSTimeInterval)socket:(GCDAsyncSocket *)sock shouldTimeoutReadWithTag:(long)tag
                 elapsed:(NSTimeInterval)elapsed
               bytesDone:(NSUInteger)length{
    DTrace();
    return 0;
}
//

#pragma mark - Media Streaming
-(void) requestForHttpMediaStreaming{
    DTrace();
    
    @synchronized(self)
    {
        NSString * filePath = nil;
        if ([self.mediaStreamingDelegate respondsToSelector:@selector(mediaLocation)]) {
            filePath = [self.mediaStreamingDelegate mediaLocation];
        }

        if (([filePath hasPrefix:@"http://"] || [filePath hasPrefix:@"https://"] ||
             [filePath hasPrefix:@"rtsp://"] || [filePath hasPrefix:@"mms://"] || [filePath hasPrefix:@"rtmp://"]) == NO) {
            filePath = [NSString stringWithFormat:@"http://%@:%d/LocalVideo.html?location=%@",
//                         [AMiDeviceIpAddress getWIFIIPAddress],
                         [self.tcpSocket localHost],
                         [self.mediaHttpServer listeningPort],
                         AFPercentEscapedQueryStringKeyFromStringWithEncoding(filePath, NSUTF8StringEncoding)];
        }
        DLog(@"Filepath:%@",filePath);
        if (filePath.length>0) {
            if ([self.mediaStreamingDelegate respondsToSelector:@selector(httpUserAgentString)] && [self.mediaStreamingDelegate httpUserAgentString]!=nil) {
                self.httpDataSource = [[AMMediaStreamingHTTPDataSource alloc] initWithURL:filePath
                                                                                userAgent:[self.mediaStreamingDelegate httpUserAgentString]
                                                                                   sender:self];
            } else {
                self.httpDataSource = [[AMMediaStreamingHTTPDataSource alloc] initWithURL:filePath
                                                                                userAgent:@"Mozilla/5.0 (iPad; CPU OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141"
                                                                                   sender:self];
            }
            [self sendPicoCommand:[AMPicoMediaStreamingCommand startHTTPMeidaStreamingCommand]];
        }
        else{
            if([self.mediaStreamingDelegate respondsToSelector:@selector(projectorDidEndMediaStreamingWithError:)]){
                NSError * error = [NSError errorWithDomain:kAMMediaStreamingStandardErrorDomain code:AV_RESULT_ERROR_URL_DIVERT_LINK_ERROR userInfo:nil];
                [self.mediaStreamingDelegate projectorDidEndMediaStreamingWithError:error];
            }
        }
    }
}
-(void) requestForAudioStreaming{
    DTrace();
    
    @synchronized(self)
    {
        NSString * filePath = nil;
        if ([self.mediaStreamingDelegate respondsToSelector:@selector(mediaLocation)]) {
            filePath = [self.mediaStreamingDelegate mediaLocation];
        }
        if ([filePath hasPrefix:@"http://"] ||
            [filePath hasPrefix:@"https://"]) {
            if ([self.mediaStreamingDelegate respondsToSelector:@selector(httpUserAgentString)] && [self.mediaStreamingDelegate httpUserAgentString]!=nil) {
                self.httpDataSource = [[AMMediaStreamingHTTPDataSource alloc] initWithURL:filePath
                                                                                 userAgent:[self.mediaStreamingDelegate httpUserAgentString]
                                                                                    sender:self];
            } else {
                self.httpDataSource = [[AMMediaStreamingHTTPDataSource alloc] initWithURL:filePath
                                                                                 userAgent:@"Mozilla/5.0 (iPad; CPU OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141"
                                                                                    sender:self];
            }
            [self sendPicoCommand:[AMPicoMediaStreamingCommand startHTTPMeidaStreamingCommand]];
        } else {
            self.fileDataSource = [[AMMediaStreamingFileDataSource alloc] initWithFile:filePath sender:self];
            [self sendPicoCommand:[AMPicoMediaStreamingCommand startAudioStreamingCommand]];
        }
    }
}

-(void) requestForMediaStreaming{
    DTrace();
    
    @synchronized(self)
    {
        NSString * filePath = nil;
        if ([self.mediaStreamingDelegate respondsToSelector:@selector(mediaLocation)]) {
            filePath = [self.mediaStreamingDelegate mediaLocation];
        }
        
        if ([filePath hasPrefix:@"http://"] || [filePath hasPrefix:@"https://"] || [filePath hasPrefix:@"rtsp://"] || [filePath hasPrefix:@"mms://"]) {
            if ([self.mediaStreamingDelegate respondsToSelector:@selector(httpUserAgentString)] && [self.mediaStreamingDelegate httpUserAgentString]!=nil) {
                self.httpDataSource = [[AMMediaStreamingHTTPDataSource alloc] initWithURL:filePath
                                                                                 userAgent:[self.mediaStreamingDelegate httpUserAgentString]
                                                                                    sender:self];
            } else {
                self.httpDataSource = [[AMMediaStreamingHTTPDataSource alloc] initWithURL:filePath
                                                                                 userAgent:@"Mozilla/5.0 (iPad; CPU OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141"
                                                                                    sender:self];
            }
            [self sendPicoCommand:[AMPicoMediaStreamingCommand startHTTPMeidaStreamingCommand]];
        } else if(filePath.length>0){
            self.fileDataSource = [[AMMediaStreamingFileDataSource alloc] initWithFile:filePath sender:self];
            [self sendPicoCommand:[AMPicoMediaStreamingCommand startMediaStreamingCommand]];
        }
        else{
            if([self.mediaStreamingDelegate respondsToSelector:@selector(projectorDidEndMediaStreamingWithError:)]){
                NSError * error = [NSError errorWithDomain:kAMMediaStreamingStandardErrorDomain code:AV_RESULT_ERROR_URL_DIVERT_LINK_ERROR userInfo:nil];
                [self.mediaStreamingDelegate projectorDidEndMediaStreamingWithError:error];
            }
        }
    }
}
-(void) stopMediaStreaming{
    @synchronized(self){
            DTrace();
            //After SVN8883, need to do clean up here.Because we won't receive stop again
            
            if(self.fileDataSource){
                AMPicoMediaStreamingCommand * command = [AMPicoMediaStreamingCommand stopMediaStreamingCommand];
                [self sendPicoCommand:command];
                
                [self stopReadingFileAndWait];
                [self.fileDataSource stopMediaStreaming];
                if([self.mediaStreamingDelegate respondsToSelector:@selector(projectorDidEndMediaStreamingWithError:)]){
                    NSDictionary * userInfo = @{NSLocalizedDescriptionKey:NSLocalizedStringFromTable(@"User canceled media streaming process", @"AMCommon", "DPFSender - User canceled media streaming process")};
                    NSError * error = [NSError errorWithDomain:kAMMediaStreamingStandardErrorDomain code:NSUserCancelledError userInfo:userInfo];
                    self.fileDataSource = nil;
                    [self.mediaStreamingDelegate projectorDidEndMediaStreamingWithError:error];
                }
            } else if(self.httpDataSource) {
                AMPicoMediaStreamingCommand * command = [AMPicoMediaStreamingCommand stopHTTPMeidaStreamingCommand];
                [self sendPicoCommand:command];
                
                if([self.mediaStreamingDelegate respondsToSelector:@selector(projectorDidEndMediaStreamingWithError:)]){
                    NSDictionary * userInfo = @{NSLocalizedDescriptionKey:NSLocalizedStringFromTable(@"User canceled media streaming process", @"AMCommon", "DPFSender - User canceled media streaming process")};
                    NSError * error = [NSError errorWithDomain:kAMMediaStreamingStandardErrorDomain code:NSUserCancelledError userInfo:userInfo];
                    self.httpDataSource = nil;
                    [self.mediaStreamingDelegate projectorDidEndMediaStreamingWithError:error];
                }
            }
    }
}
-(void) seekToTime:(uint32_t) seekTime{
    AMPicoMediaStreamingCommand * command = [[AMPicoMediaStreamingCommand alloc] init];
    command.fileOperation = AV_PLAYER_SEEKPLAY;
    command.dataType = AV_TYPE_INT32;
    command.dataSize = AV_DATA_SIZE_INT32;
    command.requestResult = AV_RESULT_OK;
    command.additionalData = seekTime;
    [self sendPicoCommand:command];
}

-(void) volumnUp{
    AMPicoMediaStreamingCommand * command = [AMPicoMediaStreamingCommand volumeUpMediaStreamingCommand];
    [self sendPicoCommand:command];
}
-(void) volumnDown{
    AMPicoMediaStreamingCommand * command = [AMPicoMediaStreamingCommand volumeDownMediaStreamingCommand];
    [self sendPicoCommand:command];
}
-(void) pauseMediaStreaming{
    AMPicoMediaStreamingCommand * command = [AMPicoMediaStreamingCommand pauseMediaStreamingCommand];
    [self sendPicoCommand:command];
}
-(void) resumeMediaStreaming{
    AMPicoMediaStreamingCommand * command = [AMPicoMediaStreamingCommand resumeMediaStreamingCommand];
    [self sendPicoCommand:command];
}
-(void) sendEOFMeidaStreamingCommand{
    [self sendPicoCommand:[AMPicoMediaStreamingCommand eofMediaStreamingCommand]];
}
- (void)sendMediaStreamingData:(NSData *)readData
{
    dispatch_semaphore_wait(socketSemaphore, DISPATCH_TIME_FOREVER);
    AMPicoMediaStreamingDataBlock * dataBlockCommand = [[AMPicoMediaStreamingDataBlock alloc] init];
    NSData * carryData = readData;
    dataBlockCommand.carryData = carryData;
    [self sendPicoCommand:dataBlockCommand];
}
#pragma mark Session Concept Method
+(void) beginSessionWithSender:(AMDPFImageSender*) sender{
    @synchronized([self class]){
        _currentSessionSender = sender;
    }
}
+(void) endCurrentSession{
    @synchronized([self class]){
        _currentSessionSender.applicationDelegate = nil;
        [_currentSessionSender disconnect];
        _currentSessionSender = nil;
    }
}
+(AMDPFImageSender*) currentSessionSender{
    @synchronized([self class]){
        return _currentSessionSender;
    }
}
+(BOOL) isCurrentlyAbleToSend{
    return ([AMDPFImageSender currentSessionSender]!=nil);
}

#pragma mark - 
-(NSString *) localHost {
    return self.tcpSocket.localHost;
}
static NSString* const kAFCharactersToBeEscapedInQueryString = @":/?&=;+!@#$',*";
static NSString * AFPercentEscapedQueryStringKeyFromStringWithEncoding(NSString *string, NSStringEncoding encoding) {
    static NSString * const kAFCharactersToLeaveUnescapedInQueryStringPairKey = @"[].";
    
    return (__bridge_transfer  NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)string, (__bridge CFStringRef)kAFCharactersToLeaveUnescapedInQueryStringPairKey, (__bridge CFStringRef)kAFCharactersToBeEscapedInQueryString, CFStringConvertNSStringEncodingToEncoding(encoding));
}
static NSByteCountFormatter * formatter = nil;
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    if(!formatter){
        formatter = [[NSByteCountFormatter alloc] init];
        formatter.countStyle =  NSByteCountFormatterCountStyleMemory;
    }
    if (object == self.bytesMeter && [keyPath isEqualToString:@"speed"]) {
        
//        let formatter = ByteCountFormatter()
//        formatter.countStyle = .memory
//        print("\(formatter.string(fromByteCount: Int64(secretary.speed)))")
        
        
//        DLog(@"tcp write speed: %@", [formatter stringFromByteCount: (long long)self.bytesMeter.speed]);
    }
}

@end
