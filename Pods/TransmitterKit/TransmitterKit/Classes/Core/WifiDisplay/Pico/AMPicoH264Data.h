//
//  AMh264Data.h
//  AMCommon_Mac
//
//  Created by brianliu on 2014/12/1.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMPicoCommand.h"

@interface AMPicoH264Data : AMPicoCommand
-(instancetype) initWithH264Data:(NSData*)data width:(uint32_t)width height:(uint32_t)height;
@property (nonatomic, assign) uint32_t width;
@property (nonatomic, assign) uint32_t height;
@property (nonatomic ,strong) NSData * h264Data;
@end
