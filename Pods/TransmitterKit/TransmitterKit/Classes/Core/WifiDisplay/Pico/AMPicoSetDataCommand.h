//
//  AMPicoSetDataCommand.h
//  EZRemoteControliOS
//
//  Created by brianliu on 12/9/21.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import "AMPicoCommand.h"

@interface AMPicoSetDataCommand : AMPicoCommand
@property (nonatomic, strong) NSString * hostname;
@property (nonatomic, assign) BOOL isSetDataAllow;
@end
