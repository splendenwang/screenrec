//
//  AMPicoMediaStreamingCommand.h
//  EZRemoteControliOS
//
//  Created by brianliu on 12/9/26.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import "AMPicoCommand.h"
#import "AMMediaStreamingStandardErrorDefinition.h"
@interface AMPicoMediaStreamingCommand : AMPicoCommand

@property (nonatomic, assign) AV_FILE fileOperation;

@property (nonatomic, assign) AV_DATA_SIZE dataSize;

@property (nonatomic, assign) AV_TYPE dataType;

@property (nonatomic, assign) AMMediaStreamingResultCode requestResult;

@property (nonatomic, assign) uint64_t additionalData;

@property (nonatomic, weak) NSData *stringData;

+(AMPicoMediaStreamingCommand *) startMediaStreamingCommand;
+(AMPicoMediaStreamingCommand *) startAudioStreamingCommand;
+(AMPicoMediaStreamingCommand *) getLengthMediaStreamingCommand:(uint64_t)fileLength;
+(AMPicoMediaStreamingCommand *) seekableMediaStreamingCommand;
+(AMPicoMediaStreamingCommand *) readMediaStreamingCommand:(uint64_t) offset;
+(AMPicoMediaStreamingCommand *) stopMediaStreamingCommand;
+(AMPicoMediaStreamingCommand *) endMediaStreamingCommand;
+(AMPicoMediaStreamingCommand *) eofMediaStreamingCommand;
+(AMPicoMediaStreamingCommand *) volumeUpMediaStreamingCommand;
+(AMPicoMediaStreamingCommand *) volumeDownMediaStreamingCommand;
+(AMPicoMediaStreamingCommand *) pauseMediaStreamingCommand;
+(AMPicoMediaStreamingCommand *) resumeMediaStreamingCommand;
+(AMPicoMediaStreamingCommand *) getURLResponseWithURL:(NSString *)url;
+(AMPicoMediaStreamingCommand *) getUserAgentResponseWithUserAgent:(NSString *)userAgent;
+(AMPicoMediaStreamingCommand *) startHTTPMeidaStreamingCommand;
+(AMPicoMediaStreamingCommand *) stopHTTPMeidaStreamingCommand;
@end
