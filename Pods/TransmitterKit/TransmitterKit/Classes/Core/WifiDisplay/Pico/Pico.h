//
//  Pico.h
//  EZRemoteControliOS
//
//  Created by brianliu on 12/8/20.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//


#ifndef EZRemoteControliOS_Pico_h
#define EZRemoteControliOS_Pico_h
typedef struct _TCP_HEADER{
    uint32_t sequence;
    uint32_t size;
}TCP_HEADER;


typedef NS_OPTIONS(NSUInteger, FLAG_TYPE){
	FLAG_TYPE_PC_PROJECTOR = 0x00,
	FLAG_TYPE_PROJECTOR_PC = 0x01
};

/* Declare the special image header data here */
typedef struct PICO_CMD_S
{
    uint32_t  tag; //unique per command ID
    unsigned char flag; //0:pc->projector 1:projector->pc
    unsigned char len; //CDB len
    unsigned char reserve0;
    unsigned char reserve1;
    signed char   CDB[16];
}PICO_CMD_T;


typedef NS_ENUM(NSInteger, ezRequestResult_e){
	REQUEST_RESULT_ALLOW = 1,
	REQUEST_RESULT_DENY,
	REQUEST_RESULT_NOT_SUPPORTED,
	REQUEST_RESULT_INVALID_PARAM,
	REQUEST_RESULT_FULL,
	REQUEST_RESULT_NOT_AVAILABLE,
};

typedef struct PICO_HBSC_S
{
    char H;
    char B;
    char S;
    char C;
}PICO_HBSC_T;

typedef struct PICO_STATUS_S
{
    unsigned int  p_w;
    unsigned int  p_h;
    PICO_HBSC_T	  hbcs;
    unsigned int  chip_id;
}PICO_STATUS_T;


typedef struct PICO_REQUEST_S
{
	uint32_t	request;
	uint32_t	request_data1;
	uint32_t	request_data2;
//	uint32_t	request_result;
	union
	{
		uint32_t	isize;
		uint32_t	request_result;
		uint32_t	format;
	};
}PICO_REQUEST_T;


#define NOTIFICATION_PLEASE_TRY             1
#define NOTIFICATION_STOP_STREAM			2
#define NOTIFICATION_CHANGE_STREAM			3
#define NOTIFICATION_DISCONNECT				4

typedef struct PICO_NOTIFICATION_S
{
	uint32_t notification;
	uint32_t notification_data1;
	uint32_t notification_data2;
	uint32_t reserve0;
}PICO_NOTIFICATION_T;


#define OUT_PIX_FMT_JPEG			1
#define OUT_PIX_FMT_YUV420          2
#define OUT_PIX_FMT_YUV422          3
#define OUT_PIX_FMT_EZENCODE		4
#define OUT_SND_FMT_PCM             5
#define OUT_AV_FMT_EZSTREAM         6
#define OUT_PIX_FMT_EZENCODEPRO     7   //actually, it's just h.264

typedef struct PICO_PIC_FORMAT_S
{
	uint32_t  format;
	uint32_t  width;
	uint32_t  height;
	uint32_t  isize;
}PICO_PIC_FORMAT_T;


#define INFO_WEB_CONSOLE_URL						1
#define INFO_TCP_CONNECTIONS						2
#define INFO_DISP_LAYOUT                            3

#define QUERY_INFO_OK				1
#define QUERY_INFO_ERROR			2
#define QUERY_INFO_NOT_SUPPORTED	3

typedef struct PICO_QUERY_INFO_S
{
	uint32_t 	info;
	uint32_t	isize;
	uint32_t	query_result;
	uint32_t	query_reply_data;
}PICO_QUERY_INFO_T;



#define SET_DATA_HOSTNAME               1
#define SET_DATA_RESULT_ALLOW			1
#define SET_DATA_RESULT_DENY			2
#define SET_DATA_RESULT_NOT_SUPPORTED	3
#define SET_DATA_RESULT_INVALID_PARAM	4

///setting hostname
typedef struct PICO_SET_DATA_S
{
	uint32_t	data_id;    //SET_DATA_HOSTNAME
	uint32_t	set_data1;
	uint32_t	set_data2;
	union
	{
		uint32_t	isize;
		uint32_t	set_data_result;
	};
}PICO_SET_DATA_T;


typedef NS_ENUM(NSInteger, AV_FILE){
    AV_FILE_START				=1,
    AV_FILE_STOP				=2,
    AV_FILE_GET_LENGTH          =3,
    AV_FILE_GET_SEEKABLE		=4,
    AV_FILE_READ				=5,
    AV_FILE_PAUSE				=6,
    AV_FILE_EOF                 =7,
    AV_PLAYER_GET_LENGTH		=8,
    AV_PLAYER_GET_TIME          =9,
    AV_PLAYER_SEEKPLAY          =10,
    AV_PLAYER_PAUSE             =11,
    AV_PLAYER_RESUME			=12,
    AV_PLAYER_FFPLAY			=13,
    AV_PLAYER_FBPLAY			=14,
    AV_PLAYER_RESET             =15,
    AV_PLAYER_VOLUME_UP         =16,
    AV_PLAYER_VOLUME_DOWN       =17,
    AV_HTTP_START               =18,
    AV_HTTP_STOP                =19,
    AV_HTTP_GET_URL             =20,
    AV_HTTP_GET_USERAGENT       =21,
    AV_FILE_START_AUDIO         =22
} ;


typedef NS_ENUM(NSInteger, AV_TYPE){
    AV_TYPE_VOID				=0,
    AV_TYPE_INT32				=1,
    AV_TYPE_INT64				=2,
    AV_TYPE_UTF8                =3
} ;

typedef NS_ENUM(NSUInteger, AV_DATA_SIZE){
    AV_DATA_SIZE_VOID           =0,
    AV_DATA_SIZE_INT32          =4,
    AV_DATA_SIZE_INT64          =8
};

typedef struct PICO_AV_STREAM_S
{
	uint32_t  request_cmd;
	uint32_t  request_data_type;
	uint32_t  request_data_size;
	uint32_t  request_result;
} PICO_AV_STREAM_T;

typedef NS_ENUM(NSInteger, PicoCommandFormat){
	PICO_STATUS_CMD = 0x01,
	PICO_PIC_FORMAT_CMD,
	PICO_HBSC_CMD,
	PICO_HEARTBEAT,
	PICO_QUERY_INFO,
	PICO_REQUEST,
	PICO_NOTIFICATION,
    PICO_AV_STREAM_CMD,
    PICO_SET_DATA,
	PICO_VENDOR_CMD = 0x40,
};



#endif
