//
//  AMQueryInfo.m
//  AMCommon_Mac
//
//  Created by brianliu on 13/3/8.
//  Copyright (c) 2013年 ActionsMicro. All rights reserved.
//

#import "AMPicoQueryInfoCommand.h"
#import "Pico.h"
@implementation AMPicoQueryInfoCommand
@synthesize webURL = _webURL;

-(instancetype) init{
    self = [super init];
    if(self){
        self.tag = PICO_QUERY_INFO;
        self.flag = FLAG_TYPE_PC_PROJECTOR;
        self.len =sizeof(PICO_QUERY_INFO_T);
    }
    return self;
}

-(instancetype) initWithData:(NSData *)data{
    self = [super initWithData:data];
    if(self){
        PICO_QUERY_INFO_T queryInfo;
        memset(&queryInfo, 0, sizeof(PICO_QUERY_INFO_T));
        [self.CDB getBytes:&queryInfo length:sizeof(PICO_QUERY_INFO_T)];

        
        if(queryInfo.query_result == QUERY_INFO_OK){
            if(queryInfo.info == INFO_WEB_CONSOLE_URL){
                NSData * copyData = [NSData dataWithData:data];
                char lastByte = 0;
                [copyData getBytes:&lastByte range:NSMakeRange(copyData.length-1, 1)];
                if(lastByte=='\0'){
                    DLog(@"data is null terminated. Correcting data");
                    
                    copyData = [copyData subdataWithRange:NSMakeRange(0, copyData.length-1)];
                }
                
                NSMutableData * webURLData = [NSMutableData dataWithData:[copyData subdataWithRange:NSMakeRange( copyData.length - (NSUInteger)queryInfo.isize, queryInfo.isize)]];
                
                _webURL = [[NSURL alloc] initWithString:[[NSString alloc] initWithBytes:webURLData.bytes length:webURLData.length encoding:NSASCIIStringEncoding]];
                
            }
        }
    }
    return self;
}


-(NSData *) data{
    
    PICO_QUERY_INFO_T queryInfo;
    memset(&queryInfo, 0, sizeof(PICO_QUERY_INFO_T));
    queryInfo.info = INFO_WEB_CONSOLE_URL;
    self.CDB = [NSData dataWithBytes:&queryInfo length:sizeof(PICO_QUERY_INFO_T)];
    
    NSMutableData * returnData = [[NSMutableData alloc] initWithData:[super data]];
    
    return returnData;
}
@end
