//
//  AMPicoAudio.h
//  AMCommon_Mac
//
//  Created by brianliu on 12/10/22.
//  Copyright (c) 2012年 ActionsMicro. All rights reserved.
//

#import "AMPicoCommand.h"

@interface AMPicoAudio : AMPicoCommand
-(instancetype) initWithPCMData:(NSData*) pcmData NS_DESIGNATED_INITIALIZER;
@property (nonatomic, strong) NSData* pcmData;
@end
