//
//  AMh264Data.m
//  AMCommon_Mac
//
//  Created by brianliu on 2014/12/1.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMPicoH264Data.h"

@implementation AMPicoH264Data

-(instancetype) initWithH264Data:(NSData*)data width:(uint32_t)width height:(uint32_t)height{
    self = [super initWithData:nil];
    if(self){
        self.tag = PICO_PIC_FORMAT_CMD;
        self.flag = FLAG_TYPE_PC_PROJECTOR;
        self.len = sizeof(PICO_PIC_FORMAT_T);
        self.width = width;
        self.height = height;
        self.h264Data = data;

    }
    return self;
}

-(NSData *) data{
    
    PICO_PIC_FORMAT_T picoImgFormat;
    memset(&picoImgFormat, 0, sizeof(PICO_PIC_FORMAT_T));
    picoImgFormat.format = NSSwapHostIntToLittle(OUT_PIX_FMT_EZENCODEPRO);
    picoImgFormat.width = NSSwapHostIntToLittle(self.width);
    picoImgFormat.height = NSSwapHostIntToLittle(self.height);
    picoImgFormat.isize = NSSwapHostIntToLittle((uint32_t) self.h264Data.length);
    self.CDB = [NSData dataWithBytes:&picoImgFormat length:sizeof(PICO_PIC_FORMAT_T)];
    
    NSMutableData * returnData = [[NSMutableData alloc] initWithData:[super data]];
    [returnData appendData:self.h264Data];
    return returnData;
    
}

@end
