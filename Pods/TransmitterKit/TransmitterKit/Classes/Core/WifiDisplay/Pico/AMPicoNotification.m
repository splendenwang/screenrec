//
//  AMPicoNotification.m
//  EZRemoteControliOS
//
//  Created by brianliu on 12/9/6.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import "AMPicoNotification.h"
#import "Pico.h"
@implementation AMPicoNotification
-(instancetype) initWithData:(NSData *) data;{
    self =  [super initWithData:data];
    if(self){
        PICO_NOTIFICATION_T picoNotification;
        memset(&picoNotification, 0, sizeof(PICO_NOTIFICATION_T));
        [self.CDB getBytes:&picoNotification length:sizeof(PICO_NOTIFICATION_T)];
        self.notificationType = NSSwapLittleIntToHost(picoNotification.notification);
        self.data1 = NSSwapLittleIntToHost(picoNotification.notification_data1);
        self.data2 = NSSwapLittleIntToHost(picoNotification.notification_data2);
    }
    return self;
}

-(NSString*) description{
    NSString * typeString= nil;
    switch (self.notificationType) {
        case NOTIFICATION_CHANGE_STREAM:
            typeString = @"NOTIFICATION_CHANGE_STREAM";
            break;
        case NOTIFICATION_DISCONNECT:
            typeString = @"NOTIFICATION_DISCONNECT";
            break;
        case NOTIFICATION_PLEASE_TRY:
            typeString = @"NOTIFICATION_PLEASE_TRY";
            break;
        case NOTIFICATION_STOP_STREAM:
            typeString = @"NOTIFICATION_STOP_STREAM";
            break;
        default:
            break;
    }
    return [NSString stringWithFormat:@"<%@,%p> notificaion:%@, data1:%d, data2:%d", NSStringFromClass([self class]), self, typeString ,self.data1, self.data2];
}
-(NSData *) data{
    PICO_NOTIFICATION_T picoNotification;
    memset(&picoNotification, 0, sizeof(PICO_NOTIFICATION_T));
    picoNotification.notification = NSSwapHostIntToLittle(self.notificationType);
    picoNotification.notification_data1 = NSSwapHostIntToLittle(self.data1);
    picoNotification.notification_data2 = NSSwapHostIntToLittle(self.data2);
    self.CDB = [NSData dataWithBytes:&picoNotification length:sizeof(PICO_NOTIFICATION_T)];
    
    NSMutableData * returnData = [[NSMutableData alloc] initWithData:[super data]];

    return returnData;
}

@end
