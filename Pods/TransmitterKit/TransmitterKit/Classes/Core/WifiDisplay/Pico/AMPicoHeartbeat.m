//
//  AMPicoHeartbeat.m
//  EZRemoteControliOS
//
//  Created by brianliu on 12/8/20.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import "AMPicoHeartbeat.h"
#import "Pico.h"
@implementation AMPicoHeartbeat

-(instancetype) init{
    self = [super init];
    if(self){
        self.tag = PICO_HEARTBEAT;
        self.flag = FLAG_TYPE_PC_PROJECTOR;
        self.len = 0x0;
        self.CDB = nil;
    }
    return self;
}


@end
