//
//  AMPicoMediaStreamingCommand.m
//  EZRemoteControliOS
//
//  Created by brianliu on 12/9/26.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import "AMPicoMediaStreamingCommand.h"

@implementation AMPicoMediaStreamingCommand


-(instancetype) init {
    self = [super init];
    if(self){
        self.tag = PICO_AV_STREAM_CMD;
        self.flag = FLAG_TYPE_PC_PROJECTOR;
        self.len =sizeof(PICO_AV_STREAM_T);
        self.requestResult = AV_RESULT_OK;
    }
    return self;
}

-(instancetype) initWithData:(NSData *)data{
    self = [super initWithData:data];
    if(self){
        PICO_AV_STREAM_T streamCommand;
        memset(&streamCommand, 0, sizeof(PICO_AV_STREAM_T));
        [self.CDB getBytes:&streamCommand length:sizeof(PICO_AV_STREAM_T)];
        streamCommand.request_cmd =         NSSwapLittleIntToHost(streamCommand.request_cmd);
        streamCommand.request_data_size =   NSSwapLittleIntToHost(streamCommand.request_data_size);
        streamCommand.request_data_type =   NSSwapLittleIntToHost(streamCommand.request_data_type);
        streamCommand.request_result =      NSSwapLittleIntToHost(streamCommand.request_result);
        
        self.fileOperation =    streamCommand.request_cmd;
        self.dataSize =         streamCommand.request_data_size;
        self.dataType =         streamCommand.request_data_type;
        self.requestResult =    streamCommand.request_result;
        
    
        if(self.dataType == AV_TYPE_INT32){
            NSData * readData = [data subdataWithRange:NSMakeRange(data.length-sizeof(uint32_t), sizeof(uint32_t))];
            uint32_t value;
            [readData getBytes:&value length:sizeof(value)];
            self.additionalData = (uint64_t) value;
        }
        else if(self.dataType == AV_TYPE_INT64){
            NSData * readData = [data subdataWithRange:NSMakeRange(data.length-sizeof(uint64_t), sizeof(uint64_t))];
            uint64_t value;
            [readData getBytes:&value length:sizeof(value)];
            self.additionalData = value;
        }
//        DLog(@"%s, self.additionalData:%ld", __PRETTY_FUNCTION__, self.additionalData);
    }
    return self;
}



-(NSData *) data{
    
    PICO_AV_STREAM_T streamCommand;
    memset(&streamCommand, 0, sizeof(PICO_AV_STREAM_T));
    streamCommand.request_cmd = NSSwapHostIntToLittle(self.fileOperation);
    streamCommand.request_data_size = NSSwapHostIntToLittle(self.dataSize);
    streamCommand.request_data_type = NSSwapHostIntToLittle(self.dataType);
    streamCommand.request_result = NSSwapHostIntToLittle(self.requestResult);
    self.CDB = [NSData dataWithBytes:&streamCommand length:sizeof(PICO_AV_STREAM_T)];
    
    
    NSMutableData * returnData = [[NSMutableData alloc] initWithData:[super data]];

    if(self.dataType == AV_TYPE_INT32){
//        DLog(@"%s, append addition data type int32", __PRETTY_FUNCTION__);
        uint32_t value = (uint32_t) self.additionalData;
        NSData * data = [NSData dataWithBytes: &value length: sizeof(value)];
//        uint32_t valueBack;
//        [data getBytes:&valueBack length:sizeof(valueBack)];
//        DLog(@"valueBack:%d", valueBack);
        [returnData appendData:data];
    }
    else if ( self.dataType == AV_TYPE_INT64){
//        DLog(@"%s, append addition data type int64", __PRETTY_FUNCTION__);
        uint64_t value = self.additionalData;
        NSData * data = [NSData dataWithBytes: &value length: sizeof(value)];
//        uint64_t valueBack;
//        [data getBytes:&valueBack length:sizeof(valueBack)];
//        DLog(@"valueBack:%llu", valueBack);
        [returnData appendData:data];
    } else if (self.dataType == AV_TYPE_UTF8) {
        NSAssert(_stringData != nil, @"_stringData should not be nil!");
        if (_stringData != nil) {
            [returnData appendData:_stringData];
        }
    }
//    else
//        DLog(@"%s, append nothing", __PRETTY_FUNCTION__);
    return returnData;
}

- (NSString *)description {
    
    NSString * string = nil;
    switch (self.fileOperation) {
        case AV_FILE_START:
            string = @"AV_FILE_START";
            break;
        case AV_FILE_STOP:
            string = @"AV_FILE_STOP";
            break;
        case AV_FILE_GET_LENGTH:
            string = @"AV_FILE_GET_LENGTH";
            break;
        case AV_FILE_GET_SEEKABLE:
            string = @"AV_FILE_GET_SEEKABLE";
            break;
        case AV_FILE_READ:
            string = @"AV_FILE_READ";
            break;
        case AV_FILE_PAUSE:
            string = @"AV_FILE_PAUSE";
            break;
        case AV_FILE_EOF:
            string = @"AV_FILE_EOF";
            break;
        case AV_HTTP_START:
            string = @"AV_HTTP_START";
            break;
        case AV_HTTP_STOP:
            string = @"AV_HTTP_STOP";
            break;
        case AV_HTTP_GET_URL:
            string = @"AV_HTTP_GET_URL";
            break;
        case AV_HTTP_GET_USERAGENT:
            string = @"AV_HTTP_GET_USERAGENT";
            break;
        case AV_PLAYER_GET_TIME:
            string = @"AV_PLAYER_GET_TIME";
            break;
        case AV_PLAYER_SEEKPLAY:
            string = @"AV_PLAYER_SEEKPLAY";
            break;
        case AV_PLAYER_PAUSE:
            string = @"AV_PLAYER_PAUSE";
            break;
        case AV_PLAYER_RESUME:
            string = @"AV_PLAYER_RESUME";
            break;
        case AV_PLAYER_RESET:
            string = @"AV_PLAYER_RESET";
            break;
        case AV_PLAYER_VOLUME_UP:
            string = @"AV_PLAYER_VOLUME_UP";
            break;
        case AV_PLAYER_VOLUME_DOWN:
            string = @"AV_PLAYER_VOLUME_DOWN";
            break;
        case AV_PLAYER_GET_LENGTH:
            string = @"AV_PLAYER_GET_LENGTH";
            break;
        default:
            string = [NSString stringWithFormat:@"uncategorized media streaming: %zd", self.fileOperation];
            break;
    }
    
    NSString * dataTypeString = nil;
    switch (self.dataType) {
        case AV_TYPE_VOID:
            dataTypeString = @"AV_TYPE_VOID";
            break;
        case AV_TYPE_INT32:
            dataTypeString = @"AV_TYPE_INT32";
            break;
        case AV_TYPE_INT64:
            dataTypeString = @"AV_TYPE_INT64";
            break;
        case AV_TYPE_UTF8:
            dataTypeString = @"AV_TYPE_UTF8";
            break;
        default:
            break;
    }
    
    return [NSString stringWithFormat:@"<%@: %p> fileOperation:%@, dataType:%@, dataSize:%zd, requestResult:%zd, self.additionalData:%llu",
            NSStringFromClass([self class]), self, string, dataTypeString, self.dataSize, self.requestResult, self.additionalData];
}

+(AMPicoMediaStreamingCommand *) startMediaStreamingCommand{
    AMPicoMediaStreamingCommand * command  =  [[AMPicoMediaStreamingCommand alloc] init];
    command.fileOperation = AV_FILE_START;
    command.dataType = AV_TYPE_VOID;
    command.dataSize = AV_DATA_SIZE_VOID;
    command.requestResult = AV_RESULT_OK;
    return command;
}
+(AMPicoMediaStreamingCommand *) startAudioStreamingCommand{
    AMPicoMediaStreamingCommand * command  =  [[AMPicoMediaStreamingCommand alloc] init];
    command.fileOperation = AV_FILE_START_AUDIO;
    command.dataType = AV_TYPE_VOID;
    command.dataSize = AV_DATA_SIZE_VOID;
    command.requestResult = AV_RESULT_OK;
    return command;
}
+(AMPicoMediaStreamingCommand *) getLengthMediaStreamingCommand:(uint64_t)fileLength{
    DLog(@"%s: fileLength:%lld", __PRETTY_FUNCTION__,fileLength);
    AMPicoMediaStreamingCommand * streamCommand = [[AMPicoMediaStreamingCommand alloc] init] ;
    streamCommand.flag = FLAG_TYPE_PC_PROJECTOR;
    streamCommand.dataSize = AV_DATA_SIZE_INT64;
    streamCommand.dataType = AV_TYPE_INT64;
    streamCommand.fileOperation = AV_FILE_GET_LENGTH;
    streamCommand.requestResult = AV_RESULT_OK;
    streamCommand.additionalData = fileLength;
    return streamCommand;
}
+(AMPicoMediaStreamingCommand *) seekableMediaStreamingCommand{
    AMPicoMediaStreamingCommand * streamCommand = [[AMPicoMediaStreamingCommand alloc] init] ;
    streamCommand.flag = FLAG_TYPE_PC_PROJECTOR;
    streamCommand.dataSize = AV_DATA_SIZE_INT32;
    streamCommand.dataType = AV_TYPE_INT32;
    streamCommand.fileOperation = AV_FILE_GET_SEEKABLE;
    streamCommand.requestResult = AV_RESULT_OK;
    streamCommand.additionalData = 1;
    return streamCommand;
}
+(AMPicoMediaStreamingCommand *) readMediaStreamingCommand:(uint64_t) offset{
    AMPicoMediaStreamingCommand * streamCommand = [[AMPicoMediaStreamingCommand alloc] init] ;
    streamCommand.flag = FLAG_TYPE_PC_PROJECTOR;
    streamCommand.dataSize = AV_DATA_SIZE_INT64;
    streamCommand.dataType = AV_TYPE_INT64;
    streamCommand.fileOperation = AV_FILE_READ;
    streamCommand.requestResult = AV_RESULT_OK;
    streamCommand.additionalData = offset;
    return streamCommand;
}
+(AMPicoMediaStreamingCommand *) stopMediaStreamingCommand{
    AMPicoMediaStreamingCommand * command  =  [[AMPicoMediaStreamingCommand alloc] init];
    command.fileOperation = AV_FILE_STOP;
    command.dataType = AV_TYPE_VOID;
    command.dataSize = AV_DATA_SIZE_VOID;
    command.requestResult = AV_RESULT_OK;
    return command;
}
+(AMPicoMediaStreamingCommand *) endMediaStreamingCommand{
    AMPicoMediaStreamingCommand * command  =  [[AMPicoMediaStreamingCommand alloc] init];
    command.fileOperation = AV_FILE_STOP;
    command.dataType = AV_TYPE_VOID;
    command.dataSize = AV_DATA_SIZE_VOID;
    command.requestResult = AV_RESULT_OK;
    return command;
}
+(AMPicoMediaStreamingCommand *) eofMediaStreamingCommand{
    AMPicoMediaStreamingCommand * command  =  [[AMPicoMediaStreamingCommand alloc] init];
    command.fileOperation = AV_FILE_EOF;
    command.dataType = AV_TYPE_VOID;
    command.dataSize = AV_DATA_SIZE_VOID;
    command.requestResult = AV_RESULT_OK;
    return command;
}
+(AMPicoMediaStreamingCommand *) volumeUpMediaStreamingCommand{
    AMPicoMediaStreamingCommand * command  =  [[AMPicoMediaStreamingCommand alloc] init];
    command.fileOperation = AV_PLAYER_VOLUME_UP;
    command.dataType = AV_TYPE_VOID;
    command.dataSize = AV_DATA_SIZE_VOID;
    command.requestResult = AV_RESULT_OK;
    return command;
}
+(AMPicoMediaStreamingCommand *) volumeDownMediaStreamingCommand{
    AMPicoMediaStreamingCommand * command  =  [[AMPicoMediaStreamingCommand alloc] init];
    command.fileOperation = AV_PLAYER_VOLUME_DOWN;
    command.dataType = AV_TYPE_VOID;
    command.dataSize = AV_DATA_SIZE_VOID;
    command.requestResult = AV_RESULT_OK;
    return command;
}
+(AMPicoMediaStreamingCommand *) pauseMediaStreamingCommand{
    AMPicoMediaStreamingCommand * command  =  [[AMPicoMediaStreamingCommand alloc] init];
    command.fileOperation = AV_PLAYER_PAUSE;
    command.dataType = AV_TYPE_VOID;
    command.dataSize = AV_DATA_SIZE_VOID;
    command.requestResult = AV_RESULT_OK;
    return command;
}
+(AMPicoMediaStreamingCommand *) resumeMediaStreamingCommand{
    AMPicoMediaStreamingCommand * command  =  [[AMPicoMediaStreamingCommand alloc] init];
    command.fileOperation = AV_PLAYER_RESUME;
    command.dataType = AV_TYPE_VOID;
    command.dataSize = AV_DATA_SIZE_VOID;
    command.requestResult = AV_RESULT_OK;
    return command;
}
+ (AMPicoMediaStreamingCommand *)responseWithCommand:(AV_FILE)cmd string:(NSString *)string
{
    AMPicoMediaStreamingCommand * command = [[AMPicoMediaStreamingCommand alloc] init];
    command.fileOperation = cmd;
    command.dataType = AV_TYPE_UTF8;
    command.stringData = [string dataUsingEncoding:NSUTF8StringEncoding];
    command.dataSize = [command.stringData length];
    command.requestResult = AV_RESULT_OK;
    return command;
    
}
+ (AMPicoMediaStreamingCommand *) getURLResponseWithURL:(NSString *)url
{
    return [self responseWithCommand:AV_HTTP_GET_URL string:url];
}
+ (AMPicoMediaStreamingCommand *) getUserAgentResponseWithUserAgent:(NSString *)userAgent
{
    return [self responseWithCommand:AV_HTTP_GET_USERAGENT string:userAgent];
}
+(AMPicoMediaStreamingCommand *) startHTTPMeidaStreamingCommand{
    AMPicoMediaStreamingCommand * command  =  [[AMPicoMediaStreamingCommand alloc] init];
    command.fileOperation = AV_HTTP_START;
    command.dataType = AV_TYPE_VOID;
    command.dataSize = AV_DATA_SIZE_VOID;
    command.requestResult = AV_RESULT_OK;
    return command;
}
+(AMPicoMediaStreamingCommand *) stopHTTPMeidaStreamingCommand{
    AMPicoMediaStreamingCommand * command  =  [[AMPicoMediaStreamingCommand alloc] init];
    command.fileOperation = AV_HTTP_STOP;
    command.dataType = AV_TYPE_VOID;
    command.dataSize = AV_DATA_SIZE_VOID;
    command.requestResult = AV_RESULT_OK;
    return command;
}
@end
