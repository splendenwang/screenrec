//
//  AMPicoSetDataCommand.m
//  EZRemoteControliOS
//
//  Created by brianliu on 12/9/21.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import "AMPicoSetDataCommand.h"
#import "Pico.h"
@implementation AMPicoSetDataCommand
-(instancetype) init{
    self = [super init];
    if(self){
        self.tag = PICO_SET_DATA;
        self.flag = FLAG_TYPE_PC_PROJECTOR;
        self.len = sizeof(PICO_SET_DATA_T);
    }
    return self;
}


-(instancetype) initWithData:(NSData *)data{
    self = [super initWithData:data];
    if(self){
        PICO_SET_DATA_T picoSetData;
        memset(&picoSetData, 0, sizeof(PICO_SET_DATA_T));
        [self.CDB getBytes:&picoSetData length:sizeof(PICO_SET_DATA_T)];
        self.isSetDataAllow = NSSwapLittleIntToHost(picoSetData.set_data_result) == SET_DATA_RESULT_ALLOW;
        
    }
    return self;
}

-(NSData *) data{
    PICO_SET_DATA_T picoSetData;
    memset(&picoSetData, 0, sizeof(PICO_SET_DATA_T));
    picoSetData.data_id = NSSwapHostIntToLittle(SET_DATA_HOSTNAME);
    picoSetData.set_data1 = 0;
    picoSetData.set_data2 = 0;
    NSData * strData = nil;
    if(self.hostname.length>0){
        strData = [self.hostname dataUsingEncoding:NSUTF8StringEncoding];
        
        picoSetData.isize = NSSwapHostIntToLittle((uint32_t)strData.length);
    }
    self.CDB = [NSData dataWithBytes:&picoSetData length:sizeof(picoSetData)];
    
    NSMutableData * returnData = [[NSMutableData alloc] initWithData:[super data]];
    if(strData)
        [returnData appendData:strData];
    return returnData;
}

-(NSString *) description{
    return [NSString stringWithFormat:@"<%@: %p>, isSetDataAllow :%d (super:%@)",
            NSStringFromClass([self class]), self, self.isSetDataAllow ,[super description]];
}

@end
