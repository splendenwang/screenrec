//
//  AMQueryInfo.h
//  AMCommon_Mac
//
//  Created by brianliu on 13/3/8.
//  Copyright (c) 2013年 ActionsMicro. All rights reserved.
//

#import "AMPicoCommand.h"

@interface AMPicoQueryInfoCommand : AMPicoCommand
@property (readonly, nonatomic) NSURL * webURL;
@end
