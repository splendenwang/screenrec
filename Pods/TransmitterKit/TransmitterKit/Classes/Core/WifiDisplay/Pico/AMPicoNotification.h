//
//  AMPicoNotification.h
//  EZRemoteControliOS
//
//  Created by brianliu on 12/9/6.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMPicoCommand.h"
@interface AMPicoNotification : AMPicoCommand
@property (nonatomic, assign) uint32_t notificationType;
@property (nonatomic, assign) uint32_t data1;
@property (nonatomic, assign) uint32_t data2;
@end
