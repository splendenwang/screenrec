//
//  AMPicoStatus.h
//  AMCommon_Mac
//
//  Created by brianliu on 2014/12/5.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMPicoCommand.h"

@interface AMPicoStatus : AMPicoCommand
@property uint32_t p_w;
@property uint32_t p_h;
@property uint32_t chip_id;
@end
