//
//  AMPicoMediaStreamingDataBlock.h
//  EZRemoteControliOS
//
//  Created by brianliu on 12/9/26.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import "AMPicoCommand.h"

@interface AMPicoMediaStreamingDataBlock : AMPicoCommand
@property (nonatomic, strong) NSData * carryData;
@end
