//
//  AMPicoImage.m
//  EZWifiMac
//
//  Created by brian on 12/5/14.
//  Copyright (c) 2012年 Actions-Micro. All rights reserved.
//

#import "AMPicoImage.h"
#import "Pico.h"
// this is a strange problem.
#if TARGET_OS_IPHONE
#import <ImageIO/ImageIO.h>
#endif
@interface AMPicoImage ()

@end

@implementation AMPicoImage

-(instancetype) initWithData:(NSData *)data{
    self = [super initWithData:data];
    if(self){
        PICO_PIC_FORMAT_T picoImgFormat;
        memset(&picoImgFormat, 0, sizeof(PICO_PIC_FORMAT_T));
        [self.CDB getBytes:&picoImgFormat length:sizeof(PICO_PIC_FORMAT_T)];
        picoImgFormat.format = NSSwapLittleIntToHost(picoImgFormat.format);
        picoImgFormat.height = NSSwapLittleIntToHost(picoImgFormat.height);
        picoImgFormat.width = NSSwapLittleIntToHost(picoImgFormat.width);
        picoImgFormat.isize = NSSwapLittleIntToHost(picoImgFormat.isize);
        self.width = picoImgFormat.width;
        self.height = picoImgFormat.height;
        self.imageData = [data subdataWithRange:NSMakeRange(8+sizeof(PICO_PIC_FORMAT_T), data.length - sizeof(PICO_PIC_FORMAT_T) - 8)];
        self.format = picoImgFormat.format;
    }
    return self;
}



-(instancetype) initWithCompressedData:(NSData *)data{
    self = [super init];
    if(self){
        self.tag = PICO_PIC_FORMAT_CMD;
        self.flag = FLAG_TYPE_PC_PROJECTOR;
        self.len = sizeof(PICO_PIC_FORMAT_T);
        self.imageData = data;
        CGSize size =  [self getWidthAndHeight:self.imageData];
        self.width = size.width;
        self.height = size.height;
    }
    return self;
}

-(NSData *) data{
    
    PICO_PIC_FORMAT_T picoImgFormat;
    memset(&picoImgFormat, 0, sizeof(PICO_PIC_FORMAT_T));
    picoImgFormat.format = NSSwapHostIntToLittle(OUT_PIX_FMT_JPEG);//jpg=0x01
    picoImgFormat.width = NSSwapHostIntToLittle(self.width);
    picoImgFormat.height = NSSwapHostIntToLittle(self.height);
    picoImgFormat.isize = NSSwapHostIntToLittle((uint32_t) self.imageData.length);
    self.CDB = [NSData dataWithBytes:&picoImgFormat length:sizeof(PICO_PIC_FORMAT_T)];
    
    NSMutableData * returnData = [[NSMutableData alloc] initWithData:[super data]];
    [returnData appendData:self.imageData];
    return returnData;
    
}



-(CGSize) getWidthAndHeight:(NSData*)imageData{
    CGImageSourceRef imageSource = CGImageSourceCreateWithData((__bridge CFDataRef)imageData, 0);
    if (imageSource == NULL) {
        // Error loading image
        DLog(@"[AMPicoImage] Erorr: Unable to get width and height from image data(%@)", imageData);
        return CGSizeZero;
    }
    
    int width = 0, height = 0;
    CFDictionaryRef imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, NULL);
    if (imageProperties != NULL) {
        CFNumberRef widthNum  = CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelWidth);
        if (widthNum != NULL) {
            CFNumberGetValue(widthNum, kCFNumberIntType, &width);
        }
        
        CFNumberRef heightNum = CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelHeight);
        if (heightNum != NULL) {
            CFNumberGetValue(heightNum, kCFNumberIntType, &height);
        }
        
        CFRelease(imageProperties);
        NSAssert(width > 0, @"width get from CGImageSourceCopyPropertiesAtIndex(kCGImagePropertyPixelWidth) should be greater than 0");
        NSAssert(height > 0, @"height get from CGImageSourceCopyPropertiesAtIndex(kCGImagePropertyPixelHeight) should be greater than 0");
    }
    CFRelease(imageSource);

    return CGSizeMake((float)width, (float)height);
    
}
- (NSString *)description {
    return [NSString stringWithFormat:@"<%@: %p> width:%d, length:%d (super:%@)",
            NSStringFromClass([self class]), self, self.width, self.width, [super description]];
}
@end
