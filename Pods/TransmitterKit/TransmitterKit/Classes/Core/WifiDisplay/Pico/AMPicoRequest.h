//
//  AMPicoRequest.h
//  EZRemoteControliOS
//
//  Created by brianliu on 12/9/6.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMPicoCommand.h"

typedef NS_ENUM(NSInteger, SPLIT_REQUEST_TYPE) {
    SPLIT_REQUEST_TYPE_AUTO =   0,
    SPLIT_REQUEST_TYPE_ONE =    0x01,  //full screen
    SPLIT_REQUEST_TYPE_TWO =    0x02,
    SPLIT_REQUEST_TYPE_THREE =  0x03,
    SPLIT_REQUEST_TYPE_QUAD =   0x04
    
};

@interface AMPicoRequest : AMPicoCommand
//-(id) initWithData:(NSData*)data;
+(AMPicoRequest*) autoSplitRequest;

@property (nonatomic, assign) SPLIT_REQUEST_TYPE splitNumber;

@property (nonatomic, assign) SPLIT_REQUEST_TYPE splitPosition;

@property (nonatomic, assign) BOOL      isSplitRequestAccept;

@property (nonatomic, assign) uint32_t mediaFormat; //OUT_PIX_FMT_JPEG
//-(NSData *) data;
//-(uint32_t) length;

//
//  cuz we want to know pico request Result, but isSplitRequestAccept only give Request_allow, boolean
//                                                                      added by Splenden 14/1/23
//
@property (NS_NONATOMIC_IOSONLY, getter=getSplitRequestResult, readonly) ezRequestResult_e splitRequestResult;

@end
