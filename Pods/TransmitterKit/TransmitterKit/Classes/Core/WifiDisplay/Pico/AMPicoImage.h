//
//  AMPicoImage.h
//  EZWifiMac
//
//  Created by brian on 12/5/14.
//  Copyright (c) 2012年 Actions-Micro. All rights reserved.
//  


//#import "Pico.h"
#import "AMPicoCommand.h"
@interface AMPicoImage : AMPicoCommand

-(instancetype) initWithData:(NSData *)data NS_DESIGNATED_INITIALIZER;

-(instancetype) initWithCompressedData:(NSData *)data NS_DESIGNATED_INITIALIZER;

/*
 Returns a image byte with pico header filled.
 */
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSData *data; 

/*
 Returns the length of pico image data.
 */
//-(uint32_t) length;

@property (nonatomic, assign) uint32_t width;

@property (nonatomic, assign) uint32_t height;

//this property may be valid only if the image format is JPEG, in other format, this property only retain data for getting length
@property (atomic, strong) NSData * imageData;


@property (nonatomic, assign) UInt16 format;

@end


//@protocol AMPicoImageDelegate
//@optional
///*
// Specifies whether AMPicoImage should compress the loaded image.
// */
//-(BOOL) shouldCompress;
///*
// Specifies the quality (0.1~1.0)
// Note: This parameter is valid only if shouldCpmpress is set to YES;
// Note: this value will now have a cap with 0.7
// */
//-(float) jpgImageQuality;
//@end
