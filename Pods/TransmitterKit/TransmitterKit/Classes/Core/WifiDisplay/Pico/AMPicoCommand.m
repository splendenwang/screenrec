//
//  AMPicoCommand.m
//  EZRemoteControliOS
//
//  Created by brianliu on 12/9/7.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import "AMPicoCommand.h"
#import "Pico.h"
@implementation AMPicoCommand
-(instancetype) initWithData:(NSData *) data{
    self = [super init];
    if(self){
        if(data.length>0){
            PICO_CMD_T picoCommand;
            memset(&picoCommand, 0, sizeof(PICO_CMD_T));
            [data getBytes:&picoCommand length:sizeof(PICO_CMD_T)];
            self.tag = NSSwapLittleIntToHost(picoCommand.tag);
            self.flag = picoCommand.flag;
            self.len = picoCommand.len; //pojector may fill wrong data ... =_=
            self.CDB = [data subdataWithRange:NSMakeRange(8, 16)];
        }
    }
    return self;
}


-(NSData*) data{
    
    PICO_CMD_T picoCommand;
    memset(&picoCommand, 0, sizeof(PICO_CMD_T));
    picoCommand.tag = NSSwapHostIntToLittle(self.tag) ; 
    picoCommand.flag = self.flag;    //0: pc -> DPF
    picoCommand.len = self.len; 
    
    NSMutableData * returnData = [NSMutableData dataWithBytes:&picoCommand length:sizeof(PICO_CMD_T)];
    if(self.CDB){
        void * bytes = malloc(sizeof(self.len));
        [self.CDB getBytes:bytes range:NSMakeRange(0, self.CDB.length)];   //be aware of the usage, bytes must be a alloced space
        [returnData replaceBytesInRange:NSMakeRange(8, 16) withBytes:bytes];   //fill CDB(16 bytes)
        free(bytes);
    }
    else{
//        DLog(@"%@ is not filling CDB data because self.CDB:%@, self.len:%d", NSStringFromClass([self class]), self.CDB, self.len);
    }
    
    return returnData;
}
- (NSString *)description{
    
    NSString * string = nil;
    
    switch (self.tag) {
        case PICO_STATUS_CMD:
            string = @"PICO_STATUS_CMD";
            break;
        case PICO_PIC_FORMAT_CMD:
            string = @"PICO_PIC_FORMAT_CMD";
            break;
        case PICO_HBSC_CMD:
            string = @"PICO_HBSC_CMD";
            break;
        case PICO_HEARTBEAT:
            string = @"PICO_HEARTBEAT";
            break;
        case PICO_QUERY_INFO:
            string = @"PICO_QUERY_INFO";
            break;
        case PICO_REQUEST:
            string = @"PICO_REQUEST";
            break;
        case PICO_NOTIFICATION:
            string = @"PICO_NOTIFICATION";
            break;
        case PICO_AV_STREAM_CMD:
            string = @"PICO_AV_STREAM_CMD";
            break;
        case PICO_SET_DATA:
            string = @"PICO_SET_DATA";
            break;
        case PICO_VENDOR_CMD:
            string = @"PICO_VENDOR_CMD";
            break;
        default:
            break;
    }
    
    return [NSString stringWithFormat:@"<%@, %p> tag:%@, len:%d, flag:%tu", NSStringFromClass([self class]), self, string, self.len, self.flag];
}
@end
