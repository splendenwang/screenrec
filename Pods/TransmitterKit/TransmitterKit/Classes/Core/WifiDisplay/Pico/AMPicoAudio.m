//
//  AMPicoAudio.m
//  AMCommon_Mac
//
//  Created by brianliu on 12/10/22.
//  Copyright (c) 2012年 ActionsMicro. All rights reserved.
//

#import "AMPicoAudio.h"
#import "Pico.h"
@implementation AMPicoAudio

-(id) initWithPCMData:(NSData*) pcmData{
    self = [super initWithData:nil];
    if(self){
        self.flag = FLAG_TYPE_PC_PROJECTOR;
        self.tag = PICO_PIC_FORMAT_CMD;
        self.len = 0x10;
        self.pcmData = pcmData;
    }
    return self;
}

-(id) initWithData:(NSData *)data{
    return [self initWithPCMData:nil];
}


-(NSData*) data{
    
    PICO_PIC_FORMAT_T audio;    //why does audio use the same structure(naming) as image ? 
    memset(&audio, 0, sizeof(PICO_PIC_FORMAT_T));
    audio.format = NSSwapHostIntToLittle(OUT_SND_FMT_PCM);
    audio.isize = NSSwapHostIntToLittle((uint32_t) self.pcmData.length);
    self.CDB = [NSData dataWithBytes:&audio length:sizeof(PICO_PIC_FORMAT_T)];
    
    NSMutableData * returnData = [[NSMutableData alloc] initWithData:[super data]];
    [returnData appendData:self.pcmData];
    return returnData;
}
@end
