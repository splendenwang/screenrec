//
//  AMPicoRequest.m
//  EZRemoteControliOS
//
//  Created by brianliu on 12/9/6.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import "AMPicoRequest.h"
#import "Pico.h"

@interface AMPicoRequest ()
@property (nonatomic, assign) PICO_REQUEST_T picoRequest;
@end

@implementation AMPicoRequest

+(AMPicoRequest*) autoSplitRequest{
    AMPicoRequest * request = [[AMPicoRequest new] init];
    request.splitNumber = SPLIT_REQUEST_TYPE_AUTO;
    return request;
}

-(instancetype) initWithData:(NSData*)data{
    self = [super initWithData:data];
    if(self){
        memset(&_picoRequest, 0, sizeof(PICO_REQUEST_T));
        [self.CDB getBytes:&_picoRequest length:sizeof(PICO_REQUEST_T)];
        _picoRequest.request = NSSwapLittleIntToHost(_picoRequest.request);
        _picoRequest.request_data1 = NSSwapLittleIntToHost(_picoRequest.request_data1);
        _picoRequest.request_data2 = NSSwapLittleIntToHost(_picoRequest.request_data2);
        _picoRequest.request_result = NSSwapLittleIntToHost(_picoRequest.request_result);

        self.splitNumber = _picoRequest.request_data1;
        self.splitPosition = _picoRequest.request_data2;
        self.isSplitRequestAccept = (_picoRequest.request_result == REQUEST_RESULT_ALLOW) ? YES : NO;
    }
    return self;
}

-(instancetype) init{
    self = [super init];
    if(self){
        self.tag = PICO_REQUEST;
        self.flag = FLAG_TYPE_PC_PROJECTOR;
        self.len = sizeof(PICO_REQUEST_T);
        self.splitNumber = 0;
        self.splitPosition = 0;
        self.mediaFormat = 0;
    }
    return self;
    
}
- (ezRequestResult_e)getSplitRequestResult{
    return _picoRequest.request_result;
}
-(NSData *) data{
    PICO_REQUEST_T picoRequest;
    memset(&picoRequest, 0, sizeof(PICO_REQUEST_T));
    picoRequest.request = NSSwapHostIntToLittle(1);
    picoRequest.request_data1 = NSSwapHostIntToLittle(self.splitNumber);
    picoRequest.request_data2 = NSSwapHostIntToLittle(self.splitPosition);
//    picoRequest.request_result = (self.isSplitRequestAccept)?REQUEST_RESULT_ALLOW:REQUEST_RESULT_DENY;
    picoRequest.format = OUT_PIX_FMT_JPEG;
//    if(self.mediaFormat==0)
//        picoRequest.format = NSSwapHostIntToLittle(OUT_PIX_FMT_JPEG);   //fill jpg if not specify
    self.CDB = [NSData dataWithBytes:&picoRequest length:sizeof(PICO_REQUEST_T)];
    return [super data];
}
- (NSString *)description {
    return [NSString stringWithFormat:@"<%@: %p> split numbers %zd, position: %zd, result:%d",
     NSStringFromClass([self class]), self, self.splitNumber, self.splitPosition, self.isSplitRequestAccept];
}
@end
