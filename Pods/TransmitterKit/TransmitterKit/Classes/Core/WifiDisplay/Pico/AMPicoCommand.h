//
//  AMPicoCommand.h
//  EZRemoteControliOS
//
//  Created by brianliu on 12/9/7.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pico.h"
@interface AMPicoCommand : NSObject

//flag will be FLAG_TYPE_PROJECTOR_PC
-(instancetype) initWithData:(NSData *) data;

//flag will be FLAG_TYPE_PC_PROJECTOR
//-(id) initWithTag:(PicoCommandFormat)tag CDB:(NSData *)CDB;

@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSData *data;

@property (nonatomic, strong) NSData * CDB;

@property (nonatomic, assign) FLAG_TYPE flag;

@property (nonatomic, assign) uint16_t len;

@property (nonatomic, assign) PicoCommandFormat tag;

@end
