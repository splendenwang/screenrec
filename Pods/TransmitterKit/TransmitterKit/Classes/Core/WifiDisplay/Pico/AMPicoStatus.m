//
//  AMPicoStatus.m
//  AMCommon_Mac
//
//  Created by brianliu on 2014/12/5.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMPicoStatus.h"
#import "Pico.h"
@implementation AMPicoStatus

-(instancetype) initWithData:(NSData *)data{
    self =  [super initWithData:data];
    if(self){
        PICO_STATUS_T picoStatusCmd;
        memset(&picoStatusCmd, 0, sizeof(PICO_STATUS_T));
        [self.CDB getBytes:&picoStatusCmd length:sizeof(PICO_STATUS_T)];
        self.p_w = NSSwapLittleIntToHost(picoStatusCmd.p_w);
        self.p_h = NSSwapLittleIntToHost(picoStatusCmd.p_h);
    }
    return self;
}

-(instancetype) init{
    self =[super init];
    if(self){
        self.tag = PICO_STATUS_CMD;
        self.flag = FLAG_TYPE_PC_PROJECTOR;
        self.len =0;
    }
    return self;
}


-(NSData *) data{
    PICO_STATUS_T picoStatusCmd;
    memset(&picoStatusCmd, 0, sizeof(PICO_STATUS_T));
    picoStatusCmd.p_w = NSSwapHostIntToLittle(self.p_w);
    picoStatusCmd.p_h = NSSwapHostIntToLittle(self.p_h);
    self.CDB = [NSData dataWithBytes:&picoStatusCmd length:sizeof(PICO_STATUS_T)];
    NSMutableData * returnData = [[NSMutableData alloc] initWithData:[super data]];
    
    return returnData;
}
@end
