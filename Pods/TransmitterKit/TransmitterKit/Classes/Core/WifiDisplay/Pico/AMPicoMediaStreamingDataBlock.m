//
//  AMPicoMediaStreamingDataBlock.m
//  EZRemoteControliOS
//
//  Created by brianliu on 12/9/26.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import "AMPicoMediaStreamingDataBlock.h"

@implementation AMPicoMediaStreamingDataBlock
-(instancetype) init{
    self = [super init];
    if(self){
        self.tag = PICO_PIC_FORMAT_CMD;
        self.flag = FLAG_TYPE_PC_PROJECTOR;
        self.len = sizeof(PICO_PIC_FORMAT_T);

    }
    return self;
}


-(NSData *) data{
    PICO_PIC_FORMAT_T picoImgFormat;
    memset(&picoImgFormat, 0, sizeof(PICO_PIC_FORMAT_T));
    picoImgFormat.format = NSSwapHostIntToLittle(OUT_AV_FMT_EZSTREAM);
    picoImgFormat.width = NSSwapHostIntToLittle(0);
    picoImgFormat.height = NSSwapHostIntToLittle(0);
    picoImgFormat.isize = NSSwapHostIntToLittle((uint32_t) self.carryData.length);
    self.CDB = [NSData dataWithBytes:&picoImgFormat length:sizeof(PICO_PIC_FORMAT_T)];
    
    NSMutableData * returnData = [[NSMutableData alloc] initWithData:[super data]];
    [returnData appendData:self.carryData];
    return returnData;
}
@end
