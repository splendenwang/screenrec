    //
//  AMProjectorDevice.m
//  EZRemoteControliOS
//
//  Created by brianliu on 12/8/23.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import "AMProjectorDevice.h"
//#import "NSString+NetworkUtility.h"
@import CocoaAsyncSocket;
#import "Pico.h"
#import "ipmsg.h"
#import "AMPicoStatus.h"
#import "AMSocketDefine.h"
@interface AMProjectorDevice () <GCDAsyncUdpSocketDelegate>
{
    CGFloat _maxWidth;
    CGFloat _maxHeight;
    dispatch_semaphore_t _sem;
    GCDAsyncUdpSocket *_statusUdpSocket;
}
@end


@implementation AMProjectorDevice

-(instancetype) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        _protocolVersion = [[aDecoder decodeObjectForKey:@"protocolVersion"] copy];
        _userName = [[aDecoder decodeObjectForKey:@"userName"] copy];
        _passcode = [[aDecoder decodeObjectForKey:@"passcode"] copy];
        _md5 = [[aDecoder decodeObjectForKey:@"md5"] copy];
    }
    return self;
}

-(void) encodeWithCoder:(NSCoder *)aCoder{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.protocolVersion forKey:@"protocolVersion"];
    [aCoder encodeObject:self.userName forKey:@"userName"];
    [aCoder encodeObject:self.passcode forKey:@"passcode"];
    [aCoder encodeObject:self.md5 forKey:@"md5"];
}


-(NSString*) description{
    return [NSString stringWithFormat:@"\n%@<%p> \nprotocol:%@ \nip:%@ \nusername:%@ \nhostname:%@ \ndisplayName:%@ \ndevice model:%@ \npasscode:%@ \n\t(discovery:%ld \n\tisPixViewerEnabled:%d \n\tisLiveCamEnabled:%d \n\tisStreamingMediaEnabled:%d \n\tisStreamingDocEnabled:%d \n\tisSplitScreenEnabled:%d \n\tisDropboxEnabled:%d \n\tisWebEnabled:%d \n\tisHTTPStreamingEnabled:%d \n\tisStreamingMediaAudioEnable: %d)", NSStringFromClass([self class]), self ,self.protocolVersion, self.ipAddress, self.userName, self.hostName,self.displayName, self.deviceModel, self.passcode, (long)self.discovery, self.isPixViewerEnabled, self.isLiveCamEnabled, self.isStreamingMediaEnabled, self.isStreamingDocEnabled, self.isSplitScreenEnabled, self.isDropboxEnabled, self.isWebEnabled, self.isHTTPStreamingEnabled, self.isStreamingMediaAudioEnable];
}

-(id) copyWithZone:(NSZone *)zone{
    AMProjectorDevice * anotherDevice = [[AMProjectorDevice allocWithZone:zone] init];
    anotherDevice.protocolVersion = self.protocolVersion;
    anotherDevice.ipAddress = self.ipAddress;
    anotherDevice.userName = self.userName;
    anotherDevice.hostName = self.hostName;
    anotherDevice.deviceModel = self.deviceModel;
    anotherDevice.vendor = self.vendor;
    anotherDevice.passcode = self.passcode;
    anotherDevice.split = self.split;
    anotherDevice.position = self.position;
    anotherDevice.isPixViewerEnabled = self.isPixViewerEnabled;
    anotherDevice.isLiveCamEnabled = self.isLiveCamEnabled;
    anotherDevice.isStreamingDocEnabled = self.isStreamingDocEnabled;
    anotherDevice.isStreamingMediaEnabled = self.isStreamingMediaEnabled;
    anotherDevice.isSplitScreenEnabled = self.isSplitScreenEnabled;
    anotherDevice.isDropboxEnabled = self.isDropboxEnabled;
    anotherDevice.isWebEnabled = self.isWebEnabled;
    anotherDevice.isHTTPStreamingEnabled = self.isHTTPStreamingEnabled;
    anotherDevice.discovery = self.discovery;
    anotherDevice.isStreamingMediaAudioEnable = self.isStreamingMediaAudioEnable;
    
    return anotherDevice;
}

-(BOOL) isEqual:(id)object{
    if([object isKindOfClass:[self class]]){
        BOOL returnValue = YES;
        AMProjectorDevice * anotherDevice= (AMProjectorDevice*)object;
        
        if(self.ipAddress !=nil && anotherDevice.ipAddress!=nil){
            if([self.ipAddress isEqualToString:anotherDevice.ipAddress]==NO){
                returnValue = NO;
            }
        }
        else if(self.ipAddress == nil || anotherDevice.ipAddress == nil){
            DLog(@"%s, There may be an error checking ipAddress.", __PRETTY_FUNCTION__);
            returnValue = NO;
        }
        return returnValue;
    }
    return NO;
}
-(NSComparisonResult) compare:(AMProjectorDevice *)anotherDevice{
    
    if(self.ipAddress && anotherDevice.ipAddress && [self.ipAddress isEqualToString:anotherDevice.ipAddress]==NO){
        return [self.ipAddress compare:anotherDevice.ipAddress];
    }
    if(self.displayName && anotherDevice.displayName && [self.displayName isEqualToString:anotherDevice.displayName]==NO){
        return [self.displayName compare:anotherDevice.displayName];
    }
    if(self.userName && anotherDevice.userName && [self.userName isEqualToString:anotherDevice.userName]==NO){
        return [self.userName compare:anotherDevice.userName];
    }
    if(self.hostName && anotherDevice.hostName && [self.hostName isEqualToString:anotherDevice.hostName]==NO){
        return [self.hostName compare:anotherDevice.hostName];
    }
    if(self.deviceModel && anotherDevice.deviceModel && [self.deviceModel isEqualToString:anotherDevice.deviceModel]==NO){
        return [self.deviceModel compare:anotherDevice.deviceModel];
    }
    if(self.vendor && anotherDevice.vendor && [self.vendor isEqualToString:anotherDevice.vendor]==NO){
        return [self.vendor compare:anotherDevice.vendor];
    }
    if(self.passcode && anotherDevice.passcode && [self.passcode isEqualToString:anotherDevice.passcode]==NO){
        return [self.passcode compare:anotherDevice.passcode];
    }
    if(self.md5 && anotherDevice.md5 && [self.md5 isEqualToString:anotherDevice.md5]==NO){
        return [self.md5 compare:anotherDevice.md5];
    }
    if(self.protocolVersion && anotherDevice.protocolVersion && [self.protocolVersion isEqualToString:anotherDevice.protocolVersion]==NO){
        return [self.protocolVersion compare:anotherDevice.protocolVersion];
    }
    return NSOrderedSame;
}

-(void) dealloc{
    [_statusUdpSocket close];
}
#pragma mark - AMPrototypeDevice
-(NSString*) displayName{
    return self.deviceModel;
}

#pragma mark -
-(CGSize) getMaxResolution{
    
    if(!self.ipAddress){
        return CGSizeZero;
    }
    
    
    //create a new one always in case users' network have been switch
    _statusUdpSocket.delegate = nil;
    _statusUdpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
    NSError * error = nil;
    [_statusUdpSocket bindToPort:0 error:&error];
    [_statusUdpSocket beginReceiving:&error];
    
    _sem = dispatch_semaphore_create(0);
    
    NSMutableData *wifiDisplayCommandData = [NSMutableData dataWithData:[[NSString stringWithFormat:@"2:1:Stan:Stan:34:\0"] dataUsingEncoding:NSASCIIStringEncoding]];
    
    IMAGE_PACKET_HEADER packetHeader;
    memset(&packetHeader, 0, sizeof(IMAGE_PACKET_HEADER));
    packetHeader.dwPacketLen=24+24;
    packetHeader.wTotalPacket=1;
    NSData * packetHeaderData = [NSData dataWithBytes:&packetHeader length:sizeof(IMAGE_PACKET_HEADER)];
    [wifiDisplayCommandData appendData:packetHeaderData];
    
    AMPicoStatus * status = [AMPicoStatus new];
    NSData * commandData = [status data];
    [wifiDisplayCommandData appendData:commandData];
    [wifiDisplayCommandData appendData:[commandData copy]];
    
    [_statusUdpSocket sendData:wifiDisplayCommandData toHost:self.ipAddress port:2425
                       withTimeout:10.0 tag:100];
    if(error){
        DLog(@"");
    }
    
    dispatch_time_t delayInNanoSeconds=dispatch_time(DISPATCH_TIME_NOW, 5*NSEC_PER_SEC);
    
    dispatch_semaphore_wait(_sem, delayInNanoSeconds);
    
    if(_maxHeight>0.f && _maxHeight >0.f){
        return CGSizeMake(_maxWidth, _maxHeight);
    }
    
    return CGSizeZero;
}
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext{
    NSString *host = nil;
    uint16_t port = 0;
    
    [GCDAsyncUdpSocket getHost:&host port:&port fromAddress:address];
    
    if(host!= nil && port==PORT_EZWIFIDISPLAY && data)
    {
        NSData * picoStatusData = [data subdataWithRange:NSMakeRange(data.length-24, 24)];
        AMPicoStatus * status = [[AMPicoStatus alloc] initWithData:picoStatusData];
        _maxWidth = status.p_w;
        _maxHeight = status.p_h;
        dispatch_semaphore_signal(_sem);
        //        DLog(@"p_w:%d, p_h:%d" , status.p_w, status.p_h);
    }
    else {
        //        DLog(@"%s, possibly receiving error data.",__PRETTY_FUNCTION__);
    }
}


@end
