//
//  AMRemoteHost.h
//  AMCommon_iOS
//
//  Created by brianliu on 12/12/7.
//  Copyright (c) 2012年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMPrototypeDevice.h"
#import "AMSocketDataParser.h"
@import CocoaAsyncSocket;
@class AMJSONRPCObject;
/**
 Standard key code supported by default.
 */
typedef NS_ENUM(NSInteger, AMRemoteHostStandardKeyCode) {
    /**
      Key code defines up key.
     */
    AMRemoteHostStandardKeyCodeUp = 10,
    /**
     Key code defines down key.
     */
    AMRemoteHostStandardKeyCodeDown,
    /**
     Key code defines left key.
     */
    AMRemoteHostStandardKeyCodeLeft,
    /**
     Key code defines right key.
     */
    AMRemoteHostStandardKeyCodeRight,
    /**
     Key code defines enter key.
     */
    AMRemoteHostStandardKeyCodeEnter,
    /**
     Key code defines escape key.
     */
    AMRemoteHostStandardKeyCodeEscape,
};
NS_ASSUME_NONNULL_BEGIN
extern NSString * const kAMRemoteHostMessageTCPDisconnected;
extern NSString * const kAMRemoteHostMessageTCPConnectionTimeOut __deprecated;
NS_ASSUME_NONNULL_END
@class AMRemoteHost;
@class AMRemoteHostScanner;

/**
 Define callback when receive message from the device.
 Application should implement this protocol if it would like to receive message from deivce.
 See also [AMRemoteHost addMessageListener:] and [AMRemoteHost removeMessageListener:].
 */
@protocol AMRemoteHostMessageListener <NSObject>

/**
 Callback when receive message from the device.
 @param message Message in string sent by the device.
 @param remoteHost The device which sends this message.
 */
- (void)didReceiveMessage:(NSString * _Null_unspecified)message fromRemoteHost:(AMRemoteHost * _Null_unspecified)remoteHost;

/**
 Callback when receive jsonrpc from the device.
 @param jrpcMsg Raw JSONRPC message in string sent by the device.
 @param jrpcObject Wrapped JSONRPC object. Could be nil if there is any error while converting from jrpcMsg.
 @param remoteHost The device which sends this message.
 */
@optional
- (void)didReceiveJsonrpcMessage:(NSString * _Null_unspecified)jrpcMsg jsonrpcObject:(AMJSONRPCObject*_Null_unspecified)jrpcObject fromRemoteHost:(AMRemoteHost *_Null_unspecified)remoteHost;

@end
/**
 This class contains information of the device.
 Application should not create any instance of it directly. 
 Application should implement AMRemoteHostScannerDelegate and use AMRemoteHostScanner to discover devices in the network.
 
 Application can also send key to the device via this class.
 
 To receive and handle message from the device, application should implement AMRemoteHostMessageListener and invoke addMessageListener:.
 Remember to call removeMessageListener: before listener is released; otherwise, AMRemoteHost will send message to a deallocated object.
 */
@interface AMRemoteHost : AMPrototypeDevice <GCDAsyncSocketDelegate, AMSocketDataParserDelegate>
- (_Null_unspecified instancetype)init NS_DESIGNATED_INITIALIZER;
//-(instancetype)initWithCoder:(NSCoder *)aDecoder NS_DESIGNATED_INITIALIZER;
- (_Nullable instancetype)initWithScanner:(AMRemoteHostScanner *_Null_unspecified)scanner;
- (_Nullable instancetype)initWithCoder:(NSCoder *_Null_unspecified)aDecoder NS_DESIGNATED_INITIALIZER;
/**
 Close the UDP socket and TCP socket. Remember to send this message before you release it.
 */
-(void) close;

/**
 Comapre the host to see if they are the same host.
 @param host A host object to be compared to the receiver.
 @return NSComparisonResult Describes the result. If the result is not NSOrderedSame. The ipAddress is compared in NSOrderedAscending order.
 */
-(NSComparisonResult) compare:(AMRemoteHost*_Null_unspecified) host;

/**
 This BOOL value determines whether this object will send Key and VendorKey via UDP socket. Default is NO.(NO means nothing will be sent via UDP socket)
 */
@property (assign) BOOL sendMessageViaUDP;

/**
 This BOOL value determines whether this object will send Key and VendorKey via TCP socket. Default is YES.(NO means nothing will be sent via TCP socket.)
 */
@property (assign) BOOL sendMessageViaTCP;

/**
 Basic function of all sending message API.
 */
-(void) sendString:(NSString*_Null_unspecified) string;

/**
 Indicate protocol version.
 **/
@property (strong) NSString * _Null_unspecified protocolVersion;


/**
 The model name of the device.
 */
@property (nonatomic, strong) NSString * _Null_unspecified deviceModel;

/**
 Send key command to the device.
 
 
 Supported key are:
 
 
 - AMRemoteHostStandardKeyCodeUp: Key code defines up key.
 - AMRemoteHostStandardKeyCodeDown: Key code defines down key.
 - AMRemoteHostStandardKeyCodeLeft: Key code defines left key.
 - AMRemoteHostStandardKeyCodeRight: Key code defines right key.
 - AMRemoteHostStandardKeyCodeEnter: Key code defines enter key.
 - AMRemoteHostStandardKeyCodeEscape: Key code defines escape key.
 @param keyCode Predifined key code which are supported by default.
 */
- (void)sendKey:(AMRemoteHostStandardKeyCode)keyCode;

/**
 Send vendor specific key command to the device.
 @param keyCode Vendor specific key code.
 */
- (void)sendVendorKey:(NSInteger)keyCode;

/**
 Resigster callback when receive message from the device.
 @param listener A AMRemoteHostMessageListener.
 */
- (void)addMessageListener:(id<AMRemoteHostMessageListener> _Null_unspecified)listener;
/**
 Unresigster callback when receive message from the device.
 @param listener A AMRemoteHostMessageListener.
 */
- (void)removeMessageListener:(id<AMRemoteHostMessageListener> _Null_unspecified)listener;

/**
 Dispatch customer type message to listeners. Remote Device ---> App.
 */
- (void)dispatchCustomerMessage:(NSString * _Null_unspecified)message;

/**
 Dispatch JSONRPC type message to listeners. Remote Device ---> App.
 */
- (void)dispatchJsonrpcMessage:(NSString*_Null_unspecified)jrpcMsg isEncrypted:(BOOL)encrypted;

@property (weak, nonatomic) AMRemoteHostScanner * _Null_unspecified scanner;

/**
 This string value determines whether devcie is ezcast, ezcastpro or customer. If there's no type value, it might be an old ezcast device.
 @see: http://www.actions-micro.com/tiki/tiki-index.php?page=EZCast+Pro#Protocol_
 @return: dongle family
 */
@property (readonly) NSString * _Null_unspecified family;
/**
 This string value determines device type, different family has different type series.
 @see: http://www.actions-micro.com/tiki/tiki-index.php?page=EZCast+Pro#Protocol_
 @return: dongle type
 */
@property (readonly) NSString * _Null_unspecified type;

/**
 This string value determines device web service URL host
 @see: http://www.actions-micro.com/tiki/tiki-index.php?page=EZ+Cast#EZ_Remote_Protocol_Extension_for_EZ_Cast
 @return: web root ip address (with port)
 */
@property (readonly) NSString * _Null_unspecified webRootHost;

/**
 Support after SVN 13262. The value is nil if not supported.
 */
@property (readonly) NSString * _Null_unspecified customName;

@property (readonly) BOOL isClientModeEnable;

@property NSString * _Null_unspecified passcode;

/**
 *  Get localHost address string from tcp socket. The value is nil if not connected
 */
@property (readonly) NSString * _Null_unspecified localHost;

@property NSTimeInterval timeout;   //default = 10
@end

@interface AMRemoteHost (JSONRPC)
/**
 *  set JSONRPC encryption key
 *
 *  @param key value from device response message
 */
-(void) setJrpcEncryptionKey:(NSString *_Null_unspecified )key;
/**
 Send a un-encrypted JSONRPC object to a remote device. App ---> Remote Device.
 @param jrpcObject An AMJSONRPCObject.
 */
-(void) sendJSONRPCObject:(AMJSONRPCObject*_Null_unspecified ) jrpcObject;
/**
 Send a JSONRPC object to a remote device. App ---> Remote Device.
 @param jrpcObject An AMJSONRPCObject.
 */
-(void) sendJSONRPCObject:(AMJSONRPCObject*_Null_unspecified ) jrpcObject encrypted:(BOOL)encrypted;
/**
 Send a un-encrypted JSONRPC object to a remote device. App ---> Remote Device.
 @param jrpcMsg A JSONRPC message.
 */
-(void) sendJSONRPCMessage:(NSString *_Null_unspecified ) jrpcMsg;
/**
 Send a JSONRPC object to a remote device. App ---> Remote Device.
 @param jrpcMsg A JSONRPC message.
 */
-(void) sendJSONRPCMessage:(NSString * _Null_unspecified) jrpcMsg encrypted:(BOOL)encrypted;
@end


@interface AMRemoteHost (Availability)
/**
 Establish TCP connection.
 */
-(void) establishTCPConnection:(void(^ __nonnull )(BOOL connected, NSError * _Null_unspecified error))  result;
@end
