//
//  AMEZRemoteSocketDataParser.h
//  EZRemoteControliOS
//
//  Created by brianliu on 12/8/23.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMSocketDataParser.h"
@interface AMEZRemoteSocketDataParser : AMSocketDataParser
@property BOOL isDiscoveryMessage;
@property BOOL isStandardMessage;
@property BOOL isCustomerMessage;
@property BOOL isJsonrpcMessage;
@property BOOL isEncrypted;
@property (nonatomic, strong) NSString *messageBody;
@property (nonatomic, strong) NSString *srcvers;
@property (nonatomic, strong) NSString * protocolVersion;
@end
