//
//  AMRemoteCommandGenerator.m
//  EZRemoteControliOS
//
//  Created by brian on 12/4/5.
//  Copyright (c) 2012年 Actions-Micro. All rights reserved.
//
//  Generate command for remote control use

#import "AMRemoteCommandGenerator.h"
enum REMOTE_CONTROL_TYPE{
    CONN_LOOKUP=0,
    CONN_FOUND,
    CONN_CONNECT,
    CONN_DISCONNECT,
    KEY_UP=10,  //general "up"
    KEY_DOWN,   //general "down"
    KEY_LEFT,   //general "left"
    KEY_RIGHT,  //general "right"
    KEY_ENTER,  //general "enter"
    KEY_ESC,    //general "esc"
    MOUSE_UP=20,
    MOUSE_DOWN,
    MOUSE_LEFT,
    MOUSE_RIGHT,
    MOUSE_TAP_ONCE,
    MOUSE_TAP_DOUBLE,
    MOUSE_TAP_SCROLL,
    REV_FUN_1 = 31, //BenQ "Auto" 
    REV_FUN_2 = 32, //BenQ "Source"
    REV_FUN_3 = 33, //BenQ "ECO"
    REV_FUN_4 = 34, //BenQ "Power"
    REV_FUN_5 = 35, //BenQ "MenuExit"
    REV_FUN_6 = 36, //GP20 "Mute"
    REV_FUN_7 = 37, //GP20 "volume up"
    REV_FUN_8 = 38, //GP20 "volume down"
    REV_FUN_9 = 39, //GP20 "Home", ASUS "Page Up"
    REV_FUN_10 = 40, //GP20 "Blank"
    REV_FUN_11 = 41, //GP20 "Media Forward", ASUS "Page Down"
    REV_FUN_12 = 42, //GP20 "Media Backward"
    REV_FUN_13 = 43,
    REV_FUN_14 = 44,
    REV_FUN_15 = 45,
    REV_FUN_16 = 46,
    REV_FUN_17 = 47,
    REV_FUN_18 = 48,
    REV_FUN_19 = 49,
    REV_FUN_20 = 50
};

enum REMOTE_CONTROL_COMMAND{
    COMMAND_CONNECT=0,
	COMMAND_KEY,
	COMMAND_KEYBOARD,
	COMMAND_KEYBOARD_NUMPAD,
	COMMAND_MOUSE,
    COMMAND_STRING,
    COMMAND_JSONRPC,
    COMMAND_VENDOR = 10,
    COMMAND_VENDOR_STRING
};

@implementation AMRemoteCommandGenerator
-(NSString *) commandKeyUp{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, KEY_UP];
}
-(NSString *) commandKeyDown{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, KEY_DOWN];    
}
-(NSString *) commandKeyLeft{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, KEY_LEFT];
}
-(NSString *) commandKeyRight{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, KEY_RIGHT];
}

-(NSString *) commandKeyEnter{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, KEY_ENTER];
}
-(NSString *) commandKeyEsc{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, KEY_ESC];
}
-(NSString *) commandGetEcho{
/**
 From jccheng@actions-micro.com
 
 Dear all,
 
 這次的解法預計像這樣,請大家看看有沒有什麼問題.
 
 原本 app remote control discovery 是固定送 0:0,改成送 0:[最新protocol版本號],例如這次就是 0:3
 而小機收到 0:3或以上,才回 EZREMOTE:3:xxxxxxx
 如果小機收到 0:0 ~ 0:2 (為了兼容ios舊app),則回 EZREMOTE:2:xxxxxx
 */
    return [NSString stringWithFormat:@"%d:%d", COMMAND_CONNECT, 3];
}
+ (NSString*)vendorCommand:(NSInteger)commandCode {
    return [NSString stringWithFormat:@"%d:%zd", COMMAND_VENDOR, commandCode];
}
+ (NSString*)standardCommand:(NSInteger)commandCode {
    return [NSString stringWithFormat:@"%d:%zd", COMMAND_KEY, commandCode];
}
#pragma mark - Reserved Function Key

//Auto Command
//BenQ
-(NSString*) commandKeyAuto{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, REV_FUN_1];
}

//Source Command
//BenQ
-(NSString*) commandKeySource{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, REV_FUN_2];
}
//ECO Command
//BenQ
-(NSString*) commandKeyECO{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, REV_FUN_3];
}
//Power Command
//BenQ
-(NSString*) commandKeyPower{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, REV_FUN_4];
}
//MenuExit Command
//BenQ
-(NSString*) commandKeyMenuExit{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, REV_FUN_5];
}

//BenQ
-(NSString*) commandKeyMute{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, REV_FUN_6];
}

//BenQ
-(NSString*) commandKeyVolumeDown{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, REV_FUN_7];
}

//BenQ
-(NSString*) commandKeyVolumeUp{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, REV_FUN_8];
}

//BenQ
-(NSString*) commandKeyHome{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, REV_FUN_9];
}
//Page Up Command
//Asus
-(NSString*) commandKeyPageUp{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, REV_FUN_9];
}
//Blank Command
//BenQ
-(NSString*) commandKeyBlank{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, REV_FUN_10];
}
//MediaForward Command
//BenQ
-(NSString*) commandKeyForward{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, REV_FUN_11];
}
//Page Down Command
//Asus
-(NSString*) commandKeyRevPageDown{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, REV_FUN_11];
}
//Mediabackward Command
//BenQ
-(NSString*) commandKeyBackward{
    return [NSString stringWithFormat:@"%d:%d", COMMAND_KEY, REV_FUN_12];
}
@end
