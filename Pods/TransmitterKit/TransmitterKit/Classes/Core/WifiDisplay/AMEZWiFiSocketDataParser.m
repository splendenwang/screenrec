//
//  AMEZWiFiSocketDataParser.m
//  EZRemoteControliOS
//
//  Created by brianliu on 12/8/23.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import "AMEZWiFiSocketDataParser.h"
#import "ipmsg.h"
@import SimpleAES;

NSString  * const AMParameterKeyPasscode = @"passcode";
NSString  * const AMParameterKeyService = @"service";
NSString  * const AMParameterKeyDiscovery = @"discovery";
NSString  * const AMParameterKeyMD5 = @"md5";
NSString  * const AMMD5Secret = @":secret=82280189";


@implementation AMEZWiFiSocketDataParser


- (void)main{
    if (![self isCancelled])
    {
        @autoreleasepool {
            NSString *msg = self.stringContent;
            NSArray * commandComponents = [msg componentsSeparatedByString:@":"];
            if (commandComponents.count > 4) {
                unsigned long command = (unsigned long)[commandComponents[4] intValue]; //command is on the 5th elemet
                self.IPMSG_COMMAND = command;
                if(self.IPMSG_COMMAND == IPMSG_ANSENTRY){
                /* IP Messenger Protocol
                 1) Command(Format version-1)
                 
                 Ver(1) : PacketNo : SenderName : SenderHost : CommandNo : AdditionalSection
                 
                 2) An example for Message Send/Receive by using the current command format
                 
                 "1:100:shirouzu:jupiter:32:Hello"
                 
                 
                 1,
                 14795,
                 root,
                 "(none)",
                 3,
                 root,
                 "model=BENQ_GP10",
                 "passcode=6870"
                 
                 */
                    NSDictionary *parameters = [self parseKeyValuePairs:commandComponents];
                    if (parameters[AMParameterKeyDiscovery] != nil) {
                        // contains 'discovery', it's new protocol which needs to check MD5 checksum
                        if ([self checkMD5:commandComponents]) {
                            [self processAllValues:commandComponents];
                        } else {
                            self.isFraud = YES;
                        }
                    } else {
                        [self processAllValues:commandComponents];
                    }            
                }
                else if (self.IPMSG_COMMAND == IPMSG_SENDDATA){
                    DLog(@"not implemented yet");
                }
            }

            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                if([self.delegate respondsToSelector:@selector(didFinishParsing:fromParser:)]){
                    [self.delegate didFinishParsing:nil fromParser:self];
                }
            });
        }
    }
    
}

- (NSString *)responseStringWithoutMD5Pair:(NSArray *)commandComponents
{
    NSMutableArray *commandComponentsWithoutMD5Pair = [NSMutableArray new];
    for (NSString *component in commandComponents) {
        if (![component hasPrefix:AMParameterKeyMD5]) {
            [commandComponentsWithoutMD5Pair addObject:component];
        }
    }
    NSString *responseStringWithoutMD5Pair = [commandComponentsWithoutMD5Pair componentsJoinedByString:@":"];
    return responseStringWithoutMD5Pair;
}

- (BOOL)checkMD5:(NSArray *)commandComponents
{
    NSDictionary *parameters = [self parseKeyValuePairs:commandComponents];
    if (parameters[AMParameterKeyMD5] != nil) {
        NSString *responseStringWithoutMD5Pair = [self responseStringWithoutMD5Pair:commandComponents];
        responseStringWithoutMD5Pair = [responseStringWithoutMD5Pair stringByAppendingString:AMMD5Secret];
        
//        DLog(@"esponseStringWithoutMD5Pair.md5Value:%@. (%d)", responseStringWithoutMD5Pair.md5Value , responseStringWithoutMD5Pair.md5Value.length);
//        DLog(@"[parameters objectForKey:AMParameterKeyMD5]:%@ (%d)", [[parameters objectForKey:AMParameterKeyMD5] lowercaseString] ,[[parameters objectForKey:AMParameterKeyMD5] lowercaseString].length);
//        NSComparisonResult result = [responseStringWithoutMD5Pair.md5Value caseInsensitiveCompare:[[parameters objectForKey:AMParameterKeyMD5] lowercaseString] ];
//        NSComparisonResult result2 = [[[parameters objectForKey:AMParameterKeyMD5] lowercaseString] compare:responseStringWithoutMD5Pair.md5Value options:NSCaseInsensitiveSearch ];
        if ([responseStringWithoutMD5Pair.md5Value caseInsensitiveCompare:[parameters[AMParameterKeyMD5] uppercaseString]] == NSOrderedSame) {
            return YES;
        }
    }
    return NO;
}
- (void)processServiceValue:(NSDictionary *)parameters
{
    if (parameters[AMParameterKeyDiscovery] != nil && parameters[AMParameterKeyService] != nil) {
        NSString *servicesString = parameters[AMParameterKeyService];
        NSArray *services = [servicesString componentsSeparatedByString:@","];
        if (services.count >= 1) {
            NSScanner *scanner = [NSScanner scannerWithString:services[0]];
            unsigned serviceMask = 0;
            if ([scanner scanHexInt:&serviceMask]) {
                self.isPixViewerEnabled = (serviceMask & EZWIFI_SERVICE_APP_PHOTO_VIEWER)!=0;
                self.isLiveCamEnabled = (serviceMask & EZWIFI_SERVICE_APP_LIVE_CAM)!=0;
                self.isStreamingMediaEnabled = (serviceMask & EZWIFI_SERVICE_MEDIA_STREAMING)!=0;
                self.isStreamingDocEnabled = (serviceMask & EZWIFI_SERVICE_APP_STREAMIG_DOC)!=0;
                self.isSplitScreenEnabled = (serviceMask & EZWIFI_SERVICE_SPLIT_SCREEN)!=0;
                self.isDropboxEnabled = (serviceMask & EZWIFI_SERVICE_APP_DROPBOX)!=0;
                self.isWebEnabled = (serviceMask & EZWIFI_SERVICE_APP_WEB_VIEWER)!=0;
                self.isHTTPStreamingEnabled = (serviceMask & EZWIFI_SERVICE_APP_HTTP_STREAMING)!=0;
                self.discovery = ((NSString*)parameters[AMParameterKeyDiscovery]).integerValue;
                self.isStreamingMediaAudioEnabled = (serviceMask & EZWIFI_SERVICE_MEDIA_STREAM_AUDIO)!=0;
                self.isEZEncodeProEnable = (serviceMask & EZWIFI_SERVICE_EZENCODEPRO)!=0;
            }
        }
    } else {
        // turn all functons on by default
        self.isPixViewerEnabled = YES;
        self.isLiveCamEnabled = YES;
        self.isStreamingMediaEnabled = YES;
        self.isStreamingDocEnabled = YES;
        self.isSplitScreenEnabled = YES;
        self.isDropboxEnabled = YES;
        self.isWebEnabled = YES;
        self.discovery = 0;
    }
}

- (void)processAllValues:(NSArray *)commandComponents
{
    NSDictionary *parameters = [self parseKeyValuePairs:commandComponents];
    self.protocolVersion = commandComponents[0];
    self.userName = commandComponents[2];
    self.hostName = commandComponents[3];
    self.deviceModel = parameters[AMParameterKeyModel];
    self.passcode = parameters[AMParameterKeyPasscode];
    self.vendor = parameters[AMParameterKeyVendor];
    self.md5 = parameters[AMParameterKeyMD5];
    
    [self processServiceValue:parameters];
}
@end
