//
//  AMSocketDefine.h
//  AMCommon_iOS
//
//  Created by brianliu on 12/9/27.
//  Copyright (c) 2012年 ActionsMicro. All rights reserved.
//

#ifndef AMCommon_iOS_AMSocketDefine_h
#define AMCommon_iOS_AMSocketDefine_h

#define PORT_EZWIFIDISPLAY 2425
#define PORT_EZREMOTE 63630

#define TCP_TAG_WRITE_HEADER        100
#define TCP_TAG_WRITE_PICOIMAGE     101
#define TCP_TAG_WRITE_HEARTBEAT     102
#define TCP_TAG_WRITE_STREAM        106
#define TCP_TAG_WRITE_OTHER_COMMAND 107
#define TCP_TAG_WRITE_AUDIO         108
#define TCP_TAG_WRITE_H264          109

#define TCP_TAG_SPLIT_REQUEST       103
#define TCP_TAG_READ_HEADER         104
#define TCP_TAG_READ_PAYLOAD        105


#define UDP_TAG_BROADCAST_REMOTE      106
#define UDP_TAG_BROADCAST_WIFIDISPLAY 107


#endif
