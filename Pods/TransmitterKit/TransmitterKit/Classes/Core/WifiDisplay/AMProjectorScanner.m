//
//  AMProjectorScanner.m
//  AMCommon_iOS
//
//  Created by brianliu on 12/9/28.
//  Copyright (c) 2012年 ActionsMicro. All rights reserved.
//

#import "AMProjectorScanner.h"
@import CocoaAsyncSocket;
#import "AMRemoteCommandGenerator.h"
#import "AMSocketDefine.h"
#import "AMEZRemoteSocketDataParser.h"
#import "AMEZWiFiSocketDataParser.h"
#import "AMProjectorDevice.h"
#import "AMNetworkUtility.h"
#import "ipmsg.h"

NSString * const AMProjectorScannerErrorDomain = @"com.action.AMProjectorScannerErrorDomain";
NSString * const kAMProjectorScannerSearchCommand = @"kAMProjectorScannerSearchCommand";
NSString * const kAMProjectorScannerBroadcastIPs = @"kAMProjectorScannerBroadcastIPs";
NSString * const kAMProjectorScannerUDPPort = @"kAMProjectorScannerUDPPort";

@interface AMProjectorScanner () <GCDAsyncUdpSocketDelegate>
{
    float _duration;
    NSTimer * _timer;
    dispatch_queue_t _serialQueue;
    dispatch_source_t _timerSource;
}
@property (nonatomic, strong)   GCDAsyncUdpSocket * udpSocket;
@property (atomic, strong)      NSMutableDictionary * workingDict;
@property (nonatomic, strong)   NSOperationQueue * operationQueue;
@end


@implementation AMProjectorScanner

-(instancetype)init{
    NSLog(@"Use initWithDelegate: instead please");
    return [self initWithDelegate:nil];
}

-(instancetype) initWithDelegate:(id<AMPrototypeDeviceScannerDelegate>) delegate{
    self = [super init];
    if(self){
        self.delegate = delegate;
        _duration = 2.0;
        self.operationQueue = [[NSOperationQueue alloc] init];
        [self.operationQueue setMaxConcurrentOperationCount:1];
        self.workingDict = [NSMutableDictionary new];
        [self setupUDPSocket];
    }
    return self;
}
-(void) setupUDPSocket{
    if(self.udpSocket){
        return;
    }
    if(_serialQueue==NULL){
        _serialQueue = dispatch_queue_create("AMProjectorScanner", DISPATCH_QUEUE_SERIAL);
    }
    
    self.udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:_serialQueue];
    NSError * error = nil;
    [self.udpSocket enableBroadcast:YES error:&error];
    
    if (![self.udpSocket bindToPort:0 error:&error])
    {
//        DLog(@"%s, Error binding: %@", __PRETTY_FUNCTION__, error);
        if(self.delegate && [self.delegate respondsToSelector:@selector(scanner:failToScan:)]){
            NSDictionary * dict = @{ kAMProjectorScannerUDPPort: @PORT_EZWIFIDISPLAY};
            NSError * error = [NSError errorWithDomain:AMProjectorScannerErrorDomain code:AMProjectorScannerErrorUDPBindPortError userInfo:dict];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                if([self.delegate respondsToSelector:@selector(scanner:failToScan:)]){
                    [self.delegate scanner:self failToScan:error];
                }
            });
            
        }
    }
    if (![self.udpSocket beginReceiving:&error])
    {
        DLog(@"%s, Error receiving: %@", __PRETTY_FUNCTION__, error);
    }
}

-(void) sendSearchCommand:(NSData*)command viaInterfaces:(NSArray*)broadcastIPs{
    for (NSString * ip in broadcastIPs) {
        [self.udpSocket sendData:command toHost:ip port:PORT_EZWIFIDISPLAY withTimeout:10 tag:UDP_TAG_BROADCAST_REMOTE];
    }
}

-(void) startScanAddresses:(NSArray *)addresses{
    [self.workingDict removeAllObjects];
    if(_timerSource != nil)
    {
        return;
    }
    
    dispatch_time_t now = dispatch_walltime(DISPATCH_TIME_NOW, 0);
    _timerSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)); //not the key
    dispatch_source_set_timer(_timerSource, now, NSEC_PER_SEC*(1.0), 0ull);
    __weak AMProjectorScanner * weakSelf = self;
    dispatch_source_set_event_handler(_timerSource, ^{
        NSData *wifiDisplayCommandData = [[NSString stringWithFormat:@"2:1:Stan:Stan:1:hello"] dataUsingEncoding:NSASCIIStringEncoding];
        NSArray * broadcastIPs = [AMNetworkUtility broadcastAddressForAllInterface];
        if(addresses.count>0){
            [weakSelf sendSearchCommand:wifiDisplayCommandData viaInterfaces:addresses];
        } else if (broadcastIPs.count>0) {
            [weakSelf sendSearchCommand:wifiDisplayCommandData viaInterfaces:broadcastIPs];
        }
            
    });
    dispatch_resume(_timerSource);
}

-(void) startScan{
    [self setupUDPSocket];
    [self startScanAddresses:nil];
}
-(void) stopScan{
    @synchronized(self){
        dispatch_source_cancel(_timerSource);
        _timerSource = nil;
    }
    [self close];
}

-(void) close{
    [self.udpSocket close];
    self.udpSocket.delegate = nil;
    self.udpSocket = nil;
}
-(void) dealloc{
//    DTrace();
    [self close];
    self.delegate = nil;
    [self.operationQueue setSuspended:YES];
    [self.operationQueue cancelAllOperations];
}
- (void) removeDevice:(NSString*)IP{
    [self.workingDict removeObjectForKey:IP];
}
#pragma mark - CocoaAsyncSocket UDP sDelegate
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)theTag{
    //    DLog(@"%s:%lu", __PRETTY_FUNCTION__, theTag);
    if(theTag == UDP_TAG_BROADCAST_REMOTE){
        
    }
}
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error{
    //    DLog(@"%s:%@", __PRETTY_FUNCTION__, error);
    if(tag == UDP_TAG_BROADCAST_REMOTE){
    }
    
}
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext{
    NSString *host = nil;
    uint16_t port = 0;
    [GCDAsyncUdpSocket getHost:&host port:&port fromAddress:address];

    if(host!= nil && port==PORT_EZWIFIDISPLAY && data)
    {
        AMEZWiFiSocketDataParser * dataParser = [[AMEZWiFiSocketDataParser alloc] init];
        dataParser.dataToParse = data;
        dataParser.host = host;
        dataParser.port = port;
        dataParser.delegate = self;
        [self.operationQueue addOperation:dataParser];
    }
    else {
//        DLog(@"%s, possibly receiving error data.",__PRETTY_FUNCTION__);
    }
}
- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error{
//    DLog(@"%s:%@", __FUNCTION__, error);
    [self close];
    self.udpSocket = nil;
}


#pragma mark - AMSocketDataParserDelegate
- (void)didFinishParsing:(NSDictionary *)dict fromParser:(AMSocketDataParser*) parser{
    if([parser isKindOfClass:[AMEZWiFiSocketDataParser class]]) //broadcast back from EZWifiDisplay protocol
    {
        AMEZWiFiSocketDataParser * wifiParser = (AMEZWiFiSocketDataParser *) parser;
        if(wifiParser.IPMSG_COMMAND == IPMSG_ANSENTRY && !wifiParser.isFraud)  // only IPMSG_ANSENTRY mean that the command is from projector.
        {
//            DLog(@"%s, protocol ver:%@, username:%@, hostname:%@, command:%d, device model:%@, passcode:%@", __PRETTY_FUNCTION__,wifiParser.protocolVersion, wifiParser.userName, wifiParser.hostName, wifiParser.IPMSG_COMMAND, wifiParser.deviceModel, wifiParser.passcode);
            AMProjectorDevice * newDevice = [[AMProjectorDevice alloc] init];
            newDevice.ipAddress = parser.host;
            newDevice.protocolVersion = wifiParser.protocolVersion;
            newDevice.userName = wifiParser.userName;
            newDevice.hostName = wifiParser.hostName;
            newDevice.deviceModel= wifiParser.deviceModel;
            newDevice.passcode = wifiParser.passcode;
            newDevice.isPixViewerEnabled = wifiParser.isPixViewerEnabled;
            newDevice.isLiveCamEnabled = wifiParser.isLiveCamEnabled;
            newDevice.isStreamingMediaEnabled = wifiParser.isStreamingMediaEnabled;
            newDevice.isStreamingDocEnabled = wifiParser.isStreamingDocEnabled;
            newDevice.isSplitScreenEnabled = wifiParser.isSplitScreenEnabled;
            newDevice.isDropboxEnabled = wifiParser.isDropboxEnabled;
            newDevice.isWebEnabled = wifiParser.isWebEnabled;
            newDevice.isHTTPStreamingEnabled = wifiParser.isHTTPStreamingEnabled;
            newDevice.discovery = wifiParser.discovery;
            newDevice.vendor = wifiParser.vendor;
            newDevice.isStreamingMediaAudioEnable = wifiParser.isStreamingMediaAudioEnabled;
            newDevice.isEZEncodeProEnable = wifiParser.isEZEncodeProEnable;
            newDevice.md5 = wifiParser.md5;
            
            if((self.workingDict)[newDevice.ipAddress] ==nil){
                (self.workingDict)[newDevice.ipAddress] = newDevice;
                
                if([self.delegate respondsToSelector:@selector(scanner:discoveriedDevice:)]){
                    [self.delegate scanner:self discoveriedDevice:newDevice];
                }
            }
        }
    }
}
- (void)parseErrorOccurred:(NSError *)error fromParser:(AMSocketDataParser*) parse{
    DTrace();
}

#pragma mark - AMPrototypedeviceScanner
-(instancetype) initWithScanDuration:(NSTimeInterval)timeInterval delegate:(id<AMPrototypeDeviceScannerDelegate>)delegate{
    self = [self initWithDelegate:delegate];
    if(self)
    {
        _duration = timeInterval;
    }
    return self;
}

//-(NSString*) identifier{
//    return NSStringFromClass(self.class);
//}

@end


@interface IPAddressProjectorScanner () <GCDAsyncUdpSocketDelegate, AMSocketDataParserDelegate>
{
    dispatch_semaphore_t _semaphore;
    NSString * _ip;
    AMProjectorDevice * _device;
    GCDAsyncUdpSocket *_statusUdpSocket;
}
@property (nonatomic, strong)   NSOperationQueue * operationQueue;
@end
@implementation IPAddressProjectorScanner
-(AMProjectorDevice*)seekProjector:(NSString*)ipAddress withTimeout:(NSTimeInterval)timeout{

    if(self.operationQueue==nil){
        self.operationQueue = [[NSOperationQueue alloc] init];
    }
    _ip = ipAddress;
    
    if(!_statusUdpSocket){
        _statusUdpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
    }
    NSError * error = nil;
    [_statusUdpSocket enableBroadcast:YES error:nil];
    [_statusUdpSocket bindToPort:0 error:&error];
    [_statusUdpSocket beginReceiving:&error];
    
    NSData *wifiDisplayCommandData = [[NSString stringWithFormat:@"2:1:Stan:Stan:1:hello"] dataUsingEncoding:NSASCIIStringEncoding];
    [_statusUdpSocket sendData:wifiDisplayCommandData toHost:_ip port:PORT_EZWIFIDISPLAY
                   withTimeout:timeout tag:100];
    
    _semaphore = dispatch_semaphore_create(0);
    
    dispatch_time_t delayInNanoSeconds=dispatch_time(DISPATCH_TIME_NOW, timeout*NSEC_PER_SEC);
    dispatch_semaphore_wait(_semaphore, delayInNanoSeconds);
    
    return _device;
}
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext{
    NSString *host = nil;
    uint16_t port = 0;
    [GCDAsyncUdpSocket getHost:&host port:&port fromAddress:address];
    
    if(host!= nil && port==PORT_EZWIFIDISPLAY && data)
    {
        AMEZWiFiSocketDataParser * dataParser = [[AMEZWiFiSocketDataParser alloc] init];
        dataParser.dataToParse = data;
        dataParser.host = host;
        dataParser.port = port;
        dataParser.delegate = self;
        [self.operationQueue addOperation:dataParser];
    }
    else {
        //        DLog(@"%s, possibly receiving error data.",__PRETTY_FUNCTION__);
    }
}

- (void)didFinishParsing:(NSDictionary *)dict fromParser:(AMSocketDataParser*) parser{
    if([parser isKindOfClass:[AMEZWiFiSocketDataParser class]]) //broadcast back from EZWifiDisplay protocol
    {
        AMEZWiFiSocketDataParser * wifiParser = (AMEZWiFiSocketDataParser *) parser;
        if(wifiParser.IPMSG_COMMAND == IPMSG_ANSENTRY && !wifiParser.isFraud)  // only IPMSG_ANSENTRY mean that the command is from projector.
        {
            //            DLog(@"%s, protocol ver:%@, username:%@, hostname:%@, command:%d, device model:%@, passcode:%@", __PRETTY_FUNCTION__,wifiParser.protocolVersion, wifiParser.userName, wifiParser.hostName, wifiParser.IPMSG_COMMAND, wifiParser.deviceModel, wifiParser.passcode);
            AMProjectorDevice * newDevice = [[AMProjectorDevice alloc] init];
            newDevice.ipAddress = parser.host;
            newDevice.protocolVersion = wifiParser.protocolVersion;
            newDevice.userName = wifiParser.userName;
            newDevice.hostName = wifiParser.hostName;
            newDevice.deviceModel= wifiParser.deviceModel;
            newDevice.passcode = wifiParser.passcode;
            newDevice.isPixViewerEnabled = wifiParser.isPixViewerEnabled;
            newDevice.isLiveCamEnabled = wifiParser.isLiveCamEnabled;
            newDevice.isStreamingMediaEnabled = wifiParser.isStreamingMediaEnabled;
            newDevice.isStreamingDocEnabled = wifiParser.isStreamingDocEnabled;
            newDevice.isSplitScreenEnabled = wifiParser.isSplitScreenEnabled;
            newDevice.isDropboxEnabled = wifiParser.isDropboxEnabled;
            newDevice.isWebEnabled = wifiParser.isWebEnabled;
            newDevice.isHTTPStreamingEnabled = wifiParser.isHTTPStreamingEnabled;
            newDevice.discovery = wifiParser.discovery;
            newDevice.vendor = wifiParser.vendor;
            newDevice.isStreamingMediaAudioEnable = wifiParser.isStreamingMediaAudioEnabled;
            newDevice.isEZEncodeProEnable = wifiParser.isEZEncodeProEnable;
            newDevice.md5 = wifiParser.md5;
            
            _device = newDevice;
            dispatch_semaphore_signal(_semaphore);
        }
    }
}
- (void)parseErrorOccurred:(NSError *)error fromParser:(AMSocketDataParser*) parse{
    DTrace();
}

@end
