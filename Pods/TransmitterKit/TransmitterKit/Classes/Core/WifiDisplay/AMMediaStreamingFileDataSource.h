//
//  AMMediaStreamingFileDataSource.h
//  AMCommon_iOS
//
//  Created by James Chen on 3/6/13.
//  Copyright (c) 2013 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AMDPFImageSender;

@interface AMMediaStreamingFileDataSource : NSObject {
    dispatch_source_t readSource;   //media stream
    dispatch_queue_t  workQueue;
    dispatch_semaphore_t _fileReadingSemaphore;
}
@property (nonatomic, readonly) NSString * filePath;
- (id)initWithFile:(NSString *)file sender:(AMDPFImageSender *)sender;
- (void)startReadFileWithPosition:(uint64_t)position;
- (void)stopMediaStreaming;
- (uint64_t)getFileLength;
- (void)stopReadingFile;
- (void)waitUntilStopReading;
@end
