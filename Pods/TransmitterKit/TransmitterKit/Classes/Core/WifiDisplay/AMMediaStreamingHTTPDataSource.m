//
//  AMMediaStreamingHTTPDataSource.m
//  AMCommon_iOS
//
//  Created by James Chen on 3/14/13.
//  Copyright (c) 2013 ActionsMicro. All rights reserved.
//

#import "AMMediaStreamingHTTPDataSource.h"
@interface AMMediaStreamingHTTPDataSource ()
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *userAgent;
@end
@implementation AMMediaStreamingHTTPDataSource
- (id)initWithURL:(NSString *)url userAgent:(NSString *)userAgent sender:(AMDPFImageSender *)sender
{
    self = [super init];
    if (self) {
        self.url = url;
        self.userAgent = userAgent;
    }
    return self;
}
@end
