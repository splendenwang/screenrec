//
//  AMRemoteHostScanner.h
//  AMCommon_iOS
//
//  Created by brianliu on 12/12/7.
//  Copyright (c) 2012年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMEZRemoteSocketDataParser.h"
#import "AMPrototypeDeviceScanner.h"
extern NSString * const AMRemoteScannerErrorDomain;
extern NSString * const kAMRemoteHostScannerUDPPort;

typedef NS_ENUM(NSInteger, AMRemoteScannerErrorCode){
    AMRemoteScannerErrorNoNetworkInterface = 0,
    AMRemoteScannerErrorUDPBindPortError = 1
};
@protocol AMPrototypeDeviceScannerDelegate;
@class AMRemoteHost;

/**
 AMRemoteHostScanner helps application to discover remote control enabled devices in the network.
 Remember to call -(void close; before you release instance of this object.
 */
@interface AMRemoteHostScanner : AMPrototypeDeviceScanner <AMSocketDataParserDelegate, AMPrototypeDeviceScannerProtocol>

+(AMRemoteHostScanner *) defaultScanner;

/**
 Init with an delegate.
 @param delegate The delegate object for AMRemoteHostSeeker.
 */
-(instancetype) initWithDelegate:(id<AMPrototypeDeviceScannerDelegate>) delegate NS_DESIGNATED_INITIALIZER;

/**
 Clean up resources and stop searching.
 */
-(void) close;

/**
 Return whether receiver was closed or not.
 */
//@property (readonly) BOOL isClosed;

- (void)addRemoteMessageListener:(AMRemoteHost *)remoteHost;

- (void)removeRemoteMessageListener:(AMRemoteHost *)remoteHost;

@end
