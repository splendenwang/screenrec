//
//  AMRemoteCommandGenerator.h
//  EZRemoteControliOS
//
//  Created by brian on 12/4/5.
//  Copyright (c) 2012年 Actions-Micro. All rights reserved.
//
// This class generate command for sending to chip host

#import <Foundation/Foundation.h>

@interface AMRemoteCommandGenerator : NSObject

@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyUp;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyDown;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyLeft;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyRight;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyEnter;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyEsc;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandGetEcho;

@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyAuto;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeySource;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyECO;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyPower;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyMenuExit;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyMute;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyVolumeDown;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyVolumeUp;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyHome;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyPageUp;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyRevPageDown;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyBlank;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyForward;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *commandKeyBackward;


+ (NSString*)vendorCommand:(NSInteger)commandCode;
+ (NSString*)standardCommand:(NSInteger)commandCode;
@end
