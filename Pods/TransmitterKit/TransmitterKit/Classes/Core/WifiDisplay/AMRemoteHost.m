//
//  AMRemoteHost.m
//  AMCommon_iOS
//
//  Created by brianliu on 12/12/7.
//  Copyright (c) 2012年 ActionsMicro. All rights reserved.
//

#import "AMRemoteHost.h"
@import CocoaAsyncSocket;
#import "AMRemoteCommandGenerator.h"
#import "AMSocketDefine.h"
#import "AMRemoteHostScanner.h"
#import "AMJSONRPCObject.h"
#import "AMJSONRPCResponse.h"
#import "AMJSONRPCRequest.h"
#import "AMEZWiFiSocketDataParser.h"
#import "EZCastApplicationFeatures.h"
#import "AMPrototypeDevice+_internal.h"
@import SimpleAES;

#define REMOTEHOST_MODEL @"AMRemoteHost.DeviceModel"

typedef NS_ENUM(long, AMRemoteHostTcpTag){
    AMRemoteHostTcpTagSendKey = 1000,
    AMRemoteHostTcpTagSendVendorKey,
    AMRemoteHostTcpTagReadData
};
/**
 Using the messages to tell listeners about network status.
 */
NSString * const kAMRemoteHostMessageTCPDisconnected = @"AMRemoteHost.kAMRemoteHostMessageTCPDisconnected";
NSString * const kAMRemoteHostMessageTCPConnectionTimeOut = @"AMRemoteHost.kAMRemoteHostMessageTCPConnectionTimeout";
/**
 *  Fixed key for decrypt the true key of jrpc encryption
 */
NSString * const kJrpcEncryptionFixedKey = @"AM2feY5ysJAA4oAM";
@interface AMRemoteHost () <GCDAsyncUdpSocketDelegate>
{
    void(^ _establishTCPConnectionResult)(BOOL, NSError *);
}
@property (strong, nonatomic) NSString * jrpcEncryptionKey;
@property (strong, nonatomic) NSOperationQueue * tcpProcessingQueue;
@property (strong, nonatomic) GCDAsyncUdpSocket *udpSocket;
@property (strong, nonatomic) GCDAsyncSocket * tcpSocket;
@property (strong, nonatomic) NSMutableSet *listeners;

@end

@implementation AMRemoteHost

-(instancetype)init{
    self = [super init];
    if(self){
        self.scanner = [AMRemoteHostScanner defaultScanner];
        [self commonInit];
    }
    return self;
}

-(instancetype) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        self.deviceModel = [aDecoder decodeObjectForKey:REMOTEHOST_MODEL];
        self.scanner = [AMRemoteHostScanner defaultScanner];
        [self commonInit];
    }
    return self;
}

-(void) encodeWithCoder:(NSCoder *)aCoder{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.deviceModel forKey:REMOTEHOST_MODEL];
}

- (instancetype)initWithScanner:(AMRemoteHostScanner *)scanner
{
    if (self = [self init]) {
        self.scanner = scanner;
    }
    return self;
}

-(void) commonInit{
    self.listeners = [NSMutableSet new];
    self.tcpProcessingQueue = [[NSOperationQueue alloc] init];
    self.tcpProcessingQueue.maxConcurrentOperationCount = 1;
    self.sendMessageViaUDP = NO;
    self.sendMessageViaTCP = YES;
    self.timeout = 10.0;
}

-(NSString*) family{
    if((self.parameters)[@"family"]){
        return (self.parameters)[@"family"];
    }
    return @"ezcast"; // if there is no family value on a AMRemoteHost obj, it means this might be a legacy device, I have to make it a fix value @"ezcast".
}

-(NSString*) type{
    return (self.parameters)[@"type"];
}
-(NSString*) webRootHost{
    if((self.parameters)[@"webroot"] != nil)
        //http%3A%2F%2F192.168.203.1%3A8080%2F
        return [(self.parameters)[@"webroot"] stringByRemovingPercentEncoding];
    return [NSString stringWithFormat:@"http://%@/",self.ipAddress];
}

-(NSString*) customName{
    return (self.parameters)[@"name"];
}

-(NSString*) localHost {
    return self.tcpSocket.localHost;
}
-(void) dealloc{

    [_scanner removeRemoteMessageListener:self];
    self.tcpSocket.delegate = nil;
    [self.tcpSocket disconnect];
}

-(NSString*) description{
    return [NSString stringWithFormat:@"%@<%p>, ipAddress:%@, model:%@, hostname:%@, srcvers:%@", NSStringFromClass([self class]), self, self.ipAddress, self.deviceModel, self.hostName, self.srcvers];
}
-(void) establishTCPConnection:(void(^ __nonnull)(BOOL, NSError *))result{
    if (self.sendMessageViaUDP && _udpSocket == nil) {
        self.udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    }
    if(self.sendMessageViaTCP && _tcpSocket == nil){
        _tcpSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)];
        NSError * error = nil;
        NSAssert(self.ipAddress != nil, @"IPAddress should not be nil before establishing a tcp socket");
        if([_tcpSocket connectToHost:self.ipAddress onPort:PORT_EZREMOTE withTimeout:self.timeout error:&error]==NO){
            if(error){
                DLog(@"%s, error: %@", __PRETTY_FUNCTION__, error);
                result(NO, error);
                return;
            }
        }
        _establishTCPConnectionResult = result;
    }
}

- (void)sendString:(NSString *)string
{
    if(self.sendMessageViaUDP==NO && self.sendMessageViaTCP==NO){
        NSAssert(NO, @"An AMRemoteHost should have at least one kind of socket to send message. Check the value sendMessageViaUDP and sendMessageViaTCP or you AMRemoteHost object. They should not be NO simutaniously.");
    }
    if (_tcpSocket == nil){ //    NSAssert(_tcpSocket!=nil, @"eatablishConnection first");
        DLog(@"DebugLog: Please call establishConnection first before sendString or the connection may has been terminated");
        return;
    }
//    [self prepareSocketIfNeeded];
    NSData * data = [string dataUsingEncoding:NSUTF8StringEncoding];
    if(self.sendMessageViaUDP){
        [_udpSocket sendData:data
                      toHost:self.ipAddress
                        port:PORT_EZREMOTE
                 withTimeout:self.timeout tag:0];
    }
    if(self.sendMessageViaTCP){
        uint32_t payloadLength = NSSwapHostIntToLittle((uint32_t) data.length) ;
        NSMutableData * tcpData = [NSMutableData dataWithBytes:&payloadLength length:sizeof(uint32_t)];
        [tcpData appendData:data];
        NSData * immtableData = [[NSData alloc] initWithData:tcpData];
//        DLog(@"send msg: string:%@, string len:%lu, TCP data length:%lu", string, (unsigned long)string.length,(unsigned long)tcpData.length);
        [self.tcpSocket writeData:immtableData withTimeout:self.timeout tag:AMRemoteHostTcpTagSendKey];
    }
}
- (void)sendKey:(AMRemoteHostStandardKeyCode)keyCode {
    [self sendString:[AMRemoteCommandGenerator standardCommand:keyCode]];
}
- (void)sendVendorKey:(NSInteger)keyCode {
    [self sendString:[AMRemoteCommandGenerator vendorCommand:keyCode]];
}
- (void)addMessageListener:(id<AMRemoteHostMessageListener>)listener
{
    NSAssert(_tcpSocket!=nil, @"establistConnection first");
    [self.scanner addRemoteMessageListener:self];
    @synchronized(self.listeners){
        [self.listeners addObject:[NSValue valueWithNonretainedObject:listener]];
    }
}
- (void)removeMessageListener:(id<AMRemoteHostMessageListener>)listener
{
    @synchronized(self.listeners){
        [self.listeners removeObject:[NSValue valueWithNonretainedObject:listener]];
        if (self.listeners.count == 0) {
            [self.scanner removeRemoteMessageListener:self];
        }
    }
}
- (void)dispatchCustomerMessage:(NSString *)message
{
    @synchronized(self.listeners){
        for (NSValue *listener in [_listeners allObjects]) {
            [[listener nonretainedObjectValue] didReceiveMessage:message fromRemoteHost:self];
        }
    }
}
- (void)dispatchJsonrpcMessage:(NSString*)jrpcMsg isEncrypted:(BOOL)encrypted{
    AMJSONRPCObject * jrpcObject = nil;
    NSError * error = nil;
    if(encrypted && self.jrpcEncryptionKey.length > 0) {
        AMCommonCryptoSettings setting = kAMAESCBC128BitKeySettings;
        NSString * decryptedJrpcMsg = [jrpcMsg AESDecryptKey:self.jrpcEncryptionKey settings:setting];
        if(decryptedJrpcMsg.length > 0) {
//            DLog(@"(TCP)dispatchJsonrpcMessage:\n\t%@",decryptedJrpcMsg);
            jrpcObject = [AMJSONRPCObject parse:decryptedJrpcMsg error:&error];
        }
    } else {
        jrpcObject = [AMJSONRPCObject parse:jrpcMsg error:&error];
    }
    if(jrpcObject == nil) {
        NSString * errorDomain = @"com.winnerwave.remotehost.json-rpc";
        NSInteger errorCode = -1000;
        NSString * message = NSLocalizedString(@"Disconnect (Communication data error)", nil);
        NSString * jrpcErrorDicParamString = [NSString stringWithFormat:@"{\"code\":%ld, \"domain\":\"%@\", \"message\":\"%@\"}",(long)errorCode,errorDomain,message];
        NSString * jrpcErrorMsgDicString = [NSString stringWithFormat:@"{\"jsonrpc\": \"2.0\", \"method\": \"error.parse_jrpc\", \"params\": %@}",jrpcErrorDicParamString];
        jrpcObject = [AMJSONRPCObject parse:jrpcErrorMsgDicString error:&error];
    }
    @synchronized(self.listeners){
        for (NSValue *listener in [_listeners allObjects]) {
            [[listener nonretainedObjectValue] didReceiveJsonrpcMessage:jrpcMsg jsonrpcObject:jrpcObject fromRemoteHost:self];
        }
    }
}

-(BOOL) isEqual:(id)object{
    if([object isKindOfClass:[self class]]){
        BOOL returnValue = YES;
        AMRemoteHost * anotherDevice= (AMRemoteHost*)object;
        if([self objectForKey:kAMPrototypeDeviceID]!=nil && [anotherDevice objectForKey:kAMPrototypeDeviceID]!=nil){
            if([[self objectForKey:kAMPrototypeDeviceID] isEqualToString:[anotherDevice objectForKey:kAMPrototypeDeviceID]] == NO)
                returnValue =  NO;
        }
        else if ([self objectForKey:kAMPrototypeDeviceID]==nil || [anotherDevice objectForKey:kAMPrototypeDeviceID] == nil){
            DLog(@"%s, There may be an error checking deviceid.", __PRETTY_FUNCTION__);
            returnValue =  NO;
        }
        /**
         * 2016.5.18 Jesse's ask: No need to check displayName.
         */
//        if(![self.displayName isEqualToString:anotherDevice.displayName]){
//            return NO;
//        }
        
        if(![self.hostName isEqualToString:anotherDevice.hostName]){
            return NO;
        }
        
        if(![self.vendor isEqualToString:anotherDevice.vendor]){
            return NO;
        }
        
        if(![self.deviceModel isEqualToString:anotherDevice.deviceModel]){
            return NO;
        }

        //2017.4.13 comment out by brian, whatever. (Discussion confirmed with JC and Youan)
//        for (NSString * key in self.parameters.allKeys) {
//            if([self.parameters[key] isEqual:anotherDevice.parameters[key]]==NO &&
//               [key isEqualToString:@"name"]==NO) //2016.5.18 Jesse's ask: No need to check displayName.
//                return NO;
//        }
        
        /**
         * 2014.4.9 Jesse said: No need to check IP.
         */
//        if(self.ipAddress !=nil && anotherDevice.ipAddress!=nil){
//            if([self.ipAddress isEqualToString:anotherDevice.ipAddress]==NO){
//                returnValue = NO;
//            }
//        }
//        else if(self.ipAddress == nil || anotherDevice.ipAddress == nil){
//            DLog(@"%s, There may be an error checking ipAddress.", __PRETTY_FUNCTION__);
//            returnValue = NO;
//        }
        return returnValue;
    }
    return NO;
}

-(NSComparisonResult) compare:(AMRemoteHost*) host{
    if(host.ipAddress && self.ipAddress && [host.ipAddress isEqualToString:self.ipAddress]==NO){
        return [self.ipAddress compare:host.ipAddress];
    }
    if(host.displayName && self.displayName && [host.displayName isEqualToString:self.displayName]==NO){
        return [self.displayName compare:host.displayName];
    }
    if(host.deviceModel && self.deviceModel && [host.deviceModel isEqualToString:self.deviceModel]==NO){
        return [self.deviceModel compare:host.deviceModel];
    }
    if(host.vendor && self.vendor && [host.vendor isEqualToString:self.vendor]==NO){
        return [self.vendor compare:host.vendor];
    }
    if(host.hostName && self.hostName && [host.hostName isEqualToString:self.hostName]==NO){
        return [self.hostName compare:host.hostName];
    }
    if(host.srcvers && self.srcvers && [host.srcvers isEqualToString:self.srcvers]==NO){
        return [self.srcvers compare:host.srcvers];
    }
//    if(host.parameters && self.parameters && [host.parameters isEqualToDictionary:self.parameters]==NO){
//        return NSOrderedAscending;
//    }
    
    return NSOrderedSame;
}

-(void) close{
    [self.udpSocket close];
    self.udpSocket.delegate = nil;
    self.udpSocket = nil;
    [self.tcpSocket disconnect];
    self.tcpSocket = nil;
    self.jrpcEncryptionKey = nil;
}


#pragma mark - GCDAsyncSocketDelegate
- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port{
//    DTrace();
//    DLog(@"host: %@, port:%d", host, port);
    if(_establishTCPConnectionResult){
        _establishTCPConnectionResult(YES, nil);
        _establishTCPConnectionResult = nil;
    }

    [sock readDataToLength:4 withTimeout:-1 tag:TCP_TAG_READ_HEADER];
}

//- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag{
////    DTrace();
////    DLog(@"tag:%ld", tag);
//
//}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err{
    DLog(@"%s:%@", __FUNCTION__, err);
    self.tcpSocket.delegate = nil;
    self.tcpSocket = nil;
    
    if(_establishTCPConnectionResult){
        _establishTCPConnectionResult(NO, err);
        _establishTCPConnectionResult = nil;
    }
    
    //old logic
    if(err.code == GCDAsyncSocketConnectTimeoutError){
        return;
    }
    [self dispatchCustomerMessage:kAMRemoteHostMessageTCPDisconnected];
    
}

-(void) parseData:(NSData*)data fromHost:(NSString*)host port:(uint16_t)port
            queue:(NSOperationQueue*)queue delegate:(id<AMSocketDataParserDelegate, NSObject>)delegate
{
    AMEZRemoteSocketDataParser * dataParser = [[AMEZRemoteSocketDataParser alloc] init];
    dataParser.dataToParse = data;
    dataParser.host = host;
    dataParser.port = port;
    dataParser.delegate = delegate;
    [queue addOperation:dataParser];
}


- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag{
//    DTrace();

    if(tag== TCP_TAG_READ_HEADER){ //get payload length
        uint32_t payloadSize = 0;
        payloadSize = NSSwapLittleIntToHost(  *((unsigned int *)[data bytes]) );
//        DLog(@"%s, payloadSize = %d", __PRETTY_FUNCTION__, payloadSize);
        [sock readDataToLength:payloadSize withTimeout:-1 tag:TCP_TAG_READ_PAYLOAD];
    }
    else if(tag == TCP_TAG_READ_PAYLOAD) {
        if(self.sendMessageViaTCP){
            NSString *host = nil;
            uint16_t port = 0;
            [GCDAsyncUdpSocket getHost:&host port:&port fromAddress:[sock connectedAddress]];
            [self parseData:data fromHost:host port:port queue:self.tcpProcessingQueue delegate:self];
        }
        [sock readDataToLength:4 withTimeout:-1 tag:TCP_TAG_READ_HEADER];
    }
    
    
}

- (void)socket:(GCDAsyncSocket *)sock didReadPartialDataOfLength:(NSUInteger)partialLength tag:(long)tag{
//    DTrace();
//    DLog(@"tag:%ld", tag);
}

- (void)socketDidCloseReadStream:(GCDAsyncSocket *)sock{
    DTrace();
}

#pragma mark - AMSocketDataParserDelegate
- (void)didFinishParsing:(NSDictionary *)dict fromParser:(AMSocketDataParser*) parser{
    AMEZRemoteSocketDataParser * remoteParser = (AMEZRemoteSocketDataParser*) parser;
    if ( self.sendMessageViaTCP && remoteParser.isCustomerMessage) {
        DLog(@"\n(TCP)dispatchCustomerMessage:\n\t%@\n\tfrom:%@", remoteParser.messageBody, remoteParser.host);
        [self dispatchCustomerMessage:remoteParser.messageBody];
    }
    else if(self.sendMessageViaTCP && remoteParser.isJsonrpcMessage){
        [self dispatchJsonrpcMessage:remoteParser.messageBody isEncrypted:remoteParser.isEncrypted];
    }
    else if (remoteParser.isStandardMessage) {
        NSException* myException = [NSException
                                    exceptionWithName:@"Not implemeted yet exception"
                                    reason:@"Please implement it"
                                    userInfo:nil];
        @throw myException;
    }
}
- (void)parseErrorOccurred:(NSError *)error fromParser:(AMSocketDataParser*) parser{
    DTrace();
}

#pragma mark - 
-(NSString*) displayName{
    if(self.protocolVersion.integerValue>=3){    //only protocol 3 provides custome name.
        if(self.customName && self.customName.length>0)
            return self.customName;
    }
    return self.hostName;
}

#pragma mark -
-(void) setJrpcEncryptionKey:(NSString *)key{
    if(key.length > 0)
        _jrpcEncryptionKey = [key AESDecryptKey:kJrpcEncryptionFixedKey settings:kAMAESCBC128BitKeySettings];
    else
        _jrpcEncryptionKey = nil;
}
-(void) sendJSONRPCObject:(AMJSONRPCObject*) jrpcObject{
    [self sendJSONRPCMessage:jrpcObject.JSONRPCMessage encrypted:NO];
}
-(void) sendJSONRPCObject:(AMJSONRPCObject *)jrpcObject encrypted:(BOOL)encrypted{
    [self sendJSONRPCMessage:jrpcObject.JSONRPCMessage encrypted:encrypted];
}
-(void) sendJSONRPCMessage:(NSString *) jrpcMsg{
    [self sendJSONRPCMessage:jrpcMsg encrypted:NO];
}
-(void) sendJSONRPCMessage:(NSString *)jrpcMsg encrypted:(BOOL)encrypted{
    if(encrypted && self.jrpcEncryptionKey.length > 0) {
        NSString * string = [jrpcMsg AESEncryptKey:self.jrpcEncryptionKey settings:kAMAESCBC128BitKeySettings];
        [self sendString:[@"7:" stringByAppendingString:string]];
    } else {
        [self sendString:[@"6:" stringByAppendingString:jrpcMsg]];
    }
}

#pragma mark -
-(BOOL) isClientModeEnable{
//    DLog(@"[self serviceBitMask]:%x", [self serviceBitMask]);
    return ([self serviceBitMask]&SERVICE_CLIENT_MODE)!=0;
}

@end
