//
//  AMBufferController.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/26.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AMBufferControllerDelegate;
@interface AMBufferController : NSObject

@property (atomic, strong) id cacheObj;
@property (nonatomic, weak) id<AMBufferControllerDelegate> delegate;

@property BOOL stopThread;
-(void) sendObject:(id) aObj;
-(void) startConsumerThreads;
-(void) removeAllBuffer;
-(void) stopThreadGracefully;
@end

@protocol AMBufferControllerDelegate
-(BOOL) readyToSendObj;
-(void) sendObject:(id) aObj;
@end