//
//  AMSocketDataParser.m
//  EZRemoteControliOS
//
//  Created by brian on 12/4/9.
//  Copyright (c) 2012年 Actions-Micro. All rights reserved.
//

#import "AMSocketDataParser.h"
//#import "NSString+AMStringUtility.h"
@import CocoaAsyncSocket;

NSString  * const AMParameterKeyModel = @"model";
NSString  * const AMParameterKeyVendor = @"vendor";
NSString  * const AMParameterKeyHostName = @"hostname";

@interface AMSocketDataParser ()
@end

@implementation AMSocketDataParser
- (NSDictionary *)parseKeyValuePairs:(NSArray *)commandComponents
{
    NSMutableDictionary *keyValuePairs = [NSMutableDictionary dictionary];
    for (NSString *component in commandComponents) {
        if ([component rangeOfString:@"="].location != NSNotFound) {
            NSArray *keyAndValue = [component componentsSeparatedByString:@"="];
            if (keyAndValue.count > 1) {
                keyValuePairs[keyAndValue[0]] = keyAndValue[1];
            } else {
                keyValuePairs[keyAndValue[0]] = @"";
            }
        }
    }
    return keyValuePairs;
}
- (NSString *)stringContent {
    if (_stringContent == nil) {
        // 2013.2.6 Brian Liu
        // Usually we don't need to do so, but the projector does sometimes send a null terminated cString to App side.
		// But its hard to make other people change, so I make a check here to see if the last byte is '\0'
		// It it is indeed '\0', we remove it, or it will cause unexpected result to our cocoa(or cocoa touch) application.
		// PS. No matter how u use the NSString messages to handle, at last u still need to know if the data(or byte, or cString) is null-terminated.
		// http://stackoverflow.com/a/2467856/2013140
		// https://developer.apple.com/library/mac/documentation/Cocoa/Reference/Foundation/Classes/NSString_Class/
        char lastByte = 0;
        [self.dataToParse getBytes:&lastByte range:NSMakeRange(self.dataToParse.length-1, 1)];
        if(lastByte=='\0'){
//            DLog(@"[%@]data error -> %@", self.host , self.dataToParse);
            DLog(@"data is null terminated. Correcting data");

            self.dataToParse = [self.dataToParse subdataWithRange:NSMakeRange(0, self.dataToParse.length-1)];
        }
        else{
//            DLog(@"[%@] data correct -> %@", self.host, self.dataToParse);
        }
        
        //2014.5.9 the encoding changed to UTF-8
        //Define by Jesse.
        self.stringContent = [[NSString alloc] initWithData:self.dataToParse encoding:NSUTF8StringEncoding];
    }
    return _stringContent;
}
@end
