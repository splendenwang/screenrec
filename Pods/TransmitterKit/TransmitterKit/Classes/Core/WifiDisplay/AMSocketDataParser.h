//
//  AMSocketDataParser.h
//  EZRemoteControliOS
//
//  Created by brian on 12/4/9.
//  Copyright (c) 2012年 Actions-Micro. All rights reserved.
//

@import Foundation;

@protocol AMSocketDataParserDelegate;
extern NSString  * const AMParameterKeyModel;
extern NSString  * const AMParameterKeyVendor;
extern NSString  * const AMParameterKeyHostName;

@interface AMSocketDataParser : NSOperation

@property (nonatomic, weak) id <AMSocketDataParserDelegate, NSObject> delegate;

@property (nonatomic, strong) NSData *          dataToParse;

@property (nonatomic, strong) NSString *        host;

@property (nonatomic, assign) unsigned short    port;

- (NSDictionary *)parseKeyValuePairs:(NSArray *)commandComponents;

@property (nonatomic, strong) NSString *    deviceModel;

@property (nonatomic, strong) NSString *    vendor;

@property (nonatomic, strong) NSString *    hostName;

@property (nonatomic, strong) NSString *    stringContent;

@property (nonatomic, strong) NSDictionary * parameters;

@end

@protocol AMSocketDataParserDelegate
- (void)didFinishParsing:(NSDictionary *)dict fromParser:(AMSocketDataParser*) parser;
- (void)parseErrorOccurred:(NSError *)error fromParser:(AMSocketDataParser*) parser;
@end