//
//  AMMediaStreamingHTTPDataSource.h
//  AMCommon_iOS
//
//  Created by James Chen on 3/14/13.
//  Copyright (c) 2013 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AMDPFImageSender;

@interface AMMediaStreamingHTTPDataSource : NSObject

- (id)initWithURL:(NSString *)url userAgent:(NSString *)userAgent sender:(AMDPFImageSender *)sender;

@property (nonatomic, readonly) NSString *url;
@property (nonatomic, readonly) NSString *userAgent;

@end
