//
//  AMBufferController.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/26.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMBufferController.h"
#define VIDEO_BUFFER_SIZE 5
#define AUDIO_BUFFER_SIZE 2
#define DATA_HAS_DATA               1
#define DATA_NO_DATA                2
@interface AMBufferController ()
@property (nonatomic, strong)    NSConditionLock * conditionLock;
@property (nonatomic, strong)    NSMutableArray * bufferPool; //cool stuff from james
@end
@implementation AMBufferController

-(instancetype) init{
    self = [super init];
    if(self){
        self.bufferPool = [[NSMutableArray alloc] initWithCapacity:VIDEO_BUFFER_SIZE];
        self.conditionLock = [[NSConditionLock alloc] initWithCondition:DATA_NO_DATA];
    }
    return self;
}

-(void) sendObject:(id) aObj{
    //    DTrace();
//    NSAssert(data!=nil, @"%s, para image nil", __PRETTY_FUNCTION__);
    if(!aObj){
//        DLog(@"\n\n***%s, no object to buffer\n\n", __PRETTY_FUNCTION__);
        return;
    }
    __weak AMBufferController * weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @autoreleasepool {
            [weakSelf.conditionLock tryLock];
            {
                if(weakSelf.bufferPool.count <VIDEO_BUFFER_SIZE){
                    [weakSelf.bufferPool addObject:aObj];
                }
                else{
//                    (weakSelf.bufferPool)[VIDEO_BUFFER_SIZE-1] = aObj;
                    DLog(@"drop object (buffer controller)");
                }
                weakSelf.cacheObj = aObj;
            }
            [weakSelf.conditionLock unlockWithCondition:DATA_HAS_DATA];
        }
    });
}

-(void) consumerThread {
    while(self.stopThread==NO){
        [self.conditionLock lockWhenCondition:DATA_HAS_DATA];
        if([self.delegate readyToSendObj] == YES)
        {
            @autoreleasepool {
                if(self.bufferPool.count>0){
                    id obj = nil;
                    obj = (self.bufferPool)[0];
                    [self.bufferPool removeObject:obj];
                    [self.delegate sendObject:obj];
                }
            }
        }
        else
        {   /**
             if it is not ready, we just remove the pending image so that this loop wont be always running(always got data to lock but cannot the data cannot be sent due to network is not ready), that will cause the CPU very high.
             **/
            if(self.bufferPool.count>0)
                [self.bufferPool removeAllObjects];
        }
        [self.conditionLock unlockWithCondition:(self.bufferPool.count>0)? DATA_HAS_DATA : DATA_NO_DATA];
    }
    DLog(@"%s Image consumer thread stopped",__PRETTY_FUNCTION__);
}

-(void) startConsumerThreads{
//    DTrace();
    self.stopThread = NO;
    [NSThread detachNewThreadSelector:@selector(consumerThread) toTarget:self withObject:nil];
}

-(void) removeAllBuffer{
    [self.bufferPool removeAllObjects];
}

-(void) stopThreadGracefully{
[self.conditionLock unlockWithCondition:DATA_HAS_DATA];
}
@end
