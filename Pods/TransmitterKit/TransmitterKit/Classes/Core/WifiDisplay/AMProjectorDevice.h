//
//  AMProjectorDevice.h
//  EZRemoteControliOS
//
//  Created by brianliu on 12/8/23.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMPrototypeDevice.h"
#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#endif
@interface AMProjectorDevice : AMPrototypeDevice <NSCopying>

@property (nonatomic, copy) NSString * protocolVersion;

@property (nonatomic, copy) NSString * userName;

@property (nonatomic, copy) NSString * passcode;


/**
 The model name of the device.
 */
@property (nonatomic, strong) NSString * deviceModel;

/**
 For varification only. Not really meaningful for a projector object.
 */
@property (nonatomic, copy) NSString * md5;

@property (nonatomic, assign) UInt16 split;

@property (nonatomic, assign) UInt16 position;

@property (nonatomic) BOOL isPixViewerEnabled;

@property (nonatomic) BOOL isLiveCamEnabled;

@property (nonatomic) BOOL isStreamingMediaEnabled;

@property (nonatomic) BOOL isStreamingDocEnabled;

@property (nonatomic) BOOL isSplitScreenEnabled;

@property (nonatomic) BOOL isDropboxEnabled;

@property (nonatomic) BOOL isWebEnabled;

@property (nonatomic) BOOL isHTTPStreamingEnabled;

@property (nonatomic, assign) NSInteger discovery;

@property (assign) BOOL isStreamingMediaAudioEnable;

@property (assign) BOOL isEZEncodeProEnable;    //h264

-(NSComparisonResult) compare:(AMProjectorDevice*) anotherDevice;


/**
 Synchronous method. This method will send a udp message to a AMProjectorDevice to ask for max resolution.
 
 @return Max resolution in CGSize.
 @note If there is any error getting resolution from a AMPorjectorDevice, this method will return CGSizeZero.
 */
-(CGSize) getMaxResolution;
@end
