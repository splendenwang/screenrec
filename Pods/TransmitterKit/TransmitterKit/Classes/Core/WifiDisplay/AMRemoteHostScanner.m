//
//  AMRemoteHostScanner.m
//  AMCommon_iOS
//
//  Created by brianliu on 12/12/7.
//  Copyright (c) 2012年 ActionsMicro. All rights reserved.
//

#import "AMRemoteHostScanner.h"
@import CocoaAsyncSocket;
#import "AMRemoteCommandGenerator.h"
#import "AMSocketDefine.h"
#import "AMEZRemoteSocketDataParser.h"
#import "AMNetworkUtility.h"
#import "AMRemoteHost.h"
#import "AMPrototypeDevice+_internal.h"
#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#endif
const NSUInteger tolerance  = 3;

NSString * const AMRemoteScannerErrorDomain = @"com.action.AMRemoteScannerErrorDomain";
NSString * const kAMRemoteHostScannerUDPPort = @"kAMRemoteHostScannerUDPPort";
@interface AMRemoteHostScanner () <GCDAsyncUdpSocketDelegate>
@property (nonatomic, strong)   GCDAsyncUdpSocket * udpSocket;
@property (atomic, strong)      NSMutableDictionary * workingDict;
@property (nonatomic, strong)   NSOperationQueue * operationQueue;
@property (nonatomic, strong)   NSMutableSet *listeners;
@property (nonatomic, strong)   NSMutableDictionary * deviceADiscoveriedCount;
@end


@implementation AMRemoteHostScanner
{
    float _duration;
    dispatch_queue_t _serialQueue;
    dispatch_source_t _timerSource;
    NSInteger _count;
}
-(instancetype) init{
    return [self initWithDelegate:nil];
}
static AMRemoteHostScanner * s_scanner = nil;
+(AMRemoteHostScanner *) defaultScanner{
    static dispatch_once_t predRemoteScanner;
    
	dispatch_once(&predRemoteScanner, ^{
		s_scanner = [[AMRemoteHostScanner alloc] initWithDelegate:nil];
	});
	return s_scanner;
}

-(instancetype) initWithDelegate:(id<AMPrototypeDeviceScannerDelegate>)delegate{
    self = [super init];
    if(self){
        self.delegate = delegate;
        self.operationQueue = [[NSOperationQueue alloc] init];
        [self.operationQueue setMaxConcurrentOperationCount:1];
        self.workingDict = [NSMutableDictionary new];
        self.listeners = [NSMutableSet new];
    }
    return self;
}

-(void) setupUDPSocket{
//    DTrace();
    if(self.udpSocket)
        return;
    if(_serialQueue==NULL){
        _serialQueue =  dispatch_queue_create("AMRemoteHostScanner", DISPATCH_QUEUE_SERIAL);
    }
    self.udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:_serialQueue];
    NSError * error = nil;
    [self.udpSocket enableBroadcast:YES error:&error];
    
    if (![self.udpSocket bindToPort:0 error:&error])
    {
        DLog(@"%s, Error binding: %@", __PRETTY_FUNCTION__, error);
        if(self.delegate && [self.delegate respondsToSelector:@selector(scanner:failToScan:)]){
            NSDictionary * dict = @{ kAMRemoteHostScannerUDPPort: @0 , NSLocalizedDescriptionKey:@"AMRemoteScanner bind udp socket failed(iOS is unable to provide port for binding)"};
            NSError * error = [NSError errorWithDomain:AMRemoteScannerErrorDomain code:AMRemoteScannerErrorUDPBindPortError userInfo:dict];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self.delegate scanner:self failToScan:error];
            });
        }
    }
    if (![self.udpSocket beginReceiving:&error])
    {
        DLog(@"%s, Error receiving: %@", __PRETTY_FUNCTION__, error);
    }
}

-(void) sendSearchCommand:(NSData*)command viaInterfaces:(NSArray*)broadcastIPs{
    for (NSString * ip in broadcastIPs) {
        [self.udpSocket sendData:command toHost:ip  port:PORT_EZREMOTE withTimeout:-1 tag:UDP_TAG_BROADCAST_REMOTE];
    }
}
/**
 *  send search command to addresses, if addresses are nil, it will dyanmic getting all interface addresses
 *
 *  @param addresses addresses to scan
 */
-(void) startScanAddresses:(NSArray *)addresses{
    @synchronized(self){
        //        DTrace();
        [self setupUDPSocket];
        @synchronized(self){
            [self.workingDict removeAllObjects];
            self.deviceADiscoveriedCount = [NSMutableDictionary new];
        }
        if(_timerSource != nil)
        {
            return;
        }
        _count = 0;
        dispatch_time_t now = dispatch_walltime(DISPATCH_TIME_NOW, 0);
        _timerSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
        dispatch_source_set_timer(_timerSource, now, NSEC_PER_SEC*2, 0ull);
        __weak AMRemoteHostScanner * weakSelf = self;
        dispatch_source_set_event_handler(_timerSource, ^{
            //send broadcast msgs
            NSData *remoteCommandData = [[[[AMRemoteCommandGenerator alloc] init] commandGetEcho] dataUsingEncoding:NSUTF8StringEncoding];
            NSArray * broadcastIPs = [AMNetworkUtility broadcastAddressForAllInterface];
            if (addresses.count > 0) {
                [weakSelf sendSearchCommand:remoteCommandData viaInterfaces:addresses];
            } else if (broadcastIPs.count > 0) {
                [weakSelf sendSearchCommand:remoteCommandData viaInterfaces:broadcastIPs];
            }
            
            //EI-287 Improving-Device-List-UX
            //Using a simple structure to track if a device is disappeared. If it is judged disppeared then just notify the delegate.
            @synchronized(self){
                _count++;
                //after checking, reset the numbers
                if(_count>2){
                    //check disppeared devices
                    NSMutableArray * hostsToRemove = [NSMutableArray new];
                    for (NSString * aHost in weakSelf.workingDict) {
                        NSNumber * count = weakSelf.deviceADiscoveriedCount[aHost];
                        if(count.integerValue < 1){
                            [hostsToRemove addObject:weakSelf.workingDict[aHost]];
                        }
                    }
                    for (AMRemoteHost * aHost in hostsToRemove) {
                        //                        DLog(@"hostsToRemove:%@", aHost);
                        [weakSelf.workingDict removeObjectForKey:aHost.ipAddress];
                        [weakSelf.deviceADiscoveriedCount removeObjectForKey:aHost.ipAddress];
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            if([weakSelf.delegate respondsToSelector:@selector(scanner:disappearedDevice:)]){
                                [weakSelf.delegate scanner:weakSelf disappearedDevice:aHost];
                            }
                        });
                    }
                    
                    
                    _deviceADiscoveriedCount = [NSMutableDictionary new];
                    _count = 0;
                    
                    
                }
                
                
            }
            
        });
        dispatch_resume(_timerSource);
    }
}
-(void) startScan{
    [self startScanAddresses:nil];
}
-(void) stopScan{
    @synchronized(self){
        dispatch_source_cancel(_timerSource);
        _timerSource = nil;
        [self close];   //destroy sockets if stopScan is called to make sure clean sockets next time.
    }
}

-(void) close{
    if(self.udpSocket){
//        DLog(@"%s, self:%@", __PRETTY_FUNCTION__, self);
        [self.udpSocket close];
        self.udpSocket.delegate = nil;
        self.udpSocket = nil;
    }
}
- (BOOL)isClosed
{
        return _udpSocket == nil;
}
-(void) dealloc{
//    DTrace();
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self close];
    self.delegate = nil;
    
    for (AMRemoteHost *remoteHost in _listeners) {
        remoteHost.scanner = nil;
    }
    for (AMRemoteHost *remoteHost in self.workingDict.allValues) {
        remoteHost.scanner = nil;
    }
    

}
- (void)dispatchCustomerMessage:(NSString *)message from:(NSString *)host
{
    for(AMRemoteHost *device in [self.listeners allObjects]){
        if(device.sendMessageViaUDP && [host isEqualToString:device.ipAddress]) {
            DLog(@"(UDP)dispatchCustomerMessage:%@ from:%@", message, host);
            [device dispatchCustomerMessage:message];
            break;
        }
    }
}

-(void) parseData:(NSData*)data fromAddress:(NSData*)address{
    NSString *host = nil;
    uint16_t port = 0;
    [GCDAsyncUdpSocket getHost:&host port:&port fromAddress:address];
    [self parseData:data fromHost:host port:port];
}


-(void) parseData:(NSData*)data fromHost:(NSString*)host port:(uint16_t)port{
    [self parseData:data fromHost:host port:port queue:self.operationQueue delegate:self];
}

-(void) parseData:(NSData*)data fromHost:(NSString*)host port:(uint16_t)port
            queue:(NSOperationQueue*)queue delegate:(id<AMSocketDataParserDelegate, NSObject>)delegate
{
    AMEZRemoteSocketDataParser * dataParser = [[AMEZRemoteSocketDataParser alloc] init];
    dataParser.dataToParse = data;
    dataParser.host = host;
    dataParser.port = port;
    dataParser.delegate = delegate;
    [queue addOperation:dataParser];
}

#pragma mark - GCDAsyncUDSocketDelegate
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext{
    
    if ([GCDAsyncUdpSocket isIPv4Address:address]) { // we deal only IPv4 only to prevent double message
        NSString *host = nil;
        uint16_t port = 0;
        [GCDAsyncUdpSocket getHost:&host port:&port fromAddress:address];
//        NSString *msg = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//        DLog(@" RECV:(%@:%hu), msg:%@",host, port, msg);
        
        if(host!= nil && port==PORT_EZREMOTE && data){
            [self parseData:data fromHost:host port:port];
        }
    }
}
#pragma mark - AMSocketDataParserDelegate
- (void)didFinishParsing:(NSDictionary *)dict fromParser:(AMSocketDataParser*) parser{
    //            DLog(@"%s, protocol ver:%@, username:%@, hostname:%@, command:%d, device model:%@, passcode:%@", __PRETTY_FUNCTION__,wifiParser.protocolVersion, wifiParser.userName, wifiParser.hostName, wifiParser.IPMSG_COMMAND, wifiParser.deviceModel, wifiParser.passcode);

    if([parser isKindOfClass:[AMEZRemoteSocketDataParser class]])
    {  //broadcast back from EZRemote protocol
        AMEZRemoteSocketDataParser * remoteParser = (AMEZRemoteSocketDataParser *) parser;

        if (remoteParser.isDiscoveryMessage) {
            AMRemoteHost * newDevice = [[AMRemoteHost alloc] initWithScanner:self];
            newDevice.parameters = remoteParser.parameters;
            newDevice.ipAddress = remoteParser.host;
            newDevice.deviceModel = remoteParser.deviceModel;
            newDevice.vendor = remoteParser.vendor;
            newDevice.hostName = remoteParser.hostName;
            newDevice.srcvers = remoteParser.srcvers;
            newDevice.protocolVersion = remoteParser.protocolVersion;
            newDevice.passcode = remoteParser.parameters[@"passcode"];
            
            @synchronized(self){
                if((self.workingDict)[newDevice.ipAddress] ==nil){  //device not exist
                    (self.workingDict)[newDevice.ipAddress] = newDevice;
                    
                    if([self.delegate respondsToSelector:@selector(scanner:discoveriedDevice:)]){
                        [self.delegate scanner:self discoveriedDevice:newDevice];
                    }
                }
                else{   //device already exists
                    AMPrototypeDevice * origDevice = (self.workingDict)[newDevice.ipAddress];
                    if([origDevice isEqual:newDevice] == NO){
                        DLog(@"device not the same, update it plz, %@(%@) ---> %@(%@)", origDevice.displayName, origDevice.ipAddress, newDevice.displayName , newDevice.ipAddress);
                        if([self.delegate respondsToSelector:@selector(scanner:disappearedDevice:)]){
                            [self.delegate scanner:self disappearedDevice:origDevice];
                        }
                        
                        (self.workingDict)[newDevice.ipAddress] = newDevice;
                        self.deviceADiscoveriedCount[newDevice.ipAddress] =  @(0);
                        
                        if([self.delegate respondsToSelector:@selector(scanner:discoveriedDevice:)]){
                            [self.delegate scanner:self discoveriedDevice:newDevice];
                        }
                    }
                }
                
                NSNumber * number = self.deviceADiscoveriedCount[newDevice.ipAddress];
                self.deviceADiscoveriedCount[newDevice.ipAddress] =  @(number.integerValue+1);
                //            DLog(@"newDevice.ip=%@(%zd)", newDevice.ipAddress, number.integerValue);
            }

            
        } else if (remoteParser.isCustomerMessage) {
            [self dispatchCustomerMessage:remoteParser.messageBody from:parser.host];
        } else if (remoteParser.isStandardMessage) {
//            NSAssert(NO, @"not implement yet");
            NSException* myException = [NSException
                                        exceptionWithName:@"Not implemeted yet exception"
                                        reason:@"Please implement it"
                                        userInfo:nil];
            @throw myException;
        }
    }
}
- (void)parseErrorOccurred:(NSError *)error fromParser:(AMSocketDataParser*) parse{
    DTrace();
}
- (void)addRemoteMessageListener:(AMRemoteHost *)remoteHost
{
    [self.listeners addObject:remoteHost];
}
- (void)removeRemoteMessageListener:(AMRemoteHost *)remoteHost
{
    [self.listeners removeObject:remoteHost];
}

#pragma mark - AMPrototypedeviceScanner
-(instancetype) initWithScanDuration:(NSTimeInterval)timeInterval delegate:(id<AMPrototypeDeviceScannerDelegate>)delegate{
    self = [self initWithDelegate:delegate];
    if(self)
    {
        _duration = timeInterval;
    }
    return self;
}

//-(NSString*) identifier{
//    return NSStringFromClass(self.class);
//}
@end
