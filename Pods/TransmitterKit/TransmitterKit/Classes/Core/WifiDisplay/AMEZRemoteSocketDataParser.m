//
//  AMEZRemoteSocketDataParser.m
//  EZRemoteControliOS
//
//  Created by brianliu on 12/8/23.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import "AMEZRemoteSocketDataParser.h"
NSString * const AMParameterKeySrcvers = @"srcvers";

@implementation AMEZRemoteSocketDataParser
{
    dispatch_queue_t _serialQueue;
}
- (BOOL)parseMessageTypeData:(NSArray *)commandComponents
{
    if (commandComponents.count > 2 && [commandComponents[1] integerValue] >= 1) {
        self.messageBody = [[commandComponents subarrayWithRange:NSMakeRange(2, commandComponents.count - 2)] componentsJoinedByString:@":"];
        if([commandComponents[1] integerValue] == 3) {
            self.isEncrypted = YES;
        }
        return YES;
    }
    return NO;
}
- (void)main
{
    if (![self isCancelled])
    {
        @autoreleasepool {
            NSArray * commandComponents = [self.stringContent componentsSeparatedByString:@":"];
            
            if ([self.stringContent hasPrefix:@"EZREMOTE"]) {
                NSDictionary *parameters = [self parseKeyValuePairs:commandComponents];
                self.parameters = parameters;
                self.vendor = parameters[AMParameterKeyVendor];
                self.deviceModel = parameters[AMParameterKeyModel];
                self.hostName = parameters[AMParameterKeyHostName];
                self.srcvers = parameters[AMParameterKeySrcvers];
                self.protocolVersion = commandComponents.count>2 ? commandComponents[1]:nil;
                self.isDiscoveryMessage = YES;
            } else if (self.host != nil && [self.stringContent hasPrefix:self.host]) { // for backward compatibility
                self.isDiscoveryMessage = YES;
            } else if ([self.stringContent hasPrefix:@"STANDARD:"]) {
                if ([self parseMessageTypeData:commandComponents]) {
                    self.isStandardMessage = YES;
                }
            } else if ([self.stringContent hasPrefix:@"CUSTOMER:"]) {
                if ([self parseMessageTypeData:commandComponents]) {
                    self.isCustomerMessage = YES;
                }
            }
            else if([self.stringContent hasPrefix:@"JSONRPC"]){
                if ([self parseMessageTypeData:commandComponents]) {
                    self.isJsonrpcMessage = YES;
                }
            }
            
            if(_serialQueue==NULL){
                _serialQueue = dispatch_queue_create("AMEZRemoteSocketDataParser", DISPATCH_QUEUE_SERIAL);
            }
            
            if([self.delegate respondsToSelector:@selector(didFinishParsing:fromParser:)])
                [self.delegate didFinishParsing:nil fromParser:self];

        }
    }
}
@end
