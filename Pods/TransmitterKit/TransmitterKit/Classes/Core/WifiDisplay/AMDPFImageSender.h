//
//  AMDPFImageSender.h
//  EZRemoteControliOS
//
//  Created by brianliu on 12/7/11.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CocoaAsyncSocket;
#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#endif
@class AMPicoRequest, AMProjectorDevice, BytesMeter;
@protocol AMDPFImageSenderApplicationDelegate, AMDPFImageSenderMediaStreamingDelegate;

typedef NS_ENUM(NSInteger, kAMMediaStreamingError){
    kAMMediaStreamingErrorReadingFile = 0
};

extern NSString * const kAMMediaStreamingSourceURL;

typedef void (^AMDPFSenderWebURLBlock)(NSURL * webURL);

typedef NS_ENUM(NSInteger, DeviceStatus) {
	DeviceStatusSocketNotConnected = 0,
    DeviceStatusNotInWifiDisplayMode,
	DeviceStatusNotAllowToStreaming,
	DeviceStatusAllowStreaming,
    DeviceStatusStoppedByProjector
} ;

@interface AMDPFImageSender : NSObject

@property (nonatomic, readonly) BOOL readyToSendImage;
@property (nonatomic, readonly) BOOL readyToSendAudio;

@property (nonatomic, weak) id <AMDPFImageSenderApplicationDelegate> applicationDelegate;

@property (atomic, weak) id <AMDPFImageSenderMediaStreamingDelegate> mediaStreamingDelegate;

@property (nonatomic, assign) NSTimeInterval timeoutInterval;

-(void) connectToProjector:(AMProjectorDevice*) projector;

@property (NS_NONATOMIC_IOSONLY, readonly, copy) AMProjectorDevice *connectedProjector;

-(void) disconnect;

-(void) sendImageWithData:(NSData *) imageData;

#if TARGET_OS_IPHONE
-(void) sendImage:(UIImage*)image;
#else
-(void) sendImage:(NSImage*)image;
#endif
-(void) sendH264Data:(NSData*)data width:(uint32_t)width height:(uint32_t)height;

-(void) sendPCMData:(NSData*) PCMData;

-(void) requestForAutoScreenAssign;

-(void) requestForNewSplit:(NSInteger)newSplitNumber newPosition:(NSInteger) newPosition;

-(void) retrieveWebURL:(AMDPFSenderWebURLBlock)block;

//2012.12.17
#pragma mark Session Concept Method
+(void) beginSessionWithSender:(AMDPFImageSender*) sender;
+(void) endCurrentSession;
+(AMDPFImageSender*) currentSessionSender;
+(BOOL) isCurrentlyAbleToSend;

- (void)sendEOFMeidaStreamingCommand;
- (void)sendMediaStreamingData:(NSData *)data;

//2016.7.6
/**
 *  Get localHost address string from tcp socket. The value is nil if not connected
 */
-(NSString *)localHost;

@property (NS_NONATOMIC_IOSONLY, readonly) BytesMeter * bytesMeter;

@end

@protocol AMDPFImageSenderApplicationDelegate <NSObject>
-(void) projectConnectStatus:(BOOL)connected object:(AMDPFImageSender*) dpfSender;
-(void) projectorDidNotifyToStop:(AMDPFImageSender*)dpfSender;
-(void) projectorDidNotifyToStart:(AMDPFImageSender *)dpfSender;
-(void) projectorDidNotifyToDisconnect:(AMDPFImageSender *) dpfSender;
-(void) projectorRequest:(AMPicoRequest*)request  status:(BOOL) accecpted fromObject:(AMDPFImageSender*) dpfSender;
-(void) projectorDidDisconnect:(AMDPFImageSender*)dpfSender error:(NSError*) error;
//#if !TARGET_OS_IPHONE
//for MAC OS X. The delegate should return a number that describe the max KB for every image to send.
//-(NSUInteger) imageSizeLimit;
//#endif
@end


#pragma mark - Category MediaStreaming
@interface AMDPFImageSender (MediaStreaming)
/**
 * It will send only startHTTPMeidaStreamingCommand to dongle, if filePath is local file (scheme 'file:'), it will create an path via local http server
 */
-(void) requestForHttpMediaStreaming;
/**
 * It will send startHTTPMeidaStreamingCommand or startMediaStreamingCommand to dongle, according file path prefix is http related or not, 
 * if filePath length not larger than 0 it will return error
 */
-(void) requestForMediaStreaming DEPRECATED_MSG_ATTRIBUTE("Use requestForHttpMediaStreaming instead.It is not supported after 8270 dongle");
/**
 * someone:因為audio跟video都有rm的檔案，直接用requestForMediaStreaming:的話，會把rm音樂檔案誤判為電影檔案，於是乾脆多開一個給外面去call。
 *
 * It will send startHTTPMeidaStreamingCommand or startAudioStreamingCommand to dongle, according file path prefix is http related or not.
 * if filePath length not larger than 0 it will return error
 **/
-(void) requestForAudioStreaming DEPRECATED_MSG_ATTRIBUTE("Use requestForHttpMediaStreaming instead.It is not supported after 8270 dongle");
-(void) stopMediaStreaming;
-(void) seekToTime:(uint32_t) seekTime;
-(void) volumnUp;
-(void) volumnDown;
-(void) pauseMediaStreaming;
-(void) resumeMediaStreaming;
@end

@protocol  AMDPFImageSenderMediaStreamingDelegate <NSObject>

// return a file path here
-(NSString *) mediaLocation;

-(void) projectorDidAcceptMediaStreaming;

-(void) projectorDidRejectMediaStreaming:(NSError *)error;

//Deprecated
//-(void) projectorDidEndMediaStreaming;

/*
    Keys in the userInfo of error(If there is any):
    kAMMediaStreamingErrorDomain: media streaming error domain
    kAMMediaStreamingSourceURL: the original media url that is pass to AMDPFImageSender for media streaming
 */
-(void) projectorDidEndMediaStreamingWithError:(NSError *)error;

-(void) mediaStreamingTotalTime:(uint32_t)totalTime;

-(void) mediaStreamingElapseTime:(uint32_t)current;
@optional
-(NSString *) httpUserAgentString;
@end


