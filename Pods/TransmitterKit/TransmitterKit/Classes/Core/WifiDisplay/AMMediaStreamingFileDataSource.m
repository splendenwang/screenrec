//
//  AMMediaStreamingFileDataSource.m
//  AMCommon_iOS
//
//  Created by James Chen on 3/6/13.
//  Copyright (c) 2013 ActionsMicro. All rights reserved.
//

#import "AMMediaStreamingFileDataSource.h"
#import "AMDPFImageSender.h"
#import "AMMediaStreamingStandardErrorDefinition.h"
@interface AMMediaStreamingFileDataSource ()
@property (atomic, strong) NSFileHandle *fileHandle;
@property (nonatomic, weak) AMDPFImageSender *sender;
@property (nonatomic, strong) NSString *filePath;

@end

@implementation AMMediaStreamingFileDataSource
- (id)initWithFile:(NSString *)file sender:(AMDPFImageSender *)sender
{
    self = [super init];
    if (self) {
        workQueue = dispatch_queue_create("com.actions.dpf.workQueue", NULL);
        _fileReadingSemaphore = dispatch_semaphore_create(1);
        self.sender = sender;
        self.filePath = file;
        self.fileHandle = [NSFileHandle fileHandleForReadingAtPath:file];
    }
    return self;
}
- (void)dealloc
{
    if(workQueue) {
        workQueue = nil;
    }
    if(_fileReadingSemaphore) {
        _fileReadingSemaphore=nil;
    }
    if(_fileHandle) {
        [_fileHandle closeFile];
    }
}
- (void)startReadFileWithPosition:(uint64_t)position {
    DTrace();

        DLog(@"self.fileHandle.fileDescriptor:%d", self.fileHandle.fileDescriptor);
        [self.fileHandle seekToFileOffset:position];
        
        NSAssert(readSource == nil, @"readSource should be nil");
        readSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_READ, [self.fileHandle fileDescriptor], 0, workQueue);
        if (!readSource) {
            DLog(@"error creating read source");
        } else {
            //        DLog(@"new ReadSource:%p", readSource);
        }
        NSAssert(readSource != nil, @"readSource should not be nil");
        
        // If you need to print the readSource in the cancel handler(ex: log it out), u need to memorize it's value first.
        // dispatch_source_t theReadSource = readSource;
        
        dispatch_source_set_event_handler(readSource, ^{
            @autoreleasepool {
                @try {
                    NSData * readData = [self.fileHandle readDataOfLength:32768];
                    
                    if (readData.length==0) {
                        DLog(@"%@: read 0 data, maybe EOF", NSStringFromClass([AMMediaStreamingFileDataSource class]));
                        [self stopReadingFile];
                        [_sender sendEOFMeidaStreamingCommand];
                    } else {
                        __block AMDPFImageSender * weakSender = _sender;
                        [weakSender sendMediaStreamingData:readData];
                    }
                }
                @catch (NSException *exception) {
                    dispatch_semaphore_signal(_fileReadingSemaphore);
                    [self stopReadingFileAndWait];

                    NSString * localizedDescription = [NSString stringWithFormat:@"Read file error, stop media streaming process.\n File path:%@", self.filePath];
                    NSDictionary * userInfo = @{NSLocalizedDescriptionKey:localizedDescription, kAMMediaStreamingSourceURL:self.filePath};
                    NSError * error = [NSError errorWithDomain:kAMMediaStreamingStandardErrorDomain code:kAMMediaStreamingErrorReadingFile userInfo:userInfo];
                    
                    if(_sender.mediaStreamingDelegate && [_sender.mediaStreamingDelegate respondsToSelector:@selector(projectorDidEndMediaStreamingWithError:)] ){
                        [_sender.mediaStreamingDelegate projectorDidEndMediaStreamingWithError:error];
                    }
                }
                @finally {
                    
                }
            }
        });
        
        // Install the cancellation handler
        dispatch_source_set_cancel_handler(readSource, ^{
            DLog(@"%@: read source canceled", NSStringFromClass([AMMediaStreamingFileDataSource class]));
            dispatch_semaphore_signal(_fileReadingSemaphore);    //After cancel is completed, signal fileReadingSemaphore to let the succeeding read operation to start
        });
        dispatch_semaphore_wait(_fileReadingSemaphore, DISPATCH_TIME_FOREVER);   //start file reading, so we consuming a semaphore
        dispatch_resume(readSource);
}
- (void)stopReadingFile {
    DTrace();
    if(readSource){
//        DLog(@"[%@]ready to release readSource:%p", [NSThread currentThread], readSource);
        dispatch_source_cancel(readSource);
        readSource = nil;
    }
}
- (void)stopReadingFileAndWait {
    [self stopReadingFile];
    [self waitUntilStopReading];
}
- (void)waitUntilStopReading
{
    dispatch_semaphore_wait(_fileReadingSemaphore, DISPATCH_TIME_FOREVER);   //wait for the previous read operation to complete
    dispatch_semaphore_signal(_fileReadingSemaphore);    //等上一批確定結束後，增加一個fileReadSemaphore代表可以準備開始進行下一批讀資料的動作
}
- (void)stopMediaStreaming
{
    if (self.fileHandle) {
        [self.fileHandle closeFile];
        self.fileHandle = nil;        
    }
}
- (uint64_t)getFileLength
{
    uint64_t fileLength = 0;
    if (self.fileHandle) {
        fcntl([self.fileHandle fileDescriptor], F_SETFL, O_NONBLOCK);
        [self.fileHandle seekToEndOfFile];
        fileLength = [self.fileHandle offsetInFile];
        [self.fileHandle seekToFileOffset:0];
    }
    return fileLength;
}
@end
