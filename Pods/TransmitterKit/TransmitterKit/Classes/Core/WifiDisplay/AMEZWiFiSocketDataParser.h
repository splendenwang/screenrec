//
//  AMEZWiFiSocketDataParser.h
//  EZRemoteControliOS
//
//  Created by brianliu on 12/8/23.
//  Copyright (c) 2012年 Actions-Micro Taipei. All rights reserved.
//

#import "AMSocketDataParser.h"
typedef NS_OPTIONS(NSInteger, EZWIFI_SERVICE_BITMASK){
    EZWIFI_SERVICE_WIFI_LAN_DISPLAY =  0x01<<0,
    EZWIFI_SERVICE_MEDIA_STREAMING =   0x01<<1,
    EZWIFI_SERVICE_APP_PHOTO_VIEWER =  0x01<<2,
    EZWIFI_SERVICE_APP_LIVE_CAM =      0x01<<3,
    EZWIFI_SERVICE_APP_STREAMIG_DOC =  0x01<<4,
    EZWIFI_SERVICE_SPLIT_SCREEN =      0x01<<5,
    EZWIFI_SERVICE_APP_DROPBOX =       0x01<<6,
    EZWIFI_SERVICE_APP_WEB_VIEWER =    0x01<<7,
    EZWIFI_SERVICE_APP_QUALITY_MODE =  0x01<<8,
    EZWIFI_SERVICE_APP_HTTP_STREAMING = 0x01<<9,
    EZWIFI_SERVICE_APP_REMOTE_CONTROL = 0x01<<10,
    EZWIFI_SERVICE_MEDIA_STREAM_AUDIO = 0x01<<11,
    EZWIFI_SERVICE_EZENCODEPRO =        0x01 <<12
};
@interface AMEZWiFiSocketDataParser : AMSocketDataParser

@property (nonatomic, strong) NSString *    protocolVersion;

@property (nonatomic, strong) NSString *    userName;

@property (nonatomic, assign) NSInteger     IPMSG_COMMAND;

@property (nonatomic, strong) NSString *    passcode;

@property (nonatomic, strong) NSString *    md5;    //md5 string received from responses

@property (nonatomic) BOOL                  isFraud;

@property (nonatomic) BOOL                  isPixViewerEnabled;

@property (nonatomic) BOOL                  isLiveCamEnabled;

@property (nonatomic) BOOL                  isStreamingMediaEnabled;

@property (nonatomic) BOOL                  isStreamingDocEnabled;

@property (nonatomic) BOOL                  isSplitScreenEnabled;

@property (nonatomic) BOOL                  isDropboxEnabled;

@property (nonatomic) BOOL                  isWebEnabled;

@property (nonatomic) BOOL                  isHTTPStreamingEnabled;

@property (nonatomic, assign) NSInteger     discovery;

@property (assign) BOOL                     isStreamingMediaAudioEnabled;

@property (assign) BOOL                     isEZEncodeProEnable;    //h264

@end
