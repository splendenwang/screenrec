//
//  AMProjectorScanner.h
//  AMCommon_iOS
//
//  Created by brianliu on 12/9/28.
//  Copyright (c) 2012年 ActionsMicro. All rights reserved.
//
//test
#import <Foundation/Foundation.h>
#import "AMSocketDataParser.h"
#import "AMPrototypeDeviceScanner.h"
extern NSString * const AMProjectorScannerErrorDomain;
extern NSString * const kAMProjectorScannerUDPPort;


typedef NS_ENUM(NSInteger, AMProjectorScannerErrorCode){
    AMProjectorScannerErrorNoNetworkInterface = 0,
    AMProjectorScannerErrorUDPBindPortError = 1
};

@class AMProjectorDevice;
@interface AMProjectorScanner : AMPrototypeDeviceScanner <AMSocketDataParserDelegate, AMPrototypeDeviceScannerProtocol>

/**
 @param delegate a Delegate object
 @return Return the Object itself.
 */
-(instancetype) initWithDelegate:(id<AMPrototypeDeviceScannerDelegate>) delegate NS_DESIGNATED_INITIALIZER;

-(void) close;

/**
 An interface for AMGenericDeviceScanner to notify AMProjectorDeviceScanner that an AMRemoteDeviceScanner has disvoceried a remote deivce disappeared.
 @param IP The IP of the disappeared remote host.
 */
- (void) removeDevice:(NSString*)IP;
@end


// Unicast projector finder (subnet-crossable)
@interface IPAddressProjectorScanner : NSObject
/**
 Find out projector object with an ipAddress.
 */
-(AMProjectorDevice*)seekProjector:(NSString*)ipAddress withTimeout:(NSTimeInterval)timeout;
@end
