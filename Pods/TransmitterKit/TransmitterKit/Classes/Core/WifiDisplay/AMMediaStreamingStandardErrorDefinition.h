//
//  AMMediaStreamingError.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/11/11.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

@import Foundation;
#ifndef AMCommon_iOS_AMMediaStreamingError_h
#define AMCommon_iOS_AMMediaStreamingError_h

extern NSString * const kAMMediaStreamingStandardErrorDomain;

typedef NS_ENUM(NSInteger, AMMediaStreamingResultCode) {
    /* No rrror */
    AV_RESULT_OK =                                      0,
    
    /* General erros that can not be classified */
    AV_RESULT_ERROR_GENERIC =                           1,
    
    /* Remote projector fail media streaming due to a init failure, ex: memory not enough */
    AV_RESULT_ERROR_START_INIT_FAILED=                  2,
    
    /* The remote projector may be in a split-screen (multi-user) state */
    AV_RESULT_ERROR_START_OCCUPIED_OTHER_USER=          3,
    
    /* The remote projector maybe still in a state of media streaming */
    AV_RESULT_ERROR_START_OCCUPIED_ALREADY_STREAMING=   4,
    
    /* The format of the media file is not support */
    AV_RESULT_ERROR_STOP_FILE_FORMAT_UNSOPPORTED=       5,
    
    /* The media streaming process is stopped due a stop action by users. This error code describes the situation that the abort operation is directly from an AM Display Device itself, EX: IR Controller Abort */
    AV_RESULT_ERROR_STOP_ABORTED=                       6,
    
    /* The link from URL divert is is not valid. (Cannot retrieve data or link is broken) */
    AV_RESULT_ERROR_URL_DIVERT_LINK_ERROR=              7
};

#endif
