//
//  AMDataTransmitter+GenericDevice.h
//  AMCommon_iOS
//
//  Created by brianliu on 2015/8/13.
//  Copyright (c) 2015年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AMDataTransmitter_SendKey <NSObject>
/**
 Send key command to the device.
 Supported key are:
 
 - AMRemoteHostStandardKeyCodeUp: Key code defines up key.
 - AMRemoteHostStandardKeyCodeDown: Key code defines down key.
 - AMRemoteHostStandardKeyCodeLeft: Key code defines left key.
 - AMRemoteHostStandardKeyCodeRight: Key code defines right key.
 - AMRemoteHostStandardKeyCodeEnter: Key code defines enter key.
 - AMRemoteHostStandardKeyCodeEscape: Key code defines escape key.
 @param keyCode Predifined key code which are supported by default.
 */
- (BOOL)sendKey:(AMRemoteHostStandardKeyCode)keyCode;

/**
 *  Send a JSON-RPC object from App to the remote devive.
 */
- (void) sendJSONRPCOBject:(AMJSONRPCObject*)jrpcObject;

/**
 *  Send a JRON-RPC message from App to the remote devive.
 */
- (void) sendJSONRPCMessage:(NSString*)jrpcMsg;

/**
 Send vendor specific key command to the device.
 @param keyCode Vendor specific key code.
 */
- (void)sendVendorKey:(NSInteger)keyCode;
/**
 Resigster callback when receive message from the device.
 @param listener A AMRemoteHostMessageListener.
 */
- (void)addMessageListener:(id<AMRemoteHostMessageListener>)listener;
/**
 Unresigster callback when receive message from the device.
 @param listener A AMRemoteHostMessageListener.
 */
- (void)removeMessageListener:(id<AMRemoteHostMessageListener>)listener;
/**
 Dispatch the message from a remote device to all listeners.
 @param message Message to be dispatched.
 @param success Success block.
 @param failure Failure block.
 */
- (void)dispatchMessage:(NSString *)message success:(void (^)(void))success failure:(void (^)(NSError *))failure;


@end
