//
//  AMDataTransmitter.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/19.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMDataTransmitter.h"
#import "AMDataTransmitter_private.h"
#import "AMDataTransmitter_SendKey.h"

NSString * const DataTransmitterMedisStreamingMetadataKey_CoverImage = @"DataTransmitterMedisStreamingMetadataKey_CoverImage";


@implementation AMDataTransmitter


-(instancetype) init{
    self = [super init];
    if(self){
        self.state = [StateWIFIDisplayOff new];
        self.state.tramsmitter = self;
    }
    return self;
}

#pragma mark - AMDataTransmitterProtocol

-(void)connectToRemoteDevice:(AMPrototypeDevice *)remoteDevice success:(void (^)(AMPrototypeDevice *))successBlock failure:(void (^)(NSError *, AMPrototypeDevice *))failureBlock{
    DLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
}

-(void) startWifi:(void(^)(void))success
          failure:(void(^)(NSError * error))failure
       disconnect:(void(^)(AMPrototypeDevice * disconnectedDevice))disconnect{
    [self.state startWifi:success failure:failure disconnect:disconnect];
}

-(void) _startWifi:(void(^)(void))success
          failure:(void(^)(NSError * error))failure
       disconnect:(void(^)(AMPrototypeDevice * disconnectedDevice))disconnect{
    DLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
}

-(void) cancelRequest{
    DLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
}

-(void) stopWifiDisplay{
    [self.state stopWifiDisplay];
}
/**
 Disconnect from the connected device if there is any.
 */
-(void) _stopWifiDisplay{
    DLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
}

-(void) sendImageWithData:(NSData*) imageData{
    [self.state sendImageWithData:imageData];
}
-(void) _sendImageWithData:(NSData*) imageData{
    DLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
}

-(void) sendH264Data:(NSData *)data width:(uint32_t)width height:(uint32_t)height{
    [self.state sendH264Data:data width:width height:height];
}
-(void) _sendH264Data:(NSData *)data width:(uint32_t)width height:(uint32_t)height{
    DLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
}

-(void) sendPCMData:(NSData*) pcmData{
    [self.state sendPCMData:pcmData];
}
-(void) _sendPCMData:(NSData*) pcmData{
    DLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
}

/**
 Play media. (Without userInfo)
 */
//-(void) playMediaWithURL:(NSURL*) mediaURL
//                 isVideo:(BOOL) isVideo
//             subtitleURL:(NSURL *) subtitleURL
//                 success:(void(^)(NSURL * mediaURL))successBlock
//                 failure:(void(^)(NSError * error, NSURL * mediaURL))failureblock
//               totalTime:(void(^)(NSInteger totalTime))totalTimeBlock
//              elapseTime:(void(^)(NSInteger elapse))elapseTimeBlock
//                     end:(void(^)(NSURL * mediaURL))endBlock{
//    [self playMediaWithURL:mediaURL isVideo:isVideo subtitleURL:subtitleURL success:successBlock failure:failureblock totalTime:totalTimeBlock elapseTime:elapseTimeBlock end:endBlock userInfo:nil];
//}

//-(void) playMediaWithURL:(NSURL*) mediaURL
//                 isVideo:(BOOL) isVideo
//             subtitleURL:(NSURL *) subtitleURL
//                 success:(void(^)(NSURL * mediaURL))successBlock
//                 failure:(void(^)(NSError * error, NSURL * mediaURL))failureblock
//               totalTime:(void(^)(NSInteger totalTime))totalTimeBlock
//              elapseTime:(void(^)(NSInteger elapse))elapseTimeBlock
//                     end:(void(^)(NSURL * mediaURL))endBlock
//                userInfo:(id)userInfo DEPRECATED_ATTRIBUTE{
//    
//    self.mediaSteamingStart = successBlock;
//    self.mediaSteamingEnd = ^(NSURL * mediaURL, NSError * error){
//        if(error){
//            if(failureblock){
//                failureblock(error, mediaURL);
//            }
//        }
//        if(endBlock){
//            endBlock(mediaURL);
//        }
//    };
//    self.mediaSteamingTotalTime = totalTimeBlock;
//    self.mediaSteamingElapseTime = elapseTimeBlock;
//    
//    if(isVideo){
//        [self playVideoMedia:mediaURL subtitle:subtitleURL userInfo:userInfo];
//    }
//    else{
//        [self playAudioMeida:mediaURL userInfo:userInfo];
//    }
//}


-(void) pausePlayingMedia:(void (^)(void))pauseBlock DEPRECATED_ATTRIBUTE{
    self.mediaSteamingPause = pauseBlock;
    [self pausePlayingMedia];
}

-(void) resumePlayingMedia:(void (^)(void))resumeBlock DEPRECATED_ATTRIBUTE{
    self.mediaSteamingResume = resumeBlock;
    [self resumePlayingMedia];
}

-(void) setVolumeUp:(BOOL)volumeUp notifyBlock:(void(^)(float newVolume))volumeChangedBlock DEPRECATED_ATTRIBUTE{
    self.volumeChange = volumeChangedBlock;
    if(volumeUp){
        [self volumeUp];
    }
    else{
        [self volumeDown];
    }
}

-(void) stopPlayingMedia: (MediaStreamingEnd)endBlock {
    [self.state stopPlayingMedia:endBlock];
}

// for backward compatibility
-(void) stopPlayingMedia {
    [self.state stopPlayingMedia:nil];
}

-(void) _stopPlayingMedia:(MediaStreamingEnd)endBlock{
    DLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
    if (endBlock) {
        endBlock(nil, nil);
    }
}

-(void) playVideoMedia:(NSURL *)mediaURL subtitle:(NSURL *)subURL{
    [self playVideoMedia:mediaURL subtitle:subURL userInfo:nil];
}

-(void) _playVideoMedia:(NSURL *)mediaURL subtitle:(NSURL *)subURL userInfo:(id)userInfo{
    DLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
}
-(void) playVideoMedia:(NSURL *)mediaURL subtitle:(NSURL *)subURL userInfo:(id)userInfo{
    [self.state playVideoMedia:mediaURL subtitle:subURL userInfo:userInfo];
}

-(void) playAudioMeida:(NSURL *)mediaURL{
    [self playAudioMeida:mediaURL userInfo:nil];
}

-(void) playAudioMeida:(NSURL *)mediaURL userInfo:(id)userInfo{
    [self.state playAudioMeida:mediaURL userInfo:userInfo];
}

-(void) _playAudioMeida:(NSURL *)mediaUR userInfo:(id)userInfo{
    DLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
}

-(void) pausePlayingMedia{
    [self.state pausePlayingMedia];
}
-(void) _pausePlayingMedia{
     DLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
}

-(void) resumePlayingMedia{
    [self.state resumePlayingMedia];
}
-(void) _resumePlayingMedia{
     DLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
}
-(void) volumeUp{
     DLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
}
-(void) volumeDown{
     DLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
}
-(float) volume{
     DLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
    return -1;
}

-(void)removeAllMediaStreamingCallbackBlocks{
    self.mediaSteamingElapseTime = nil;
    self.mediaSteamingEnd = nil;
    self.mediaSteamingPause = nil;
    self.mediaSteamingResume = nil;
    self.mediaSteamingStart = nil;
    self.mediaSteamingTotalTime = nil;
    self.mediaStreamingMetadata = nil;
}

-(BOOL) isVirtualTransmitter{
    return NO;
}

#pragma mark - 
-(void) disconnectRemoteDevice{
    DLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
}

-(void) seekToTime:(NSTimeInterval)seekTime{
    [self.state seekToTime:seekTime];
}
-(void) _seekToTime:(NSTimeInterval)seekTime{
    DLog(@"subclass should implement this method:%s", __PRETTY_FUNCTION__);
}

//-(NSString*) role{
//    return [_role copy];
//}

#pragma mark -
-(void) setState:(AMDataTransmitterStateObject *)state{
    if( [NSStringFromClass([_state class]) isEqualToString:NSStringFromClass([state class])] ==NO && _state!=state){
        DLog(@"transmitter(%@) state changed:[%@] -> [%@]", self.connectedDevice.displayName, NSStringFromClass([_state class]), NSStringFromClass([state class]));
        [self willChangeValueForKey:@"currentState"];
        _state = state;
        [self didChangeValueForKey:@"currentState"];

    }
}
-(AMDataTransmitterState) currentState{
    return self.state.state;
}

#pragma mark - Capability
- (AMDataTransmitter <AMDataTransmitter_SendKey>*) keySender{
    return [self conformsToProtocol:@protocol(AMDataTransmitter_SendKey)]? (AMDataTransmitter<AMDataTransmitter_SendKey>*)self:nil;
}

- (AMDataTransmitter <AMDataTransmitter_SplitScreen>*) splitScreenController{
    return [self conformsToProtocol:@protocol(AMDataTransmitter_SplitScreen)]? (AMDataTransmitter<AMDataTransmitter_SplitScreen>*)self:nil;
}

- (AMDataTransmitter <AMDataTransmitter_ConferenceControl>*) conferenceController{
    return [self conformsToProtocol:@protocol(AMDataTransmitter_ConferenceControl)]? (AMDataTransmitter<AMDataTransmitter_ConferenceControl>*)self:nil;
}

- (AMDataTransmitter <AMDataTransmitter_ChromecastTextMsg>*) chromecastTextMsg{
    return [self conformsToProtocol:@protocol(AMDataTransmitter_ChromecastTextMsg)]? (AMDataTransmitter<AMDataTransmitter_ChromecastTextMsg>*)self:nil;
}

-(AMDataTransmitter<AMGenericDevice_HIDEventController>*) HIDEventController{
    return [self conformsToProtocol:@protocol(AMGenericDevice_HIDEventController)]? (AMDataTransmitter<AMGenericDevice_HIDEventController>*)self:nil;
}

-(AMDataTransmitter<AMDataTransmitter_HTTPProxy>*) HTTPProxyConfigurator{
    return [self conformsToProtocol:@protocol(AMDataTransmitter_HTTPProxy)]? (AMDataTransmitter<AMDataTransmitter_HTTPProxy>*)self:nil;
}

-(AMDataTransmitter<AMDataTransmitter_SocketStatus>*) socketStatus{
        return [self conformsToProtocol:@protocol(AMDataTransmitter_SocketStatus)]? (AMDataTransmitter<AMDataTransmitter_SocketStatus>*)self:nil;
}

#pragma mark -
-(BOOL) isWIFIDisplayOn{
    if( (self.currentState & kTransmitterStateWIFIDisplayOff)==kTransmitterStateWIFIDisplayOff )
        return NO;
    return YES;
}

-(BOOL)isMediaStreaming{
    if( (self.currentState & kTransmitterStateMediaStreamingPreraring)==kTransmitterStateMediaStreamingPreraring
       ||  (self.currentState & kTransmitterStateMediaStreamingPaused)==kTransmitterStateMediaStreamingPaused
       ||  (self.currentState & kTransmitterStateMediaStreamingPlaying)==kTransmitterStateMediaStreamingPlaying )
        return YES;
    return NO;
}

-(BOOL)isMediaStreamingPreparing{
    if( (self.currentState & kTransmitterStateMediaStreamingPreraring)==kTransmitterStateMediaStreamingPreraring )
        return YES;
    return NO;
}

-(BOOL)isMediaStreamingPlaying{
    if( (self.currentState & kTransmitterStateMediaStreamingPlaying)==kTransmitterStateMediaStreamingPlaying )
        return YES;
    return NO;
}

-(BOOL)isMediaStreamingPause{
    if( (self.currentState & kTransmitterStateMediaStreamingPaused)==kTransmitterStateMediaStreamingPaused )
        return YES;
    return NO;
}
@end
