//
//  AMDataTransmitter_SocketStatus.h
//  Pods
//
//  Created by brianliu on 2017/3/16.
//
//

#ifndef AMDataTransmitter_SocketStatus_h
#define AMDataTransmitter_SocketStatus_h

@protocol AMDataTransmitter_SocketStatus <NSObject>
//Tells the caller if it a transmitter is ready to send H264 data
-(BOOL)readyToSendH264;

-(BOOL)readyToSendPCMData;

@end

#endif /* AMDataTransmitter_SocketStatus_h */
