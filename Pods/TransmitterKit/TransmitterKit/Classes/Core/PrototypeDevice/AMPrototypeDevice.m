//
//  AMPrototypeDevice.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/20.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMPrototypeDevice.h"
#import "AMPrototypeDevice+_internal.h"
NSString * const kAMPrototypeDeviceID = @"deviceid";
@implementation AMPrototypeDevice

-(instancetype)init{
    self = [super init];
    return self;
}

-(void) encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:_ipAddress forKey:@"ipaddress"];
    [aCoder encodeObject:_vendor forKey:@"vendor"];
    [aCoder encodeObject:_hostName forKey:@"hostname"];
    [aCoder encodeObject:_srcvers forKey:@"srcvers"];
    [aCoder encodeObject:_parameters forKey:@"parameters"];
}

-(instancetype) initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if(self){
        _ipAddress = [[aDecoder decodeObjectForKey:@"ipaddress"] copy];
        _vendor = [[aDecoder decodeObjectForKey:@"vendor"] copy];
        _hostName = [[aDecoder decodeObjectForKey:@"hostname"] copy];
        _srcvers = [[aDecoder decodeObjectForKey:@"srcvers"] copy];
        _parameters = [[aDecoder decodeObjectForKey:@"parameters"] copy];
    }
    return self;
}

-(id) objectForKey:(NSString *)key{
    return  (self.parameters)[key];
}

-(unsigned) serviceBitMask{

    unsigned serviceMask = 0;
    NSString * platform = @"ezcast.service.ios"; //prevent nil string
#if !TARGET_OS_IPHONE
    platform = @"ezcast.service.mac";
#else
    platform = @"ezcast.service.ios";
#endif
    NSString * servicesString = [self objectForKey:platform];
    if(servicesString.length>0){
        NSScanner *scanner = [NSScanner scannerWithString:servicesString];
        [scanner scanHexInt:&serviceMask];
    }
    return serviceMask;
}

+(NSArray *) supportedAudioFormat{
    return nil;
}

+(NSArray *) supportedVideoFormat{
    return nil;
}

+(NSArray *) supportedSubtitleFormat{
    return nil;
}
-(BOOL) isEqual:(id)object{
    NSAssert(NO, @"subclass should implement isEqual:");
    return NO;
}

-(void) setParameters:(NSDictionary *)parameters{
    if(_parameters != parameters){
        _parameters = parameters;
    }
}

-(NSDictionary*)parameters{
    return _parameters;
}

-(void) setBitmaskDictionary:(NSDictionary *)bitmaskDictionary{
    if(_bitmaskDictionary != bitmaskDictionary){
        _bitmaskDictionary = bitmaskDictionary;
    }
}
-(NSDictionary*)bitmaskDictionary{
    return _bitmaskDictionary;
}


- (void)getAppID:(void (^)(NSString * appID ,NSError * error))completionBlock{
    NSString * uuid = nil;
#if TARGET_OS_IPHONE
    uuid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
#else
    uuid = [[NSUUID UUID] UUIDString];
#endif
    if(completionBlock){
        completionBlock(uuid, nil);
    }
}

+(NSString *) vendorStringFromPlist{
    NSString * vendor = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AMVendorString"];
    if(vendor== nil || [vendor stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        NSAssert(NO, @"Brian & Splenden: I mean really, what do you think that operation is supposed to mean with a nil VENDOR?");
    }
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AMVendorString"];
}
@end
