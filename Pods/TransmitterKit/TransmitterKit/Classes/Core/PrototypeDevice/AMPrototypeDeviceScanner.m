//
//  AMPrototypeDeviceScanner.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/19.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMPrototypeDeviceScanner.h"

@implementation AMPrototypeDeviceScanner
-(instancetype) initWithScanDuration:(NSTimeInterval)timeInterval delegate:(id<AMPrototypeDeviceScannerDelegate>)delegate{
    self = [super init];
    if(self){
        _duration = timeInterval;
        _isScanning = NO;
        _delegate= delegate;
    }
    return self;
}
@end
