//
//  AMPrototypeDeviceDiscover.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/18.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol AMPrototypeDeviceScannerDelegate;
@protocol AMPrototypeDeviceScannerProtocol <NSObject>
@optional
-(nullable instancetype) initWithScanDuration:(NSTimeInterval) timeInterval delegate:(id<AMPrototypeDeviceScannerDelegate> _Nullable) delegate;
-(void) startScan;
-(void) startScanAddresses:(NSArray * _Nullable)addresses;
-(void) stopScan;
@end
@class AMPrototypeDevice;
@protocol AMPrototypeDeviceScannerDelegate <NSObject>
@optional
-(void) scanner:(id<AMPrototypeDeviceScannerProtocol> _Nonnull)scanner discoveriedDevice:(AMPrototypeDevice*  _Nonnull)newDevice;
-(void) scanner:(id<AMPrototypeDeviceScannerProtocol> _Nonnull)scanner disappearedDevice:(AMPrototypeDevice*  _Nonnull)removedDevice;
-(void) scanner:(id<AMPrototypeDeviceScannerProtocol> _Nonnull)scanner failToScan:(NSError* _Nullable)error;
-(void) scanner:(id<AMPrototypeDeviceScannerProtocol> _Nonnull)scanner updatedDevice:(AMPrototypeDevice* _Nonnull)updatedDevice;
@end

@interface AMPrototypeDeviceScanner : NSObject <AMPrototypeDeviceScannerProtocol>
{
    BOOL _isScanning;
    NSTimeInterval _duration;
}
@property (nonatomic, readonly) BOOL        isScanning;
@property (nonatomic, weak, nullable) id <AMPrototypeDeviceScannerDelegate> delegate;
@property (nonatomic, strong, nullable) NSPredicate * predicate;
@end
