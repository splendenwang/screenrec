//
//  AMPrototypeDevice+_internal.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/11/6.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMProjectorDevice.h"

@interface AMPrototypeDevice (_internal)

/**
 A dictionary that contains all keys and values which came from a broadcast message.
 */
@property (nonatomic, strong) NSDictionary * parameters;

/**
 Get the value from "parameter".
 */
-(id) objectForKey:(NSString*) key;

/**
 The service bitmask from UDP broadcast. (old protcol defined before 2014/5/5)
 
 @note Old protocol, this bitmask coomes from UDP broadcast.
 */
@property (NS_NONATOMIC_IOSONLY, readonly) unsigned int serviceBitMask;

/**
 Service bitmask from UDP broadcast. (NEW protcol defined before 2014/5/5)
 
 @note New protocol, this dictionary comes from JSON-RPC query command.
 @see https://office.iezvu.com/tiki/tiki-index.php?page=EZCast+Pro&highlight=ezcast%20pro
 */
@property (nonatomic, strong) NSDictionary * bitmaskDictionary;

- (void)getAppID:(void (^)(NSString * appID ,NSError * error))completionBlock;

+(NSString *) vendorStringFromPlist;
@end
