//
//  AMPrototypeDevice.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/18.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#if TARGET_OS_IPHONE
@import UIKit;
#endif
extern NSString * const kAMPrototypeDeviceID;   //the MAC address of device, this is a required field
@protocol AMPrototypeImageStreamingProtocol <NSObject>
@required
@property (nonatomic, getter=getMaxResolution) CGSize maxResolution;
/**
 *  get Max resolution for Mirror/Extension, prevent out of range
 */
//-(CGSize) getMaxResolution;
@optional
@end
/**
 EZCastApp-compatible device.
 */
@interface AMPrototypeDevice : NSObject <NSCoding>
{
    NSDictionary * _parameters;
    
    NSDictionary * _bitmaskDictionary;
}
-(instancetype) init NS_DESIGNATED_INITIALIZER;
-(instancetype) initWithCoder:(NSCoder *)aDecoder NS_DESIGNATED_INITIALIZER;

@property (nonatomic, copy) NSString * ipAddress;

/**
 *  @brief Generic display name of a device
 *
 *  Generic display name of a device
 *  @note This is a derived property.
 */
@property (nonatomic, readonly) NSString * displayName;

#pragma mark RemoteHost

/**
 The vendor string of the device.
 */
@property (nonatomic, strong) NSString * vendor;

/**
 The hostname of the remote host machine.
 */
@property (nonatomic, strong) NSString * hostName;

/**
 The SVN number of the remote host machine.
 */
@property (nonatomic, strong) NSString * srcvers;

/**
 Class method describes video formats support by this device class.
 */
+(NSArray *) supportedVideoFormat;

/**
 Class method describes audio formats support by this device class.
 */
+(NSArray *) supportedAudioFormat;

/**
 Class method describes subtitle formats support by this device class.
 */
+(NSArray *) supportedSubtitleFormat;
@end
