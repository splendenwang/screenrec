//
//  AMDeviceController+_internal.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/11/3.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//


#import "AMDeviceController.h"

@interface AMDeviceController (_internal)
#pragma mark - Class Mapping and Instance Getter. AMDataTransmitter, AMDeviceController.
-(void) registerScanner:(Class)scannerClass TransmitterClass:(Class)transmitterClass forDeviceClass:(Class)deviceClass;

-(void) registerScanner:(Class)scannerClass scannerPredicate:(NSPredicate*)predicate TransmitterClass:(Class)transmitterClass forDeviceClass:(Class)deviceClass ;

-(void) unregisterScannerDeviceClass:(Class)deviceClass;

/**
 Unregister all.
 @note This will also clear all discoveried devices property.
 */
-(void) unregisterAllScannerForAllDevices;

/**
 Registers following devices: EZCast, EZCastPro, EZCastScreen, EZCastDemo device.
 */
-(void) registerAllDeviceTypes;

@end
