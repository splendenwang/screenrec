//
//  AMDeviceController.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/17.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMDeviceController.h"
#import "AMPicoRequest.h"
#import "AMProjectorDevice.h"
#import "AMPrototypeDeviceScanner.h"
#import "AMGenericDeviceScanner.h"
#import "AMGenericDevice.h"
#import "AMDataTransmitter.h"
#import "EZCastApplicationFeatures.h"
#import "AMDeviceController+_internal.h"

#import "AMGenericDevice.h"
#import "AMGenericDeviceDataTransmitter.h"
#import "AMGenericDeviceScanner.h"
#import "AMScreenDevice.h"
#import "AMScreenDataTransmitter.h"
#import "AMScreenDeviceScanner.h"
#import "AMPrototypeDevice+_internal.h"

#import "AMDemoDevice.h"
#import "AMDemoDeviceScanner.h"
#import "AMDemoDataTransmitter.h"
#import "EZCastApplicationFeatures.h"

@interface AMDeviceController ()
@property (nonatomic, strong) AMDataTransmitter * transmitter;
@end


@implementation AMDeviceController
{
    NSMutableDictionary * _scanners;
//    NSMutableDictionary * _transmitters;
    
    NSMutableDictionary * _discoveredDevices;
    
    /**
     _Mapping Dictionary
     Key:   a device class
     Value: a scanner class
     */
    NSMutableDictionary * _scannerClassMappingDict;
    
    NSMutableDictionary * _scannerPredicateClassMappingDict;
    
    
    /**
     _Mapping Dictionary
     Key:   a device class
     Value: a transmitter class
     */
    NSMutableDictionary * _transmitterClassMappingDict;
    
    BOOL _scanning;
    
    NSMutableSet * _listeners;
    
    AMDataTransmitter * _workingTransmitter;
}

+(AMDeviceController*) sharedDeviceController{
    static AMDeviceController *_sharedController = nil;
    static dispatch_once_t onceTokenDeviceController;
    dispatch_once(&onceTokenDeviceController, ^{
        _sharedController = [[[self class] alloc] init];
    });
    return _sharedController;
}

-(instancetype) init{
    self = [super init];
    if(self){
        _scanners = [NSMutableDictionary new];
//        _transmitters = [NSMutableDictionary new];
        _discoveredDevices = [NSMutableDictionary new];
        _scannerClassMappingDict = [NSMutableDictionary new];
        _scannerPredicateClassMappingDict = [NSMutableDictionary new];
        _transmitterClassMappingDict = [NSMutableDictionary new];
        _scanning = NO;
        _listeners = [NSMutableSet new];
    }
    return self;
}

- (void)logFromFunction:(const char *)function message:(NSString *)format{
    DLog(@"google msg:%@", format);
}

-(AMPrototypeDeviceScanner *) scannerForClass:(Class) scannerClass{
    AMPrototypeDeviceScanner * returnObj = _scanners[NSStringFromClass(scannerClass)];
    if(returnObj==nil){
        returnObj = [[scannerClass alloc] initWithScanDuration:0.5 delegate:self];
        returnObj.predicate = _scannerPredicateClassMappingDict[NSStringFromClass(scannerClass)];
        _scanners[NSStringFromClass(scannerClass)] = returnObj;
    }
    return _scanners[NSStringFromClass(scannerClass)];
}

-(void) startScan{
//    DTrace();
    @synchronized(self){
        if(_scanning==NO){
            _scanning = YES;
            [_discoveredDevices removeAllObjects];
            //create scanners which have not been created
            for (NSString * classString in [_scannerClassMappingDict allValues]) {
                [[self scannerForClass:NSClassFromString(classString)] startScan];
            }
        }
    }

}

-(void) startScanAddresses:(NSArray *)addresses
{
    @synchronized(self){
        if(_scanning==NO){
            _scanning = YES;
            [_discoveredDevices removeAllObjects];
            //create scanners which have not been created
            for (NSString * classString in [_scannerClassMappingDict allValues]) {
                AMPrototypeDeviceScanner * scanner = [self scannerForClass:NSClassFromString(classString)];
                if([scanner respondsToSelector:@selector(startScanAddresses:)]){
                    [scanner startScanAddresses:addresses];
                } else {
                    [scanner startScan];
                }
            }
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:1.0]];
        }
    }
}

-(void) stopScan{
//    DTrace();
    @synchronized(self){
        for (AMPrototypeDeviceScanner * scanner in [_scanners allValues]) {
            [scanner stopScan];
        }
        _scanning = NO;
    }

}

-(NSArray*) discoveredDevices{
    return [[_discoveredDevices allValues] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        AMPrototypeDevice * device1 = obj1;
        AMPrototypeDevice * device2 = obj2;
        return [device1.ipAddress compare:device2.ipAddress];
    }];
}

-(void) dealloc{
    [self unregisterAllScannerForAllDevices];   //trigger to unregister observer
}


#pragma mark - AMPrototypeDeviceScannerDelegate
-(void) scanner:(id<AMPrototypeDeviceScannerProtocol>)scanner discoveriedDevice:(AMPrototypeDevice *)newDevice{
//    DTrace();
    _discoveredDevices[[newDevice ipAddress]] = newDevice;
    if([self.delegate respondsToSelector:@selector(deviceController:foundNewDevice:)])
    {
        [self.delegate deviceController:self foundNewDevice:newDevice];
    }
}
-(void) scanner:(id<AMPrototypeDeviceScannerProtocol>)scanner disappearedDevice:(AMPrototypeDevice *)removedDevice{
    AMPrototypeDevice * targetDevice = nil;
    for (AMPrototypeDevice * device in [_discoveredDevices allValues]) {
        if ([[device ipAddress] isEqualToString:[removedDevice ipAddress]]) {
            targetDevice = device;
            break;
        }
    }
    if(targetDevice)
    {
        [_discoveredDevices removeObjectForKey:[targetDevice ipAddress]];
    }
    
    if([self.delegate respondsToSelector:@selector(deviceController:deviceDisappeared:)]){
        [self.delegate deviceController:self deviceDisappeared:targetDevice];
    }

}

-(void) scanner:(id<AMPrototypeDeviceScannerProtocol>)scanner failToScan:(NSError *)error{
    DLog(@"%s, error:%@", __PRETTY_FUNCTION__, error);
//#if TARGET_OS_IPHONE
//    
//    //dont show bonjour error code(caused by entering background)
//    if([error.domain isEqualToString:NSNetServicesErrorDomain]){
//        return;
//    }
//    
////    dispatch_async(dispatch_get_main_queue(), ^{
////        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTable(@"Error", @"AMCommon", @"Error")
////                                                         message:error.localizedDescription
////                                                        delegate:nil
////                                               cancelButtonTitle:@"OK"
////                                               otherButtonTitles:nil
////                               ];
////        [alert show];
////    });
//#endif
}

#pragma mark - AMDataTransmitter Control
-(void) registerScanner:(Class)scannerClass scannerPredicate:(NSPredicate*)predicate TransmitterClass:(Class)transmitterClass forDeviceClass:(Class)deviceClass{
    _scannerClassMappingDict[NSStringFromClass(deviceClass)] = NSStringFromClass(scannerClass);
    _transmitterClassMappingDict[NSStringFromClass(deviceClass)] = NSStringFromClass(transmitterClass);
    if(predicate){
        _scannerPredicateClassMappingDict[NSStringFromClass(scannerClass)] = predicate;
    }
}

-(void) registerScanner:(Class)scannerClass TransmitterClass:(Class)transmitterClass forDeviceClass:(Class)deviceClass{
    [self registerScanner:scannerClass scannerPredicate:nil TransmitterClass:transmitterClass forDeviceClass:deviceClass];
}

-(void) unregisterScannerDeviceClass:(Class)deviceClass{
    NSArray * keys = _discoveredDevices.allKeys;
    for(int i=0; i<keys.count; i++){
        if([_discoveredDevices[keys[i]] isKindOfClass:deviceClass]){
            [_discoveredDevices removeObjectForKey:keys[i]];
        }
    }
    [_scanners removeObjectForKey:NSStringFromClass(deviceClass)];
    [_scannerPredicateClassMappingDict removeObjectForKey:_scannerClassMappingDict[NSStringFromClass(deviceClass)]];
    [_scannerClassMappingDict removeObjectForKey:NSStringFromClass(deviceClass)];
    ;
//    [_transmitters removeObjectForKey:_transmitterClassMappingDict[NSStringFromClass(deviceClass)]];
    [_transmitterClassMappingDict removeObjectForKey:NSStringFromClass(deviceClass)];
}


-(void) unregisterAllScannerForAllDevices{
    [self stopScan];
    [_discoveredDevices removeAllObjects];
    [_scanners removeAllObjects];
    [_scannerClassMappingDict removeAllObjects];
//    [_transmitters removeAllObjects];
    [_transmitterClassMappingDict removeAllObjects];
    [_scannerPredicateClassMappingDict removeAllObjects];
}

-(void) registerAllDeviceTypes{
    
    NSCompoundPredicate * predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[[AMGenericDeviceScanner ezcastPredicate:[AMPrototypeDevice vendorStringFromPlist]],
                                                        [AMGenericDeviceScanner ezcastCarPredicate],
                                                        [AMGenericDeviceScanner ezcastLitePredicate],
                                                        [AMGenericDeviceScanner ezcastMusicPredicate],
                                                        [AMGenericDeviceScanner ezcastProPredicate:[AMPrototypeDevice vendorStringFromPlist]]
                                                        ]];
    
    [[AMDeviceController sharedDeviceController] registerScanner:[AMGenericDeviceScanner class]
                                                scannerPredicate:predicate
                                                TransmitterClass:[AMGenericDeviceDataTransmitter class]
                                                  forDeviceClass:[AMGenericDevice class]];
    
    [[AMDeviceController sharedDeviceController] registerScanner:[AMScreenDeviceScanner class]
                                                TransmitterClass:[AMScreenDataTransmitter class]
                                                  forDeviceClass:[AMScreenDevice class]];

    
    [[AMDeviceController sharedDeviceController] registerScanner:[AMDemoDeviceScanner class] TransmitterClass:[AMDemoDataTransmitter class] forDeviceClass:[AMDemoDevice class]];
}

- (AMDataTransmitter *) createNewTransmitterForDevive:(AMPrototypeDevice*)device{
    NSString * transmitterClassString = _transmitterClassMappingDict[NSStringFromClass([device class])];
    AMDataTransmitter * aTransmitter = [[NSClassFromString(transmitterClassString) alloc] init];
    return aTransmitter;
}

-(BOOL) isDeviceConnected{
    return self.connectedDevice!=nil;
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if([keyPath isEqualToString:@"currentState"]){
        if(self.connectedDevice){  //check if it is the current
            for (NSValue *listenerValue in _listeners) {
                id<AMDeviceControllerListener> listener = [listenerValue nonretainedObjectValue];
                if([listener respondsToSelector:@selector(deviceStatusDidChange:device:fromState:toState:)]){
                    AMDataTransmitterState old = ((NSNumber*)change[NSKeyValueChangeOldKey]).integerValue;
                    AMDataTransmitterState new = ((NSNumber*)change[NSKeyValueChangeNewKey]).integerValue;
                    
                    /**
                     Dispatch to async queue to notify listeners. Preventing a listenser creating(or destroy) a new AMBaseViewController which would addDeviceEventListener/removeDeviceEvenetListener and causing changing _listeners while enumerating.
                     */
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [listener deviceStatusDidChange:self device:self.connectedDevice fromState:old toState:new];
                    });
                    
                }
            }
        }
    }
}

-(void) addDeviceEventListener:(id<AMDeviceControllerListener>)object{
    [_listeners addObject:[NSValue valueWithNonretainedObject:object]];
}
-(void) removeDeviceEventListener:(id<AMDeviceControllerListener>)object{
    [_listeners removeObject:[NSValue valueWithNonretainedObject:object]];
}


#pragma mark - Connecting and WifiDisplay

- (AMDeviceController*__nonnull)connect:(AMPrototypeDevice* __nonnull)newDevice{
    NSAssert(newDevice != nil, @"connect: should receive a nonnull newDevice object");
    
    if(_workingTransmitter){
        DLog(@"Previous connection attempt is still running, return directly");
        return self;
    }
    
    //get a already-connected transmitter if there is any
    AMDataTransmitter * oldTransmitter = self.transmitter;
    
    if([oldTransmitter.connectedDevice isEqual:newDevice]){
        //execute the connection callback
        if(self.connectionCallback){
            self.connectionCallback(newDevice, nil);
            self.connectionCallback = nil;
        }
        return self;
    }
    
    _workingTransmitter = [self createNewTransmitterForDevive:newDevice];
    
    if(!_workingTransmitter && self.connectionCallback){
        NSError * error = [NSError errorWithDomain:@"AMDeviceControllerErrorDomain" code:-1 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"No transmitter for device: %@, do you forget to register for this kind of device? ", newDevice]}];
        self.connectionCallback(newDevice, error);
        self.connectionCallback = nil;
    }
    
    [_workingTransmitter connectToRemoteDevice:newDevice success:^(AMPrototypeDevice *device) {

        //disconnect previous connected transmitter(also remove observer for currentState)
        if(oldTransmitter){
            @try{
                [self.transmitter removeObserver:self forKeyPath:@"currentState"];
            }@catch(id anException){
                NSLog(@"caught exception:%@", anException);
            }
            [oldTransmitter disconnectRemoteDevice];
        }
        
        //assign new transmitter
        self.transmitter = _workingTransmitter; _workingTransmitter = nil;
        //add obsersever so that I can get state change event ASAP
        [self.transmitter addObserver:self forKeyPath:@"currentState" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:nil];
        
        //execute the connection callback
        if(self.connectionCallback){
            self.connectionCallback(device, nil);
            self.connectionCallback = nil;
        }
        
        //tell listeners about connectedDeviceDidChange
        for (NSValue *listenerValue in _listeners) {
            id<AMDeviceControllerListener> listener = [listenerValue nonretainedObjectValue];
            if([listener respondsToSelector:@selector(connectedDeviceDidChange:transmitter:newConnectedDevice:)]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [listener connectedDeviceDidChange:self transmitter:self.transmitter newConnectedDevice:self.connectedDevice];
                });
                
            }
        }
        
    } failure:^(NSError *error, AMPrototypeDevice *device) {
        if(self.connectionCallback){
            self.connectionCallback(device, error);
            self.connectionCallback = nil;
        }
        _workingTransmitter = nil;
    }];
    return self;
}

- (AMDeviceController*__nonnull)disconnect{
    @try{
        [self.transmitter removeObserver:self forKeyPath:@"currentState"];
    }@catch(id anException){
        DLog(@"caught exception:%@", anException);
    }
    [self.transmitter disconnectRemoteDevice];
    self.transmitter = nil;
    
    for (NSValue *listenerValue in _listeners) {
        id<AMDeviceControllerListener> listener = [listenerValue nonretainedObjectValue];
        if([listener respondsToSelector:@selector(connectedDeviceDidChange:transmitter:newConnectedDevice:)]){
            dispatch_async(dispatch_get_main_queue(), ^{
                [listener connectedDeviceDidChange:self transmitter:nil newConnectedDevice:nil];
            });
            
        }
    }
    return self;
}

- (AMDeviceController*__nonnull)startWifiDisplay{
    __weak AMDeviceController * weakself = self;
    [weakself.transmitter startWifi:^{
        if(weakself.wifiDisplayStartCallback){
            weakself.wifiDisplayStartCallback(nil);
            weakself.wifiDisplayStartCallback = nil;
        }
    } failure:^(NSError *error) {
        if(weakself.wifiDisplayStartCallback){
            weakself.wifiDisplayStartCallback(error);
        } else if(_wifiDisplayConnectionCallback) {
            _wifiDisplayConnectionCallback(error);
        }
        weakself.wifiDisplayStartCallback = nil;
        _wifiDisplayConnectionCallback = nil;
        weakself.wifiDisplayDisconnectCallback = nil;
    } disconnect:^(AMPrototypeDevice *disconnectedDevice) {
        if(weakself.wifiDisplayDisconnectCallback){
            weakself.wifiDisplayDisconnectCallback(nil);
        }
        weakself.wifiDisplayStartCallback = nil;
        _wifiDisplayConnectionCallback = nil;
        weakself.wifiDisplayDisconnectCallback = nil;
    }];
    return self;
}

- (AMDeviceController*__nonnull) stopWifiDisplay{
    [self.transmitter stopPlayingMedia:nil];
    [self.transmitter stopWifiDisplay];
    return self;
}

- (AMPrototypeDevice *)connectedDevice{
    return self.transmitter.connectedDevice;
}


- (BOOL) isWIFIDisplayOn{
    return self.transmitter.isWIFIDisplayOn;
}

@end
