//
//  AMDeviceController.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/17.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMPrototypeDevice.h"
#import "AMPrototypeDeviceScanner.h"
#import "AMDataTransmitterStateObject.h"

@class AMDeviceController;
@protocol AMDeviceControllerDelegate<NSObject>
@optional
- (void) deviceController:(nonnull AMDeviceController*)controllder foundNewDevice:(nonnull AMPrototypeDevice*)device;
- (void) deviceController:(nonnull AMDeviceController*)controllder deviceDisappeared:(nonnull AMPrototypeDevice*)device;
@end

@class AMDataTransmitter;
@protocol AMDPFImageSenderApplicationDelegate;
@protocol AMDPFImageSenderMediaStreamingDelegate;
@protocol AMDeviceControllerListener;
@interface AMDeviceController : NSObject <AMPrototypeDeviceScannerDelegate>

+(nonnull AMDeviceController*) sharedDeviceController;

#pragma mark - Scanning

@property (weak, nullable) id<AMDeviceControllerDelegate> delegate;

/**
 *  Synchronous method. This method will need around 1 sec to go on.
 *  This method will also clear cache of found devices.
 *
 *  scan for broadcast addresses of interfaces 
 */
-(void) startScan;
/**
 *  scan for specific addresses, if scanner class not implementing startScanAddresses: in AMPrototypeDeviceScannerProtocol
 *  it will call startScan instead
 *
 *  @param addresses to broadcast
 */
-(void) startScanAddresses:(NSArray * __nonnull)addresses;

/**
 Stops scanning.
 */
-(void) stopScan;

/**
 Devices that have been already discoveried will remain in this array even if the AMDeviceControll is set a StopScan message.
 */
@property (NS_NONATOMIC_IOSONLY, readonly, nonnull) NSArray<AMPrototypeDevice*> * discoveredDevices;

#pragma mark - Connecting and WifiDisplay

/**
 Connect to a device.
 @param device A device to connect to.
 */
- (AMDeviceController*__nonnull)connect:(AMPrototypeDevice* __nonnull)device;

/**
 Disconnect from currently connected device.
 */
- (AMDeviceController*__nonnull)disconnect;

/**
 Indicates if a device is connected.
 */
@property (nonatomic, readonly) BOOL isDeviceConnected;

/**
 A callback get executed after connect: is called.
 @param device The device connected originally attempt to connect to.
 @param error Tells if there is any error.
 */
@property (strong, nullable) void (^connectionCallback)(AMPrototypeDevice * __nonnull device, NSError * __nullable error);

/**
 Starts wifi display.(There should be a device connected so that this method can work)
 */
- (AMDeviceController*__nonnull)startWifiDisplay;

/**
 Stop wifi display service.
 */
- (AMDeviceController*__nonnull)stopWifiDisplay;

/**
 Call when wifi display starts or fails to start.
 */
@property (strong, nullable) void (^wifiDisplayStartCallback)(NSError * __nullable error);

/**
 *  Deprecated, use wifiDisplayStartCallback and check for error instead.
 Call when wifi display fails in connection.
 */
@property (strong, nullable) void (^wifiDisplayConnectionCallback)(NSError * __nullable error) __attribute__((deprecated("wifiDisplayConnectionCallback is deprecated, use wifiDisplayStartCallback and check for error instead."))) ;

/**
 Call when wifi display changes to off.
 */
@property (strong, nullable) void (^wifiDisplayDisconnectCallback)(NSError * __nullable error);

/**
 Represet the currently active transmitter if there is any. Return nil if there is none.
 */
@property (nonatomic, readonly, nullable) AMDataTransmitter * transmitter;

/**
 A Read-only property indicates the currently connected device.
 */
@property (nonatomic, readonly, nullable) AMPrototypeDevice * connectedDevice;

/**
 Tells If wifi display is currently on.
 */
@property (nonatomic, readonly) BOOL isWIFIDisplayOn;


#pragma mark - Listener

- (void)addDeviceEventListener:(nonnull id<AMDeviceControllerListener>)object;
- (void)removeDeviceEventListener:(nonnull id<AMDeviceControllerListener>)object;
@end

@protocol AMDeviceControllerListener<NSObject>
@optional
- (void)connectedDeviceDidChange:(nonnull AMDeviceController*)deviceController  //self
                     transmitter:(nullable AMDataTransmitter*)transmitter    //selected device
              newConnectedDevice:(nullable AMPrototypeDevice*)device;    //connected device

- (void)deviceStatusDidChange:(nonnull AMDeviceController*)deviceController
                       device:(nonnull AMPrototypeDevice*)device
                    fromState:(AMDataTransmitterState)oldState
                      toState:(AMDataTransmitterState)newState;
@end
