//
//  AMDataTransmitter.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/19.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMDataTransmitterProtocol.h"
#import "AMPrototypeDevice.h"
#import "AMMediaStreamingStandardErrorDefinition.h"
#import "AMDataTransmitterStateObject.h"

extern NSString * const DataTransmitterMedisStreamingMetadataKey_CoverImage;

@interface AMDataTransmitter : NSObject <AMDataTransmitterProtocol>
{
    
    //Connection related blocks
    void(^_connectionSuccessBlock)(void);
    void(^_connectionFailureBlock)(NSError * error);
    void(^_afterDisconnectBlock)(AMPrototypeDevice *);
    
    void(^_splitScreenPauseBlock)(void);
    void(^_splitScreenResumeBlock)(void);

    //Media Streaming
    NSURL * _mediaURL;
}
@property (nonatomic, strong) AMPrototypeDevice * connectedDevice;

@property (nonatomic, strong) MediaStreamingStart mediaSteamingStart;
@property (nonatomic, strong) MediaStreamingEnd mediaSteamingEnd;
@property (nonatomic, strong) MediaTimeNotify mediaSteamingTotalTime;
@property (nonatomic, strong) MediaTimeNotify mediaSteamingElapseTime;
@property (nonatomic, strong) MediaStreamingAction mediaSteamingPause;
@property (nonatomic, strong) MediaStreamingAction mediaSteamingResume;
@property (nonatomic, strong) MediaStreamingActionVolumeChange volumeChange;
/*
 For some type of music, we rely on remote device(ex: demo device for local playback) for decoding music file. This block helps to propergate additional metadata back to app level.
 */
@property (nonatomic, strong) MediaStreamingMetadataCallback mediaStreamingMetadata;

-(void)removeAllMediaStreamingCallbackBlocks;

-(void) _startWifi:(void(^)(void))success
          failure:(void(^)(NSError * error))failure
       disconnect:(void(^)(AMPrototypeDevice * disconnectedDevice))disconnect;
-(void) _stopWifiDisplay;
-(void) _sendImageWithData:(NSData*) imageData;
-(void) _sendH264Data:(NSData*)data width:(uint32_t)width height:(uint32_t)height;
-(void) _sendPCMData:(NSData*) pcmData;

-(void) _playVideoMedia:(NSURL *)mediaURL subtitle:(NSURL *)subURL userInfo:(id)userInfo;
-(void) _playAudioMeida:(NSURL *)mediaURL userInfo:(id)userInfo;
-(void) _pausePlayingMedia;
-(void) _resumePlayingMedia;
/* Please be noted.
 Due to the limitation of remote device protocol(EZCAst dongle, Wifidisplay), implementation of _stopPlayingMedia should not trigger MediaStreamingEnd.
 */
-(void) _stopPlayingMedia:(MediaStreamingEnd)endBlock;
-(void) _seekToTime:(NSTimeInterval)seekTime;
@end

@interface AMDataTransmitter (StateCheck)

@property (readonly) BOOL isWIFIDisplayOn;


//playing, preparing, pause
@property (readonly) BOOL isMediaStreaming;

//preparing
@property (readonly) BOOL isMediaStreamingPreparing;

//playing
@property (readonly) BOOL isMediaStreamingPlaying;

//pause
@property (readonly) BOOL isMediaStreamingPause;

@end