//
//  AMDataTransmitterProtocol.h
//  AMCommon_iOS
//
//  Created by raxcat on 2014/2/18.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMPrototypeDevice.h"
#import "AMMediaStreamingStandardErrorDefinition.h"
#import "AMRemoteHost.h"
#import "AMDataTransmitter_SendKey.h"
#import "AMDataTransmitter_SplitScreen.h"
#import "AMDataTransmitter_ConferenceControl.h"
#import "AMDataTransmitter_ChromecastTextMsg.h"
#import "AMDataTransmitter_OTA.h"
#import "AMDataTransmitter_HIDControl.h"
#import "AMDataTransmitter_HTTPProxy.h"
#import "AMDataTransmitter_SocketStatus.h"

typedef void (^MediaStreamingStart)(NSURL * mediaURL);
typedef void (^MediaStreamingEnd)(NSURL * mediaURL, NSError * error);
typedef void (^MediaTimeNotify)(NSInteger timestamp);
typedef void (^MediaStreamingAction)(void);
typedef void (^MediaStreamingActionVolumeChange)(float newVolume);
typedef void (^MediaStreamingMetadataCallback)(NSDictionary * metadata);

@protocol AMDataTransmitterHostControlDelegate;
@protocol AMDataTransmitterProtocol <NSObject>

#pragma mark - Remote Control

/**
 Connect to a remote device.(host control delegate is set to nil)
 @param device The remote device you needs conect to.
 @param successBlock Called after success.
 @param failureBlock Called after failure.
 */
-(void) connectToRemoteDevice:(AMPrototypeDevice *)device
                      success:(void(^)(AMPrototypeDevice * device))successBlock
                      failure:(void(^)(NSError * error, AMPrototypeDevice * device))failureBlock;

/**
 Connect to a remote device.
 @param remoteDevice The remote device you needs conect to.
 @param successBlock Called after success.
 @param failureBlock Called after failure.
 @param hostControlDelegate A host control delegate.
 @param userInfo Custome info you pass in.
 */
//-(void) connectToRemoteDevice:(AMPrototypeDevice *)remoteDevice
//                      success:(void(^)(AMPrototypeDevice * device))successBlock
//                      failure:(void(^)(NSError * error, AMPrototypeDevice * device))failureBlock
//          hostControlDelegate:(id<AMDataTransmitterHostControlDelegate>) hostControlDelegate
//                     userInfo:(id) userInfo;

-(void) disconnectRemoteDevice;



#pragma mark - Optional capability check

//Default by NO. A virtual data transmitter is not capable of play any media.
@property (nonatomic, readonly) BOOL isVirtualTransmitter;

@property (nonatomic, readonly) AMDataTransmitter <AMDataTransmitter_SendKey> * keySender;

@property (nonatomic, readonly) AMDataTransmitter <AMDataTransmitter_SplitScreen> * splitScreenController;

@property (nonatomic, readonly) AMDataTransmitter <AMDataTransmitter_ConferenceControl> * conferenceController;

@property (nonatomic, readonly) AMDataTransmitter <AMDataTransmitter_ChromecastTextMsg> * chromecastTextMsg;

@property (nonatomic, readonly) AMDataTransmitter <AMGenericDevice_HIDEventController>* HIDEventController;

@property (nonatomic, readonly) AMDataTransmitter <AMDataTransmitter_HTTPProxy> * HTTPProxyConfigurator;

@property (nonatomic, readonly) AMDataTransmitter <AMDataTransmitter_SocketStatus> * socketStatus;

#pragma mark - Wifi Display & Media Streaming

/**
 *  Start WIFI Display service.
 *  @param success Success block.
 *  @param failure Fialure block.
 *  @param disconnect If the socket disconnect without any error, this block will not be execute.
 */
-(void) startWifi:(void(^)(void))success
          failure:(void(^)(NSError * error))failure
       disconnect:(void(^)(AMPrototypeDevice * disconnectedDevice))disconnect;

/**
 Cancel a pending request stream request.
 */
-(void) cancelRequest;

/**
 Disconnect from the connected device if there is any.
 */
-(void) stopWifiDisplay;

-(void) sendImageWithData:(NSData*) imageData;

-(void) sendH264Data:(NSData*)data width:(uint32_t)width height:(uint32_t)height;

-(void) sendPCMData:(NSData*) pcmData;


@property (nonatomic, strong) MediaStreamingStart mediaSteamingStart;
@property (nonatomic, strong) MediaStreamingEnd mediaSteamingEnd;
@property (nonatomic, strong) MediaTimeNotify mediaSteamingTotalTime;
@property (nonatomic, strong) MediaTimeNotify mediaSteamingElapseTime;
@property (nonatomic, strong) MediaStreamingAction mediaSteamingPause;
@property (nonatomic, strong) MediaStreamingAction mediaSteamingResume;
@property (nonatomic, strong) MediaStreamingActionVolumeChange volumeChange;

/* 
Play media by providing a media URL, which can be a file url or a web media resource url.
 After started, "start" block is called. If there is any error, error block is called.
 */
- (void)playVideoMedia:(NSURL*)mediaURL subtitle:(NSURL*)subURL;
- (void)playVideoMedia:(NSURL*)mediaURL subtitle:(NSURL*)subURL userInfo:(id)userInfo;
- (void)playAudioMeida:(NSURL *)mediaURL;
- (void)playAudioMeida:(NSURL *)mediaURL userInfo:(id)userInfo;

/* 
 After stopped, "end" block is called.
 */
- (void)stopPlayingMedia:(MediaStreamingEnd)endBlock;

/* 
 After paused, "pause" block is called. 
 */
-(void)pausePlayingMedia;

/* 
 After resumed, "resume" block is called. 
 */
-(void)resumePlayingMedia;

/* 
 After volume changed, "volumeChange" block is called. 
 */
-(void)volumeUp;

/* 
 After volume changed, "volumeChange" block is called. 
 */
-(void)volumeDown;

/*
 @return -1 This device does not support getting volume value.
 @return Other value represents current valume.
 */
-(float)volume;

/*
 Seek to time.
 */
-(void)seekToTime:(NSTimeInterval) seekTime;

@end
