//
//  AMDataTransmitter_HIDControl.h
//  TransmitterKit
//
//  Created by brianliu on 2016/8/3.
//  Copyright © 2016年 winnerwave. All rights reserved.
//


#ifndef AMDataTransmitter_HIDControl_h
#define AMDataTransmitter_HIDControl_h

@import Foundation;
@import CoreGraphics;
#import "AMGenericDevice.h"

@protocol GenericDeviceHIDEventListener <NSObject>
@optional

#pragma mark Mouse plugged/unplugged
-(void)mousePluggedIn;
-(void)mousePluggedOut;


#pragma mark Mouse events
-(void)mouseMoveDetected:(CGPoint)delta;
-(void)LeftMouseDownDetected;
-(void)LeftMouseUpDetected;
-(void)RightMouseDownDetected;
-(void)RightMouseUpDetected;
-(void)wheelDownDetected;
-(void)wheelUpDetected;
-(void)wheelScrollDetected:(CGPoint)delta;
-(void)leftMouseDragged:(CGPoint)delta;

#pragma mark Convert touch events to mouse event
/**
    @param absPoint IOKit Coordinate System. This point based on second parameter 'resolution'.
    @param resolution This parameter indicates how 'absPoint' is calculated and determined. You should check on this parameter and re-calculate your original point in your screen size. 
    @note if resolution is the same with your current sceeen size, your can use 'absPoint' directly.
 */
-(void)touchEventConvertedToSingleMouseEvent_Down:(CGPoint)absPoint screenBase:(CGSize)resolution;

/**
 @param absPoint IOKit Coordinate System. This point based on second parameter 'resolution'.
 @param resolution This parameter indicates how 'absPoint' is calculated and determined. You should check on this parameter and re-calculate your original point in your screen size.
 @note if resolution is the same with your current sceeen size, your can use 'absPoint' directly.
 */
-(void)touchEventConvertedToSingleMouseEvent_Up:(CGPoint)absPoint screenBase:(CGSize)resolution;

/**
 @param absPoint IOKit Coordinate System. This point based on second parameter 'resolution'.
 @param resolution This parameter indicates how 'absPoint' is calculated and determined. You should check on this parameter and re-calculate your original point in your screen size.
 @note if resolution is the same with your current sceeen size, your can use 'absPoint' directly.
 */
-(void)touchEventConvertedToSingleMouseEvent_Dragged:(CGPoint)absPoint screenBase:(CGSize)resolution;
@end

#pragma mark HID Control
@protocol AMGenericDevice_HIDEventController

/**
 Default is NO. 
 @note Set this value to YES before you call startWifi:
 */
@property (nonatomic, assign, getter=isHIDControlEnable) BOOL enableHIDControl;

/**
 Add an event listener.
 */
-(void)addHIDEventListener:(id<GenericDeviceHIDEventListener>)listener;

/**
 Reomve an event listener.
 */
-(void)removeHIDEventListener:(id<GenericDeviceHIDEventListener>)listener;

@end


#endif /* AMDataTransmitter_HIDControl_h */
