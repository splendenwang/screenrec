//
//  TransmitterState.m
//  AMCommon_iOS
//
//  Created by brianliu on 6/10/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import "AMDataTransmitterStateObject.h"
#import "AMDataTransmitter.h"
#import "AMDataTransmitter_private.h"
#import "AMDataTransmitterProtocol.h"

NSString * const kStateWIFIDisplayPrefix =          @"kStateWIFIDisplay";
NSString * const kStateWIFIDisplayOff =             @"kStateWIFIDisplayOff";
NSString * const kStateWIFIDisplayOn =              @"kStateWIFIDisplayOn";
NSString * const kStateMediaStreamingPrefix =       @"kStateMediaStreaming";
NSString * const kStateMediaStreamingPlaying =      @"kStateMediaStreamingPlaying";
NSString * const kStateMediaStreamingPreraring =    @"kStateMediaStreamingPreraring";
NSString * const kStateMediaStreamingPaused =       @"kStateMediaStreamingPaused";


@implementation AMDataTransmitterStateObject
-(void) startWifi:(void(^)(void))success
          failure:(void(^)(NSError * error))failure
       disconnect:(void(^)(AMPrototypeDevice * disconnectedDevice))disconnect{
    NSAssert(NO, ([NSString stringWithFormat:@"Possibly startWifi  in error state. state:%zd", self.state]));
}
-(void) stopWifiDisplay{
    NSAssert(NO, ([NSString stringWithFormat:@"Possibly stopWifiDisplay in error state. state:%zd", self.state]));
}
-(void) sendImageWithData:(NSData*) imageData{
    NSAssert(NO, ([NSString stringWithFormat:@"Possibly sendImage in error state. state:%zd", self.state]));
}
-(void) sendH264Data:(NSData*)data width:(uint32_t)width height:(uint32_t)height{
    NSAssert(NO, ([NSString stringWithFormat:@"Possibly sendH264Data in error state. state:%zd", self.state]));
}
-(void) sendPCMData:(NSData*) pcmData{
    NSAssert(NO, ([NSString stringWithFormat:@"Possibly sendPCMData in error state. state:%zd", self.state]));
}

-(void) playVideoMedia:(NSURL *)mediaURL subtitle:(NSURL *)subURL userInfo:(id)userInfo{
    NSAssert(NO, ([NSString stringWithFormat:@"Possibly playVideoMedia in error state. state:%zd", self.state]));
}
-(void) playAudioMeida:(NSURL *)mediaURL userInfo:(id)userInfo{
    NSAssert(NO, ([NSString stringWithFormat:@"Possibly playAudioMeida in error state. state:%zd", self.state]));
}
-(void) pausePlayingMedia{}
-(void) resumePlayingMedia{}
-(void) stopPlayingMedia:(MediaStreamingEnd)endBlock{
    if (endBlock) {
        endBlock(nil, nil);
    }
}
-(void) seekToTime:(NSTimeInterval)seekTime{}

-(void) onWIDIDisplayStart{}
-(void) onWIDIDisplayDisconnect{}
-(void) onMediaStreamingStop{}
-(void) onMediaStreamingPlay{}
-(void) onMediaStreamingReject{}
-(AMDataTransmitterState) state{
    NSString * string = [NSString stringWithFormat:@"No implement of state method, class:%@", NSStringFromClass([self class])];
    NSAssert( NO , string);
    return -1;
}
@end


@implementation StateWIFIDisplayOff
-(void)startWifi:(void (^)(void))success
         failure:(void (^)(NSError *))failure
      disconnect:(void (^)(AMPrototypeDevice *))disconnect{
    [self.tramsmitter _startWifi:success failure:failure disconnect:disconnect];
}
-(void) stopWifiDisplay{

}

-(void) sendImageWithData:(NSData *)imageData{
    DLog(@"Not allowed to send image under WIFIDisplayOff");
}

//Specially, due to transmitter can start play media in off state(it will start wifidisplay service if necessary), I would allow this state to play video.
-(void) playVideoMedia:(NSURL *)mediaURL subtitle:(NSURL *)subURL userInfo:(id)userInfo{
    [self.tramsmitter _playVideoMedia:mediaURL subtitle:subURL userInfo:userInfo];
}
-(void) playAudioMeida:(NSURL *)mediaURL userInfo:(id)userInfo{
    [self.tramsmitter _playAudioMeida:mediaURL userInfo:userInfo];
}


-(void) onWIDIDisplayStart{
    StateWIFIDisplayOn * wifion = [StateWIFIDisplayOn new];
    wifion.tramsmitter = self.tramsmitter;
    self.tramsmitter.state = wifion;
}

-(void)onMediaStreamingPlay{
    StateMediaStreamingPlaying * playing = [StateMediaStreamingPlaying new];
    playing.tramsmitter = self.tramsmitter;
    self.tramsmitter.state = playing;
}

-(AMDataTransmitterState) state{
    return kTransmitterStateWIFIDisplayOff;
}

@end


@implementation StateMediaStreamingPlaying
-(void) startWifi:(void (^)(void))success failure:(void (^)(NSError *))failure disconnect:(void (^)(AMPrototypeDevice *))disconnect{
    [self.tramsmitter _startWifi:success failure:failure disconnect:disconnect];
}

-(AMDataTransmitterState) state{
    return kTransmitterStateMediaStreamingPlaying;
}


-(void) sendImageWithData:(NSData*) imageData{
    DLog(@"Playing state will send no image");
}
-(void) sendH264Data:(NSData*)data width:(uint32_t)width height:(uint32_t)height{
    DLog(@"Playing state will send no image");    
}
-(void) sendPCMData:(NSData *)pcmData{
    DLog(@"Playing state will send no sound");
}

-(void) playVideoMedia:(NSURL *)mediaURL subtitle:(NSURL *)subURL userInfo:(id)userInfo{
    [self.tramsmitter _playVideoMedia:mediaURL subtitle:subURL userInfo:userInfo];
}
-(void) playAudioMeida:(NSURL *)mediaURL userInfo:(id)userInfo{
    [self.tramsmitter _playAudioMeida:mediaURL userInfo:userInfo];
}
-(void) pausePlayingMedia{
    [self.tramsmitter _pausePlayingMedia];
    AMDataTransmitterStateObject * state = [StateMediaStreamingPaused new];
    state.tramsmitter = self.tramsmitter;
    self.tramsmitter.state = state;
}
-(void) stopPlayingMedia:(MediaStreamingEnd)endBlock{
    [self.tramsmitter _stopPlayingMedia:endBlock];
}
-(void) seekToTime:(NSTimeInterval)seekTime{
    [self.tramsmitter _seekToTime:seekTime];
}

-(void) onMediaStreamingStop{
    AMDataTransmitterStateObject * stop = [StateWIFIDisplayOn new];
    stop.tramsmitter = self.tramsmitter;
    self.tramsmitter.state = stop;
}
@end

@implementation StateWIFIDisplayOn

-(void) startWifi:(void (^)(void))success failure:(void (^)(NSError *))failure disconnect:(void (^)(AMPrototypeDevice *))disconnect{
    [self.tramsmitter _startWifi:success failure:failure disconnect:disconnect];
}
-(AMDataTransmitterState) state{
    return kTransmitterStateWIFIDisplayOn;
}
-(void) playVideoMedia:(NSURL *)mediaURL subtitle:(NSURL *)subURL userInfo:(id)userInfo{
    //change to preparing state first
    StateMediaStreamingPreparing * preparing = [StateMediaStreamingPreparing new];
    preparing.tramsmitter = self.tramsmitter;
    self.tramsmitter.state = preparing;
    
    [self.tramsmitter _playVideoMedia:mediaURL subtitle:subURL userInfo:userInfo];
}

-(void) playAudioMeida:(NSURL *)mediaURL userInfo:(id)userInfo{
    //change to preparing state first
    StateMediaStreamingPreparing * preparing = [StateMediaStreamingPreparing new];
    preparing.tramsmitter = self.tramsmitter;
    self.tramsmitter.state = preparing;
    
    [self.tramsmitter _playAudioMeida:mediaURL userInfo:userInfo];
}

-(void) sendImageWithData:(NSData*) imageData{
    [self.tramsmitter _sendImageWithData:imageData];
}
-(void) sendH264Data:(NSData*)data width:(uint32_t)width height:(uint32_t)height{
    [self.tramsmitter _sendH264Data:data width:width height:height];
}
-(void) sendPCMData:(NSData*) pcmData{
    [self.tramsmitter _sendPCMData:pcmData];
}

-(void) stopWifiDisplay{
    [self.tramsmitter _stopWifiDisplay];
}

-(void)onMediaStreamingPlay{
    StateMediaStreamingPlaying * playing = [StateMediaStreamingPlaying new];
    playing.tramsmitter = self.tramsmitter;
    self.tramsmitter.state = playing;
}

-(void) onWIDIDisplayDisconnect{
    StateWIFIDisplayOff * off = [StateWIFIDisplayOff new];
    off.tramsmitter = self.tramsmitter;
    self.tramsmitter.state = off;
}
@end

@implementation StateMediaStreamingPaused
-(AMDataTransmitterState) state{
    return kTransmitterStateMediaStreamingPaused;
}
-(void) playVideoMedia:(NSURL *)mediaURL subtitle:(NSURL *)subURL userInfo:(id)userInfo{
    //change to preparing state first
    StateMediaStreamingPreparing * preparing = [StateMediaStreamingPreparing new];
    preparing.tramsmitter = self.tramsmitter;
    self.tramsmitter.state = preparing;
    
    [self.tramsmitter _playVideoMedia:mediaURL subtitle:subURL userInfo:userInfo];
}

-(void) playAudioMeida:(NSURL *)mediaURL userInfo:(id)userInfo{
    //change to preparing state first
    StateMediaStreamingPreparing * preparing = [StateMediaStreamingPreparing new];
    preparing.tramsmitter = self.tramsmitter;
    self.tramsmitter.state = preparing;
    
    [self.tramsmitter _playAudioMeida:mediaURL userInfo:userInfo];
}
-(void) sendImageWithData:(NSData*) imageData{
    [self.tramsmitter _sendImageWithData:imageData];
}
-(void) sendH264Data:(NSData*)data width:(uint32_t)width height:(uint32_t)height{
    [self.tramsmitter _sendH264Data:data width:width height:height];
}
-(void) sendPCMData:(NSData*) pcmData{
    [self.tramsmitter _sendPCMData:pcmData];
}
-(void) resumePlayingMedia{
    [self.tramsmitter _resumePlayingMedia];
    StateMediaStreamingPlaying * playing = [StateMediaStreamingPlaying new];
    playing.tramsmitter = self.tramsmitter;
    self.tramsmitter.state = playing;
}
-(void) seekToTime:(NSTimeInterval)seekTime{
    [self.tramsmitter _seekToTime:seekTime];
}
-(void) stopPlayingMedia:(MediaStreamingEnd)endBlock{
    [self.tramsmitter _stopPlayingMedia:endBlock];
}
-(void)onMediaStreamingStop{
    AMDataTransmitterStateObject * stop = [StateWIFIDisplayOn new];
    stop.tramsmitter = self.tramsmitter;
    self.tramsmitter.state = stop;
}
-(void) stopWifiDisplay{
    [self.tramsmitter _stopWifiDisplay];
}
-(void) onWIDIDisplayDisconnect{
    StateWIFIDisplayOff * off = [StateWIFIDisplayOff new];
    off.tramsmitter = self.tramsmitter;
    self.tramsmitter.state = off;
}

@end

@implementation StateMediaStreamingPreparing

-(void) startWifi:(void (^)(void))success failure:(void (^)(NSError *))failure disconnect:(void (^)(AMPrototypeDevice *))disconnect {
    [self.tramsmitter _startWifi:success failure:failure disconnect:disconnect];
}
-(void) stopWifiDisplay{
    [self.tramsmitter _stopWifiDisplay];
}
-(AMDataTransmitterState) state{
    return kTransmitterStateMediaStreamingPreraring;
}
-(void)stopPlayingMedia:(MediaStreamingEnd)endBlock{
    [self.tramsmitter _stopPlayingMedia:endBlock];
}

-(void) sendImageWithData:(NSData*) imageData{
    DLog(@"Playing state will send no image");
}
-(void) sendH264Data:(NSData*)data width:(uint32_t)width height:(uint32_t)height{
    DLog(@"Playing state will send no image");
}
-(void) sendPCMData:(NSData *)pcmData{
    DLog(@"Playing state will send no sound");
}
-(void)playAudioMeida:(NSURL *)mediaURL userInfo:(id)userInfo{
    [self.tramsmitter stopPlayingMedia:nil];
    [self.tramsmitter _playAudioMeida:mediaURL userInfo:userInfo];
}
-(void)playVideoMedia:(NSURL *)mediaURL subtitle:(NSURL *)subURL userInfo:(id)userInfo{
    [self.tramsmitter stopPlayingMedia:nil];
    [self.tramsmitter _playVideoMedia:mediaURL subtitle:subURL userInfo:userInfo];
}

-(void) onMediaStreamingStop{
    AMDataTransmitterStateObject * stop = [StateWIFIDisplayOn new];
    stop.tramsmitter = self.tramsmitter;
    self.tramsmitter.state = stop;
}

-(void)onMediaStreamingPlay{
    StateMediaStreamingPlaying * playing = [StateMediaStreamingPlaying new];
    playing.tramsmitter = self.tramsmitter;
    self.tramsmitter.state = playing;
}
-(void)onMediaStreamingReject{
    AMDataTransmitterStateObject * stop = [StateWIFIDisplayOn new];
    stop.tramsmitter = self.tramsmitter;
    self.tramsmitter.state = stop;
}

@end
