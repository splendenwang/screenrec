//
//  AMDataTransmitter_OTA.h
//  TransmitterKit
//
//  Created by Splenden on 2016/7/5.
//  Copyright © 2016年 winnerwave. All rights reserved.
//

#ifndef AMDataTransmitter_OTA_h
#define AMDataTransmitter_OTA_h

#import <Foundation/Foundation.h>
#import "AMJSONRPCResponse.h"

@protocol AMDataTransmitter_OTA <NSObject>
/**
 *  get info from device then upload to site, get another info
 *
 *  @return NSString, server version, if device is update to date retuns nil, if request timeout or error occurs, returns NSError
 */
-(id) getOTAUpdateVersion:(NSURL *)otaSiteURL;
-(id) getOTAUpdateVersion:(NSURL *)otaSiteURL completion:(void(^)(BOOL enforce, NSString * version, NSURL * firmwareURL, NSError * error))completion;;
-(void) setDeviceOTAReady:(void(^)(AMJSONRPCResponse * response))completion;
-(void) setDeviceOTAReady:(void(^)(AMJSONRPCResponse * response))completion progress:(void(^)(NSInteger totalBytes, NSInteger totalBytesWritten))progress;

@end

#endif /* AMDataTransmitter_OTA_h */
