//
//  AMDemoDataTransmitter.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/27.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMDemoDataTransmitter.h"
#import "AMDataTransmitter_private.h"
#if	TARGET_OS_IPHONE
#import <MediaPlayer/MediaPlayer.h>
#import "IDZAQAudioPlayer.h"
#import "IDZAudioDecoder.h"
#import "FlacFileDecoder.h"
#import "IDZOggVorbisFileDecoder.h"
#import "TransmitterKit/TransmitterKit-Swift.h"
#import <AVFoundation/AVFoundation.h>

//static bool isFlacFile(NSURL * fileURL){
//    if([fileURL.pathExtension caseInsensitiveCompare:@"flac"] == NSOrderedSame){
//        return YES;
//    }
//    return NO;
//}
//static bool isOggFile(NSURL * fileURL){
//    if([fileURL.pathExtension caseInsensitiveCompare:@"ogg"] == NSOrderedSame){
//        return YES;
//    }
//    return NO;
//}
//static bool isHDAudioFile(NSURL * fileURL){
//    return isFlacFile(fileURL) || isOggFile(fileURL);
//}

//static bool is_iOS(){
//    return YES;
//}

#else
/* Define empty protocol on macOS */
@protocol IDZAudioPlayerDelegate
@end
#endif


#if	TARGET_OS_IPHONE
static id<IDZAudioDecoder> decoderFor(NSURL * fileURL, NSError ** error){
    NSMutableArray *decoders = [[NSMutableArray alloc] init];
    [decoders addObject:NSStringFromClass([FlacFileDecoder class])];
    [decoders addObject:NSStringFromClass([GenericFileDecoder class])];

    for(id deocderClass in decoders) {
        id<IDZAudioDecoder> decoder = nil;
        id obj = NSClassFromString(deocderClass);
        decoder = [[obj alloc] initWithContentsOfURL:fileURL error:error];
        if(decoder != nil && *error == nil) {
            return decoder;
        }
    }
    return nil;
}
#endif

@interface AMDemoDataTransmitter() <IDZAudioPlayerDelegate>
#if	TARGET_OS_IPHONE
@property (strong) NSTimer * timer;
@property (strong) IDZAQAudioPlayer * hdAudioPlayer;
#endif
@end
@implementation AMDemoDataTransmitter
@synthesize connectedDevice = _connectedDevice;
-(void) connectToRemoteDevice:(AMPrototypeDevice *)remoteDevice
                      success:(void (^)(AMPrototypeDevice *))successBlock
                      failure:(void (^)(NSError *, AMPrototypeDevice *))failureBlock{
    _connectedDevice = remoteDevice;    //we have to avoid triggering KVO at this moment.
    if(self.connectedDevice){
        [self stopPlayingMedia:nil];
        [self stopWifiDisplay];
    }
    if(successBlock){
        successBlock(remoteDevice);
    }
    [self willChangeValueForKey:@"connectedDevice"];
    [self didChangeValueForKey:@"connectedDevice"];
}
-(void) _startWifi:(void (^)(void))success failure:(void (^)(NSError *))failure disconnect:(void (^)(AMPrototypeDevice * disconnectedDevice))disconnect{
    [self.state onWIDIDisplayStart];
    if(success){
        success();
    }
}
-(void)_playAudioMeida:(NSURL *)mediaURL userInfo:(id)userInfo{
#if	TARGET_OS_IPHONE
    if(self.timer.valid){
        [self.timer invalidate];
        self.timer = nil;
    }
    [_hdAudioPlayer stop];
    _hdAudioPlayer = nil;
    
    
    if([self launchHDAudioPlayer:mediaURL] == NO){
        DLog(@"unable to launche hd audio player for file: %@", mediaURL);
        return;
    }
    
#endif
    if(self.mediaSteamingStart){
        self.mediaSteamingStart(mediaURL);
    }
}

-(void)_playVideoMedia:(NSURL *)mediaURL subtitle:(NSURL *)subURL userInfo:(id)userInfo{
    if(self.mediaSteamingStart){
        self.mediaSteamingStart(mediaURL);
    }
    if(self.mediaSteamingEnd){
        NSDictionary *userInfo = @{ NSLocalizedDescriptionKey: @"DemoDataTransmitter is not able to play local vidoes from now on" };
        NSError * error = [NSError errorWithDomain:kAMMediaStreamingStandardErrorDomain code:1500 userInfo:userInfo];
        self.mediaSteamingEnd(mediaURL, error);
    }
}

-(void)_stopPlayingMedia:(MediaStreamingEnd)endBlock{
#if TARGET_OS_IPHONE
    if(self.timer.valid){
        [self.timer invalidate];
        self.timer = nil;
    }
    if(_hdAudioPlayer){
        [_hdAudioPlayer stopWithoutNotify];
    }
#endif
    NSError *error = nil;
    NSDictionary *userInfo = @{ NSLocalizedDescriptionKey: NSLocalizedStringFromTable(@"User canceled media streaming process", @"AMCommon", "DPFSender - User canceled media streaming process") };
    error = [NSError errorWithDomain:kAMMediaStreamingStandardErrorDomain code:NSUserCancelledError userInfo:userInfo];
    
    if(self.mediaSteamingEnd){
        self.mediaSteamingEnd(_mediaURL, error);
        self.mediaSteamingEnd = nil;
    }
    if (endBlock) {
        endBlock(_mediaURL, error);
    }
}

-(void) _pausePlayingMedia{
#if TARGET_OS_IPHONE
    [_hdAudioPlayer pause];
#endif
    if (self.mediaSteamingPause) {
        self.mediaSteamingPause();
    }
}

-(void) _resumePlayingMedia{
#if TARGET_OS_IPHONE
    [_hdAudioPlayer play];
#endif
    if (self.mediaSteamingResume) {
        self.mediaSteamingResume();
    }
}

-(void) _sendImageWithData:(NSData *)imageData{
    
}

-(void) _sendH264Data:(NSData *)data width:(uint32_t)width height:(uint32_t)height{
    
}

-(void) disconnectRemoteDevice{
    [self stopPlayingMedia:nil];
    [self stopWifiDisplay];
    self.connectedDevice = nil;
}
-(void) _stopWifiDisplay{
    [self.state onWIDIDisplayDisconnect];
}


-(BOOL) isVirtualTransmitter{
    return YES;
}
#if	TARGET_OS_IPHONE

-(BOOL)launchHDAudioPlayer:(NSURL*)mediaURL{
    NSError * error = nil;
    id<IDZAudioDecoder> decoder = decoderFor(mediaURL, &error);
    if(decoder){
        self.hdAudioPlayer = [[IDZAQAudioPlayer alloc] initWithDecoder:decoder error:&error];
        self.hdAudioPlayer.delegate = self;
        [self.hdAudioPlayer prepareToPlay];
        [self.hdAudioPlayer play];
        __weak AMDemoDataTransmitter * weakself = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            AMDemoDataTransmitter * transmitter = weakself;
            weakself.timer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:transmitter selector:@selector(timerFired:) userInfo:nil repeats:YES];
        });
        
        return YES;
    }
    return NO;
}
-(void)timerFired:(NSTimer*)timer{
    NSTimeInterval currentTime = self.hdAudioPlayer.currentTime;
    if(self.mediaSteamingElapseTime){
        self.mediaSteamingElapseTime(currentTime);
    }
}

-(void)_seekToTime:(NSTimeInterval)seekTime{
    [_hdAudioPlayer setCurrentTime:seekTime];
}

-(void) adjustVolume:(float)newVolume{
    MPVolumeView* volumeView =[[MPVolumeView alloc] init];
    UISlider* volumeViewSlider = nil;
    for (UIView *view in [volumeView subviews]) {
        if ([view.class.description isEqualToString:@"MPVolumeSlider"]) {
            volumeViewSlider = (UISlider*) view;
            break;
        }
    }
    
    [volumeViewSlider setValue:newVolume animated:NO];
}

-(void) volumeUp{
    
    float newVolume = [[AVAudioSession sharedInstance] outputVolume] + 0.0625;
    if(newVolume > 1) {
        newVolume = 1;
    }
    [self adjustVolume:newVolume];
    
    if(self.volumeChange)
    {
        self.volumeChange(newVolume);
    }
}
-(void) volumeDown{

    float newVolume = [[AVAudioSession sharedInstance] outputVolume] - 0.0625;
    if(newVolume < 0) {
        newVolume = 0;
    }
    
    [self adjustVolume:newVolume];
    
    if(self.volumeChange)
    {
        self.volumeChange(newVolume);
    }
}
#pragma mark IDZAudioPlayerDelegate

- (void)audioPlayerDidStartPlaying:(id<IDZAudioPlayer>)player{
    
    if( player.coverImageData != nil && self.mediaStreamingMetadata){
        self.mediaStreamingMetadata(@{DataTransmitterMedisStreamingMetadataKey_CoverImage: player.coverImageData});
    }
    
    if(self.mediaSteamingTotalTime)
    {
        self.mediaSteamingTotalTime(player.duration);
    }
    [self.state onMediaStreamingPlay];
}

- (void)audioPlayerDidFinishPlaying:(id<IDZAudioPlayer>)player successfully:(BOOL)flag{
    [self.state onMediaStreamingStop];
    if(self.mediaSteamingEnd){
        self.mediaSteamingEnd(_mediaURL, nil);
        self.mediaSteamingEnd = nil;
    }
}
- (void)audioPlayerDecodeErrorDidOccur:(id<IDZAudioPlayer>)player error:(NSError *)error{
    [self.state onMediaStreamingStop];
    if(self.mediaSteamingEnd){
        self.mediaSteamingEnd(_mediaURL, error);
    }
}
#endif

@end
