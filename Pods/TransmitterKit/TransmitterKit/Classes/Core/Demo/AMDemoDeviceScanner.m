//
//  AMDemoDeviceScanner.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/27.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMDemoDeviceScanner.h"
#import "AMDemoDevice.h"
#import "EZCastApplicationFeatures.h"
#import "AMPrototypeDevice+_internal.h"
@import EZCastBitmask;
@interface AMDemoDeviceScanner()
{
    NSTimer * _timer;
    NSArray * _devices;
}
@end

@implementation AMDemoDeviceScanner
-(instancetype) initWithScanDuration:(NSTimeInterval)timeInterval delegate:(id<AMPrototypeDeviceScannerDelegate>)delegate{
    self = [super initWithScanDuration:timeInterval delegate:delegate];
    if(self){
        _devices = @[];
    }
    return self;
}

-(void) reportFakeDevice:(NSTimer*)timer{
    
    for (AMDemoDevice * device in _devices) {
        if([self.predicate evaluateWithObject:device]){
            if([self.delegate respondsToSelector:@selector(scanner:discoveriedDevice:)]){
                [self.delegate scanner:self discoveriedDevice:device];
            }
        }
    }
}

-(void) startScan{
    if(!_timer){
        _timer = [NSTimer timerWithTimeInterval:_duration target:self selector:@selector(reportFakeDevice:) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
    }
    [self reportFakeDevice:nil];//report at start
}

-(void) stopScan{
    if(_timer){
        [_timer invalidate];
        _timer = nil;
        
    }
}
+(AMDemoDevice *) demoDevice{
    AMDemoDevice * aDevice = [[AMDemoDevice alloc] initWithOfflineMode:NO];
    aDevice.ipAddress = @"254.255.255.254";
    return aDevice;
}
+(AMDemoDevice *) offlineDevice{
    AMDemoDevice * aDevice = [[AMDemoDevice alloc] initWithOfflineMode:YES];
    aDevice.ipAddress = @"254.255.255.255";
    return aDevice;
}
+(AMDemoDevice *) headphoneDemoDevice{
    AMDemoDevice * aDevice = [[AMDemoDevice alloc] initWithOfflineMode:NO];
    aDevice.ipAddress = @"254.255.255.255";
    aDevice.bitmaskDictionary = @{
                               kRemoteServiceMusic        : kRemoteServiceEnable,
                               kRemoteServiceCloudStorage : kRemoteServiceEnable,
                               kRemoteServiceEZChannel    : kRemoteServiceEnable,
                              };
    aDevice.hostName = @"Headphone Mode";
    return aDevice;
}

+(NSPredicate *) demoDeviceOnlyPredicate{
    return [NSPredicate predicateWithFormat:@"isOfflineMode == NO && hostName != %@",[[self class] headphoneDemoDevice].hostName];
}

+(NSPredicate *) headphoneDemoDevicePredicate{
    return [NSPredicate predicateWithFormat:@"isOfflineMode == NO && hostName == %@",[[self class] headphoneDemoDevice].hostName];
}

+(NSPredicate *) offlineDeviceOnlyPredicate{
    return [NSPredicate predicateWithFormat:@"isOfflineMode == YES"];
}
@end
