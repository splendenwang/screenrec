//
//  AMDemoDeviceScanner.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/27.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMPrototypeDeviceScanner.h"
#import "AMDemoDevice.h"
@interface AMDemoDeviceScanner : AMPrototypeDeviceScanner

/**
 *  Create and return a demo device directly. No need to use startScan and wait for a device.
 */
+(AMDemoDevice *) demoDevice;

+(AMDemoDevice *) offlineDevice;

+(AMDemoDevice *) headphoneDemoDevice;

+(NSPredicate *) demoDeviceOnlyPredicate;

+(NSPredicate *) headphoneDemoDevicePredicate;

+(NSPredicate *) offlineDeviceOnlyPredicate;
@end
