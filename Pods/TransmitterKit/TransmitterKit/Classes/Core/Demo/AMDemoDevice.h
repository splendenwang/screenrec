//
//  AMDemoDevice.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/27.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMPrototypeDevice.h"

@interface AMDemoDevice : AMPrototypeDevice
@property (readonly) BOOL isOfflineMode;
-(instancetype) initWithCoder:(NSCoder *)aDecoder NS_DESIGNATED_INITIALIZER;
-(instancetype) initWithOfflineMode:(BOOL)offlineMode NS_DESIGNATED_INITIALIZER ;
@end


@interface AMDemoDevice (EZCast_2_0)
/**
 @param dict Bitmask dictionary to save for offline mode
 */
+(void)saveOfflineBitmask:(NSDictionary*)dict;
@end
