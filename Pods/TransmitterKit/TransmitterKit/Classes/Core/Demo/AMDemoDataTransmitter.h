//
//  AMDemoDataTransmitter.h
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/27.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMDataTransmitter.h"
#if	TARGET_OS_IPHONE
@import UIKit;
#endif


@interface AMDemoDataTransmitter : AMDataTransmitter
@end
