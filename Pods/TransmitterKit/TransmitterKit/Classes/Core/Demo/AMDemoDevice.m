//
//  AMDemoDevice.m
//  AMCommon_iOS
//
//  Created by brianliu on 2014/2/27.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMDemoDevice.h"
#import "AMGenericDevice.h"
#import "EZCastApplicationFeatures.h"
#import "AMPrototypeDevice+_internal.h"
@import EZCastBitmask;
NSString * const kOfflineModePreviousBitmaskDictionary = @"AMDemoDevice.Offline.kOfflineModePreviousBitmaskDictionary";

#if TARGET_OS_IPHONE
@import UIKit;
#endif

@interface AMDemoDevice ()
@property (assign, nonatomic) BOOL isOfflineMode;
@end

@implementation AMDemoDevice
{
    NSMutableDictionary * _dict;
}
-(instancetype)init{
    NSAssert(NO, @"Use initWithOfflineMode: instead");
    return [self initWithOfflineMode:YES];
}
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder: aDecoder];
    return self;
}

-(instancetype) initWithOfflineMode:(BOOL)offlineMode{
    self = [super init];
    if(self){
        _dict = [NSMutableDictionary dictionary];
        _dict[@"ezcast.service.ios"] = @"0x1337CF";
        _isOfflineMode = offlineMode;
        
        if(_isOfflineMode){
            NSDictionary * dict = [[NSUserDefaults standardUserDefaults] objectForKey:kOfflineModePreviousBitmaskDictionary];
            if(dict == nil){
                dict = [[self class] appOfflineCustomizationSetting];
            }
            self.bitmaskDictionary = dict;
            DLog(@"AMDemoDevie.OfflineMode.bitmaskDictionary: %@", dict);
            self.hostName = @"Offline Device";
        }
        else{
            self.bitmaskDictionary = @{
                                       kRemoteServiceEZCast : kRemoteServiceEnable,
                                       kRemoteServicePhoto : kRemoteServiceEnable,
                                       kRemoteServiceCamera : kRemoteServiceEnable,
                                       kRemoteServiceMusic : kRemoteServiceEnable,
                                       kRemoteServiceVideo : kRemoteServiceEnable,
                                       kRemoteServiceDLNA : kRemoteServiceEnable,
                                       // by WW's ask
                                       //                                       kRemoteServiceDocument : kRemoteServiceEnable,
                                       kRemoteServiceWeb : kRemoteServiceEnable,
                                       // by Jesse's ask
                                       //                                       kRemoteServiceSetting : kRemoteServiceEnable,
                                       //                                       kRemoteServiceEZAir        : kRemoteServiceEnable,
                                       //                                       kRemoteServiceHotSpotConnection : kRemoteServiceEnable,
                                       kRemoteServiceCloudVideo : kRemoteServiceEnable,
                                       kRemoteServiceCloudStorage : kRemoteServiceEnable,
                                       kRemoteServiceComment : kRemoteServiceEnable,
                                       kRemoteServiceUpdate : kRemoteServiceEnable,
                                       kRemoteServiceHelp : kRemoteServiceEnable,
                                       kRemoteServiceLiveRadio : kRemoteServiceEnable,
                                       kRemoteServiceSocial : kRemoteServiceEnable,
                                       kRemoteServiceEZChannel : kRemoteServiceEnable,
                                       //hide More Apps by Jesse's ask, due to Apple's rejcection (
                                       /*
                                        3.1 app contains irrelevant platform information on the "Download" page in the "Apps" section. Referencing third-party platforms in your app or its metadata is not appropriate on the App Store.
                                        2.25 feature in app displays or promotes third-party apps, which violates the App Store                                        Please remove the "Apps" feature from your app.
                                        */
                                       //kRemoteServiceMoreApps     : kRemoteServiceEnable,
//                                       kRemoteServiceShop : kRemoteServiceEnable,
                                       kRemoteServiceBookmark : kRemoteServiceEnable,
                                       //hide Game by Jesse's ask
                //kRemoteServiceGame         : kRemoteServiceEnable
            };
            self.hostName = @"Demo Device";
        }
        

        NSString * deviceID = @"Unknown_demoDevice";
#if TARGET_OS_IPHONE
        deviceID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
#else
        deviceID = @"Unknown_device";
        io_service_t platformExpert = IOServiceGetMatchingService(kIOMasterPortDefault,IOServiceMatching("IOPlatformExpertDevice"));
        if (platformExpert){
            
            CFTypeRef serialNumberAsCFString = IORegistryEntryCreateCFProperty(platformExpert,CFSTR(kIOPlatformUUIDKey),kCFAllocatorDefault, 0);
            if (serialNumberAsCFString){
                deviceID = (__bridge NSString *)(serialNumberAsCFString);
            }
            IOObjectRelease(platformExpert);
        }
#endif
        _dict[kAMPrototypeDeviceID] = deviceID;
    }
    return self;
}


-(id) objectForKey:(NSString *)key{
    return _dict[key];
}
+(NSArray *) supportedAudioFormat{
    return @[@"mp3", @"m4v", @"flac"];
}

+(NSArray *) supportedVideoFormat{
#if DEBUG
    return @[@"mp4", @"mov", @"m4v"];
#endif
    return @[@"mp4", @"mov"];
}

-(NSString *) displayName{
    if(self.isOfflineMode){
        return NSLocalizedStringFromTable(@"Offline Mode", @"AMCommon", @"Offline Mode");
    }
#if TARGET_OS_IPHONE
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return @"On the iPad";
    }
    else{
        return @"On the iPhone";
    }
#endif
    
    return NSLocalizedString(@"Try a demo", "Try a demo");
    
}
-(NSString*) srcvers{
    return @"";
}

-(BOOL) isEqual:(id)object{
    if([object isKindOfClass:[self class]]){
        return  [self.ipAddress isEqualToString:((AMDemoDevice*)object).ipAddress];
    }
    return NO;
}

+(NSDictionary*)appOfflineCustomizationSetting{
    NSURL * url = [[NSBundle mainBundle] URLForResource:@"OfflineModeCustomizationSetting" withExtension:@"plist"];
    if(url == nil){
        return nil; //considering some apps which use TrnasmitterKit but not EZCast-iOS/EZCastPro-iOS, and does not have this plist file
    }
    NSDictionary * appDict = [NSDictionary dictionaryWithContentsOfURL:url];
    return appDict;
}

@end

@implementation AMDemoDevice (EZCast_2_0)

+(void)saveOfflineBitmask:(NSDictionary*)dict{
    DLog(@"saveOfflineBitmask receive para dict: %@", dict);
    if(!dict){
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kOfflineModePreviousBitmaskDictionary];
        
    }else{
        NSDictionary * appDict = [[self class] appOfflineCustomizationSetting];
        
        if(appDict == nil){
            NSLog(@"Skip saving offline bitmasks doe to this app bundle doees not conain \"OfflineModeCustomizationSetting.plist\"");
        }
        
        NSMutableDictionary * previousConnectedDict = [NSMutableDictionary dictionaryWithDictionary:dict];
    
        for (NSString * k in previousConnectedDict.allKeys) {
            if(appDict[k] == nil){
                [previousConnectedDict removeObjectForKey:k];
            }
        }
        [[NSUserDefaults standardUserDefaults] setObject:previousConnectedDict forKey:kOfflineModePreviousBitmaskDictionary];
        DLog(@"saveOfflineBitmask save to user dedault: %@", previousConnectedDict);
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}


@end
