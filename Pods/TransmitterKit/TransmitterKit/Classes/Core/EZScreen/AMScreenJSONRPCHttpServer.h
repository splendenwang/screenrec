//
//  AMJSONRPCHttpServer.h
//  AMCommon_iOS
//
//  Created by Splenden on 2014/7/30.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

@import WinnerWave_CocoaHTTPServer;

#import "AMJSONRPCDispatcher.h"
#import "HTTPServer+StartCompletionBlock.h"
@interface AMScreenJSONRPCHttpServer : HTTPServer
-(NSArray *)httpConnections;
@property (nonatomic, strong) AMJSONRPCDispatcher * jrpcDispatcher;
@end
