//
//  AMScreenDataTransmitter.h
//  AMCommon_iOS
//
//  Created by Splenden on 2014/7/10.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMDataTransmitter.h"
#import "AMBufferController.h"
@import SimpleAES;
@import CocoaAsyncSocket;
#import "AMNTPPacket.h"

extern int const kAMScreenH264PacketHeaderLength;
extern NSString *const kAMScreenH264MesssageEncryptionKey;
extern NSString * const kAMScreenDataTransmitterErrorDomain;
typedef NS_ENUM(NSInteger, kAMScreenDataTransmitterErrorCode){
    kAMScreenDataTransmitterErrorFailure = 1000,
    kAMScreenDataTransmitterErrorTimeout,
    kAMScreenDataTransmitterErrorNoWIFIIPAddress,
    kAMScreenDataTransmitterErrorConnectedDeviceIncorrect,
    kAMScreenDataTransmitterErrorHttpServerInitFail,
    kAMScreenDataTransmitterErrorMjpegServerInitFail,
    kAMScreenDataTransmitterErrorMediaStreamFail,
    kAMScreenDataTransmitterErrorH264StreamFail,
};
typedef enum {
    AMScreenH264PacketTypeVideoBitStream = 0,
    AMScreenH264PacketTypeCodecData,
    AMScreenH264PacketTypeHeartBeat,
    AMScreenH264PacketTypeHandShaking,
    AMScreenH264PacketTypeHeader = 1000,
} AMScreenH264PacketType;
@interface AMScreenDataTransmitter : AMDataTransmitter <AMBufferControllerDelegate, GCDAsyncSocketDelegate, GCDAsyncUdpSocketDelegate>
/**
 *  Create h264 header packet data, little endian
 *  @see https://192.168.32.20/wiki/pages/D2w1R2h5b/EZCast_Screen_Proprietary_Protocol.html
 *
 *  @param payloadSize 4 bytes
 *  @param payloadType 2 bytes
 *
 *  @return header data to send
 */
+ (NSData *)h264HeaderPacket:(uint32_t)payloadSize type:(int16_t)payloadType;
/**
 *  Create h264 header packet data, little endian
 *  @see https://192.168.32.20/wiki/pages/D2w1R2h5b/EZCast_Screen_Proprietary_Protocol.html
 *
 *  @param payloadSize 4 bytes
 *  @param payloadType 2 bytes
 *  @param timeStamp   8 bytes
 *
 *  @return header data to send
 */
+ (NSData *)h264HeaderPacket:(uint32_t)payloadSize type:(int16_t)payloadType ntpTimestamp:(struct ntpTimestamp)timeStamp;
/**
 *  Create h264 codec packet data
 *  @see https://192.168.32.20/wiki/pages/D2w1R2h5b/EZCast_Screen_Proprietary_Protocol.html
 */
+ (NSData*)h264CodecPacket:(NSData *)sps pps:(NSData *)pps;

- (void)h264HandShaking:(id)sender completion:(void (^)(NSError *))completionBlock;
@end
