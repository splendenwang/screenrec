//
//  AMNTPClient.m
//  
//
//  Created by Splenden on 2015/11/24.
//
//

#import "AMNTPClient.h"

@interface AMNTPClient() {
    struct ntpTimestamp
    ntpClientSendTime,
    ntpServerRecvTime,
    ntpServerSendTime,
    ntpClientRecvTime,
    ntpServerBaseTime;
    
    int li, vn, mode, stratum, poll, prec, refid;
}
@property (readonly) double root_delay;                     // milliSeconds
@property (readonly) double dispersion;                     // milliSeconds
@property (readonly) double roundtrip;                      // seconds
@end

@implementation AMNTPClient
- (NSData *) createQueryPacket {
    uint32_t        wireData[12];
    
    memset(wireData, 0, sizeof wireData);
    wireData[0] = htonl((0 << 30) |                                         // no Leap Indicator
                        (4 << 27) |                                         // NTP v4
                        (3 << 24) |                                         // mode = client sending
                        (0 << 16) |                                         // stratum (n/a)
                        (4 << 8)  |                                         // polling rate (16 secs)
                        (-6 & 0xff));                                       // precision (~15 mSecs)
    wireData[1] = htonl(0<<16);
    wireData[2] = htonl(0<<16);
    
    ntp_time_now(&ntpClientSendTime);
    
    wireData[10] = htonl(ntpClientSendTime.wholeSeconds);                   // Transmit Timestamp
    wireData[11] = htonl(ntpClientSendTime.fractSeconds);
    
    return [NSData dataWithBytes:wireData length:48];
}
- (void) parseNTPTimePacket:(NSData *)data {
    /*┌──────────────────────────────────────────────────────────────────────────────────────────────────┐
     │  grab the packet arrival time as fast as possible, before computations below ...                 │
     └──────────────────────────────────────────────────────────────────────────────────────────────────┘*/
    ntp_time_now(&ntpClientRecvTime);
    
    uint32_t        wireData[12];
    [data getBytes:wireData length:48];
    
    li      = ntohl(wireData[0]) >> 30 & 0x03;
    vn      = ntohl(wireData[0]) >> 27 & 0x07;
    mode    = ntohl(wireData[0]) >> 24 & 0x07;
    stratum = ntohl(wireData[0]) >> 16 & 0xff;
    /*┌──────────────────────────────────────────────────────────────────────────────────────────────────┐
     │  Poll: 8-bit signed integer representing the maximum interval between successive messages,       │
     │  in log2 seconds.  Suggested default limits for minimum and maximum poll intervals are 6 and 10. │
     └──────────────────────────────────────────────────────────────────────────────────────────────────┘*/
    poll    = ntohl(wireData[0]) >>  8 & 0xff;
    /*┌──────────────────────────────────────────────────────────────────────────────────────────────────┐
     │  Precision: 8-bit signed integer representing the precision of the system clock, in log2 seconds.│
     │  (-10 corresponds to about 1 millisecond, -20 to about 1 microSecond)                            │
     └──────────────────────────────────────────────────────────────────────────────────────────────────┘*/
    prec    = ntohl(wireData[0])       & 0xff;
    if (prec & 0x80) prec |= 0xffffff00;                                // -ve byte --> -ve int
    
    _root_delay = ntohl(wireData[1]) * 0.0152587890625;                 // delay (mS) [1000.0/2**16].
    _dispersion = ntohl(wireData[2]) * 0.0152587890625;                 // error (mS)
    
    refid   = ntohl(wireData[3]);
    
    ntpServerBaseTime.wholeSeconds = ntohl(wireData[4]);                // when server clock was wound
    ntpServerBaseTime.fractSeconds = ntohl(wireData[5]);
    
    /*┌──────────────────────────────────────────────────────────────────────────────────────────────────┐
     │  if the send time in the packet isn't the same as the remembered send time, ditch it ...         │
     └──────────────────────────────────────────────────────────────────────────────────────────────────┘*/
    if (ntpClientSendTime.wholeSeconds != ntohl(wireData[6]) ||
        ntpClientSendTime.fractSeconds != ntohl(wireData[7])) return;   //  NO;
    
    ntpServerRecvTime.wholeSeconds = ntohl(wireData[8]);
    ntpServerRecvTime.fractSeconds = ntohl(wireData[9]);
    ntpServerSendTime.wholeSeconds = ntohl(wireData[10]);
    ntpServerSendTime.fractSeconds = ntohl(wireData[11]);
    
    //  NTP_Logging(@"%@", [self prettyPrintPacket]);
    
    /*┌──────────────────────────────────────────────────────────────────────────────────────────────────┐
     │ determine the quality of this particular time ..                                                 │
     │ .. if max_error is less than 50mS (and not zero) AND                                             │
     │ .. stratum > 0 AND                                                                               │
     │ .. the mode is 4 (packet came from server) AND                                                   │
     │ .. the server clock was set less than 1 minute ago                                               │
     └──────────────────────────────────────────────────────────────────────────────────────────────────┘*/
    /**
     *  @see https://nto.github.io/AirPlay.html
     *  _dispersion, _root_delay always be 0
     *  diffSecond between BaseTime, SendTime, it's useless
     */
    _offset = INFINITY;                                                 // clock meaningless
    if (((_dispersion < 50.0 && _dispersion > 0.00001) || YES)&&
        (stratum > 0) && (mode == 4) &&
        ((ntpDiffSeconds(&ntpServerBaseTime, &ntpServerSendTime) < 60.0) || YES)) {
        
        double  t41 = ntpDiffSeconds(&ntpClientSendTime, &ntpClientRecvTime);   // .. (T4-T1)
        double  t32 = ntpDiffSeconds(&ntpServerRecvTime, &ntpServerSendTime);   // .. (T3-T2)
        
        _roundtrip  = t41 - t32;
        
        double  t21 = ntpDiffSeconds(&ntpServerSendTime, &ntpClientRecvTime);   // .. (T2-T1)
        double  t34 = ntpDiffSeconds(&ntpServerRecvTime, &ntpClientSendTime);   // .. (T3-T4)
        
        _offset = (t21 + t34) / 2.0;                                            // calculate offset
        
        //      NSLog(@"t21=%.6f t34=%.6f delta=%.6f offset=%.6f", t21, t34, _roundtrip, _offset);
        _active = TRUE;
        
        //      NTP_Logging(@"%@", [self prettyPrintTimers]);
    }
    
    //    [_delegate reportFromDelegate];                                 // tell delegate we're done
}
@end
