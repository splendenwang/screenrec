//
//  AMNTPPacket.h
//  
//
//  Created by Splenden on 2015/11/23.
//
//

#import <Foundation/Foundation.h>
#define JAN_1970    		0x83aa7e80
struct ntpTimestamp {
    uint32_t      wholeSeconds;
    uint32_t      fractSeconds;
};
@interface AMNTPPacket :NSObject
void unix2ntp(const struct timeval * tv, struct ntpTimestamp * ntp);
void ntp2unix(const struct ntpTimestamp * ntp, struct timeval * tv);
void ntp_time_now(struct ntpTimestamp * ntp);
double ntpDiffSeconds(struct ntpTimestamp * start, struct ntpTimestamp * stop);
+ (NSDate *) dateFromNetworkTime:(struct ntpTimestamp *) networkTime;
@end

