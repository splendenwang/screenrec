//
//  AMH264NALUParser.m
//
//
//  Created by Splenden on 2015/11/23.
//
//

#import "AMH264NALUParser.h"
/*Stop debug loggin in this file*/
#ifdef DEBUG
#undef DLog
#define DLog(...)
#undef DTrace
#define DTrace();
#endif
/********************************/
const static char nalHeader[4] = { 0x00, 0x00, 0x00, 0x01 };
const static uint nalHeaderLength = 4;
NSString* const naluTypesStrings[] = {
    @"Unspecified (non-VCL)",
    @"Coded slice of a non-IDR picture (VCL)",
    @"Coded slice data partition A (VCL)",
    @"Coded slice data partition B (VCL)",
    @"Coded slice data partition C (VCL)",
    @"Coded slice of an IDR picture (VCL)",
    @"Supplemental enhancement information (SEI) (non-VCL)",
    @"Sequence parameter set (non-VCL)",
    @"Picture parameter set (non-VCL)",
    @"Access unit delimiter (non-VCL)",
    @"End of sequence (non-VCL)",
    @"End of stream (non-VCL)",
    @"Filler data (non-VCL)",
    @"Sequence parameter set extension (non-VCL)",
    @"Prefix NAL unit (non-VCL)",
    @"Subset sequence parameter set (non-VCL)",
    @"Reserved (non-VCL)",
    @"Reserved (non-VCL)",
    @"Reserved (non-VCL)",
    @"Coded slice of an auxiliary coded picture without partitioning (non-VCL)",
    @"Coded slice extension (non-VCL)",
    @"Coded slice extension for depth view components (non-VCL)",
    @"Reserved (non-VCL)",
    @"Reserved (non-VCL)",
    @"Unspecified (non-VCL)",
    @"Unspecified (non-VCL)",
    @"Unspecified (non-VCL)",
    @"Unspecified (non-VCL)",
    @"Unspecified (non-VCL)",
    @"Unspecified (non-VCL)",
    @"Unspecified (non-VCL)",
    @"Unspecified (non-VCL)",
};
@interface AMH264NALUParser() {
    NSData* _spsData;
    NSData* _ppsData;
    NSMutableData* _sliceBufferData;
    CMVideoFormatDescriptionRef _videoFormatDescription;
    BOOL _shouldUpdateVideoFormatDescription;
    BOOL _usingTimeStamp;
    CMTime _previousBufferTimestamp;
    CMTime _baseTimestamp;
}
@property (nonatomic, strong) NSMutableData* dataToWrite;
@end
@implementation AMH264NALUParser
- (void)setup
{
    _spsData = nil;
    _ppsData = nil;
    _sliceBufferData = [NSMutableData new];
    _videoFormatDescription = NULL;
    _shouldUpdateVideoFormatDescription = NO;
    _usingTimeStamp = YES;
    _previousBufferTimestamp = kCMTimeInvalid;
    _baseTimestamp = kCMTimeInvalid;
//    _dataToWrite = [NSMutableData new];
}

- (instancetype)init
{
    if (self = [super init]) {
        [self setup];
    }
    
    return self;
}

- (instancetype)initWithDelegate:(id<AMH264NALUParserDelegate>)delegate
{
    if (self = [self init]) {
        _delegate = delegate;
    }
    
    return self;
}

#pragma mark - nal units
+ (NALUType)getNALUType:(NSData*)NALU
{
    uint8_t* bytes = (uint8_t*)NALU.bytes;
    
    return bytes[nalHeaderLength] & 0x1F;
}

static struct ntpTimestamp parseTimeStamp = {0,0};
+ (void)nalUnitsInData:(NSData*)data operation:(void (^)(NSData *))operation
{
    NSData* h264Data = data;
    NSData* nalHeaderData = [AMH264NALUParser nalHeaderData];
    NSRange range = [h264Data rangeOfData:nalHeaderData options:0 range:NSMakeRange(0, h264Data.length)];
    
    if (range.location == NSNotFound) {
        DLog(@"Invalid Packet (No NALU Header)");
    }
    else {
        while (range.location != NSNotFound) {
            NSUInteger startPostition = range.location + range.length;
            NSRange nextRange = [h264Data rangeOfData:nalHeaderData options:0 range:NSMakeRange(startPostition, h264Data.length - startPostition)];
            NSRange naluRange;
            if (nextRange.location == NSNotFound) {
                naluRange = NSMakeRange(range.location, h264Data.length - range.location); // range.location to EOF
            }
            else {
                naluRange = NSMakeRange(range.location, nextRange.location - range.location); //range.location to nextRange.location
            }
            operation([data subdataWithRange:naluRange]);
            range = nextRange;
        }
    }
    
}
- (void)parseNALUs:(NSData *)data timeStamp:(struct ntpTimestamp)timeStamp
{
    DLog(@"NALU parseTime - %u.%u",timeStamp.wholeSeconds,timeStamp.fractSeconds);
    __weak AMH264NALUParser * weakSelf = self;
    [AMH264NALUParser nalUnitsInData:data operation:^(NSData *nalUnitData) {
        [weakSelf parseNALU:nalUnitData timeStamp:timeStamp];
    }];
}
- (void)parseNALU:(NSData*)NALU timeStamp:(struct ntpTimestamp)timeStamp
{
    NALUType type = [AMH264NALUParser getNALUType:NALU];
    parseTimeStamp = timeStamp;
    DLog(@"NALU size:%lu Type (%d)\"%@\" received.", (unsigned long)NALU.length, type, naluTypesStrings[type]);
//    if(_dataToWrite.length > 2048 * 1024) {
//        [_dataToWrite writeToFile:@"/Users/brian/Downloads/test.h264" atomically:YES];
//        _dataToWrite = nil;
//    } else {
//        if(_videoFormatDescription != NULL)
//            [_dataToWrite appendData:NALU];
//    }
    
    NSMutableData* nalData = [NALU mutableCopy];
    const uint8_t lengthInBytes[] = { (uint8_t)((nalData.length - nalHeaderLength) >> 24), (uint8_t)((nalData.length - nalHeaderLength) >> 16), (uint8_t)((nalData.length - nalHeaderLength) >> 8), (uint8_t)(nalData.length - nalHeaderLength) };
    [nalData replaceBytesInRange:NSMakeRange(0, nalHeaderLength) withBytes:lengthInBytes];
//    if(_videoFormatDescription==nil)
//        return;
    switch (type) {
        case NALUTypeSliceNoneIDR:
        case NALUTypeSliceIDR:{
            /**
             *  first_mb_in_slice
             *  @see:http://blog.csdn.net/huanggang982/article/details/37929905
             *  @see:http://stackoverflow.com/questions/15034532/determine-the-end-of-h-264-i-frame-in-rtp-stream
             *
             *  There might be multi nalu in blockbuffer
             *  @see:http://stackoverflow.com/questions/28396622/extracting-h264-from-cmblockbuffer
             *  @see:https://forums.developer.apple.com/thread/14212
             */
            NSData *first_mb_in_slice = [nalData subdataWithRange:NSMakeRange(5, 2)];
            uint8_t* first_mb_in_slice_bytes = (uint8_t*)first_mb_in_slice.bytes;
            BOOL isFirstMBinSlice = (first_mb_in_slice_bytes[0] > 127); //1000 0000(2
            DLog(@"first slice:%@-%@",isFirstMBinSlice?@"YES":@"NO",first_mb_in_slice);
            if (isFirstMBinSlice == NO) {
//                DLog(@"append slice");
                [_sliceBufferData appendData:[nalData copy]];
            } else {
                if(_sliceBufferData.length > 0) {
                    [self handleSlice:[_sliceBufferData copy]];
                }
//                DLog(@"new slice");
                _sliceBufferData = nalData;
            }
            break;
        }
            
        case NALUTypeSPS:
            [self handleSPS:nalData];
            [self updateFormatDescriptionIfPossible];
//            [_dataToWrite appendData:NALU];
            break;
            
        case NALUTypePPS:
            [self handlePPS:nalData];
            [self updateFormatDescriptionIfPossible];
//            [_dataToWrite appendData:NALU];
            break;
            
        default:
            if(_ppsData && _spsData)
//                [_dataToWrite appendData:NALU];
            break;
    }
}

/**
 *  Using pts, decodeFPS if no timestamp
 */
static int pts = 1;
static int decodeFPS = 60;
- (void)handleSlice:(NSData*)nalData
{
    NSLog(@"handle Slice");
    //    Not sure we need big edian or not
    //    /* The length of the NALU in big endian */
    //    const uint32_t NALUlengthInBigEndian = CFSwapInt32HostToBig((uint32_t) NALU.length);
    //
    //    /* Create the slice */
    //    NSMutableData * slice = [[NSMutableData alloc] initWithBytes:&NALUlengthInBigEndian length:4];
    
    OSStatus status;
    /**
     *  create block buffer
     */
    CMBlockBufferRef videoBlock = NULL;
    status = CMBlockBufferCreateWithMemoryBlock(kCFAllocatorDefault, NULL, nalData.length, kCFAllocatorDefault, NULL, 0, nalData.length, 0, &videoBlock);
    DLog(@"block create:%d", (int)status);
    status = CMBlockBufferReplaceDataBytes([nalData bytes], videoBlock, 0, nalData.length);
    DLog(@"block byte replaced:%d", (int)status);
    
    /**
     *  create sample buffer
     */
    CMSampleBufferRef sampleBuffer = NULL;
    const size_t sampleSizeArray[] = { nalData.length };
    
    if (_usingTimeStamp) {
        /**
         *  create time stamp
         */
        CMSampleTimingInfo timingInfo;
        BOOL invalidTimeStamp = parseTimeStamp.wholeSeconds == 0 && parseTimeStamp.fractSeconds == 0;
        if(invalidTimeStamp) {
            timingInfo.presentationTimeStamp = CMTimeMake(pts, decodeFPS);
            pts++;
            timingInfo.duration = CMTimeMake(1, decodeFPS);
            timingInfo.decodeTimeStamp = timingInfo.decodeTimeStamp;//If the samples are in presentation order, this must be set to kCMTimeInvalid.
        } else {
            struct timeval tempTime;
            ntp2unix(&parseTimeStamp, &tempTime);
            int64_t combinedTime = (int64_t)parseTimeStamp.wholeSeconds*1000000 + tempTime.tv_usec;
            if(CMTimeCompare(_baseTimestamp, kCMTimeInvalid) == 0) {
                _baseTimestamp = CMTimeMake(combinedTime, 1000000);
            }
            
            timingInfo.presentationTimeStamp = CMTimeSubtract(CMTimeMake(combinedTime, 1000000), _baseTimestamp); //some magic....using relative time
            
            if(CMTimeCompare(_previousBufferTimestamp, kCMTimeInvalid) == 0) { //equal
                timingInfo.duration = CMTimeMake(1, decodeFPS);
            } else {
                timingInfo.duration = CMTimeSubtract(timingInfo.presentationTimeStamp, _previousBufferTimestamp);
            }
            _previousBufferTimestamp = timingInfo.presentationTimeStamp;
            timingInfo.decodeTimeStamp = timingInfo.presentationTimeStamp;//If the samples are in presentation order, this must be set to kCMTimeInvalid.
        };
        DLog(@"timestamp - %f",CMTimeGetSeconds(timingInfo.presentationTimeStamp));
        status = CMSampleBufferCreate(kCFAllocatorDefault, videoBlock, true, NULL, NULL, _videoFormatDescription, 1, 1, &timingInfo, 1, sampleSizeArray, &sampleBuffer);
    }
    else {
        status = CMSampleBufferCreate(kCFAllocatorDefault, videoBlock, true, NULL, NULL, _videoFormatDescription, 1, 0, NULL, 1, sampleSizeArray, &sampleBuffer);
        /**
         *  Display immediately
         */
        CFArrayRef attachments = CMSampleBufferGetSampleAttachmentsArray(sampleBuffer, YES);
        CFMutableDictionaryRef dict = (CFMutableDictionaryRef)CFArrayGetValueAtIndex(attachments, 0);
        CFDictionarySetValue(dict, kCMSampleAttachmentKey_DisplayImmediately, kCFBooleanTrue);
    }
    
    DLog(@"sampelBuffer create:%d", (int)status);
    
    __weak AMH264NALUParser * weakSelf = self;
    
    if ([weakSelf.delegate respondsToSelector:@selector(didParseH264:status:usingTimeStamp:)]) {
        [weakSelf.delegate didParseH264:sampleBuffer status:status usingTimeStamp:_usingTimeStamp];
    }
}

- (void)handleSPS:(NSData*)NALU
{
    _spsData = [[NALU subdataWithRange:NSMakeRange(nalHeaderLength, NALU.length - nalHeaderLength)] copy];
}

- (void)handlePPS:(NSData*)NALU
{
    _ppsData = [[NALU subdataWithRange:NSMakeRange(nalHeaderLength, NALU.length - nalHeaderLength)] copy];
}

- (void)updateFormatDescriptionIfPossible
{
    //    if(_sliceBufferData.length > 0) {
    //        [self handleSlice:[_sliceBufferData copy]];
    //        _sliceBufferData = [NSMutableData new];
    //    }
    
    if (_spsData != nil && _ppsData != nil) {
        const uint8_t* const parameterSetPointers[2] = {
            (const uint8_t*)_spsData.bytes,
            (const uint8_t*)_ppsData.bytes
        };
        
        const size_t parameterSetSizes[2] = {
            _spsData.length,
            _ppsData.length
        };
        
        CMVideoFormatDescriptionCreateFromH264ParameterSets(
                                                            kCFAllocatorDefault,
                                                            2,
                                                            parameterSetPointers,
                                                            parameterSetSizes,
                                                            4,
                                                            &_videoFormatDescription);
        
        _shouldUpdateVideoFormatDescription = YES;
        _spsData = nil;
        _ppsData = nil;
        
        DLog(@"Updated CMVideoFormatDescription. Creation: %@.", (status == noErr) ? @"successfully." : @"failed.");
        if([self.delegate respondsToSelector:@selector(didUpdateVideoFormatDescription:)]){
            [self.delegate didUpdateVideoFormatDescription:_videoFormatDescription];
        }
    }
}
- (void)cleanupTimestampAndVideoFormatDescription
{
    @synchronized(self){
        pts = 1;
        _baseTimestamp = kCMTimeInvalid;
        _previousBufferTimestamp = kCMTimeInvalid;
    }
}
+(NSData *)nalHeaderData {
    return [NSData dataWithBytes:nalHeader length:nalHeaderLength];
}
@end
