//
//  AMNTPPacket.m
//  
//
//  Created by Splenden on 2015/11/23.
//
//

#import "AMNTPPacket.h"
#include <sys/time.h>

@implementation AMNTPPacket
/*┌──────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Convert from Unix time to NTP time                                                               │
  └──────────────────────────────────────────────────────────────────────────────────────────────────┘*/
void unix2ntp(const struct timeval * tv, struct ntpTimestamp * ntp) {
    ntp->wholeSeconds = (uint32_t)(tv->tv_sec + JAN_1970);
    ntp->fractSeconds = (uint32_t)(((double)tv->tv_usec + 0.5) * (double)(1LL<<32) * 1.0e-6);
}

/*┌──────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Convert from NTP time to Unix time                                                               │
  └──────────────────────────────────────────────────────────────────────────────────────────────────┘*/
void ntp2unix(const struct ntpTimestamp * ntp, struct timeval * tv) {
    tv->tv_sec  = ntp->wholeSeconds - JAN_1970;
    tv->tv_usec = (uint32_t)((double)ntp->fractSeconds / (1LL<<32) * 1.0e6);
}

/*┌──────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ get current time in NTP format                                                                   │
  └──────────────────────────────────────────────────────────────────────────────────────────────────┘*/
void ntp_time_now(struct ntpTimestamp * ntp) {
    struct timeval          now;
    gettimeofday(&now, (struct timezone *)NULL);
    unix2ntp(&now, ntp);
}

/*┌──────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ get (ntpTime2 - ntpTime1) in (double) seconds                                                    │
  └──────────────────────────────────────────────────────────────────────────────────────────────────┘*/
double ntpDiffSeconds(struct ntpTimestamp * start, struct ntpTimestamp * stop) {
    int32_t         a;
    uint32_t        b;
    a = stop->wholeSeconds - start->wholeSeconds;
    if (stop->fractSeconds >= start->fractSeconds) {
        b = stop->fractSeconds - start->fractSeconds;
    }
    else {
        b = start->fractSeconds - stop->fractSeconds;
        b = ~b;
        a -= 1;
    }
    
    return a + b / 4294967296.0;
}


/*┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
  ┃ Make an NSDate from ntpTimestamp ... (via seconds from JAN_1970) ...                             ┃
  ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛*/
+ (NSDate *) dateFromNetworkTime:(struct ntpTimestamp *) networkTime {
    static struct ntpTimestamp NTP_1970 = {JAN_1970, 0};
    return [NSDate dateWithTimeIntervalSince1970:ntpDiffSeconds(&NTP_1970, networkTime)];
}
@end
