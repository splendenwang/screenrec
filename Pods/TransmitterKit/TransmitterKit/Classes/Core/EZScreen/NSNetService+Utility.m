//
//  NSNetService+Utility.m
//  AMCommon_iOS
//
//  Created by brianliu on 13/10/2.
//  Copyright (c) 2013年 ActionsMicro. All rights reserved.
//

#import "NSNetService+Utility.h"

#include <arpa/inet.h>

@implementation NSNetService (Utility)
- (NSArray *)ipAddress
{
    NSMutableArray * returnArray = [NSMutableArray new];
	for(NSData *data in [self addresses])
	{
		char addressBuffer[100];
		struct sockaddr_in* socketAddress = (struct sockaddr_in*) [data bytes];
		int sockFamily = socketAddress->sin_family;
        
		if(sockFamily == AF_INET)
		{
			const char* addressStr = inet_ntop(sockFamily,
											   &(socketAddress->sin_addr), addressBuffer,
											   sizeof(addressBuffer));
            
			if(addressStr)
                //				return [[NSString alloc] initWithCString:addressStr encoding:NSASCIIStringEncoding];
            {
                [returnArray addObject:[[NSString alloc] initWithCString:addressStr encoding:NSASCIIStringEncoding]];
            }
			
		}
	}
	return [[NSArray alloc] initWithArray:returnArray];
}

- (NSArray *)ipv6Address
{
    NSMutableArray * returnArray = [NSMutableArray new];
    for(NSData *data in [self addresses])
    {
        char addressBuffer[100];
        struct sockaddr_in* socketAddress = (struct sockaddr_in*) [data bytes];
        int sockFamily = socketAddress->sin_family;
        
        if(sockFamily == AF_INET6)
        {
            const char* addressStr = inet_ntop(sockFamily,
                                               &(socketAddress->sin_addr), addressBuffer,
                                               sizeof(addressBuffer));
            
            if(addressStr)
                //				return [[NSString alloc] initWithCString:addressStr encoding:NSASCIIStringEncoding];
            {
                [returnArray addObject:[[NSString alloc] initWithCString:addressStr encoding:NSASCIIStringEncoding]];
            }
            
        }
    }
    return [[NSArray alloc] initWithArray:returnArray];
}
+(NSDictionary*) stringDictionaryFromTXTRecordData:data{
    NSMutableDictionary * returnDict = [NSMutableDictionary new];
    NSDictionary * dict = [NSNetService dictionaryFromTXTRecordData:data];
    for (NSString * key in [dict allKeys]) {
        NSString * string = [[NSString alloc] initWithData:dict[key] encoding:NSUTF8StringEncoding];
        returnDict[key] = string;
    }
    return returnDict;
}

@end
