//
//  AMH264NALUParser.h
//  
//
//  Created by Splenden on 2015/11/23.
//
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "AMNTPPacket.h"
@import CocoaAsyncSocket;

/**
*  @see http://guoh.org/lifelog/2013/10/h-264-bit-stream-sps-pps-idr-nalu/
*  @see http://qkeye.com/thread-614629-1-1.html
*  13-23: reserved
*  24-31: unused
*/
typedef enum {
    NALUTypeSliceNoneIDR = 1,
    NALUTypeDPA,
    NALUTypeDPB,
    NALUTypeDPC,
    NALUTypeSliceIDR = 5,
    NALUTypeSEI,
    NALUTypeSPS = 7,
    NALUTypePPS = 8,
    NALUTypeAUD,
    NALUTypeEOSEQ,
    NALUTypeEOSTREAM = 12,
} NALUType;
@protocol AMH264NALUParserDelegate <NSObject>
@required
@optional
- (void)didParseH264:(CMSampleBufferRef)sampleBuffer status:(OSStatus)status usingTimeStamp:(BOOL)usingTimeStatmp;
- (void)didUpdateVideoFormatDescription:(CMVideoFormatDescriptionRef)description;
@end
@interface AMH264NALUParser : NSObject
+ (void)nalUnitsInData:(NSData*)data operation:(void (^)(NSData * nalUnitData))operation;
+ (NALUType)getNALUType:(NSData*)NALU;
- (void)parseNALU:(NSData*)NALU timeStamp:(struct ntpTimestamp)timeStamp;
- (void)parseNALUs:(NSData*)data timeStamp:(struct ntpTimestamp)timeStamp;
- (instancetype)initWithDelegate:(id<AMH264NALUParserDelegate>)delegate;
- (void)cleanupTimestampAndVideoFormatDescription;
@property (nonatomic, weak) id<AMH264NALUParserDelegate> delegate;
+(NSData *)nalHeaderData;
@end
