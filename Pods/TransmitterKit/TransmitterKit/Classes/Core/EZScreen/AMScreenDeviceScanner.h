//
//  AMScreenDeviceScanner.h
//  AMCommon_iOS
//
//  Created by Splenden on 2014/7/10.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMPrototypeDeviceScanner.h"

@interface AMScreenDeviceScanner : AMPrototypeDeviceScanner <AMPrototypeDeviceScannerProtocol, NSNetServiceBrowserDelegate ,NSNetServiceDelegate>

@end
