//
//  AMScreenJSONRPCConnection.h
//  AMCommon_iOS
//
//  Created by Splenden on 2014/7/30.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//
@import WinnerWave_CocoaHTTPServer;

@class AMJSONRPCDispatcher;
@class HTTPDataResponse;

@protocol AMScreenJSONRPCConnectionDelegate;
@interface AMScreenHTTPConfig : HTTPConfig
{
    AMJSONRPCDispatcher * _jrpcDispatcher;
}
@property (strong, readonly) AMJSONRPCDispatcher * jrpcDispatcher;
- (id)initWithServer:(HTTPServer *)server documentRoot:(NSString *)documentRoot queue:(dispatch_queue_t)q jsonrpcDispatcher:(AMJSONRPCDispatcher *)jrpcdispatcher;
@end;
@interface AMScreenJSONRPCConnection : HTTPConnection
- (NSString *)connectedHost;
@end
