//
//  AMScreenDataTransmitter.m
//  AMCommon_iOS
//
//  Created by Splenden on 2014/7/10.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMScreenDataTransmitter.h"
#import "AMScreenDevice.h"
#import "AMPrototypeDevice.h"

#import "AMiDeviceIpAddress.h"
//#import "NSString+AMStringUtility.h"
@import WinnerWave_CocoaHTTPServer;
#import "AMMJpegHttpServer.h"
#import "AMNTPServer.h"

@import AFNetworking;
#import "AFJSONRPCClient.h"
#import "AMJSONRPCObject.h"
#import "AMJSONRPCRequest.h"
#import "AMJSONRPCNotification.h"
#import "AMJSONRPCResponse.h"
#import "AMJSONRPCDispatcher.h"

#import "EZCastApplicationFeatures.h"

#import "AMScreenMediaHttpConnection.h"
#import "AMScreenMediaHttpConnection.h"
#import "AMScreenJSONRPCConnection.h"
#import "AMScreenJSONRPCHttpServer.h"
#import "AMDataTransmitter_private.h"
#import "AMH264NALUParser.h"
@import SimpleAES;

int const kAMScreenH264PacketHeaderLength = 32; //bytes
NSString* const kAMScreenH264MesssageEncryptionKey = @"SCREEN21SCREEN90SCREEN23SCREEN43";
NSString* const kAMScreenDataTransmitterErrorDomain = @"kAMScreenDataTransmitterErrorDomain";

@interface AMScreenDataTransmitter () {
    ;
    /**
     *  Remote
     */
    AMScreenJSONRPCHttpServer* _jrpcHttpServer;
    AMJSONRPCDispatcher* _jrpcDispatcher;
    AFJSONRPCClient* _jrpcClient;
    dispatch_source_t _heartbeatTimer;
    /**
     *  WifiDisplay
     */
    HTTPServer* _mediaHttpServer;
    AMMJpegHttpServer* _mjpegServer;
    /**
     *  ETC.
     */
    float _currentVolumeLevel;
    void (^_remoteSuccessBlock)(AMPrototypeDevice*);
    void (^_remoteFailureBlock)(NSError*, AMPrototypeDevice*);
    
    void (^_h264CompletionBlock)(NSError*);
}
@property AMBufferController* bufferController;
/**
 *  WifiDisplay - H264
 */
@property (nonatomic, strong) GCDAsyncSocket* h264TxSocket;
@property (nonatomic, assign) BOOL supportH264;
@property (nonatomic, assign) BOOL h264TxSocketHandShaked;
@property (nonatomic, strong) dispatch_source_t h264HeartbeatTimer;
@property (nonatomic, strong) GCDAsyncUdpSocket* h264NtpServerSocket;
@property (nonatomic, strong) AMNTPServer* h264NtpServer;
@property (nonatomic, strong) NSString* h264EncryptionKey;
@property (nonatomic, strong) NSString* h264EncryptionIV;
@property (nonatomic, assign) BOOL h264PayloadEncrypted;
@end
@implementation AMScreenDataTransmitter
@synthesize connectedDevice = _connectedDevice;

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        [self setupBufferController];
        [self setupJRPCDispatcher];
        [self startJRPCHttpServer];
        [self setupH264];
    }
    
    return self;
}

static uint16_t h264NtpServerPort = 65535;
dispatch_source_t createDispatchTimer(double interval, dispatch_queue_t queue, dispatch_block_t block)
{
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    if (timer) {
        dispatch_source_set_timer(timer, dispatch_time(DISPATCH_TIME_NOW, interval * NSEC_PER_SEC), interval * NSEC_PER_SEC, (1ull * NSEC_PER_SEC) / 10);
        dispatch_source_set_event_handler(timer, block);
        dispatch_resume(timer);
    }
    return timer;
}
- (void)setupH264
{
    /**
     *  Socket
     */
    self.h264TxSocketHandShaked = NO;
    self.h264TxSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
    self.h264TxSocket.delegate = self;
    
    /**
     *  NTP
     */
    self.h264NtpServer = [AMNTPServer new];
    self.h264NtpServerSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                                             delegateQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
    NSError* error = nil;
    /**
     *  random port @see:https://en.wikipedia.org/wiki/Ephemeral_port
     */
    int randLowerBound = 32768;
    int randUpperBound = 61000;
    h264NtpServerPort = arc4random_uniform(randUpperBound - randLowerBound) + randUpperBound;
    [self.h264NtpServerSocket bindToPort:h264NtpServerPort error:&error];
    while (error != nil) {
        DLog(@"create socket error:%@", error);
        h264NtpServerPort = arc4random_uniform(randUpperBound - randLowerBound) + randUpperBound;
        [self.h264NtpServerSocket bindToPort:h264NtpServerPort error:&error];
    }
    [self.h264NtpServerSocket beginReceiving:&error];
    if (error) {
        DLog(@"ScreenTransmitter - NtpServerSocket:%@", error);
    }
    
    /**
     *  Encryption
     */
    self.h264EncryptionKey = [AMScreenDataTransmitter randomString:32];
    self.h264EncryptionIV = [AMScreenDataTransmitter randomString:16];
    self.h264PayloadEncrypted = YES;
}

- (void)setupBufferController
{
    self.bufferController = [[AMBufferController alloc] init];
    self.bufferController.delegate = self;
    [self.bufferController startConsumerThreads];
}

- (void)setupJRPCClient
{
    _jrpcClient = [[AFJSONRPCClient alloc]
                   initWithEndpointURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@/jsonrpc", self.connectedDevice.ipAddress, [(AMScreenDevice*)self.connectedDevice port]]]];
    
    NSMutableSet* set = [[NSMutableSet alloc] initWithSet:_jrpcClient.responseSerializer.acceptableContentTypes];
    
    [set addObject:@"text/plain"];
    [set addObject:@"text/html"];
    _jrpcClient.responseSerializer.acceptableContentTypes = set;
}

- (void)setupJRPCDispatcher
{
    _jrpcDispatcher = [[AMJSONRPCDispatcher alloc] init];
    
    [_jrpcDispatcher registerNotification:@"ezcastplayer.ondurationchange"
                                operation:^(AMJSONRPCNotification* notification, id context) {
                                    //duration:媒體檔的總長度。以秒為單位。格式為浮點數。
                                    id durationObj = (notification.parameters)[@"duration"];
                                    
                                    if (durationObj && durationObj != [NSNull null] && self.mediaSteamingTotalTime) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            if (self.mediaSteamingTotalTime) {
                                                self.mediaSteamingTotalTime([durationObj integerValue]);
                                            }
                                        });
                                    }
                                }];
    
    [_jrpcDispatcher registerNotification:@"ezcastplayer.onloadstart"
                                operation:^(AMJSONRPCNotification* notification, id context) {
                                    if (self.mediaSteamingStart) {
                                        self.mediaSteamingStart(_mediaURL);
                                    }
                                }];
    
    [_jrpcDispatcher registerNotification:@"ezcastplayer.onplay"
                                operation:^(AMJSONRPCNotification* notification, id context){
                                }];
    
    [_jrpcDispatcher registerNotification:@"ezcastplayer.ontimeupdate"
                                operation:^(AMJSONRPCNotification* notification, id context) {
                                    id timeObj = (notification.parameters)[@"time"];
                                    
                                    if (timeObj && timeObj != [NSNull null] && self.mediaSteamingElapseTime) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            if (self.mediaSteamingElapseTime) {
                                                self.mediaSteamingElapseTime([timeObj integerValue]);
                                            }
                                        });
                                    }
                                }];
    
    [_jrpcDispatcher registerNotification:@"ezcastplayer.onerror"
                                operation:^(AMJSONRPCNotification* notification, id context) {
                                    //error: 請參考HTML 5 Video Element定義。格式為整數。
                                    id errorObject = (notification.parameters)[@"error"];
                                    
                                    if (errorObject && errorObject != [NSNull null] && self.mediaSteamingEnd) {
                                        NSInteger errorCode = 0;
                                        
                                        if ([(NSNumber *)errorObject isEqualToNumber:@1]) {
                                            errorCode = AV_RESULT_ERROR_STOP_ABORTED;
                                        } else if ([(NSNumber *)errorObject isEqualToNumber:@2]) {
                                            errorCode = AV_RESULT_ERROR_GENERIC;
                                        } else if ([(NSNumber *)errorObject isEqualToNumber:@3] || [(NSNumber *)errorObject isEqualToNumber:@4]) {
                                            errorCode = AV_RESULT_ERROR_STOP_FILE_FORMAT_UNSOPPORTED;
                                        }
                                        
                                        NSString *errorDescription = [self htmlMediaErrorDictionary][[NSString stringWithFormat:@"%@", errorObject]];
                                        NSError *error = [NSError errorWithDomain:kAMMediaStreamingStandardErrorDomain
                                                                             code:errorCode
                                                                         userInfo:@{ NSLocalizedDescriptionKey: errorDescription }];
                                        
                                        [self executeMediaEndBlock:error];
                                    }
                                }];
    
    [_jrpcDispatcher registerNotification:@"ezcastplayer.onended"
                                operation:^(AMJSONRPCNotification* notification, id context) {
                                    //time:目前播放的時間位置。以秒為單位。格式為浮點數。
                                    [self executeMediaEndBlock:nil];
                                }];
}

- (void)dealloc
{
    [self disconnectRemoteDevice];
    [self stopJRPCHttpServer];
    [self.bufferController removeAllBuffer];
    [self.bufferController stopThreadGracefully];
}

#pragma mark - jrpc methods
- (void)sendJRPCNotification:(NSString*)method parameters:(NSDictionary*)parameters success:(void (^)(void))success failure:(void (^)(NSError* error))failure
{
    if (_jrpcClient == nil) {
        _jrpcClient = nil;
        [self setupJRPCClient];
    }
    [_jrpcClient sendNotification:method
                   withParameters:parameters
                          success:^(AFHTTPRequestOperation* operation, id responseObject) {
                              DLog(@"send JRPC notification %@", method);
                              
                              if (success) {
                                  success();
                              }
                          }
                          failure:^(AFHTTPRequestOperation* operation, NSError* error) {
                              DLog(@"JRPC notification error:%@", error);
                              
                              if (failure) {
                                  failure(error);
                              }
                          }];
}

- (void)sendJRPCMethod:(NSString*)method aId:(NSUInteger)aId parameters:(NSDictionary*)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError* error))failure
{
    if (_jrpcClient == nil) {
        _jrpcClient = nil;
        [self setupJRPCClient];
    }
    [_jrpcClient invokeMethod:method withParameters:parameters requestId:@(aId)
                      success:^(AFHTTPRequestOperation* operation, id responseObject) {
                          DLog(@"send JRPC %@, response: \"%@\"", method, [responseObject description]);
                          if(success) {
                              success(responseObject);
                          }
                      }
                      failure:^(AFHTTPRequestOperation* operation, NSError* error) {
                          DLog(@"JRPC request error:%@", error);
                          
                          if (failure) {
                              failure(error);
                          }
                      }];
}

- (void)sendJRPCMethod:(NSString*)method parameters:(NSDictionary*)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError* error))failure
{
    [self sendJRPCMethod:method aId:(arc4random() % 65535)parameters:parameters success:success failure:failure];
}

#pragma mark - HeartBeat
- (void)setupHeartbeat
{
    __weak AMScreenDataTransmitter* weakSelf = self;
    if (!_heartbeatTimer) {
        __block NSInteger heartbeatResponsed = 0;
        _heartbeatTimer = createDispatchTimer(5.00f, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            if(heartbeatResponsed > 5) {
                DLog(@"heartbeat - timeout");
                NSError* error = [NSError errorWithDomain:kAMScreenDataTransmitterErrorDomain code:kAMScreenDataTransmitterErrorTimeout userInfo:nil];
                [weakSelf executeRemotefailureBlock:error];
                [weakSelf disconnectRemoteDevice];
                heartbeatResponsed = 0;
            }
            heartbeatResponsed ++;
            [weakSelf sendJRPCNotification:kRemoteHeartbeat
                                parameters:nil
                                   success:^{
                                       heartbeatResponsed = 0;
                                   }
                                   failure:^(NSError* error) {
                                       DLog(@"heartbeat - timeout");
                                       [weakSelf executeRemotefailureBlock:error];
                                       [weakSelf disconnectRemoteDevice];
                                       heartbeatResponsed = 0;
                                   }];
        });
    }
}

- (void)cancelHeartbeat
{
    if (_heartbeatTimer) {
        dispatch_source_cancel(_heartbeatTimer);
        _heartbeatTimer = nil;
    }
}

#pragma mark - HTTPServer
- (void)startJRPCHttpServer
{
    if (_jrpcHttpServer == nil) {
        _jrpcHttpServer = [[AMScreenJSONRPCHttpServer alloc] init];
        [_jrpcHttpServer setJrpcDispatcher:_jrpcDispatcher];
        [_jrpcHttpServer startWithConnectionClass:[AMScreenJSONRPCConnection class]
                                          success:^{
                                          }
                                          failure:^(NSError* error) {
                                              NSError *selfError = [NSError  errorWithDomain:kAMScreenDataTransmitterErrorDomain
                                                                                        code:kAMScreenDataTransmitterErrorHttpServerInitFail
                                                                                    userInfo:@{ NSLocalizedDescriptionKey: [error localizedDescription] }];
                                              DLog(@"%@", selfError.localizedDescription);
                                          }];
    }
}

- (void)stopJRPCHttpServer
{
    if (_jrpcHttpServer) {
        [_jrpcHttpServer stop];
        _jrpcHttpServer = nil;
    }
}

- (NSDictionary*)htmlMediaErrorDictionary
{
    /**
     1 = MEDIA_ERR_ABORTED - fetching process aborted by user
     2 = MEDIA_ERR_NETWORK - error occurred when downloading
     3 = MEDIA_ERR_DECODE - error occurred when decoding
     4 = MEDIA_ERR_SRC_NOT_SUPPORTED - audio/video not supported
     */
    return @{ @"1" : @"fetching process aborted by user",
              @"2" : @"error occurred when downloading",
              @"3" : @"error occurred when decoding",
              @"4" : @"audio/video not supported" };
}

- (void)startMediaHttpServer:(void (^)(void))success
                     failure:(void (^)(NSError* error))failure
{
    if (_mediaHttpServer == nil) {
        _mediaHttpServer = [[HTTPServer alloc] init];
        [_mediaHttpServer startWithConnectionClass:[AMScreenMediaHttpConnection class]
                                           success:^{
                                               if (success) {
                                                   success();
                                               }
                                           }
                                           failure:^(NSError* error) {
                                               DLog(@"Error starting HTTP Server: %@", error);
                                               
                                               if (failure) {
                                                   failure(error);
                                               }
                                           }];
    }
    else {
        if (success) {
            success();
        }
    }
}

#pragma mark - MJPEG
- (void)startMJPEGServer:(void (^)(void))success
                 failure:(void (^)(NSError* error))failure
              disconnect:(void (^)(NSError* error))disconnect
{
    if (_mjpegServer == nil) { //if there is already a mjpeg server, means already running. We tell the caller it's
        _mjpegServer = [[AMMJpegHttpServer alloc] init];
        [_mjpegServer startOnPort:0
                  completionBlock:^(NSError *error) {
//#warning[AMiDeviceIpAddress getWIFIIPAddress] might be wrong when mac to mac condition
                      DLog(@"Starting MJPEG HTTP Server---> http://%@:%d", [AMiDeviceIpAddress getWIFIIPAddress], [_mjpegServer port]);
                      
                      if ([AMiDeviceIpAddress getWIFIIPAddress] == nil) {
                          error = [NSError errorWithDomain:kAMScreenDataTransmitterErrorDomain
                                                               code:kAMScreenDataTransmitterErrorNoWIFIIPAddress
                                                           userInfo:nil];
                          
                          if (failure) {
                              failure(error);
                          }
                      }
                      else {
                          [self sendJRPCMethod:@"display"
                                    parameters:@{ @"url": [NSString stringWithFormat:@"http://%@:%d", [AMiDeviceIpAddress getWIFIIPAddress], [_mjpegServer port]] }
                                       success:^(id responseObject) {
                                           if (success) {
                                               success();
                                           }
                                       }
                                       failure: ^(NSError *error) {
                                           if (failure) {
                                               NSError *selfError = [NSError errorWithDomain:kAMScreenDataTransmitterErrorDomain
                                                                                        code:kAMScreenDataTransmitterErrorMjpegServerInitFail
                                                                                    userInfo:@{ NSLocalizedDescriptionKey: [error localizedDescription] }];
                                               failure(selfError);
                                           }
                                       }];
                      }
                  }
                       disconnect:^(NSError* error) {
                           DLog(@"mjpeg server disconnected, error:%@", error.localizedDescription);
                           NSError *selfError = nil;
                           if (error) {
                               selfError = [NSError errorWithDomain:kAMScreenDataTransmitterErrorDomain
                                                               code:kAMScreenDataTransmitterErrorMjpegServerInitFail
                                                           userInfo:@{ NSLocalizedDescriptionKey: [error localizedDescription] }];
                           }
                           if (disconnect) {
                               disconnect(selfError);
                           }
                       }];
    }
    else {
        if (success) {
            success();
        }
    }
}

#pragma mark -
- (void)_sendImageWithData:(NSData*)imageData
{
    DTrace();
    [self.bufferController sendObject:imageData];
}
- (void)h264HandShaking:(id)sender completion:(void (^)(NSError *))completionBlock
{
    if(self.h264TxSocketHandShaked) {
        completionBlock(nil);
        return;
    }
    if(self.supportH264){
        __weak AMScreenDataTransmitter * weakSelf = self;
        NSString* base64EncodedIV = [[weakSelf.h264EncryptionIV dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0];;
        NSNumber* ntpServerPort = [NSNumber numberWithUnsignedInt:h264NtpServerPort];
        NSString* encryptedKey = [weakSelf.h264EncryptionKey AESEncryptKey:kAMScreenH264MesssageEncryptionKey
                                                                        iv:weakSelf.h264EncryptionIV
                                                                  settings:kAMAESCBC256BitKeySettings];
            
        [self sendJRPCMethod:@"stream"
                  parameters:@{ @"param1" : encryptedKey,
                                @"param2" : base64EncodedIV,
                                @"ntp-server-port" : ntpServerPort }
                     success:^(id responseObject) {
                         
                         NSError * error =nil;
                         if([responseObject isKindOfClass:[NSDictionary class]]) {
                             //                                     NSString * connectionType = responseObject[@"connection-type"];
                             NSNumber * tcpPort = responseObject[@"tcp-port"];
                             //                                     NSString * version = responseObject[@"version"];
                             [weakSelf.h264TxSocket connectToHost:weakSelf.connectedDevice.ipAddress onPort:[tcpPort unsignedIntValue] withTimeout:5 error:&error];
                             _h264CompletionBlock = completionBlock;
                         } else {
                             error = [NSError errorWithDomain:kAMScreenDataTransmitterErrorDomain code:kAMScreenDataTransmitterErrorH264StreamFail userInfo:@{NSLocalizedDescriptionKey:@"Not a valid json response"}];
                             completionBlock(error);
                         }
                     }
                     failure:^(NSError* error) {
                         completionBlock(error);
                     }];
    }
}
- (void)_sendH264Data:(NSData*)data width:(uint32_t)width height:(uint32_t)height
{
    @synchronized(self.h264TxSocket)
    {
        if (self.h264TxSocket.isConnected && self.h264TxSocketHandShaked) {
            
            __block NSData* blockSPSData = nil;
            __block NSData* blockPPSData = nil;
            __weak AMScreenDataTransmitter* weakSelf = self;
            [AMH264NALUParser nalUnitsInData:data operation:^(NSData* nalUnitData) {
                
                if (blockSPSData != nil && blockPPSData != nil) {
                    NSData* codecData = [AMScreenDataTransmitter h264CodecPacket:[blockSPSData copy] pps:[blockPPSData copy]];
                    blockSPSData = nil;
                    blockPPSData = nil;
                    [weakSelf sendStreamData:codecData
                                        type:AMScreenH264PacketTypeCodecData
                                   encrypted:weakSelf.h264PayloadEncrypted
                                   viaSocket:weakSelf.h264TxSocket
                           listeningResponse:NO];
                }
                
                NALUType nalutype = [AMH264NALUParser getNALUType:nalUnitData];
                DLog(@"NALU type:%d",(int)nalutype);
                switch (nalutype) {
                    case NALUTypePPS:
                        blockPPSData = [nalUnitData subdataWithRange:NSMakeRange(4, nalUnitData.length - 4)];
                        break;
                    case NALUTypeSPS:
                        blockSPSData = [nalUnitData subdataWithRange:NSMakeRange(4, nalUnitData.length - 4)];
                        break;
                        
                    case NALUTypeSliceNoneIDR:
                    case NALUTypeSliceIDR: {
                        [weakSelf sendStreamData:nalUnitData
                                            type:AMScreenH264PacketTypeVideoBitStream
                                       encrypted:weakSelf.h264PayloadEncrypted
                                       viaSocket:weakSelf.h264TxSocket
                               listeningResponse:NO];
                        break;
                    }
                    default:
                        break;
                }
            }];
        }
        else {
            DLog(@"H264Socket not connected");
        }
    }
}

- (void)clearWifiConnectionFailureCompletionBlock
{
    _connectionFailureBlock = nil;
    _connectionSuccessBlock = nil;
    _afterDisconnectBlock = nil;
}

- (void)executeMediaEndBlock:(NSError*)error
{
    [self.state onMediaStreamingStop];
    
    if (self.mediaSteamingEnd) {
        self.mediaSteamingEnd(_mediaURL, error);
    }
    [self clearPlayMediaBlock];
}
- (void)executeWifiSuccessBlock
{
    [self.state onWIDIDisplayStart];
    
    if (_connectionSuccessBlock) {
        _connectionSuccessBlock();
        _connectionSuccessBlock = nil;
    }
}
- (void)executeWifiFailureBlock:(NSError*)error
{
    @synchronized(self)
    {
        if (_connectionFailureBlock) {
            _connectionFailureBlock(error);
        }
        [self clearWifiConnectionFailureCompletionBlock];
        [self.state stopWifiDisplay]; //onWIDIDisplayDisconnect
    }
}
- (void)executeWifiDisconnectBlock:(NSError*)error
{
    if (_afterDisconnectBlock) {
        _afterDisconnectBlock(self.connectedDevice);
        _afterDisconnectBlock = nil;
    }
    [self.state stopWifiDisplay]; //onWIDIDisplayDisconnect
}

- (void)_startWifi:(void (^)(void))success
           failure:(void (^)(NSError* error))failure
        disconnect:(void (^)(AMPrototypeDevice* disconnectedDevice))disconnect
{
    //2014.5.15
    ///TODO:These codes may need to make it simpler since the flow has been changed.
    _connectionSuccessBlock = success;
    _connectionFailureBlock = failure;
    _afterDisconnectBlock = disconnect;
    
    __weak AMScreenDataTransmitter* weakSelf = self;
    if (self.connectedDevice && [self.connectedDevice isKindOfClass:[AMScreenDevice class]]) {
        __block NSError* startWifiError = nil;
        BOOL usingH264 = [(AMScreenDevice*)weakSelf.connectedDevice supportH264];
#if TARGET_OS_IPHONE
        usingH264 = NO;
#endif
            dispatch_group_t startWifiGroup = dispatch_group_create(); //make sure all service is initialized
            //mjpg
            dispatch_group_enter(startWifiGroup);
            [weakSelf startMJPEGServer:^{
                dispatch_group_leave(startWifiGroup);
            }
                               failure:^(NSError* error) {
                                   dispatch_group_leave(startWifiGroup);
                               }
                            disconnect:^(NSError* error) {
                                [weakSelf executeWifiDisconnectBlock:error];
                            }];
            
            //h264
            if(usingH264) {
//                NSString* base64EncodedIV = [[weakSelf.h264EncryptionIV dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0];;
//                NSNumber* ntpServerPort = [NSNumber numberWithUnsignedInt:h264NtpServerPort];
//                NSString* encryptedKey = [weakSelf.h264EncryptionKey AESEncryptKey:kAMScreenH264MesssageEncryptionKey
//                                                                                iv:weakSelf.h264EncryptionIV
//                                                                          settings:kAMAESCBC256BitKeySettings];
                dispatch_group_enter(startWifiGroup);
                [weakSelf sendJRPCMethod:@"getStreamInfo"
                              parameters:nil
                                 success:^(id responseObject) {
                                     if([responseObject isKindOfClass:[NSDictionary class]]){
                                         AMScreenDevice * screenDevice = (AMScreenDevice*)weakSelf.connectedDevice;
                                         NSNumber * supportH264 = responseObject[@"h264stream"];
                                         [screenDevice setSupportH264:supportH264.boolValue];
                                         NSNumber * width = responseObject[@"width"];
                                         NSNumber * height = responseObject[@"height"];
                                         CGSize screenSize = CGSizeMake(width.floatValue,height.floatValue);
                                         [screenDevice setMaxResolution:screenSize];
                                         DLog(@"getStreamInfo - %@",responseObject);
                                         self.supportH264 = supportH264.boolValue;
                                     }
                                     dispatch_group_leave(startWifiGroup);
                                 }
                                 failure:^(NSError* error) {
                                     startWifiError = error;
                                     dispatch_group_leave(startWifiGroup);
                                 }];
            }
            
            dispatch_group_notify(startWifiGroup, dispatch_get_main_queue(), ^{
                if (startWifiError) {
                    [weakSelf executeWifiFailureBlock:startWifiError];
                }
                else {
                    [weakSelf executeWifiSuccessBlock];
                }
            });
    }
    else {
        NSError* error = [NSError errorWithDomain:kAMScreenDataTransmitterErrorDomain code:kAMScreenDataTransmitterErrorConnectedDeviceIncorrect userInfo:nil];
        [self executeWifiFailureBlock:error];
    }
}

/**
 *  Disconnect from the connected device if there is any.
 *  -Stop and clean JPEG Server
 *  -Stop and clean Media Server
 *  -Disconnect H264 Socket
 */
- (void)_stopWifiDisplay
{
    DTrace();
    [self sendJRPCMethod:@"stop_display"
              parameters:nil
                 success:^(id responseObject) {
                 }
                 failure:^(NSError* error) {
                     NSError *selfError = [NSError errorWithDomain:kAMScreenDataTransmitterErrorDomain
                                                              code:kAMScreenDataTransmitterErrorFailure
                                                          userInfo:@{ NSLocalizedDescriptionKey: error.localizedDescription }];
                     DLog(@"%@", selfError.localizedDescription);
                 }];
    //mjpg
    if (_mjpegServer != nil) {
        [_mjpegServer stop];
        _mjpegServer = nil;
    }
    //media
    if (_mediaHttpServer != nil) {
        [_mediaHttpServer stop];
        _mediaHttpServer = nil;
    }
    //h264
    if (self.h264HeartbeatTimer) {
        dispatch_source_cancel(self.h264HeartbeatTimer);
        self.h264HeartbeatTimer = nil;
    }
    if(self.h264TxSocket.isConnected){
        [self.h264TxSocket disconnect];
    }
    
    [self.state onWIDIDisplayDisconnect];
}

#pragma mark - AMRemoteDataTransmitterProtocol
- (void)clearRemoteConnectionCompletionBlock
{
    _remoteSuccessBlock = nil;
    _remoteFailureBlock = nil;
}

- (void)executeRemotefailureBlock:(NSError*)error
{
    if (_remoteFailureBlock) {
        _remoteFailureBlock(error, self.connectedDevice);
    }
    [self disconnectRemoteDevice];
    [self clearRemoteConnectionCompletionBlock];
}

- (void)connectToRemoteDevice:(AMPrototypeDevice*)device
                      success:(void (^)(AMPrototypeDevice* device))successBlock
                      failure:(void (^)(NSError* error, AMPrototypeDevice* device))failureBlock
{
    if (self.connectedDevice) {
        [self stopWifiDisplay];
        [self disconnectRemoteDevice];
    }
    
    if (device && [device isKindOfClass:[AMScreenDevice class]]) {
        _connectedDevice = device; //we have to avoid triggering KVO at this moment.
        _remoteSuccessBlock = successBlock;
        _remoteFailureBlock = failureBlock;
        
//#warning undefined parameter:jrpc-server-port for rx jrpcClient connection
        [self sendJRPCNotification:@"connect"
                        parameters:@{ @"jrpc-server-port" : @([_jrpcHttpServer listeningPort]) }
                           success:^{
                               if (_remoteSuccessBlock) {
                                   _remoteSuccessBlock(self.connectedDevice);
                                   _remoteSuccessBlock = nil;
                                   [self setupHeartbeat];
                               }
                               
                               [self willChangeValueForKey:@"connectedDevice"];
                               [self didChangeValueForKey:@"connectedDevice"];
                           }
                           failure:^(NSError* error) {
                               [self executeRemotefailureBlock:error];
                           }];
    }
    else {
        NSError* error = [NSError errorWithDomain:kAMScreenDataTransmitterErrorDomain code:kAMScreenDataTransmitterErrorConnectedDeviceIncorrect userInfo:nil];
        [self executeRemotefailureBlock:error];
    }
}

- (void)disconnectRemoteDevice
{
    __weak AMScreenDataTransmitter * weakSelf = self;
    [self stopPlayingMedia:^(NSURL *mediaURL, NSError *error) {
        [weakSelf stopWifiDisplay];
        [weakSelf sendJRPCNotification:@"disconnect"
                        parameters:nil
                           success:^{
                           }
                           failure:^(NSError* error) {
                               NSError *selfError = [NSError errorWithDomain:kAMScreenDataTransmitterErrorDomain
                                                                        code:kAMScreenDataTransmitterErrorFailure
                                                                    userInfo:@{ NSLocalizedDescriptionKey: error.localizedDescription }];
                               DLog(@"%@", selfError.localizedDescription);
                           }];
    }];
    [self cancelHeartbeat];
    self.connectedDevice = nil;
}

#pragma mark - mediaStreaming
- (void)clearPlayMediaBlock
{
    _mediaURL = nil;
    
    self.mediaSteamingStart = nil;
    self.mediaSteamingEnd = nil;
    self.mediaSteamingTotalTime = nil;
    self.mediaSteamingElapseTime = nil;
    self.mediaSteamingResume = nil;
    self.mediaSteamingPause = nil;
    self.volumeChange = nil;
}

- (void)_stopPlayingMedia:(MediaStreamingEnd)endBlock
{
    __weak AMScreenDataTransmitter* weakSelf = self;
    __weak AMJSONRPCDispatcher* weakJrpcDispatcher = _jrpcDispatcher;
    NSError *error = nil;
    NSDictionary *userInfo = @{ NSLocalizedDescriptionKey: NSLocalizedStringFromTable(@"User canceled media streaming process", @"AMCommon", "DPFSender - User canceled media streaming process") };
    error = [NSError  errorWithDomain:kAMMediaStreamingStandardErrorDomain
                                 code:NSUserCancelledError
                             userInfo:userInfo];
    [_jrpcDispatcher clearBlockForNotification:@"ezcastplayer.onended"];
    [_jrpcDispatcher registerNotification:@"ezcastplayer.onended"
                                operation:^(AMJSONRPCNotification* notification, id context) {

//                                    if (endBlock) {
//                                        endBlock(_mediaURL, error);
//                                    }
//                                    [weakSelf executeMediaEndBlock:error];
                                    // restore back to default notificaiton handler imp.
                                    [weakJrpcDispatcher clearBlockForNotification:@"ezcastplayer.onended"];
                                    [weakJrpcDispatcher registerNotification:@"ezcastplayer.onended"
                                                                   operation:^(AMJSONRPCNotification* notification, id context) {
                                                                       [weakSelf executeMediaEndBlock:nil];
                                                                   }];
                                }];
    [self.state onMediaStreamingStop];
    [self sendJRPCMethod:@"stop" parameters:nil success:^(id responseObject) {
        if (endBlock) {
            endBlock(_mediaURL, error);
        }
    } failure:^(NSError *error) {
        DLog(@"ScreenTransmitter sendStop - %@",error);
    }];
}

- (void)_playVideoMedia:(NSURL*)mediaURL subtitle:(NSURL*)subURL userInfo:(id)userInfo
{
    _mediaURL = mediaURL;
    [self startMediaHttpServer:^{
//#warning[AMiDeviceIpAddress getWIFIIPAddress] might be wrong when mac to mac condition
        NSString *messsageServerURLString = [[NSString stringWithFormat:@"http://%@:%d"
                                              , [AMiDeviceIpAddress getWIFIIPAddress]
                                              , [_jrpcHttpServer listeningPort]]
                                             stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *mediaURLString = nil;
        
        if ([_mediaURL isFileURL]) {
            mediaURLString = [[NSString stringWithFormat:@"http://%@:%d/LocalVideo.html?location=%@", [AMiDeviceIpAddress getWIFIIPAddress], [_mediaHttpServer listeningPort], _mediaURL.path]
                              stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        }
        else {
            mediaURLString = [_mediaURL absoluteString];
        }
        [self sendJRPCMethod:@"play"
                  parameters:@{ @"url" : mediaURLString,
                                @"callback" : messsageServerURLString }
                     success:^(id responseObject) {
                         [self.state onMediaStreamingPlay];
                     }
                     failure:^(NSError* error) {
                         [self executeMediaEndBlock:error];
                     }];
    }
                       failure:^(NSError* error) {
                           [self executeMediaEndBlock:error];
                       }];
}

- (void)_playAudioMeida:(NSURL*)mediaURL userInfo:(id)userInfo
{
    [self _playVideoMedia:mediaURL subtitle:nil userInfo:userInfo];
}

- (void)_pausePlayingMedia
{
    [self sendJRPCMethod:@"pause"
              parameters:nil
                 success:^(id responseObject) {
                     StateMediaStreamingPaused *paused = [StateMediaStreamingPaused new];
                     paused.tramsmitter = self;
                     self.state = paused;
                     
                     if (self.mediaSteamingPause) {
                         self.mediaSteamingPause();
                     }
                 }
                 failure:nil];
}

- (void)_resumePlayingMedia
{
    [self sendJRPCMethod:@"resume"
              parameters:nil
                 success:^(id responseObject) {
                     [self.state onMediaStreamingPlay];
                     
                     if (self.mediaSteamingResume) {
                         self.mediaSteamingResume();
                     }
                 }
                 failure:nil];
}

- (void)volumeUp
{
    if (_mediaHttpServer) {
        _currentVolumeLevel += 0.1;
        
        if (_currentVolumeLevel > 1.0) {
            _currentVolumeLevel = 1.0;
        }
        
        [self sendJRPCMethod:@"increase_volume"
                  parameters:nil
                     success:^(id responseObject) {
                         if (self.volumeChange) {
                             self.volumeChange(_currentVolumeLevel);
                         }
                     }
                     failure:nil];
    }
}

- (void)volumeDown
{
    if (_mediaHttpServer) {
        _currentVolumeLevel -= 0.1;
        
        if (_currentVolumeLevel < 0.0) {
            _currentVolumeLevel = 0.0;
        }
        
        [self sendJRPCMethod:@"decrease_volume"
                  parameters:nil
                     success:^(id responseObject) {
                         if (self.volumeChange) {
                             self.volumeChange(_currentVolumeLevel);
                         }
                     }
                     failure:nil];
    }
}

- (float)volume
{
    return _currentVolumeLevel;
}

- (void)_seekToTime:(NSTimeInterval)seekTime
{
    [self sendJRPCMethod:@"seek" parameters:@{ @"time" : @((int)seekTime) } success:nil failure:nil];
}

- (BOOL)isWIFIDisplayOn
{
    return _mjpegServer != nil || self.h264TxSocket.connectedHost != nil;
}

#pragma mark - AMBufferControllerDelegate
- (BOOL)readyToSendObj
{
    DLog(@"%s, %@", __PRETTY_FUNCTION__, _mjpegServer.isReadyToSend ? @"YES" : @"NO");
    return _mjpegServer.isReadyToSend;
}

- (void)sendObject:(id)aObj
{
    DTrace();
    [_mjpegServer sendJpgData:aObj];
}

#pragma mark - GCDAsyncUdpSocketDelegate
- (void)udpSocket:(GCDAsyncUdpSocket*)sock didReceiveData:(NSData*)data fromAddress:(NSData*)address withFilterContext:(id)filterContext
{
    [self.h264NtpServer parseNTPQueryPacket:data];
    [self.h264NtpServerSocket sendData:[self.h264NtpServer createNTPTimePacket] toAddress:address withTimeout:-1 tag:0];
}

#pragma mark - GCDAsyncSocketDelegate

- (void)socket:(GCDAsyncSocket*)sock didConnectToHost:(NSString*)host port:(uint16_t)port
{
    DTrace();
    if (sock == self.h264TxSocket) {
        NSString* handShakingMessage = @"Luke, I am your Father!";
        NSData* handShakingData = [handShakingMessage dataUsingEncoding:NSUTF8StringEncoding];
        [self sendStreamData:handShakingData type:AMScreenH264PacketTypeHandShaking encrypted:self.h264PayloadEncrypted viaSocket:sock listeningResponse:YES];
    }
}

- (void)socketDidDisconnect:(GCDAsyncSocket*)sock withError:(NSError*)err
{
    DLog(@"%s:%@", __FUNCTION__, err);
    self.h264TxSocketHandShaked = NO;
    [self executeWifiDisconnectBlock:err];
}

static struct ntpTimestamp recvTimeStamp;
- (void)socket:(GCDAsyncSocket*)sock didReadData:(NSData*)data withTag:(long)tag
{
    DTrace();
    if (tag == AMScreenH264PacketTypeHeader) {
        uint32_t payloadSize = 0;
        [data getBytes:&payloadSize range:NSMakeRange(0, 4)];
        int16_t packetType = 0;
        [data getBytes:&packetType range:NSMakeRange(4, 2)];
        [[data subdataWithRange:NSMakeRange(6, 4)] getBytes:&recvTimeStamp.wholeSeconds length:4];
        [[data subdataWithRange:NSMakeRange(10, 4)] getBytes:&recvTimeStamp.fractSeconds length:4];
        [sock readDataToLength:payloadSize withTimeout:-1 tag:(long)packetType];
    }
    else if (tag == AMScreenH264PacketTypeHandShaking) {
        NSString* handShakingMessage = [[NSString alloc] initWithData:[data AESDecryptWithKey:self.h264EncryptionKey iv:self.h264EncryptionIV settings:kAMAESCBC256BitKeySettings] encoding:NSUTF8StringEncoding];
        NSLog(@"handShaking - %@", handShakingMessage);
        self.h264TxSocketHandShaked = YES;
        [sock readDataToLength:kAMScreenH264PacketHeaderLength withTimeout:-1 tag:AMScreenH264PacketTypeHeader];
        if(_h264CompletionBlock){
            _h264CompletionBlock(nil);
            _h264CompletionBlock = nil;
        }
        
        /**
         *  send heartbeat, every second
         */
        dispatch_time_t now = dispatch_walltime(DISPATCH_TIME_NOW, 0);
        self.h264HeartbeatTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
        dispatch_source_set_timer(self.h264HeartbeatTimer, now, NSEC_PER_SEC * 1ull, 0ull);
        __weak AMScreenDataTransmitter* weakSelf = self;
        dispatch_source_set_event_handler(self.h264HeartbeatTimer, ^{
            [weakSelf sendStreamHeartBeat:self.h264TxSocket listeningResponse:YES];
        });
        dispatch_resume(self.h264HeartbeatTimer);
    }
}

- (NSTimeInterval)socket:(GCDAsyncSocket*)sock shouldTimeoutReadWithTag:(long)tag
                 elapsed:(NSTimeInterval)elapsed
               bytesDone:(NSUInteger)length
{
    DTrace();
    return 0;
}
#pragma mark - H264 Packet
- (void)sendStreamHeartBeat:(GCDAsyncSocket*)sock listeningResponse:(BOOL)listeningResponse
{
    /**
     * heartbeat doesnt contain any payload
     */
    NSData* headerData = [AMScreenDataTransmitter h264HeaderPacket:0
                                                              type:AMScreenH264PacketTypeHeartBeat];
    [sock writeData:headerData withTimeout:-1 tag:AMScreenH264PacketTypeHeartBeat];
    if (listeningResponse) {
        [sock readDataToLength:kAMScreenH264PacketHeaderLength withTimeout:-1 tag:AMScreenH264PacketTypeHeader];
    }
}
- (void)sendStreamData:(NSData*)data type:(AMScreenH264PacketType)type encrypted:(BOOL)encrypted
             viaSocket:(GCDAsyncSocket*)sock
     listeningResponse:(BOOL)listeningResponse
{
    NSData* dataToSend = data;
    if (encrypted) {
        dataToSend = [dataToSend AESEncryptWithKey:self.h264EncryptionKey iv:self.h264EncryptionIV settings:kAMAESCBC256BitKeySettings];
    }
    NSData* headerData = [AMScreenDataTransmitter h264HeaderPacket:(uint32_t)dataToSend.length
                                                              type:type];
    [sock writeData:headerData withTimeout:-1 tag:AMScreenH264PacketTypeHeader];
    [sock writeData:dataToSend withTimeout:-1 tag:type];
    if (listeningResponse) {
        [sock readDataToLength:kAMScreenH264PacketHeaderLength withTimeout:-1 tag:AMScreenH264PacketTypeHeader];
    }
}
+ (NSData*)h264HeaderPacket:(uint32_t)payloadSize type:(int16_t)payloadType
{
    struct ntpTimestamp txSendTime;
    ntp_time_now(&txSendTime);
    return [AMScreenDataTransmitter h264HeaderPacket:payloadSize type:payloadType ntpTimestamp:txSendTime];
}
+ (NSData*)h264HeaderPacket:(uint32_t)payloadSize type:(int16_t)payloadType ntpTimestamp:(struct ntpTimestamp)timeStamp
{
    NSMutableData* data = [NSMutableData new];
    /**
     *  payloadSize, 4 bytes
     */
    [data appendBytes:&payloadSize length:sizeof(uint32_t)];
    /**
     *  payloadType, 2 bytes
     */
    [data appendBytes:&payloadType length:sizeof(uint16_t)];
    /**
     *  ntpTimeStamp, 8 bytes
     */
    uint32_t timeStampData[2];
    memset(timeStampData, 0, sizeof timeStampData);
    timeStampData[1] = timeStamp.wholeSeconds;
    timeStampData[0] = timeStamp.fractSeconds;
    [data appendBytes:timeStampData length:8];
    /**
     *  reserved bytes
     */
    [data increaseLengthBy:18];
    
    return data;
}
+ (NSData*)h264CodecPacket:(NSData*)sps pps:(NSData*)pps
{
    NSMutableData* data = [NSMutableData new];
    /**
     *  version, 1 byte, 01
     */
    unsigned char version = 1;
    [data appendBytes:&version length:1];
    /**
     *  profile (high), 1 byte, 64
     */
    unsigned char profile = 100;
    [data appendBytes:&profile length:1];
    /**
     *  compatibility, 1 byte, c0
     */
    unsigned char compatibility = 0xc0;
    [data appendBytes:&compatibility length:1];
    /**
     *  level, 1 byte, 28
     */
    unsigned char level = 40;
    [data appendBytes:&level length:1];
    /**
     *  reserved, 0x3f, 6 bits
     *
     *  NALU length size - 1, 2 bits, ff
     */
    unsigned char reservedAndNaluSize = (0x3f << 2) | 3;
    [data appendBytes:&reservedAndNaluSize length:1];
//#warning number of SPS might not always be 1
    /**
     *  reserved, 0x7, 3 bits
     *
     *  number of SPS, 5 bits, e1
     */
    unsigned char reservedAndSPSNumber = (0x7 << 5) | 1;
    [data appendBytes:&reservedAndSPSNumber length:1];
    /**
     *  length of SPS, 2 bytes, 0010
     */
    uint16_t spslength = ntohs((uint16_t)[sps length]);
    //    uint16_t spslength = [sps length];
    [data appendBytes:&spslength length:2];
    /**
     *  SPS, 16 bytes (maybe?), 00000000 00000000
     */
    [data appendBytes:[sps bytes] length:sps.length];
    //    [data increaseLengthBy:16];
//#warning number of PPS might not always be 1
    /**
     *  number of PPS, 1 byte
     */
    unsigned char ppsNumber = 1;
    [data appendBytes:&ppsNumber length:1];
    /**
     *  length of PPS, 2 bytes
     */
    uint16_t ppslength = ntohs((uint16_t)[pps length]);
    //    uint16_t ppslength = [pps length];
    [data appendBytes:&ppslength length:2];
    /**
     *  PPS, 4 bytes (maybe?)
     */
    [data appendBytes:[pps bytes] length:pps.length];
    return data;
}
+ (NSString*)randomString:(NSUInteger)length
{
    NSString* alphabet = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    NSMutableString* s = [NSMutableString stringWithCapacity:20];
    for (NSUInteger i = 0U; i < length; i++) {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
        [s appendFormat:@"%C", c];
    }
    return s;
}
@end
