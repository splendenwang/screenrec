//
//  AMJSONRPCHttpServer.m
//  AMCommon_iOS
//
//  Created by Splenden on 2014/7/30.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMScreenJSONRPCHttpServer.h"
#import "AMScreenJSONRPCConnection.h"
@implementation AMScreenJSONRPCHttpServer
-(HTTPConfig *) config
{
	// Override me if you want to provide a custom config to the new connection.
	//
	// Generally this involves overriding the HTTPConfig class to include any custom settings,
	// and then having this method return an instance of 'MyHTTPConfig'.
	
	// Note: Think you can make the server faster by putting each connection on its own queue?
	// Then benchmark it before and after and discover for yourself the shocking truth!
	//
	// Try the apache benchmark tool (already installed on your Mac):
	// $  ab -n 1000 -c 1 http://localhost:<port>/some_path.html
	NSAssert(self.jrpcDispatcher != nil, @"JsonRPC dispatcher is nil, set it!");
	return [[AMScreenHTTPConfig alloc] initWithServer:self documentRoot:documentRoot queue:connectionQueue jsonrpcDispatcher:self.jrpcDispatcher];
}
-(NSArray *)httpConnections {
    return connections;
}
@end
