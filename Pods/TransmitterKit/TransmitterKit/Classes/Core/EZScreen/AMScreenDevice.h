//
//  AMScreenDevice.h
//  AMCommon_iOS
//
//  Created by Splenden on 2014/7/10.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMPrototypeDevice.h"

@interface AMScreenDevice : AMPrototypeDevice <AMPrototypeImageStreamingProtocol>
-(instancetype) initWithCoder:(NSCoder *)aDecoder NS_DESIGNATED_INITIALIZER;
-(instancetype) initWithNetService:(NSNetService *)aNetService NS_DESIGNATED_INITIALIZER;
@property (NS_NONATOMIC_IOSONLY, readonly, strong) NSNetService *netService;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *port;
@property (NS_NONATOMIC_IOSONLY, assign) BOOL supportH264;
@property (nonatomic, getter=getMaxResolution) CGSize maxResolution;
-(NSString*) deviceID;
-(NSString*) deviceOS;
/**
 *  1. protocol verion newer or equl to 20151019
 *  2. RX device support (hardware) decoding h264
 *
 *  @see:https://192.168.32.20/wiki/pages/D2w1R2h5b/EZCast_Screen_Proprietary_Protocol.html#streaming
 *
 *  @return Screen RX support decoding H264 or not
 */
@end
