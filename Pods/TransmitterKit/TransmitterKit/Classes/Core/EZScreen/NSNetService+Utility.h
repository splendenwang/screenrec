//
//  NSNetService+Utility.h
//  AMCommon_iOS
//
//  Created by brianliu on 13/10/2.
//  Copyright (c) 2013年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNetService (Utility)
/*!
 @brief Returns the first IPv4-Address found as NSString.
 @return IP-Address as NSString.
 */
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSArray *ipAddress;
/*!
 @brief Returns the first IPv6-Address found as NSString.
 @return IP-Address as NSString.
 */
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSArray *ipv6Address;
/*!
 @brief Returns the first IP-Address found as NSString.
 @return IP-Address as NSString.
 */
+(NSDictionary*) stringDictionaryFromTXTRecordData:(NSData*)data;
@end
