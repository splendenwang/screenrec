//
//  AMNTPServer.h
//  
//
//  Created by Splenden on 2015/11/24.
//
//

#import <Foundation/Foundation.h>
#import "AMNTPPacket.h"

@interface AMNTPServer : NSObject
- (NSData *) createNTPTimePacket;
- (void) parseNTPQueryPacket:(NSData *)data;
@end
