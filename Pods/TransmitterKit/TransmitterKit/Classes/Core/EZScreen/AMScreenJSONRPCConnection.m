//
//  AMScreenJSONRPCConnection.m
//  AMCommon_iOS
//
//  Created by Splenden on 2014/7/30.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMScreenJSONRPCConnection.h"
#import "AMScreenJSONRPCHttpServer.h"
@import WinnerWave_CocoaHTTPServer;
#import "HTTPMessage+LOG.h"
@import CocoaAsyncSocket;
#import "AMJSONDataResponse.h"
//static const int httpLogLevel = HTTP_LOG_LEVEL_WARN; // | HTTP_LOG_FLAG_TRACE;

@implementation AMScreenJSONRPCConnection
- (NSString *)connectedHost {
    return [asyncSocket connectedHost];
}
- (BOOL)supportsMethod:(NSString *)method atPath:(NSString *)path
{
	HTTPLogTrace();
	
	// Add support for POST
	
	if ([method isEqualToString:@"POST"])
	{
        return YES;
	}
	
	return [super supportsMethod:method atPath:path];
}

- (BOOL)expectsRequestBodyFromMethod:(NSString *)method atPath:(NSString *)path
{
	HTTPLogTrace();
	
	// Inform HTTP server that we expect a body to accompany a POST request
	
	if([method isEqualToString:@"POST"])
		return YES;
	
	return [super expectsRequestBodyFromMethod:method atPath:path];
}

- (NSObject<HTTPResponse> *)httpResponseForMethod:(NSString *)method URI:(NSString *)path
{
	HTTPLogTrace();
//    DLog(@"%s, request.allHeaderFields:%@\nBodyString:%@\nmethod:%@, url:%@",__PRETTY_FUNCTION__, request.allHeaderFields, request.bodyString, request.method, request.url);
	if ([method isEqualToString:@"POST"]){
		HTTPLogVerbose(@"%@[%p]: postContentLength: %qu", THIS_FILE, self, requestContentLength);
        NSError * error = nil;
        AMJSONRPCObject * jrpcObject = [AMJSONRPCObject parse:request.bodyString error:&error];
        AMJSONRPCDispatcher * jrpcDispatcher = [(AMScreenHTTPConfig *)config jrpcDispatcher];;
//        DLog(@"%@,%@",jrpcObject.JSONRPCMessage, jrpcDispatcher);
        if(error != nil){
            DLog(@"%@", error);
        }
//        DLog(@"%@", jrpcObject.JSONRPCMessage);
        if([jrpcObject isKindOfClass:[AMJSONRPCObject class]]){
            
            if([jrpcObject isKindOfClass:[AMJSONRPCResponse class]])
            {
                 return [[AMJSONDataResponse alloc] initWithData:nil];   // status 200 response
            }
            else if([jrpcObject isKindOfClass:[AMJSONRPCRequest class]])    //AMJSONRPCNotification is a subclass of AMJSONRPCRequest
            {
                NSMutableDictionary * context = [NSMutableDictionary new];
                if(self.connectedHost) {
                    [context setObject:self.connectedHost forKey:@"host"];
                }
                
                
                if([jrpcObject isKindOfClass:[AMJSONRPCNotification class]])
                {
                    [jrpcDispatcher processNotification:(AMJSONRPCNotification *)jrpcObject context:context];
                    return [[AMJSONDataResponse alloc] initWithData:nil];   // status 200 response
                }
                else
                {
                    AMJSONRPCResponse * reponse = [jrpcDispatcher processRequest:(AMJSONRPCRequest *)jrpcObject context:context];
                    return [[AMJSONDataResponse alloc] initWithData:[reponse JSONRPCData]];
                }
            }
        }

        return nil;
	}
	
	return [super httpResponseForMethod:method URI:path];
}

-(void) socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err{
//    if(self.delegate && [self.delegate respondsToSelector:@selector(didJSONRPCConnectionDisconnect:error:)]){
//        [self.delegate performSelector:@selector(didJSONRPCConnectionDisconnect:error:) withObject:err];
//    }
}


- (void)prepareForBodyWithSize:(UInt64)contentLength
{
	HTTPLogTrace();
	
	// If we supported large uploads,
	// we might use this method to create/open files, allocate memory, etc.
}

- (void)processBodyData:(NSData *)postDataChunk
{
	HTTPLogTrace();
	
	// Remember: In order to support LARGE POST uploads, the data is read in chunks.
	// This prevents a 50 MB upload from being stored in RAM.
	// The size of the chunks are limited by the POST_CHUNKSIZE definition.
	// Therefore, this method may be called multiple times for the same POST request.
	
	BOOL result = [request appendData:postDataChunk];
	if (!result)
	{
		HTTPLogError(@"%@[%p]: %@ - Couldn't append bytes!", THIS_FILE, self, THIS_METHOD);
	}
}
@end
@implementation AMScreenHTTPConfig
@synthesize jrpcDispatcher;
- (id)initWithServer:(HTTPServer *)aServer documentRoot:(NSString *)aDocumentRoot queue:(dispatch_queue_t)aQ jsonrpcDispatcher:(AMJSONRPCDispatcher *)jrpcdispatcher{
    self = [super initWithServer:aServer documentRoot:aDocumentRoot queue:aQ];
    if(self){
        jrpcDispatcher = jrpcdispatcher;
    }
    return self;
}
@end
