//
//  AMScreenDevice.m
//  AMCommon_iOS
//
//  Created by Splenden on 2014/7/10.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMScreenDevice.h"
#import "NSNetService+Utility.h"
#import "EZCastApplicationFeatures.h"
#import "AMPrototypeDevice+_internal.h"
@import EZCastBitmask;
@implementation AMScreenDevice
{
    NSNetService * _service;
    NSDictionary * _serviceTxtDictionary;
    NSMutableDictionary * _dict;
}
-(NSNetService *) netService{
    return _service;
}
-(instancetype)init{
    NSAssert(NO, @"Use initWithNetService: instead");
    return [self initWithNetService:nil];
}

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    return self;
}

-(instancetype)initWithNetService:(NSNetService *)aNetService{
    self = [super init];
    if(self){
        _service = aNetService;
        _serviceTxtDictionary = [NSNetService stringDictionaryFromTXTRecordData:aNetService.TXTRecordData];
        _dict = [NSMutableDictionary new];
        
        NSString * hostName;
        if(aNetService.hostName != nil){
            hostName = aNetService.hostName;
        } else {
            if(aNetService.name != nil){
                hostName = aNetService.name;
            } else {
                hostName = @"UNKNOWN_EZSCREEN_DEVICE";
            }
        }
        self.hostName = hostName;
       
        NSString * deviceID;
        if(_serviceTxtDictionary[kAMPrototypeDeviceID] != nil){
            deviceID = _serviceTxtDictionary[kAMPrototypeDeviceID];
        } else {
            deviceID = hostName;
        }
        _dict[kAMPrototypeDeviceID] = deviceID;
        _dict[@"ezcast.service.ios"] = @"0x13B4CF";
        
        if ([[self srcvers] integerValue] >= 20151019) {
            _supportH264 = YES;
        }
        else {
            _supportH264 = NO;
        }
        
        _maxResolution = CGSizeZero;
        /**
         Default dictionary with all services
         
         kRemoteServicePhoto        : kRemoteServiceEnable,
         kRemoteServiceCamera       : kRemoteServiceEnable,
         kRemoteServiceMusic        : kRemoteServiceEnable,
         kRemoteServiceVideo        : kRemoteServiceEnable,
         kRemoteServiceDLNA         : kRemoteServiceDisable,
         kRemoteServiceEZMirror     : kRemoteServiceDisable,
         kRemoteServiceDocument     : kRemoteServiceEnable,
         kRemoteServiceWeb          : kRemoteServiceEnable,
         kRemoteServiceSetting      : kRemoteServiceDisable,
         kRemoteServiceEZAir        : kRemoteServiceDisable,
         kRemoteServiceCloudVideo   : kRemoteServiceEnable,
         kRemoteServiceMap          : kRemoteServiceDisable,
         kRemoteServiceCloudStorage : kRemoteServiceEnable,
         kRemoteServiceTV           : kRemoteServiceEnable,
         kRemoteServiceSplitScreen  : kRemoteServiceDisable,
         kRemoteServiceEZCast       : kRemoteServiceDisable,
         kRemoteServiceComment      : kRemoteServiceEnable,
         kRemoteServiceUpdate       : kRemoteServiceEnable,
         kRemoteServiceNews         : kRemoteServiceDisable,
         kRemoteServiceMessages     : kRemoteServiceDisable,
         kRemoteServiceSocial       : kRemoteServiceEnable,
         kRemoteServicePreference   : kRemoteServiceDisable,
         kRemoteServiceAirControl   : kRemoteServiceDisable,
         kRemoteServiceAirDisk      : kRemoteServiceDisable,
         kRemoteServiceGame         : kRemoteServiceEnable,
         kRemoteServiceAirSetup     : kRemoteServiceDisable,
         kRemoteServiceLiveRadio    : kRemoteServiceDisable
         **/
        self.bitmaskDictionary = @{
                                   kRemoteServicePhoto        : kRemoteServiceEnable,
                                   kRemoteServiceCamera       : kRemoteServiceEnable,
                                   kRemoteServiceMusic        : kRemoteServiceEnable,
                                   kRemoteServiceVideo        : kRemoteServiceEnable,
                                   kRemoteServiceDocument     : kRemoteServiceEnable,
                                   kRemoteServiceWeb          : kRemoteServiceEnable,
                                   kRemoteServiceCloudVideo   : kRemoteServiceEnable,
                                   kRemoteServiceCloudStorage : kRemoteServiceEnable,
                                   //https://220.128.123.30/mantis/view.php?id=15045 要先關掉
                                   //kRemoteServiceTV           : kRemoteServiceEnable,
                                   kRemoteServiceComment      : kRemoteServiceEnable,
                                   kRemoteServiceUpdate       : kRemoteServiceEnable,
//                                   kRemoteServiceMoreApps     : kRemoteServiceEnable,
//                                   kRemoteServiceShop         : kRemoteServiceEnable,
#if !TARGET_OS_IPHONE
                                   kRemoteServiceEZCast       : kRemoteServiceEnable,
#endif
                                   //kRemoteServiceSocial       : kRemoteServiceEnable
                                   //hide Game by Jesse's ask
                                   //kRemoteServiceGame         : kRemoteServiceEnable
                                   kRemoteServiceEZChannel      :kRemoteServiceEnable,  //henry's reuqets. 2015.6.15
                                   kRemoteServiceBookmark      :kRemoteServiceEnable,  //henry's reuqets. 2015.6.15
                                   };
    }
    return self;
}
-(NSComparisonResult) compare:(AMPrototypeDevice *)aDevice{
    if([[self ipAddress] isEqualToString:[aDevice ipAddress]]){
        return NSOrderedSame;
    }
    return NSOrderedAscending;
}

-(NSString *) ipAddress{
    return [[_service ipAddress] firstObject];
}
-(NSString *) port{
    return [NSString stringWithFormat:@"%ld",(long)[_service port]];
}
-(NSString *) displayName{
    return _service.name;
}
-(NSString *) srcvers{
    return _serviceTxtDictionary[@"srcvers"];
}

+(NSArray *) supportedAudioFormat{
    return @[@"ape",@"flac",@"ogg",@"mp3",@"wma",@"wav",@"m4a",@"aac",@"rm",@"dts",@"ac3",@"ra",@"aif",@"aiff",@"mka"];
}

+(NSArray *) supportedVideoFormat{
    return @[@"avi",@"divx",@"xvid",@"mp4",@"mov",@"vob",@"dat",@"ts",@"m2ts",@"mts",@"mkv",@"rmvb",@"rm",@"mpg",@"mpeg",@"wmv",@"m4v",@"3gp",@"flv", @"asf"];
}

-(id) objectForKey:(NSString*) key{
    return _dict[key];
}

-(NSString*) deviceID{
    return _dict[kAMPrototypeDeviceID];
}
-(NSString*) deviceOS{
    if(_serviceTxtDictionary[@"deviceOS"])
        return _serviceTxtDictionary[@"deviceOS"];
    return @"Unknown_OS";
}

-(BOOL) isEqual:(id)object{
    if([object isKindOfClass:[self class]]){
        AMScreenDevice * anotherDevice = (AMScreenDevice *) object;
        return ([self.deviceID isEqualToString:anotherDevice.deviceID]);
    }
    return NO;
}
@end

