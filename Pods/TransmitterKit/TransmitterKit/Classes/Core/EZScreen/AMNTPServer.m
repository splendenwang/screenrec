//
//  AMNTPServer.m
//  
//
//  Created by Splenden on 2015/11/24.
//
//

#import "AMNTPServer.h"

@interface AMNTPServer() {
    struct ntpTimestamp
    ntpClientSendTime,
    ntpServerRecvTime,
    ntpServerSendTime,
    ntpClientRecvTime,
    ntpServerBaseTime;
    
    int li, vn, mode, stratum, poll, prec, refid;
}

@property (readonly) double root_delay;                     // milliSeconds
@property (readonly) double dispersion;                     // milliSeconds
@property (readonly) double roundtrip;                      // seconds
@end
static struct ntpTimestamp NTP_1970 = {JAN_1970, 0};        // network time for 1 January 1970, GMT
@implementation AMNTPServer 
- (instancetype)init {
    if(self = [super init]) {
        ntpServerBaseTime = NTP_1970;
    }
    return self;
}

- (NSData *) createNTPTimePacket {
    uint32_t        wireData[12];
    
    memset(wireData, 0, sizeof wireData);
    wireData[0] = htonl((0 << 30) |                                         // no Leap Indicator
                        (4 << 27) |                                         // NTP v4
                        (4 << 24) |                                         // mode = Server sending
                        (1 << 16) |                                         // stratum (n/a)
                        (4 << 8)  |                                         // polling rate (16 secs)
                        (-6 & 0xff));                                       // precision (~15 mSecs)
    wireData[1] = htonl(0<<16);
    wireData[2] = htonl(0<<16);
    
    ntp_time_now(&ntpServerSendTime);
    
    wireData[4]  = htonl(ntpServerBaseTime.wholeSeconds);
    wireData[5]  = htonl(ntpServerBaseTime.fractSeconds);
    wireData[6]  = htonl(ntpClientSendTime.wholeSeconds);
    wireData[7]  = htonl(ntpClientSendTime.fractSeconds);
    wireData[8]  = htonl(ntpServerRecvTime.wholeSeconds);
    wireData[9]  = htonl(ntpServerRecvTime.fractSeconds);
    wireData[10] = htonl(ntpServerSendTime.wholeSeconds);                   // Transmit Timestamp
    wireData[11] = htonl(ntpServerSendTime.fractSeconds);
    
    return [NSData dataWithBytes:wireData length:48];
}

- (void)parseNTPQueryPacket:(NSData *)data {
    
    ntp_time_now(&ntpServerRecvTime);
    
    uint32_t        wireData[12];
    [data getBytes:wireData length:48];
    
    li      = ntohl(wireData[0]) >> 30 & 0x03;
    vn      = ntohl(wireData[0]) >> 27 & 0x07;
    mode    = ntohl(wireData[0]) >> 24 & 0x07;
    stratum = ntohl(wireData[0]) >> 16 & 0xff;
    /*┌──────────────────────────────────────────────────────────────────────────────────────────────────┐
     │  Poll: 8-bit signed integer representing the maximum interval between successive messages,       │
     │  in log2 seconds.  Suggested default limits for minimum and maximum poll intervals are 6 and 10. │
     └──────────────────────────────────────────────────────────────────────────────────────────────────┘*/
    poll    = ntohl(wireData[0]) >>  8 & 0xff;
    /*┌──────────────────────────────────────────────────────────────────────────────────────────────────┐
     │  Precision: 8-bit signed integer representing the precision of the system clock, in log2 seconds.│
     │  (-10 corresponds to about 1 millisecond, -20 to about 1 microSecond)                            │
     └──────────────────────────────────────────────────────────────────────────────────────────────────┘*/
    prec    = ntohl(wireData[0])       & 0xff;
    if (prec & 0x80) prec |= 0xffffff00;                                // -ve byte --> -ve int
    
    _root_delay = ntohl(wireData[1]) * 0.0152587890625;                 // delay (mS) [1000.0/2**16].
    _dispersion = ntohl(wireData[2]) * 0.0152587890625;                 // error (mS)
    
    refid   = ntohl(wireData[3]);
    
    ntpClientSendTime.wholeSeconds = ntohl(wireData[10]);                // when server clock was wound
    ntpClientSendTime.fractSeconds = ntohl(wireData[11]);
}
@end
