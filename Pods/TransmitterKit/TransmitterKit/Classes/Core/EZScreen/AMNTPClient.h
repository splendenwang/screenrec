//
//  AMNTPClient.h
//  
//
//  Created by Splenden on 2015/11/24.
//
//

#import <Foundation/Foundation.h>
#import "AMNTPPacket.h"

@interface AMNTPClient : NSObject
@property (readonly) double             offset;             // offset from device time (secs)
@property (readonly) BOOL               active;             // is this clock running yet?
- (NSData *) createQueryPacket;
- (void) parseNTPTimePacket:(NSData *)data;
@end
