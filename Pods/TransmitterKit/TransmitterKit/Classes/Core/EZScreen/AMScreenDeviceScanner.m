//
//  AMScreenDeviceScanner.m
//  AMCommon_iOS
//
//  Created by Splenden on 2014/7/10.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import "AMScreenDeviceScanner.h"
#import "AMScreenDevice.h"
#import "NSNetService+Utility.h"
@implementation AMScreenDeviceScanner
{
    NSMutableDictionary * _screenDevices;
    NSNetServiceBrowser * _bonjourBrowser;
    NSMutableDictionary * _unresolvedServices;
}
-(instancetype) initWithScanDuration:(NSTimeInterval)timeInterval delegate:(id<AMPrototypeDeviceScannerDelegate>)delegate{
    self = [super initWithScanDuration:timeInterval delegate:delegate];
    if(self){
        _bonjourBrowser = [[NSNetServiceBrowser alloc] init];
        _bonjourBrowser.delegate = self;
        _screenDevices = [NSMutableDictionary new];
        _unresolvedServices = [NSMutableDictionary new];
    }
    return self;
}
#pragma mark - AMPrototypeDeviceScannerProtocol
-(void) startScan{
    @synchronized(self){
        [_bonjourBrowser searchForServicesOfType:@"_ezscreen._tcp." inDomain:@""];
    }
}
-(void) stopScan{
    @synchronized(self){
        [_bonjourBrowser stop];
    }
}
#pragma mark - NSNetServiceBrowserDelegate
/* Sent to the NSNetServiceBrowser instance's delegate before the instance begins a search. The delegate will not receive this message if the instance is unable to begin a search. Instead, the delegate will receive the -netServiceBrowser:didNotSearch: message.
 */
- (void)netServiceBrowserWillSearch:(NSNetServiceBrowser *)aNetServiceBrowser{
    @synchronized(_bonjourBrowser){
        _isScanning = YES;
    }
}

/* Sent to the NSNetServiceBrowser instance's delegate when the instance's previous running search request has stopped.
 */
- (void)netServiceBrowserDidStopSearch:(NSNetServiceBrowser *)aNetServiceBrowser{
    @synchronized(_bonjourBrowser){
        _isScanning = NO;
    }
}

/* Sent to the NSNetServiceBrowser instance's delegate when an error in searching for domains or services has occurred. The error dictionary will contain two key/value pairs representing the error domain and code (see the NSNetServicesError enumeration above for error code constants). It is possible for an error to occur after a search has been started successfully.
 */
- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didNotSearch:(NSDictionary *)errorDict{
    NSError * error = [NSError errorWithDomain:NSNetServicesErrorDomain code:[errorDict[NSNetServicesErrorCode] integerValue] userInfo:errorDict];
    if([self.delegate respondsToSelector:@selector(scanner:failToScan:)]){
        [self.delegate scanner:self failToScan:error];
    }
}


/* Sent to the NSNetServiceBrowser instance's delegate for each service discovered. If there are more services, moreComing will be YES. If for some reason handling discovered services requires significant processing, accumulating services until moreComing is NO and then doing the processing in bulk fashion may be desirable.
 */
- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didFindService:(NSNetService *)aNetService moreComing:(BOOL)moreComing{
    [aNetService startMonitoring];
    [aNetService setDelegate:self];
    [aNetService resolveWithTimeout:5.0];
    if(!moreComing)
    {
        
    }
}
/* Sent to the NSNetServiceBrowser instance's delegate when a previously discovered service is no longer published.
 */
- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didRemoveService:(NSNetService *)aNetService moreComing:(BOOL)moreComing{
    @synchronized(_screenDevices){
        AMScreenDevice * wrapperDevice = _screenDevices[aNetService.name];
        if(wrapperDevice){
            [_screenDevices removeObjectForKey:aNetService.name];
            if([self.delegate respondsToSelector:@selector(scanner:disappearedDevice:)]){
                [self.delegate scanner:self disappearedDevice:wrapperDevice];
            }
        }
    }
}
#pragma mark - NSNetServiceDelegate
- (void)netServiceWillPublish:(NSNetService *)sender{
//    DTrace();
}
- (void)netServiceDidPublish:(NSNetService *)sender{
//    DTrace();
}
- (void)netService:(NSNetService *)sender didNotPublish:(NSDictionary *)errorDict{
//    DTrace();
}
- (void)netServiceWillResolve:(NSNetService *)sender{
//    DTrace();
    //save netservices to dictionary due to ARC will auto release
    if(sender.name){
        _unresolvedServices[sender.name] = sender;
    }
    
}
- (void)netServiceDidResolveAddress:(NSNetService *)sender{
//    DLog(@"ip:%@, port:%ld", [sender ipAddress], (long)[sender port]);
//    DLog(@"txt:%@", [NSNetService stringDictionaryFromTXTRecordData:sender.TXTRecordData]);
    sender.delegate = nil;
    [_unresolvedServices removeObjectForKey:sender.name];
    AMScreenDevice * wrapperDevice = [[AMScreenDevice alloc] initWithNetService:sender];
    
    if([self.predicate evaluateWithObject:wrapperDevice] == NO && self.predicate != nil){
        return;
    }
    
    @synchronized(_screenDevices){
        _screenDevices[sender.name] = wrapperDevice;
        if([self.delegate respondsToSelector:@selector(scanner:discoveriedDevice:)]){
            [self.delegate scanner:self discoveriedDevice:wrapperDevice];
        }
    }
    //    if(self.detailViewController){
    //        [self.detailViewController refreshUI];
    //    }
    //    else{
    //        NSUInteger row = [_objects indexOfObject:sender];
    //        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    //    }
    //
}
- (void)netService:(NSNetService *)sender didNotResolve:(NSDictionary *)errorDict{
    DLog(@"%s, error:%@",__PRETTY_FUNCTION__, errorDict);
    //    [sender resolveWithTimeout:5.0];
//        DLog(@"%@, class:%@", [errorDict objectForKey:@"NSNetServicesErrorCode"] , [[errorDict objectForKey:@"NSNetServicesErrorCode"] class]);
    sender.delegate = nil;
    if( ((NSNumber*)errorDict[@"NSNetServicesErrorCode"]).integerValue == NSNetServicesTimeoutError){
        DLog(@"Timeout, resolve again");
        [sender resolveWithTimeout:5.0];
    } else {
        [_unresolvedServices removeObjectForKey:sender.name];
    }
}
- (void)netServiceDidStop:(NSNetService *)sender{
    DTrace();
    sender.delegate = nil;
}
- (void)netService:(NSNetService *)sender didUpdateTXTRecordData:(NSData *)data{
//    DTrace();
//    DLog(@"updated data:%@", [NSNetService stringDictionaryFromTXTRecordData:data]);
//    DLog(@"%s, %@",__PRETTY_FUNCTION__, [NSNetService stringDictionaryFromTXTRecordData:sender.TXTRecordData]);
}
@end
