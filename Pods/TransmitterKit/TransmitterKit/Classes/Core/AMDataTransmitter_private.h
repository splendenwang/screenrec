//
//  AMDataTransmitter_private.h
//  AMCommon_iOS
//
//  Created by brianliu on 6/11/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import "AMDataTransmitter.h"
#import "AMDataTransmitterStateObject.h"
@interface AMDataTransmitter ()
@property (nonatomic, strong) AMDataTransmitterStateObject * state;


/**
 @return State value.
 - kStateWIFIDisplayOff
 - kStateMediaStreamingPlaying
 - kStateWIFIDisplayOn
 - kStateMediaStreamingPreraring
 - kStateMediaStreamingPaused
 */
@property (nonatomic, readonly) AMDataTransmitterState currentState;


@end
