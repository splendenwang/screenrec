//
//  TransmitterState.h
//  AMCommon_iOS
//
//  Created by brianliu on 6/10/15.
//  Copyright (c) 2015 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AMDataTransmitterProtocol.h"

typedef NS_OPTIONS(NSInteger, AMDataTransmitterState){
    kTransmitterStateWIFIDisplayOff =           1 << 0,
    kTransmitterStateWIFIDisplayOn =            1 << 1,
    kTransmitterStateWIFIDisplayAll =           (kTransmitterStateWIFIDisplayOff | kTransmitterStateWIFIDisplayOn),
    kTransmitterStateMediaStreamingPreraring =  1 << 2,
    kTransmitterStateMediaStreamingPlaying =    1 << 3,
    kTransmitterStateMediaStreamingPaused =     1 << 4,
    kTransmitterStateMediaStreamingAll =        (kTransmitterStateMediaStreamingPreraring|kTransmitterStateMediaStreamingPlaying|kTransmitterStateMediaStreamingPaused),
};


@class AMDataTransmitter, AMPrototypeDevice;
@interface AMDataTransmitterStateObject : NSObject
@property (nonatomic, weak) AMDataTransmitter * tramsmitter;
-(void) startWifi:(void(^)(void))success
          failure:(void(^)(NSError * error))failure
       disconnect:(void(^)(AMPrototypeDevice * disconnectedDevice))disconnect;
-(void) stopWifiDisplay;
-(void) sendImageWithData:(NSData*) imageData;
-(void) sendH264Data:(NSData*)data width:(uint32_t)width height:(uint32_t)height;
-(void) sendPCMData:(NSData*) pcmData;

-(void) playVideoMedia:(NSURL *)mediaURL subtitle:(NSURL *)subURL userInfo:(id)userInfo;
-(void) playAudioMeida:(NSURL *)mediaURL userInfo:(id)userInfo;
-(void) pausePlayingMedia;
-(void) resumePlayingMedia;
-(void) stopPlayingMedia:(MediaStreamingEnd)endBlock;
-(void) seekToTime:(NSTimeInterval)seekTime;

-(void) onWIDIDisplayStart;         //remote machine really starts
-(void) onWIDIDisplayDisconnect;    //remote machine receives disconnect
-(void) onMediaStreamingStop;       //remote machine really stops
-(void) onMediaStreamingPlay;       //remote machine really starts
-(void) onMediaStreamingReject;     //remote machine really rejects, fails,
@property (nonatomic, readonly)  AMDataTransmitterState state;
@end

@interface StateWIFIDisplayOff : AMDataTransmitterStateObject

@end

/**
 Equals to "StateMediaStreamingStopped".
 */
@interface StateWIFIDisplayOn : AMDataTransmitterStateObject

@end

@interface StateMediaStreamingPlaying : AMDataTransmitterStateObject

@end

@interface StateMediaStreamingPreparing : AMDataTransmitterStateObject

@end

@interface StateMediaStreamingPaused : AMDataTransmitterStateObject

///TODO: spilit screen state
@end