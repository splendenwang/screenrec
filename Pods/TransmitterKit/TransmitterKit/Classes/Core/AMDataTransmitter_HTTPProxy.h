//
//  AMDataTransmitter_HTTPProxy.h
//  TransmitterKit
//
//  Created by brianliu on 2016/11/1.
//  Copyright © 2016年 winnerwave. All rights reserved.
//

#ifndef AMDataTransmitter_HTTPProxy_h
#define AMDataTransmitter_HTTPProxy_h

@import Foundation;

typedef uint16_t (^HTTPProxyRequestActication)(BOOL activate);

@protocol AMDataTransmitter_HTTPProxy <NSObject>

/**
 Set a block for receiving request from remote device for enabling HTTP Proxy.
 
 @param block A block with parameter telling we should enable/disable. And returns port number if activate.
 */
@property (nullable, strong) uint16_t(^HTTPProxyRequestActivationBlock)(BOOL activate);

/**
 Specify a callback queue for calling 'block' in setHTTPProxyRequestActivationBlock:. If not set, main queue will be used.
 
 HTTPProxyRequestCallbackQueue a dispatch_queue_t object.
 */
@property (strong, nullable) dispatch_queue_t HTTPProxyRequestCallbackQueue;

@end


#endif /* AMDataTransmitter_HTTPProxy_h */
