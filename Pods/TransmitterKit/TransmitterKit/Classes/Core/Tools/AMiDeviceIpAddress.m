//
//  AMDeviceIpAddress.m
//  AMCommon_Mac
//
//  Created by Splenden on 2014/8/7.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "AMiDeviceIpAddress.h"

@implementation AMiDeviceIpAddress

+ (NSString*) getIPAddress{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([@(temp_addr->ifa_name) isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = @(inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr));
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
}
+ (NSString *)getWIFIIPAddress {
    
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    NSString *wifiAddress = nil;
    NSString *cellAddress = nil;
    
    // retrieve the current interfaces - returns 0 on success
    if(!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            sa_family_t sa_type = temp_addr->ifa_addr->sa_family;
            if(sa_type == AF_INET || sa_type == AF_INET6) {
                NSString *name = @(temp_addr->ifa_name);
                NSString *addr = @(inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)); // pdp_ip0
                //                NSLog(@"NAME: \"%@\" addr: %@", name, addr); // see for yourself
                
                if([name isEqualToString:@"en0"] && [addr isEqualToString:@"0.0.0.0"]==NO) {
                    // Interface is the wifi connection on the iPhone
                    wifiAddress = addr;
                }
                else if([name isEqualToString:@"pdp_ip0"] && [addr isEqualToString:@"0.0.0.0"]==NO) {
                    // Interface is the cell connection on the iPhone
                    cellAddress = addr;
                }
                else if([name isEqualToString:@"en1"] && [addr isEqualToString:@"0.0.0.0"]==NO){
                    //en1 is wifi address on mac, (ios simulator)
                    wifiAddress = addr;
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    return wifiAddress;
}

@end
