//
//  AMNetworkUtility.m
//  AMCommon_iOS
//
//  Created by brianliu on 12/10/2.
//  Copyright (c) 2012年 ActionsMicro. All rights reserved.
//

#import "AMNetworkUtility.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <net/if.h>
#include <net/if_dl.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
@implementation AMNetworkUtility
+ (NSString *) localAddressForInterface:(NSString *)interface {
	BOOL success;
	struct ifaddrs * addrs;
	const struct ifaddrs * cursor;
	
	success = getifaddrs(&addrs) == 0;
	if (success) {
		cursor = addrs;
        //        DLog(@"request interface:%@", interface);
		while (cursor != NULL) {
			// the second test keeps from picking up the loopback address
			if (cursor->ifa_addr->sa_family == AF_INET && (cursor->ifa_flags & IFF_LOOPBACK) == 0)
			{
                //                DLog(@"ifa->ifa_addr:%@", [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)cursor->ifa_addr)->sin_addr)]);
                //                DLog(@"ifa->ifa_netmask:%@", [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)cursor->ifa_netmask)->sin_addr)]);
                //                DLog(@"ifa->ifa_dstaddr:%@\n", [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)cursor->ifa_dstaddr)->sin_addr)]);
				NSString *name = @(cursor->ifa_name);
                //                DLog(@"interface name:%@", name);
				if ([name isEqualToString:interface])  // Wi-Fi adapter
					return @(inet_ntoa(((struct sockaddr_in *)cursor->ifa_addr)->sin_addr));
			}
			cursor = cursor->ifa_next;
		}
		freeifaddrs(addrs);
	}
    
	return nil;
}

+ (NSArray *) localAddresses{
    NSMutableArray * workArray = [NSMutableArray new];
    for ( NSString * interface in [AMNetworkUtility networkInterfaces]) {
        [workArray addObject:[AMNetworkUtility localAddressForInterface:interface]];
    }
    return [NSArray arrayWithArray:workArray];
}

+ (NSString *) broadcacstAddressForInterface: (NSString*) interface{
    BOOL success;
	struct ifaddrs * addrs;
	const struct ifaddrs * cursor;
	NSString * address = nil;
    
	success = (getifaddrs(&addrs) == 0);
	if (success) {
		cursor = addrs;
		while (cursor != NULL) {
			// the second test keeps from picking up the loopback address
			if (cursor->ifa_addr->sa_family == AF_INET && (cursor->ifa_flags & IFF_LOOPBACK) == 0)
			{
				NSString *name = @(cursor->ifa_name);
				if ([name isEqualToString:interface]){
					address = @(inet_ntoa(((struct sockaddr_in *)cursor->ifa_dstaddr)->sin_addr));
                    break;
                }
			}
			cursor = cursor->ifa_next;
		}
        freeifaddrs(addrs);
        addrs = NULL;
    }
    return address;
}

+(NSArray *) networkInterfaces{
    BOOL success;
	struct ifaddrs * addrs;
	const struct ifaddrs * cursor;
	NSMutableArray * array = [NSMutableArray new];
    
	success = getifaddrs(&addrs) == 0;
	if (success) {
		cursor = addrs;
		while (cursor != NULL) {
			// the second test keeps from picking up the loopback address
			if (cursor->ifa_addr->sa_family == AF_INET && (cursor->ifa_flags & IFF_LOOPBACK) == 0)
			{
				NSString *name = @(cursor->ifa_name);
                [array addObject:name];
			}
			cursor = cursor->ifa_next;
		}
        freeifaddrs(addrs);
    }
    
    return [NSArray arrayWithArray:array];
}

+ (NSArray *) broadcastAddressForAllInterface{
    NSArray * interfaces = [AMNetworkUtility networkInterfaces];
    NSMutableArray * array = [NSMutableArray new];
    for (NSString * interface in interfaces){
        NSString * addr = [AMNetworkUtility broadcacstAddressForInterface:interface];
        if( [addr hasPrefix:@"169.254"]){
//            DLog(@"Skipping non-usable broadcast ipaddr : :%@", addr);
            continue;
        }
        [array addObject:addr];
    }
    return array;
}
@end
