//
//  AMNetworkUtility.h
//  AMCommon_iOS
//
//  Created by brianliu on 12/10/2.
//  Copyright (c) 2012年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AMNetworkUtility : NSObject
+ (NSString *) localAddressForInterface:(NSString *)interface;
+ (NSString *) broadcacstAddressForInterface: (NSString*) interface;
+ (NSArray *)  networkInterfaces;
+ (NSArray *) localAddresses;
+ (NSArray *) broadcastAddressForAllInterface;
@end
