//
//  AMDeviceIpAddress.h
//  AMCommon_Mac
//
//  Created by Splenden on 2014/8/7.
//  Copyright (c) 2014年 ActionsMicro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AMiDeviceIpAddress : NSObject
+ (NSString*) getIPAddress;
+ (NSString *)getWIFIIPAddress;
@end
