#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "NSData+AESEncrypt.h"
#import "NSString+CommonCrypto.h"
#import "MD5.h"
#import "MessageDigest.h"
#import "SHA1.h"
#import "SHA224.h"
#import "SHA256.h"
#import "SHA384.h"
#import "SHA512.h"

FOUNDATION_EXPORT double SimpleAESVersionNumber;
FOUNDATION_EXPORT const unsigned char SimpleAESVersionString[];

